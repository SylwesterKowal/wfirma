# Module Integracja z wFirma dla Magento 2

    ``kowal/wfirma``

 - [Podstawowe funkcje](#markdown-header-main-functionalities)
 - [Instalacja](#markdown-header-installation)
 - [Konfiguracja](#markdown-header-configuration)
 - [Specyfikacja](#markdown-header-specifications)


## Podstawowe funkcje
Integracja Magento z wFirma
 - Eksport zamówienia do wFirma (automatycznie po zapisaniu lub uruchamiany przez użytkownika)
 - Import kontrachentów z wFirma do Magento
 - Import produktów z wFrima do Magento
 - aktualizacja ceny i stanu magazynowego z wFrima do Magento

## Instalacja
\* = w trybie produkcyjnym Magento zastosuj opcję `--keep-generated`

### Wariant 1: plik Zip

 - Rozpakuj plik ZIP w katalogu `app/code/Kowal/Kowal_WFirma`
 - Rozpakuj plik ZIP w katalogu `app/code/Kowal/Kowal_Base`
 - Włącz moduł uruchamiając `php bin/magento module:enable Kowal_Base`
 - Dostosuj aktualizacje bazy uruchamijac `php bin/magento setup:upgrade`\*
 - Wyczyść pamieć podręczną Cache `php bin/magento cache:flush`

### Wariant 2: Composer

 - Dodaj repozytorium Composera do konfiguracji jeśli instalowaleś Magento z pliku ZIP `composer config repositories.repo.magento.com composer https://repo.magento.com/`
 - Zainstaluj moduł `composer require kowal/wfirma`
 - Włącz moduł `php bin/magento module:enable Kowal_Wfirma`
 - Dostosuj aktualizacje bazy uruchamijac `php bin/magento setup:upgrade`\*
 - Wyczyść pamieć podręczną Cache `php bin/magento cache:flush`


## Konfiguracja

 - Ustaw odpowiednie wartości w panelu administracyjnym `Kowal / Wfirma / Ustawienia`
 - Wstaw bespośrednio w szablonie email New Invoice ten kod: ``{{block class='Kowal\WFirma\Block\InvoiceLink' area='frontend' invoice_id=$invoice.entity_id template='Kowal_WFirma::invoicelink.phtml'}}``

 

## Specyfikacja
 
 Do tabeli `sales_invoice` dodana jest kolumna `wfirma` w której zapisywany jest numer ID faktury z wFirma zaraz po jej wygenerowaniu.
 Plik pdf faktury zapisywany jest w folderze `var/invoices`.
 

### Polecenia CLI

 - `php bin/magento kowal_wfirma:exportinvoices` Eksport faktur Magento do wFirma.
 - `php bin/magento kowal_wfirma:importcustomers` Import bazy klientów z wFirma do Magento.
 - `php bin/magento kowal_wfirma:importproducts` Import bazy produktów z wFirma do Magento.
 - 


### Zadania CRON

 - `kowal_wfirma_exportinvoice` Zadanie wykonywane co 2 minuty. Odczytuje listę faktur Magento, które jeszcze nie zostały wysłane i generuje dla nich faktury wFirma.

