<?php
/**
 * Created by PhpStorm.
 * User: piotrr
 * Date: 20.11.2018
 * Time: 09:46
 */

namespace Kowal\WFirma\lib;

class Good extends Entity
{
    protected $moduleName = 'goods';
    protected $entityName = 'good';
    public function setData($sku, $name, $unit, $count, $priceBrutto, $vat, /*$ean,*/ $classification, $type = 'good')
    {
        $this->data = [
//            'goods' => [
                'good' => [
                    'name' => $name,
                    'unit' => $unit,
                    'count' => $count,
                    'brutto' => $priceBrutto,
                    'vat' => $vat,
                    'code' => $sku,
                    'classification'=>$classification,
                    'type' => $type
                ]
//            ]
        ];
    }
}