<?php
/**
 * Created by PhpStorm.
 * User: piotrr
 * Date: 20.11.2018
 * Time: 10:47
 */

namespace Kowal\WFirma\lib;

class Api
{
    protected $apiType;
    protected $user;
    protected $password;
    protected $aditional_parameter = "";
    protected $authorizationToken = "";

    protected $apiUrl = 'https://api2.wfirma.pl/';

    protected $callBackUrl;
    protected $authUrl;
    protected $accessTokenUrl;
    protected $authorizationCode;
    protected $timestampToken;
    protected $refreshToken;

    protected $accessKey;
    protected $secretKey;
    protected $appKey;

    protected $companyId;

    public function setCredentials($authParam)
    {
        $this->apiType = $authParam['API_TYPE'];

        $this->accessKey = $authParam['ACCESS_KEY'];
        $this->secretKey = $authParam['SECRET_KEY'];
        $this->appKey = $authParam['APP_KEY'];
        $this->companyId = $authParam['COMPANY_ID'];

        $this->callBackUrl = $authParam['CALLBACK_URL'];
        $this->authUrl = $authParam['AUTH_URL'];
        $this->accessTokenUrl = $authParam['ACCESS_TOKEN_URL'];
        $this->clientId = $authParam['CLIENT_ID'];
        $this->clientSecret = $authParam['CLIENT_SECRET'];
        $this->authorizationCode = $authParam['AUTHORIZATION_CODE'];
        $this->timestampToken = $authParam['TIMESTAMP_TOKEN'];
        $this->refreshToken = $authParam['REFRESH_TOKEN'];

        if ($this->apiType == 'api_oauth') {
            $curent_time = time();
            $tokenTime = $this->timestampToken + 3600;
            if ($curent_time < $tokenTime) {
                $this->authorizationToken = $authParam['AUTHORIZATION_TOKEN'];

            } else if (!empty($this->refreshToken)) {
                $this->refreshToken();
                return [
                    'AUTHORIZATION_TOKEN' => $this->authorizationToken,
                    'REFRESH_TOKEN' => $this->refreshToken,
                    'TIMESTAMP_TOKEN' => $this->timestampToken
                ];
            } else {
                $this->getToken();
                return [
                    'AUTHORIZATION_TOKEN' => $this->authorizationToken,
                    'REFRESH_TOKEN' => $this->refreshToken,
                    'TIMESTAMP_TOKEN' => $this->timestampToken
                ];
            }
        }
        return false;
    }

    /**
     * &param=value
     * @param $param
     * @return void
     */
    public function setAditionalParameters($param)
    {
        $this->aditional_parameter = $param;
    }

    public function send($data, $where, $outputFormat = 'json')
    {
        $companyId = ($this->companyId) ? 'company_id=' . $this->companyId . '&' : '';
        $ch = curl_init();
        if ($this->apiType == 'api_oauth') {
            curl_setopt($ch, CURLOPT_URL, $this->apiUrl . $where . '?' . $companyId . 'oauth_version=2&inputFormat=xml&outputFormat=' . $outputFormat . $this->aditional_parameter);
            curl_setopt($ch, CURLOPT_HTTPHEADER, [
                'Authorization: Bearer ' . $this->authorizationToken
            ]);
        } else {
            curl_setopt($ch, CURLOPT_URL, $this->apiUrl . $where . '?' . $companyId . 'inputFormat=xml&outputFormat=' . $outputFormat . $this->aditional_parameter);
            curl_setopt($ch, CURLOPT_HTTPHEADER, [
                'accessKey: ' . $this->accessKey,
                'secretKey: ' . $this->secretKey,
                'appKey: ' . $this->appKey
            ]);
        }

//        file_put_contents("__order_.txt",print_r([
//            'url'=>$this->apiUrl . $where . '?' . $companyId . 'oauth_version=2&inputFormat=xml&outputFormat=' . $outputFormat . $this->aditional_parameter,
//            'data' => $data
//            ],true),FILE_APPEND);

//        if(!is_null($request)){ curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $request); }
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

//        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        if (!empty($data)) curl_setopt($ch, CURLOPT_POSTFIELDS, $this->array_to_xml($data));
        curl_setopt($ch, CURLOPT_USERPWD, $this->user . ':' . $this->password);
        $result_string = curl_exec($ch);
        if (curl_errno($ch)) {
            throw new \Exception('Failed call: ' . curl_error($ch) . ' ' . curl_errno($ch));
        }
        curl_close($ch);

        if ($outputFormat == 'download') {
            return $result_string;
        } else if ($outputFormat == 'json') {
            $result = json_decode($result_string, true);
        } else {
            $result = $result_string;
        }

        if (isset($result['status']) && isset($result['status']['code']) && ($result['status']['code'] == "OK") || isset($result['status']['code']) && $result['status']['code'] == "NOT FOUND") {
            return $result;
        } else if (isset($result['status']) && isset($result['status']['code'])) {
            $result = json_decode($result_string, true);
            if(isset($result['contractors'])){
                return false;
            }else{
                throw new \Exception($result['status']['code'] . ": " . $result_string);
            }
        }
    }

    public function delete($where, $outputFormat = 'json')
    {

        $companyId = ($this->companyId) ? 'company_id=' . $this->companyId . '&' : '';
        $ch = curl_init();
        if ($this->apiType == 'api_oauth') {
            $url = $this->apiUrl . $where . '?' . $companyId . 'oauth_version=2&inputFormat=xml&outputFormat=' . $outputFormat. $this->aditional_parameter;
            curl_setopt($ch, CURLOPT_URL, $url );
            curl_setopt($ch, CURLOPT_HTTPHEADER, [
                'Authorization: Bearer ' . $this->authorizationToken
            ]);

        } else {
            curl_setopt($ch, CURLOPT_URL, $this->apiUrl . $where . '?' . $companyId . 'inputFormat=xml&outputFormat=' . $outputFormat. $this->aditional_parameter );
            curl_setopt($ch, CURLOPT_HTTPHEADER, [
                'accessKey: ' . $this->accessKey,
                'secretKey: ' . $this->secretKey,
                'appKey: ' . $this->appKey
            ]);
//            file_put_contents("_delete.txt",print_r(['url'=>$this->apiUrl . $where . '?' . $companyId . 'inputFormat=xml&outputFormat=' . $outputFormat. $this->aditional_parameter,
//                'header'=>[
//                'accessKey: ' . $this->accessKey,
//                'secretKey: ' . $this->secretKey,
//                'appKey: ' . $this->appKey
//            ]],true));
        }

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result_string = curl_exec($ch);
        if (curl_errno($ch)) {
            throw new \Exception('Failed call: ' . curl_error($ch) . ' ' . curl_errno($ch));
        }
        curl_close($ch);

        if ($outputFormat == 'download') {
            return $result_string;
        } else if ($outputFormat == 'json') {
            $result = json_decode($result_string, true);
        } else {
            $result = $result_string;
        }

        return $result;

    }

//    // https://stackoverflow.com/a/5965940/1794387
    private function array_to_xml($data, &$xml_data = null)
    {
        if ($xml_data === null) {
            $xml_data = new \SimpleXMLElement("<?xml version=\"1.0\"?><api></api>");
        }
        foreach ($data as $key => $value) {
            if (is_numeric($key)) {
                $this->array_to_xml($value, $xml_data);
                continue;
            }
            if (is_array($value)) {
                $subnode = $xml_data->addChild($key);
                $this->array_to_xml($value, $subnode);
            } else {
                $xml_data->addChild("$key", htmlspecialchars("$value"));
            }
        }

        return $xml_data->asXML();
    }


    function getToken()
    {
        $curl = curl_init();

        $rurl = $this->accessTokenUrl . "?oauth_version=2";
        $data = [
            'grant_type' => 'authorization_code',
            'code' => $this->authorizationCode,
            'redirect_uri' => $this->callBackUrl,
            'client_id' => $this->clientId,
            'client_secret' => $this->clientSecret
        ];

        $params = array(
            CURLOPT_URL => $rurl,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_NOBODY => false,
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "content-type: application/x-www-form-urlencoded",
                "accept: *",
                "accept-encoding: gzip, deflate",
            ),
            CURLOPT_POSTFIELDS => http_build_query($data)
        );

        curl_setopt_array($curl, $params);

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            throw new \Exception("cURL Error #01: " . $err);
        } else {
            $response = json_decode($response, true);

            if (array_key_exists("access_token", $response)) {
                $this->timestampToken = time();
                $this->authorizationToken = $response['access_token'];
                $this->refreshToken = $response['refresh_token'];

            }
            if (array_key_exists("error", $response)) throw new \Exception("cURL Error: " . $response["error_description"]);

        }
    }

    function refreshToken()
    {
        $curl = curl_init();

        $rurl = $this->accessTokenUrl . "?oauth_version=2";
        $data = [
            'grant_type' => 'refresh_token',
            'refresh_token' => $this->refreshToken,
            'client_id' => $this->clientId,
            'client_secret' => $this->clientSecret
        ];

        $params = array(
            CURLOPT_URL => $rurl,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_NOBODY => false,
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "content-type: application/x-www-form-urlencoded",
                "accept: *",
                "accept-encoding: gzip, deflate",
            ),
            CURLOPT_POSTFIELDS => http_build_query($data)
        );

        curl_setopt_array($curl, $params);

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {

            throw new \Exception("cURL Error #01: " . $err);
        } else {
            $response = json_decode($response, true);


            if (array_key_exists("access_token", $response)) {
                $this->timestampToken = time();
                $this->authorizationToken = $response['access_token'];
                $this->refreshToken = $response['refresh_token'];

            }
            if (array_key_exists("error", $response)) {
                $this->getToken();
            }

        }
    }


}