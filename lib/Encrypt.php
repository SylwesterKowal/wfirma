<?php


namespace Kowal\WFirma\lib;

class Encrypt
{

    function encrypt($message)
    {
        $md5 = md5($message);
        $key = '000102030405060708090a0b0c0d0e0f101112131415161718191a1b1c1d1e1f';
        $nonceSize = openssl_cipher_iv_length('aes-256-ctr');
        $nonce = openssl_random_pseudo_bytes($nonceSize);

        $ciphertext = openssl_encrypt(
            $message.'||'.$md5,
            'aes-256-ctr',
            $key,
            OPENSSL_RAW_DATA,
            $nonce
        );

        return $this->base64url_encode($nonce.$ciphertext);

        //return base64_encode(self::key . $data);
    }

    function decrypt($message)
    {
        $message = $this->base64url_decode($message);
        if ($message === false) {
            throw new Exception(__('Nieprawidłowy link do FV'));
        }

        $key = '000102030405060708090a0b0c0d0e0f101112131415161718191a1b1c1d1e1f';
        $nonceSize = openssl_cipher_iv_length('aes-256-ctr');
        $nonce = mb_substr($message, 0, $nonceSize, '8bit');
        $ciphertext = mb_substr($message, $nonceSize, null, '8bit');

        $plaintext = openssl_decrypt(
            $ciphertext,
            'aes-256-ctr',
            $key,
            OPENSSL_RAW_DATA,
            $nonce
        );

        $message_ = explode("||",$plaintext);
        if(isset($message_[0]) && isset($message_[1]) && md5($message_[0]) == $message_[1]){
            return $message_[0];
        }else{
            throw new Exception(__('Nieprawidłowy link do FV'));
        }

        return $plaintext;
        // return str_replace(self::key, "", base64_decode($data));
    }

    function base64url_encode($data) {
        return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
    }

    function base64url_decode($data) {
        return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));
    }
}