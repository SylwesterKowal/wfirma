<?php
/**
 * Created by PhpStorm.
 * User: piotrr
 * Date: 03.10.2018
 * Time: 11:07
 */

namespace Kowal\WFirma\lib;

use Magento\Store\Model\ScopeInterface;


class Config
{
    protected $logger;
    protected $scopeConfig;

    /**
     * @param \Psr\Log\LoggerInterface $loggerInterface
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Config\Model\ResourceModel\Config $resourceConfig
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\App\RequestInterface $request
     */
    public function __construct(
        \Psr\Log\LoggerInterface                           $loggerInterface,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Config\Model\ResourceModel\Config         $resourceConfig,
        \Magento\Store\Model\StoreManagerInterface         $storeManager,
        \Magento\Framework\App\RequestInterface            $request
    )
    {
        $this->logger = $loggerInterface;
        $this->scopeConfig = $scopeConfig;
        $this->resourceConfig = $resourceConfig;
        $this->storeManager = $storeManager;
        $this->request = $request;
    }

    public function getEnabled($storeId = 0)
    {
        return $this->getConfigValue('wfirma/options/enabled', $storeId);
    }

    public function getEnabledStockByRw($storeId = 0)
    {
        return $this->getConfigValue('wfirma/options/enabled_stock_by_rw', $storeId);
    }

    public function isEnableAddNewProduct($storeId = 0)
    {
        return $this->getConfigValue('wfirma/options/import_products_to_magento', $storeId);
    }

    public function isUpdatePriceEnabel($storeId = 0)
    {
        return $this->getConfigValue('wfirma/options/update_price', $storeId);
    }

    public function isUpdateStockEnabel($storeId = 0)
    {
        return $this->getConfigValue('wfirma/options/update_stock', $storeId);
    }

    public function isMultiSourceEnabled($storeId = 0)
    {
        return $this->getConfigValue('wfirma/magazyny/multisource', $storeId);
    }

    public function getAllActiveMagentoSources($storeId = 0)
    {
        return $this->getConfigValue('wfirma/magazyny/magazyny_magento', $storeId);
    }

    public function getMagazynKompletacji($storeId = 0)
    {
        return $this->getConfigValue('wfirma/magazyny/magazyny_wfirma_kompletacja', $storeId);
    }

    public function getMagazynRezerwacji($storeId = 0)
    {
        return $this->getConfigValue('wfirma/magazyny/magazyny_wfirma_rezerwacje', $storeId);
    }

    public function getMagazynGlownyWFirma($storeId = 0)
    {
        return $this->getConfigValue('wfirma/magazyny/magazyny_wfirma', $storeId);
    }

    public function isAutoPayment($storeId = 0)
    {
        if($this->getConfigValue('wfirma/options/czy_wystawiac_faktury_proforma_zamiast_fvvat', $storeId)){
            return false;
        }else {
            return $this->getConfigValue('wfirma/options/create_payment', $storeId);
        }
    }

    public function sendInvoiceByWfirma($storeId = 0)
    {
        return $this->getConfigValue('wfirma/options/send_invoice_by_wfirma', $storeId);
    }

    public function sendInvoiceByStore($storeId = 0)
    {
        return $this->getConfigValue('wfirma/options/send_invoice_by_store', $storeId);
    }

    public function czyWystawiacFakturyOrazParagony($storeId = 0)
    {
        return $this->getConfigValue('wfirma/options/czy_wystawiac_fv_oraz_paragony', $storeId);
    }

    public function czyWystawiacFakturyTylkoFirmom($storeId = 0)
    {
        return $this->getConfigValue('wfirma/options/czy_wystawiac_tylko_dla_firm', $storeId);
    }

    public function czyWystawiacFakturyProformaZamiastFvvat($storeId = 0)
    {
        return $this->getConfigValue('wfirma/options/czy_wystawiac_faktury_proforma_zamiast_fvvat', $storeId);
    }

    public function getWebsitesIds()
    {
        return $this->getConfigValue('wfirma/options/websites_to_sync', 0);
    }

    public function getUsername($storeId = 0)
    {
        return $this->getConfigValue('wfirma/options/username', $storeId);
    }

    public function getPassword($storeId = 0)
    {
        return $this->getConfigValue('wfirma/options/password', $storeId);
    }

    public function getOrderStatusDoExportuDoWFirma($storeId = 0)
    {
        return explode(",", $this->getConfigValue('wfirma/options/statusy', $storeId));
    }

    public function getOrderStatusDoProdukcji($storeId = 0)
    {
        return $this->getConfigValue('wfirma/dokumenty/status_do_produkcji', $storeId);
    }

    public function getOrderDeliveryFilterDetal($storeId = 0)
    {
        return $this->getConfigValue('wfirma/podzial/shipping_detal', $storeId);
    }

    public function getOrderDeliveryFilterPozostale($storeId = 0)
    {
        return $this->getConfigValue('wfirma/podzial/shipping_pozostale', $storeId);
    }

        public function getOrderStatusDoWysylki($storeId = 0)
    {
        return $this->getConfigValue('wfirma/dokumenty/status_do_wysylki', $storeId);
    }

    public function getOrderStatusDoPakowania($storeId = 0)
    {
        return $this->getConfigValue('wfirma/dokumenty/status_do_pakowania', $storeId);
    }

    public function getOrderStatusDoZleceniaProd($storeId = 0)
    {
        return $this->getConfigValue('wfirma/dokumenty/status_do_zlecenia_produkcyjnego', $storeId);
    }

    public function getEmailCompanyDocs($storeId = 0)
    {
        return $this->getConfigValue('wfirma/dokumenty/email_company_docs', $storeId);
    }

    public function getNipCompanyDocs($storeId = 0)
    {
        return $this->getConfigValue('wfirma/dokumenty/nip_company_docs', $storeId);
    }

    public function getEmailCompanyDocsPz($storeId = 0)
    {
        return $this->scopeConfig->getValue(
            'wfirma/dokumenty/email_company_docs',
            ScopeInterface::SCOPE_STORE,
            $storeId
        );

    }

    public function getSposobWyswietlaniaKompletacji($storeId = 0)
    {
        return $this->getConfigValue('wfirma/produkcja/sposob_wyswietlania_kompletacji', $storeId);
    }

    public function getCredentials($storeId = 0)
    {
        return [
            'API_TYPE' => $this->getConfigValue('wfirma/wfirmaapi/api_type', $storeId),
            'CALLBACK_URL' => $this->getConfigValue('wfirma/wfirmaapi/callback_url', $storeId),
            'AUTH_URL' => $this->getConfigValue('wfirma/wfirmaapi/auth_url', $storeId),
            'ACCESS_TOKEN_URL' => $this->getConfigValue('wfirma/wfirmaapi/access_token_url', $storeId),
            'CLIENT_ID' => $this->getConfigValue('wfirma/wfirmaapi/client_id', $storeId),
            'CLIENT_SECRET' => $this->getConfigValue('wfirma/wfirmaapi/client_secret', $storeId),
            'AUTHORIZATION_CODE' => $this->getConfigValue('wfirma/wfirmaapi/authorization_code', $storeId),
            'TIMESTAMP_TOKEN' => $this->getConfigValue('wfirma/wfirmaapi/timestamp_token', $storeId),
            'AUTHORIZATION_TOKEN' => $this->getConfigValue('wfirma/wfirmaapi/access_token', $storeId),
            'REFRESH_TOKEN' => $this->getConfigValue('wfirma/wfirmaapi/refresh_token', $storeId),
            'ACCESS_KEY' => $this->getConfigValue('wfirma/wfirmaapi/access_key', $storeId),
            'SECRET_KEY' => $this->getConfigValue('wfirma/wfirmaapi/secret_key', $storeId),
            'APP_KEY' => $this->getConfigValue('wfirma/wfirmaapi/app_key', $storeId),
            'COMPANY_ID' => $this->getConfigValue('wfirma/options/wfirma_company_id', $storeId),
        ];
    }

    public function getSeries($storeId = 0, $seria = null)
    {
        if (!$this->getConfigValue('wfirma/options/multiseria', $storeId)) {
            if($this->getConfigValue('wfirma/options/czy_wystawiac_faktury_proforma_zamiast_fvvat', $storeId)){
                return $this->getConfigValue('wfirma/options/seria_numeracji_proforma', $storeId);
            }else{
                return $this->getConfigValue('wfirma/options/seria_numeracji', $storeId);
            }

        } else {
            return $seria;
        }
    }

    public function getSeriesParagon($storeId = 0)
    {
        return $this->getConfigValue('wfirma/options/seria_numeracji_paragon', $storeId);
    }

    public function getAttributeCodeEAN($storeId = 0)
    {
        return $this->getConfigValue('wfirma/mapowanie/kod_atrybutu_ean', $storeId);
    }

    public function getAttributeCodeUnit($storeId = 0)
    {
        return $this->getConfigValue('wfirma/mapowanie/kod_atrybutu_unit', $storeId);
    }

    public function getDrukarka1Nazwa($storeId = 0)
    {
        return $this->getConfigValue('wfirma/drukarki/drukarka_1', $storeId);
    }

    public function getDrukarka2Nazwa($storeId = 0)
    {
        return $this->getConfigValue('wfirma/drukarki/drukarka_2', $storeId);
    }

    public function getDrukarka1Username($storeId = 0)
    {
        return $this->getConfigValue('wfirma/drukarki/user_drukarka_1', $storeId);
    }

    public function getDrukarka2Username($storeId = 0)
    {
        return $this->getConfigValue('wfirma/drukarki/user_drukarka_2', $storeId);
    }

    public function getConfigValue($field, $storeId = null)
    {
        if($storeIdRequest = (int)$this->request->getParam('store', 0)){
            $storeId = $storeIdRequest;
        }else if(is_null($storeId)) {
            $storeId = $this->storeManager->getStore()->getId();
        }
//        file_put_contents("__store_id.txt", $storeId." | ".$field.PHP_EOL,FILE_APPEND);
        return $this->scopeConfig->getValue(
            $field,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    public function checkCatalogPriceIncludingTax($storeId = 0)
    {
        if ((int)$this->getConfigValue('tax/calculation/price_includes_tax', $storeId) === 1) {
            return true;
        } else {
            return false;
        }
    }

    public function getAuthorizationCode($storeId = 0)
    {
        return $this->getConfigValue('wfirma/wfirmaapi/authorization_code', $storeId);
    }

    /**
     * @param $path
     * @param $value
     * @return mixed
     */
    public function setConfig($path, $value)
    {
        $this->resourceConfig->saveConfig(
            $path,
            $value
        );

    }
}
