<?php
/**
 * Created by PhpStorm.
 * User: piotrr
 * Date: 20.11.2018
 * Time: 09:45
 */

namespace Kowal\WFirma\lib;

class WarehouseDocument extends Entity
{
    protected $moduleName = 'warehouse_documents';
    protected $entityName = 'warehouse_document';

    public $data;

    /**
     * @param $date
     * @param $disposalDate
     * @param $warehouse_document_contents
     * @param $type
     * @param $contractor
     * @param $price_type
     * @param $parent // numer dokumentu nadrzednego powiązanego dokumentem
     * @return void
     */
    public function setData(
        $date,
        $disposalDate,
        $warehouse_document_contents,
        $type,
        $contractor = null,
        $price_type = null,
        $parent = null,
        $description = null
    )
    {

            $this->data = [
                'warehouse_document' => [
                    'date' => $date,
                    'warehouse_document_contents' => $warehouse_document_contents
                ]
            ];

            if(!is_null($contractor)) $this->data['warehouse_document']['contractor']['id'] = $contractor;
            if(!is_null($price_type)) $this->data['warehouse_document']['price_type'] = $price_type;
            if(!is_null($parent)) $this->data['warehouse_document']['parent'] = $parent;
            if(!is_null($description)) $this->data['warehouse_document']['description'] = $description;


        //file_put_contents("__invoice.txt",print_r($this->data,true));
    }

    public function update($data){
        $this->data = $data;
    }
}