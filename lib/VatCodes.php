<?php

namespace Kowal\WFirma\lib;

use Kowal\WFirma\lib\Entity;

class VatCodes extends Entity
{
    protected $moduleName = 'vat_codes';
    protected $entityName = 'vat_code';

}