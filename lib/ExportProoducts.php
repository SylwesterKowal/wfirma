<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\lib;

class ExportProoducts
{

    protected $logger;

    /**
     * @param \Psr\Log\LoggerInterface $logger
     * @param Good $good
     * @param Query $query
     * @param ExportOrder $exportOrder
     * @param \Magento\Catalog\Model\ResourceModel\Product\Collection $productCollection
     */
    public function __construct(
        \Psr\Log\LoggerInterface                                $logger,
        \Kowal\WFirma\lib\Good                                  $good,
        \Kowal\WFirma\Helper\Varchar                            $queryVarchar,
        \Kowal\WFirma\lib\ExportOrder                           $exportOrder,
        \Magento\Catalog\Model\ResourceModel\Product\Collection $productCollection,
        \Magento\Tax\Api\TaxCalculationInterface                $taxCalculation

    )
    {
        $this->logger = $logger;
        $this->good = $good;
        $this->queryVarchar = $queryVarchar;
        $this->exportOrder = $exportOrder;
        $this->productCollection = $productCollection;
        $this->taxCalculation = $taxCalculation;

    }

    /**
     * @return int
     * @throws \Exception
     */
    public function execute($reset = false): int
    {

        if ($reset) {
            $this->resetMapowania();
        }

        if ($collection = $this->productCollection->addAttributeToSelect('*')
            ->addAttributeToFilter('type_id', ['eq' => 'simple'])
            ->addAttributeToFilter('name', ['nlike' => '%xcreator%'])
            ->addAttributeToFilter('status', ['eq' => '1'])
            ->addAttributeToFilter('wfirma_product_id', ['null' => true, 'eq' => ''])
        ) {

            echo $collection->count().PHP_EOL;

            $coubter = 1;
            foreach ($collection as $product) {
                if (!$goodId = $this->exportOrder->getGoodByEAN($product->getSku())) {
                    $tax = $this->taxCalculation->getCalculatedRate($product->getTaxClassId());
                    $this->good->setData($product->getSku(), $product->getName(), 'szt.', $product->getQty(), ceil($product->getPrice() * 100) / 100, $tax, "{$product->getSku()}");
                    $goodId = $this->good->add();
                    $this->queryVarchar->Update($product->getSku(), 'wfirma_product_id', $goodId, 0);
                    echo $coubter.' '.date('Y-m-d H:i:s')."  ".$product->getSku() . ' / ' . $product->getPrice() . ' / ' . $product->getQty() . ' / ' . $tax . PHP_EOL;
                    sleep(3);
                    $coubter++;
                }
//                break;
            }

        }
        return (int)1;
    }


    public function resetMapowania()
    {
        if ($collection = $this->productCollection->addAttributeToSelect('*')
            ->addAttributeToFilter('wfirma_product_id', ['notnull' => true])
        ) {

            foreach ($collection as $product) {
                $this->queryVarchar->Update($product->getSku(), 'wfirma_product_id', null, 0);
            }
        }
    }
}