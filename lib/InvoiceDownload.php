<?php
/**
 * Created by PhpStorm.
 * User: piotrr
 * Date: 20.11.2018
 * Time: 09:45
 */

namespace Kowal\WFirma\lib;

class InvoiceDownload extends Entity
{
    protected $moduleName = 'invoices';
    protected $entityName = 'invoice';

    public function setData($page = 'invoice')
    {
        $this->data = [
                'parameters' => [
                    'parameter' => [
                        'name' => 'page',
                        'value' => $page
                    ],
                    'parameter' => [
                        'name' => 'address',
                        'value' => 1
                    ],
                    'parameter' => [
                        'name' => 'leaflet',
                        'value' => 0
                    ],
                    'parameter' => [
                        'name' => 'duplicate',
                        'value' => 0
                    ],
                    'parameter' => [
                        'name' => 'payment_cashbox_documents',
                        'value' => 0
                    ],
                    'parameter' => [
                        'name' => 'warehouse_documents',
                        'value' => 0
                    ]
                ]
        ];
    }
}