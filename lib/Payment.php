<?php

namespace Kowal\WFirma\lib;

class Payment extends Entity
{
    protected $moduleName = 'payments';
    protected $entityName = 'payment';
    public function setData($invoice_id,$value,$date)
    {
        $this->data = [
            'payment' => [
                'object_name' => 'invoice',
                'object_id' => $invoice_id,
                'value' => $value,
                'date' => $date
            ]
        ];
    }
}