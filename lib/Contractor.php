<?php
/**
 * Created by PhpStorm.
 * User: piotrr
 * Date: 20.11.2018
 * Time: 09:46
 */

namespace Kowal\WFirma\lib;

class Contractor extends Entity
{
    protected $moduleName = 'contractors';
    protected $entityName = 'contractor';
    public function setData($name, $street, $zip, $city, $country, $nip, $phone, $email)
    {
        $this->data = [
//            'contractors' => [
                'contractor' => [
                    'name' => $name,
                    'street' => $street,
                    'zip' => $zip,
                    'city' => $city,
                    'country' => $country,
                    'nip' => $nip,
                    'phone' => $phone,
                    'email' => $email
//                    'tax_id_type'=>'nip'
                ]
//            ]
        ];
    }
}