<?php

namespace Kowal\WFirma\lib;

use Kowal\WFirma\lib\Entity;

class Series extends Entity
{
    protected $moduleName = 'series';
    protected $entityName = 'series';

}