<?php

namespace Kowal\WFirma\lib;

use Kowal\WFirma\lib\Entity;

class Warehouses extends Entity
{
    protected $moduleName = 'warehouses';
    protected $entityName = 'warehouse';

}