<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\lib;

class ExportInvoices
{

    protected $logger;
    protected $exportOrder;

    /**
     * @param \Psr\Log\LoggerInterface $logger
     * @param ExportOrder $exportOrder
     * @param \Magento\Sales\Model\ResourceModel\Order\Invoice\CollectionFactory $invoiceCollectionFactory
     */
    public function __construct(
        \Psr\Log\LoggerInterface                                           $logger,
        \Kowal\WFirma\lib\ExportOrder                                      $exportOrder,
        \Kowal\WFirma\lib\Query                                            $query,
        \Kowal\WFirma\lib\VatCodes                                         $vatCodes,
        \Magento\Sales\Model\ResourceModel\Order\Invoice\CollectionFactory $invoiceCollectionFactory

    )
    {
        $this->logger = $logger;
        $this->exportOrder = $exportOrder;
        $this->vatCodes = $vatCodes;
        $this->query = $query;
        $this->invoiceCollectionFactory = $invoiceCollectionFactory;

    }

    /**
     * Execute the cron
     *
     * @return void
     */
    public function execute($order_id = null)
    {
        $to = date("Y-m-d h:i:s"); // current date
        $from = strtotime('-2 day', strtotime($to));
        $from = date('Y-m-d h:i:s', $from); // 2 days before

        if($order_id){
            $filter = ['order_id' => ['eq' => $order_id]];
        }else{
            $filter = ['wfirma' => ['null' => true, 'eq' => ''], 'created_at' => ['gteq' => $from]];
        }
        $collection = $this->getInvoiceCollection($filter);


        if ($collection->getSize()) {
            $this->exportOrder->setVatCodes($this->getVatCodes());
            foreach ($collection as $invoice) {
                if ($invoicewFrirma = $this->exportOrder
                    ->setInvoiceMagento($invoice)
                    ->getInvoiceFromMagentoInvoice()) {

                    $this->query->setExported($invoice->getId(), $invoicewFrirma['id']);
                    $this->query->updateOrderWZ($invoice->getOrder()->getEntityId(), $invoicewFrirma['id'], $invoicewFrirma['fullnumber']);
                    echo $invoice->getIncrementId() . PHP_EOL;
                } else {

                }
            }
        }
    }

    public function getInvoiceCollection(array $filters = [])
    {

        $collection = $this->invoiceCollectionFactory->create()
            ->addAttributeToSelect('*');

        foreach ($filters as $field => $condition) {

            $collection->addFieldToFilter($field, $condition);
        }

        return $collection;
    }

    public function getVatCodes()
    {
        try {
            $limit = 20;
            $condition = ['parameters' => ['page' => 1, 'limit' => $limit]];
            $this->vatCodes->setFindData($condition);
            $vat_codes_ = [];


            if ($vat_codes = $this->vatCodes->findAll()) {

                $pages = round((int)$vat_codes['parameters']['total'] / $limit, 0) + 1;

                foreach ($vat_codes as $keyContr => $vat_code) {
                    if (isset($vat_code['vat_code']['code'])) {
                        $vat_codes_[] = [ "id" => (string)$vat_code['vat_code']['id'], 'code' => (string)$vat_code['vat_code']['code'], 'label' => (string)$vat_code['vat_code']['label'], 'rate' => (string)$vat_code['vat_code']['rate'], 'type' => (string)$vat_code['vat_code']['type'], 'country_id' => (string)$vat_code['vat_code']['declaration_country']['id']];
                    }
                }
                for ($i = 2; $i <= $pages; $i++) {
                    $condition = ['parameters' => ['page' => $i, 'limit' => $limit]];
                    $this->vatCodes->setFindData($condition);
                    if ($vat_codes = $this->vatCodes->findAll()) {

                        foreach ($vat_codes as $keyContr => $vat_code) {
                            if (isset($vat_code['vat_code']['code'])) {
                                $vat_codes_[] = [ "id" => (string)$vat_code['vat_code']['id'], 'code' => (string)$vat_code['vat_code']['code'], 'label' => (string)$vat_code['vat_code']['label'], 'rate' => (string)$vat_code['vat_code']['rate'], 'type' => (string)$vat_code['vat_code']['type'], 'country_id' => (string)$vat_code['vat_code']['declaration_country']['id']];
                            }
                        }
                    }
                }
            }


            return $vat_codes_;
        } catch (\Exception $e) {
            $this->logger->critical($e->getMessage());
            return [];
        }
    }
}