<?php
/**
 * Created by 21w.pl
 * User: Sylwester Kowal
 * Date: 27.05.2018
 * Time: 00:34
 */

namespace Kowal\WFirma\lib;
class Mapping
{


    /**
     * @param $magentoMethod
     * @return string
     */
    public function getPaymentMethodForERP($magentoMethod)
    {
        switch ($magentoMethod) {
            case 'orba_payupl':
                $erpMethod = 'PayU';
                break;
            case 'payu_gateway':
                $erpMethod = 'PayU';
                break;
            case 'payu_gateway_card':
                $erpMethod = 'PayU';
                break;

            case 'cashondelivery':
                $erpMethod = 'pobranie';
                break;

            case 'banktransfer':
                $erpMethod = 'przelew';
                break;

            case 'paypal_express':
                $erpMethod = 'PayPal';
                break;
            case 'checkmo':
                $erpMethod = 'gotówka';
                break;

            default:
                $erpMethod = 'przelew';
                break;
        }

        return $erpMethod;
    }

    /**
     * Odczyt kodu statusu zamowienia Magento
     * @param $erpOrderStatusCode
     * @return null|string
     */
    public
    function getOrderStatusCodeForMegento($erpOrderStatusCode)
    {
        $magStatusCode = null;
        switch ($erpOrderStatusCode) {
            case '':
                $magStatusCode = 'pending';
                break;
            case 'Potwierdzone':
                $magStatusCode = 'processing';
                break;
            case 'Spakowane':
                $magStatusCode = 'spakowane';
                break;
            case 'Wysłane':
                $magStatusCode = 'complete';
                break;
            case 'Gotowe do odbioru':
                $magStatusCode = 'gotowe';
                break;
            case 'Zrealizowane':
                $magStatusCode = 'closed';
                break;
            case 'Anulowane':
                $magStatusCode = 'canceled';
                break;
        }

        return $magStatusCode;
    }

    public
    function getOrderStatusCommentForMegento($magentoOrderStatusCode)
    {
        $magStatusComment = '';
        switch ($magentoOrderStatusCode) {
            case 'pending':
                $magStatusComment = 'Nowe zamówienie';
                break;
            case 'processing':
                $magStatusComment = 'Zamówienie przyjęto do realizacji';
                break;
            case 'complete':
                $magStatusComment = 'Zamówienie wysłane';
                break;
            case 'gotowe':
                $magStatusComment = 'Zamówienie gotowe do odbioru';
                break;
            case 'closed':
                $magStatusComment = 'Zamówienie zrealizowane';
                break;
            case 'canceled':
                $magStatusComment = 'Zamówienie anulowane';
                break;
        }

        return $magStatusComment;
    }

    /**
     * Odczyt kodu statusu zamówieani ERP
     * @param $magentoOrderStatusCode
     * @return null|string
     */
    public
    function getOrderStatusCodeForERP($magentoOrderStatusCode)
    {
        $erpStatusCode = null;
        switch ($magentoOrderStatusCode) {
            case 'fraud':
                $erpStatusCode = '';
                break;
            case 'pending':
                $erpStatusCode = '';
                break;
            case 'pending_payupl':
                $erpStatusCode = '';
                break;
            case 'pending_payment':
                $erpStatusCode = '';
                break;
            case 'payment_review':
                $erpStatusCode = '';
                break;
            case 'processing':
                $erpStatusCode = 'Potwierdzone';
                break;
            case 'przyjete':
                $erpStatusCode = 'Potwierdzone';
                break;
            case 'spakowane':
                $erpStatusCode = 'Spakowane';
                break;
            case 'complete':
                $erpStatusCode = 'Wysłane';
                break;
            case 'gotowe':
                $erpStatusCode = 'Gotowe do odbioru';
                break;
            case 'closed':
                $erpStatusCode = 'Zrealizowane';
                break;
            case 'canceled':
                $erpStatusCode = 'Anulowane';
                break;
        }

        return $erpStatusCode;
    }


    public function setErpOrderStatusPrioryty()
    {
        $priorytety = [
            'Potwierdzone' => 1,
            'Spakowane' => 2,
            'Wysłane' => 3,
            'Gotowe do odbioru' => 4,
            'Zrealizowane' => 5,
            'Anulowane' => 6
        ];

        return $priorytety;
    }

    /**
     * Odczyt metody wysyłki dla ERP
     * @param $magentoShippemntCode
     * @return string
     */
    public
    function getShippmentTypeForERP($magentoShippemntCode)
    {

        switch ($magentoShippemntCode) {
            case 'amstrates_amstrates1':
                $shippemnt = 'Przesyłka Paczkomaty';
                break;
            case 'amstrates_amstrates2':
                $shippemnt = 'Pocztex KURIER 48';
                break;
            case 'amstrates_amstrates3':
                $shippemnt = 'Pocztex KURIER 48 za pobraniem';
                break;
            case 'amstrates_amstrates4':
                $shippemnt = 'Przesyłka kurierska';
                break;
            case 'amstrates_amstrates5':
                $shippemnt = 'Przesyłka kurierska za pobraniem';
                break;
            case 'amstrates_amstrates6':
                $shippemnt = 'Paczka w Ruchu';
                break;
            case 'amstrates_amstrates7':
                $shippemnt = 'Paczka w Ruchu za pobraniem';
                break;
//            case 'amstrates_amstrates8':
            case 'odbior_odbior':
                $shippemnt = 'Odbiór osobisty';
                break;
            case 'paczkawruchu_paczkawruchu':
                $shippemnt = 'Paczka w Ruchu';
                break;
            case 'smpaczkomaty2_paczkomaty2':
                $shippemnt = 'Przesyłka Paczkomaty';
                break;
            case 'smpaczkomaty2_paczkomaty2cod':
                $shippemnt = 'Przesyłka kurierska za pobraniem';
                break;

            default:
                $shippemnt = 'Przesyłka kurierska';
                break;
        }

        return $shippemnt;
    }


    public function mappingCurrierTitleWitchCustomTracker($magentoShippemntCode)
    {
        switch ($magentoShippemntCode) {
            case 'amstrates_amstrates1':
                $tracker = 3; //'Przesyłka Paczkomaty';
                break;
            case 'amstrates_amstrates2':
                $tracker = 2; //'Pocztex KURIER 48';
                break;
            case 'amstrates_amstrates3':
                $tracker = 5; //'Pocztex KURIER 48 za pobraniem';
                break;
            case 'amstrates_amstrates4':
                $tracker = 1; //'Przesyłka kurierska';
                break;
            case 'amstrates_amstrates5':
                $tracker = 6; //'Przesyłka kurierska za pobraniem';
                break;
            case 'amstrates_amstrates6':
                $tracker = 4; //'Paczka w Ruchu';
                break;
            case 'amstrates_amstrates7':
                $tracker = 7; //'Paczka w Ruchu za pobraniem';
                break;
            case 'paczkawruchu_paczkawruchu':
                $tracker = 4; //'Paczka w Ruchu';
                break;
            case 'smpaczkomaty2_paczkomaty2':
                $tracker = 3; //'Przesyłka Paczkomaty';
                break;
            case 'smpaczkomaty2_paczkomaty2cod':
                $tracker = 3; //'Przesyłka kurierska za pobraniem';
                break;
        }

        return (isset($tracker)) ? 'tracker' . $tracker : false;
    }

}