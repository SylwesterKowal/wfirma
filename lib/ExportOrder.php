<?php



namespace Kowal\WFirma\lib;

//use PaczkaWruchu\Paczkawruchushipping\Model\Paczkawruchu;

class ExportOrder
{

    protected $order;
    protected $invoice;
    protected $vatCodes;
    protected $mapping;
    protected $wkowalwiModel;
    protected $objectManager;
    protected $exportToFile = null; // ścieżka do pliku

//    protected $paczkawruchu;

    public function __construct(
        \Magento\Framework\Filesystem\DirectoryList               $directoryList,
        \Magento\Framework\Filesystem\Io\File                     $file,
        \Kowal\WFirma\lib\Query                                   $query,
        \Kowal\WFirma\Helper\Validate                             $validate,
        \Psr\Log\LoggerInterface                                  $logger,
        \Kowal\WFirma\Model\ResourceModel\Seria\CollectionFactory $seriaCollection,
        \Kowal\WFirma\Helper\WarehouseDocumentData                $warehouseDocumentData,
        \Kowal\WFirma\Service\CountriesTax                        $countriesTax
    )
    {
        $this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->mapping = $this->objectManager->create('Kowal\WFirma\lib\Mapping');
        $this->goodFactory = $this->objectManager->create('Kowal\WFirma\lib\Good');
        $this->invoiceFactory = $this->objectManager->create('Kowal\WFirma\lib\Invoice');
        $this->invoiceContentFactory = $this->objectManager->create('Kowal\WFirma\lib\InvoiceContent');
        $this->contractorFactory = $this->objectManager->create('Kowal\WFirma\lib\Contractor');
        $this->paymentFactory = $this->objectManager->create('Kowal\WFirma\lib\Payment');
        $this->invoiceDownloadFactory = $this->objectManager->create('Kowal\WFirma\lib\InvoiceDownload');
        $this->vatCodes = $this->objectManager->create('Kowal\WFirma\lib\VatCodes');
        $this->config = $this->objectManager->create('Kowal\WFirma\lib\Config');


        $this->directoryList = $directoryList;
        $this->file = $file;
        $this->query = $query;
        $this->validate = $validate;
        $this->logger = $logger;
        $this->seriaCollection = $seriaCollection;
        $this->warehouseDocumentData = $warehouseDocumentData;
        $this->countriesTax = $countriesTax;
    }

    public function setInvoiceMagento($invoice)
    {
        $this->invoice = $invoice;
        return $this;
    }

    public function setVatCodes($var_codes)
    {
        $this->varCodes = $var_codes;
        return $this;
    }

    public function getContractor()
    {
        if ($this->setInvoiceType() != 'normal') return null;
        $billingAddress = $this->order->getBillingAddress();

        $contractor = $this->contractorFactory->createNew();
        $company = (trim((string)$billingAddress->getCompany()));
        $name = (($company) ? ($company . " ") : '') . $billingAddress->getFirstname() . ' ' . $billingAddress->getLastname();
        $postcode = $billingAddress->getPostcode();
        $city = $billingAddress->getCity();
        $street = implode(', ', $billingAddress->getStreet());
        $email = $this->order->getCustomerEmail();
//        $email = 'testtest@meus.net.pl';
        $country = $billingAddress->getCountryId();
        $contractor->setData($name, $street, $postcode, $city, $country, $billingAddress->getVatId(), $billingAddress->getTelephone(), $email);
        $contractorId = $contractor->add();
        return $contractorId;
    }

    public function exportToFile($orderOrInvoice, $filePath, $orderItemsFromInvoice = false, $printOnlyNotShippedMode = false)
    {
        if ($orderItemsFromInvoice) {
            $this->order = $orderOrInvoice->getOrder();
        } else {
            $this->order = $orderOrInvoice;
        }
//var_dump($filePath);

        $fp = fopen($filePath, 'w');
        fputcsv($fp, ['Lp', 'Symbol', 'Nazwa', 'Ilość', 'Jm', 'Cena netto', 'Cena brutto', 'Rabat (%)', 'VAT (%)', 'Wartość netto (R)', 'Wartość brutto (R)']);


        $orderItems = $orderOrInvoice->getAllItems();
        $znizka = 0.0;
//        $itemsArray = [];
//        foreach ($orderItems as $item) {
//            if ($item->getParentItemId()) {
//                continue;
//            }
//            $itemsArray[$item->getSku()] = $item;
//        }
//        ksort($itemsArray);

        $nr = 1;
        foreach ($orderItems as $item) {
//            if ($item->getParentItem()) {
//                continue;
//            }

            if ( $item->getParentItemId() && $item->getParentItem() && $item->getParentItem()->getProductType() == 'configurable') {
                continue;
            }
            if (!$item->getParentItemId() && $item->getProductType() == 'bundle') {
                continue;
            }

            if (!$orderItemsFromInvoice) {
                if ($printOnlyNotShippedMode) {
                    if ($item->getQtyInvoiced()) {
                        $qty = $item->getQtyOrdered() - $item->getQtyInvoiced();

                    } else {
                        $qty = $item->getQtyOrdered();
                    }
                    $item->setQty($qty);
                    if ($qty == 0) {
                        continue;
                    }

                } else {
                    $item->setQty($item->getQtyOrdered());
                }
            }

            $unit = $this->getJednMiaryForProduct($item->getSku());
            $priceBrutto = $item->getPriceInclTax();
            $priceNetto = $item->getPrice();
            $priceRowTotalNetto = $item->getRowTotal();
            $priceRowTotalBrutto = $item->getRowTotalInclTax();
            $qty = $item->getQty();
            if (!$qty) {
                throw new \Exception("function exportToFile : Cannot read quantity from order item sku={$item->getSku()} ");
            }
            $discountPercent = ($item->getDiscountPercent()) ? $item->getDiscountPercent() : '0';
            fputcsv($fp, [$nr++, $item->getSku(), $item->getName(), $qty, $unit, $priceNetto, $priceBrutto, $discountPercent, $this->getTaxPercentForProduct($item->getSku()), $priceRowTotalNetto, $priceRowTotalBrutto]);
        }

        if (!$printOnlyNotShippedMode) {
            $tax = '23';
            $name = $this->mapping->getShippmentTypeForERP($this->order->getShippingMethod());
            fputcsv($fp, [$nr, '', $name, 1, '', $this->order->getShipping(), $this->order->getShippingInclTax(), '0', $tax, $this->order->getShipping(), $this->order->getShippingInclTax()]);
        }


        fclose($fp);
    }

    public function getInvoiceFromMagentoInvoice()
    {

        $this->order = $this->invoice->getOrder();
        if ($this->order->getWfirmaWzId()) return; // jeśli już wystawiono FV lub WZ to pomiń

        $billingAddress = $this->order->getBillingAddress();
        $nip = $billingAddress->getVatId();
        $czy_wystawiac_tylko_dla_firm = $this->config->getConfigValue('wfirma/options/czy_wystawiac_tylko_dla_firm', $this->order->getStoreId());
        if (!$this->validate->Nip($nip) && $czy_wystawiac_tylko_dla_firm) {
            return false; // pomijamy vo wysyłamy tylko z nipem
        }

        $date = date('Y-m-d', strtotime($this->invoice->getCreatedAt()));


        $this->przeniesienieRezerwacjiDoKompletacji($this->order, $date);

        $this->przyjecieProdukcjiDoKompletacji($this->order, $date);


        $wFinvoice = $this->invoiceFactory->createNew();
        $contractorId = $this->getContractor();
        $invoiceContentArray = $this->getInvoiceContentsArray($this->order, true);

//        $date = date('Y-m-d', strtotime($this->order->getCreatedAt()));
        $disposalDate = $date;
        $paymentDate = $date;
        $wFinvoice->setData($contractorId, $date, $disposalDate, $paymentDate, $invoiceContentArray, $this->order->getGrandTotal(), $this->setInvoiceType(), $this->setInvoiceSeria(), $this->order->getOrderCurrencyCode(), $this->order->getIncrementId());
        $wFinvoice = $wFinvoice->add(true);
        // $this->setPayment($wFinvoiceId);
        $this->downloadInvoicePdf($wFinvoice['id']);
        return $wFinvoice;
    }

    private function przeniesienieRezerwacjiDoKompletacji($order, $date)
    {
        $data_reserved = [];
        // Przygotowanie danych
        foreach ($order->getAllItems() as $item) {
            if ( $item->getParentItemId() && $item->getParentItem() && $item->getParentItem()->getProductType() == 'configurable') {
                continue;
            }
            if (!$item->getParentItemId() && $item->getProductType() == 'bundle') {
                continue;
            }
            $qty = (int)$item->getQtyOrdered();
            if ($qty > 0 && $qty >= $item->getQtyReserved() && $item->getQtyReserved() > 0) {
                $data_reserved[] = ["sku" => $item->getSku(), "qty" => $item->getQtyReserved()];
            }
        }
        if (count($data_reserved)) {
            if (!$magazyn_kompletacji_id = $this->config->getMagazynKompletacji($this->order->getStoreId())) {
                throw new \Magento\Framework\Exception\LocalizedException(
                    __("Brak zdefiniowanego magazynu kompletacji w ustawieniach modułu wFirma")
                );
            }
            if (!$magazyn_rezerwacji_id = $this->config->getMagazynRezerwacji($this->order->getStoreId())) {
                throw new \Magento\Framework\Exception\LocalizedException(
                    __("Brak zdefiniowanego magazynu rezerwacji w ustawieniach modułu wFirma")
                );
            }
            try {

                $reserwation = $this->query->checkOrderReservationIds($order->getEntityId());

                if(isset($reserwation['wfirma_rw_rezerwacja_id'])
                    && $reserwation['wfirma_rw_rezerwacja_id'] > 0
                    && isset($reserwation['wfirma_pw_rezerwacja_id'])
                    && $reserwation['wfirma_pw_rezerwacja_id'] > 0
                ){
                    echo 'Już przesunięte'.PHP_EOL;
                }else {
                    if ($wfirma_rw_rezerwacja = $this->warehouseDocumentData->setData($date, $date, $data_reserved, "RW", $magazyn_rezerwacji_id, "warehouse_document_r_w", null, null, $order->getIncrementId(), $order->getIncrementId(), true, $this->order->getStoreId())) {
                        if ($wfirma_pw_rezerwacja = $this->warehouseDocumentData->setData($date, $date, $data_reserved, "PW", $magazyn_kompletacji_id, "warehouse_document_p_w", null, null, $order->getIncrementId(), $order->getIncrementId(), true, $this->order->getStoreId())) {
                            // zapisać numery ID do sales_order
                            $this->query->updateOrderReservationIds($order->getEntityId(), $wfirma_rw_rezerwacja['id'], $wfirma_pw_rezerwacja['id'], $wfirma_rw_rezerwacja['fullnumber'], $wfirma_pw_rezerwacja['fullnumber']);

                            echo 'Przesuwam: '. $wfirma_rw_rezerwacja['fullnumber']." | ". $wfirma_pw_rezerwacja['fullnumber'].PHP_EOL;
                        }
                    }
                }
            } catch (\Exception $e) {
                $this->logger->critical($e->getMessage());
                throw new \Magento\Framework\Exception\LocalizedException(
                    __($e->getMessage())
                );
            }
        }
    }

    private function przyjecieProdukcjiDoKompletacji($order, $date)
    {
        $data_production = [];
        // Przygotowanie danych
        foreach ($order->getAllItems() as $item) {
            if ( $item->getParentItemId() && $item->getParentItem() && $item->getParentItem()->getProductType() == 'configurable') {
                continue;
            }
            if (!$item->getParentItemId() && $item->getProductType() == 'bundle') {
                continue;
            }
            $qty = (int)$item->getQtyOrdered();
            if ($qty > 0 && $qty >= $item->getQtyProduction() && $item->getQtyProduction() > 0) {
                $data_production[] = ["sku" => $item->getSku(), "qty" => $item->getQtyProduction()];
            }
        }
        if (count($data_production)) {
            if (!$magazyn_kompletacji_id = $this->config->getMagazynKompletacji()) {
                throw new \Magento\Framework\Exception\LocalizedException(
                    __("Brak zdefiniowanego magazynu kompletacji w ustawieniach modułu wFirma")
                );
            }

            try {
                $production = $this->query->checkOrderProductionId($order->getEntityId());

                if(isset($production['wfirma_pw_produckja_id']) && $production['wfirma_pw_produckja_id'] > 0
                ){
                    echo 'Już przyjęte z produkcji'.PHP_EOL;
                }else {
                    sleep(1);
                    if ($wfirma_pw_produckja = $this->warehouseDocumentData->setData($date, $date, $data_production, "PW", $magazyn_kompletacji_id, "warehouse_document_p_w", null, null, $order->getIncrementId(), $order->getIncrementId(), true)) {
                        // zapisać numery ID do sales_order
                        $this->query->updateOrderProductionId($order->getEntityId(), $wfirma_pw_produckja['id'], $wfirma_pw_produckja['fullnumber']);
                        echo 'Przyjmuję z produkcji: '. $wfirma_pw_produckja['fullnumber'].PHP_EOL;
                    }

                }
            } catch (\Exception $e) {
                $this->logger->critical($e->getMessage());
                throw new \Magento\Framework\Exception\LocalizedException(
                    __($e->getMessage())
                );
            }
        }
    }


    public function getInvoice($order)
    {
        $this->order = $order;

        $wFinvoice = $this->invoiceFactory->createNew();
        $contractorId = $this->getContractor();
        $invoiceContentArray = $this->getInvoiceContentsArray($this->order, true);
        $date = date('Y-m-d');
//        $date = date('Y-m-d', strtotime($this->order->getCreatedAt()));
        $disposalDate = $date;
        $paymentDate = '';

        $wFinvoice->setData($contractorId, $date, $disposalDate, $paymentDate, $invoiceContentArray, $this->order->getGrandTotal(), $this->setInvoiceType(), $this->setInvoiceSeria(), $this->order->getOrderCurrencyCode(), $this->order->getIncrementId());
        $wFinvoiceId = $wFinvoice->add();
        //$this->setPayment($wFinvoiceId);
        $this->downloadInvoicePdf($wFinvoiceId);
        return $wFinvoiceId;
    }


    private function setInvoiceType()
    {
        $type = "normal";
        if (!$this->config->czyWystawiacFakturyOrazParagony()) return $type;


        $billingAddress = $this->order->getBillingAddress();
//        $company = (trim($billingAddress->getCompany()));
        $nip = $billingAddress->getVatId();
        if (!empty($nip)) {
            return $type;
        } else {
            return 'receipt_normal';
        }
    }

    private function setInvoiceSeria()
    {
        $seria = null;
        if (!$this->config->getConfigValue('wfirma/options/multiseria', 0)) {
            return $seria;
        } else {
            $attr = 'get' . $this->config->getConfigValue('wfirma/options/multiseria_order_attr_name', 0);
            if ($seriaCodeVallue = $this->order->{$attr}()) {
                $seriaCollection = $this->seriaCollection->create();
                $seriaCollection->addFieldToFilter('order_attr_value', ['eq' => $seriaCodeVallue]);
                foreach ($seriaCollection as $seria_) {
                    if ($seria = $seria_->getWfirmaSeria()) {
                        return $seria;
                    }
                    return null;
                }

            } else {
                return $seria;
            }
        }
    }

    public function setPayment($wFinvoice_id)
    {
        // do uzycia u klient który będzie chciał rejestr płatności, puki co niepotrzebne
        return;

        $value = $this->order->getGrandTotal();
        $date = date('Y-m-d');
        $this->paymentFactory = $this->objectManager->create('Kowal\WFirma\lib\Payment');
        $payment = $this->paymentFactory->createNew();
        $payment->setData($wFinvoice_id, $value, $date);
        $paymentId = $payment->add();
        return $paymentId;
    }

    public function downloadInvoicePdf($wf_invoice_id)
    {
        if (!$this->config->sendInvoiceByStore()) return;

        $invoiceDownload = $this->invoiceDownloadFactory->createNew();
        $invoiceDownload->setData('invoice');
        if ($download = $invoiceDownload->download($wf_invoice_id)) {
            $filename = $this->invoice->getIncrementId() . "_invoice.pdf";
            $filepath = $this->getFileName($filename);
            @file_put_contents($filepath, $download);
            // $this->query->setInvoicePdf($this->setInvoicePdfUrl($filename), $this->invoice->getId());
        }
    }


    public function getFileName($filename)
    {
        $this->var = $this->directoryList->getPath('var');

        if (!file_exists($this->var . DIRECTORY_SEPARATOR . 'invoices')) {
            $this->file->mkdir($this->var . DIRECTORY_SEPARATOR . 'invoices', 0775);
        }
        return $this->var . DIRECTORY_SEPARATOR . 'invoices' . DIRECTORY_SEPARATOR . $filename;
    }

    public function getGoodByEAN($ean)
    {
        $good = $this->goodFactory->createNew();
        $good->setFindData(['parameters' => ['conditions' => ['condition' => [
            'field' => 'code',
            'operator' => 'eq',
            'value' => $ean
        ]]]]);
        return ($good->findOne());
    }

    public function getInvoiceGoods($skus)
    {
        $good = $this->goodFactory->createNew();
        $good->setFindData(['parameters' => ['conditions' => ['condition' => [
            'field' => 'code',
            'operator' => 'in',
            'value' => $skus
        ]]]]);
        return ($good->findAll());
    }

    public function getInvoiceContentsArray($order, $orderItemsFromInvoice = false)
    {
        $orderItems = $order->getAllItems();
        $invoiceContentsArray = [];
        $znizka = 0.0;
        $itemsArray = [];
//        foreach ($orderItems as $item) {
//            if ($item->getParentItemId()) {
//                if (isset($itemsArray[$item->getSku()])) {
//                    $itemsArray[$item->getSku()]->setQtyOrdered($item->getQtyOrdered());
//                    $itemsArray[$item->getSku()]->setName($item->getName());
//                    $itemsArray[$item->getSku()]->setTaxPercent($item->getTaxPercent());
//                }
//                continue;
//            }
//            $itemsArray[$item->getSku()] = $item;
//        }
//        ksort($itemsArray);
        $canShip = false;
        foreach ($orderItems as $item) {
            if ( $item->getParentItemId() && $item->getParentItem() && $item->getParentItem()->getProductType() == 'configurable') {
                continue;
            }
            if (!$item->getParentItemId() && $item->getProductType() == 'bundle') {
                continue;
            }
//            $tax = $this->getTaxPercentForProduct($item->getSku());
//            $tax = $item->getTaxPercent();
            $countryId = is_null($order->getShippingAddress()) ? "" : $order->getShippingAddress()->getCountryId();
            $tax = (int)$this->countriesTax->getTaxRate('standard', $countryId);
            $good = $this->goodFactory->createNew();
            if ($orderItemsFromInvoice) {
                $znizka = $item->getDiscountAmount() / $item->getQtyOrdered();
            } else {
                $znizka = $item->getDiscountAmount() / $item->getQtyOrdered();
            }


            $price = $item->getPrice() - $znizka;
            $ean = $this->getEanForProduct($item->getSku());
            if ($goodId = $this->getGoodByEAN($item->getSku())) {

            } else {
                $good->setData($item->getSku(), $item->getName(), 'szt.', '0', ceil($price * 100) / 100, $tax, "{$item->getSku()}");
                $goodId = $good->add();
            }


            $invoiceContent = $this->invoiceContentFactory->createNew();
            $invoiceContent->setData($goodId, $item->getQtyOrdered(), $price, $this->getVatCodeId($item, $tax));
            $invoiceContentsArray[] = $invoiceContent->getData();
        }

        try {
            if ($this->order->getShippingInclTax() > 0) {
                $invoiceContent = $this->invoiceContentFactory->createNew();
                $invoiceContent->setData($this->getShippingGood(), 1, $this->order->getShippingInclTax(), $this->getVatCodeId(null, $tax));
                $invoiceContentsArray[] = $invoiceContent->getData();
//                if ($znizka != 0) {
//                    $invoiceContent = $this->invoiceContentFactory->createNew();
//                    $invoiceContent->setData($this->getZnizkaGood($znizka), 1, $this->order->getShippingInclTax(), $this->getVatCodeId(null, $tax));
//                    $invoiceContentsArray[] = $invoiceContent->getData();
//                }
            }

        } catch
        (\Exception $e) {
            $this->logger->critical($e->getMessage());
        }

        return $invoiceContentsArray;
    }

    public function getVatCodeId($item, $tax)
    {
        $billingAddress = $this->order->getBillingAddress();
        $country = $billingAddress->getCountryId();
        $vat_code_id = null;
        if ($country != 'PL') {
            foreach ($this->varCodes as $vatCode) {
                if (isset($vatCode['country_id']) && (int)$vatCode['country_id'] == $country) {
                    $vat_code_id = $vatCode['id'];
                }
            }
            if (is_null($vat_code_id) && !is_null($item)) {
                foreach ($this->varCodes as $vatCode) {
                    if (isset($vatCode['rate']) && (int)$vatCode['rate'] == (int)$item->getTaxPercent()) {
                        $vat_code_id = $vatCode['id'];
                    }
                }
            }
        }
        return ['country' => $country, 'vat_code' => $vat_code_id, 'vat' => $tax];
    }

    public function getShippingGood()
    {
        try {
            $tax = '23';
            // https://stackoverflow.com/a/23007998/1794387
//        $tax = $this->order->getFullTaxInfo()[0]['percent'];
//        var_dump($this->order->getFullTaxInfo());
            $good = $this->goodFactory->createNew();
            $shippingMethodMagento = $this->order->getShippingMethod();
            $name = $this->mapping->getShippmentTypeForERP($shippingMethodMagento);
            if ($goodId = $this->getGoodByEAN('delivery')) {
                return $goodId;
            } else {
                $good->setData('delivery', $name, 'szt.', '0', $this->order->getShippingInclTax(), $tax, '', 'service');
                $goodId = $good->add();
                return $goodId;
            }
        } catch (\Exception $e) {
            $this->logger->critical($e->getMessage());
        }
    }

    public function getZnizkaGood($discout)
    {
        $tax = 'zw';
        // https://stackoverflow.com/a/23007998/1794387
//        $tax = $this->order->getFullTaxInfo()[0]['percent'];
//        var_dump($this->order->getFullTaxInfo());

        if ($goodId = $this->getGoodByEAN('rabat')) {
            return $goodId;
        } else {
            $good = $this->goodFactory->createNew();
            $name = "Rabat";
            $good->setData('rabat', $name, 'szt.', '0', $discout, $tax, '', 'service');
            $goodId = $good->add();

            return $goodId;
        }
    }

    public function getTaxPercentForProduct($sku)
    {

        $this->productCollectionFactory = $this->objectManager->create('Magento\Catalog\Model\ResourceModel\Product\CollectionFactory');
        $this->taxCalculation = $this->objectManager->create('Magento\Tax\Api\TaxCalculationInterface');

        $collection = $this->productCollectionFactory->create();
        $collection->addAttributeToFilter('sku', $sku);
        $collection->addAttributeToSelect('*');
        if (count($collection)) {
            $product = $collection->getFirstItem();
            $taxClassId = $product->getData('tax_class_id');
            $tax = $this->taxCalculation->getCalculatedRate($taxClassId);
            return $tax;
        }
        throw new \Exception("getTaxPercentForProduct : Product sku=$sku doesn't exist");
    }

    private function getEanForProduct($sku)
    {
        $this->productCollectionFactory = $this->objectManager->create('Magento\Catalog\Model\ResourceModel\Product\CollectionFactory');

        $collection = $this->productCollectionFactory->create();
        $collection->addAttributeToFilter('sku', $sku);
        $collection->addAttributeToSelect('*');
        $ean = '';
        if (count($collection)) {
            $product = $collection->getFirstItem();
            try {
                $ean = $product->getData($this->config->getAttributeCodeEAN());
            } catch (\Exception $ex) {
                // problem z odczytem EAN
            }

            return $ean;
        }
        throw new \Exception("getEanForProduct : Product sku=$sku doesn't exist");
    }

    private function getJednMiaryForProduct($sku)
    {
        if (!$this->config->getAttributeCodeUnit()) {
            return null;
        }

        $this->productCollectionFactory = $this->objectManager->create('Magento\Catalog\Model\ResourceModel\Product\CollectionFactory');

        $collection = $this->productCollectionFactory->create();
        $collection->addAttributeToFilter('sku', $sku);
        $collection->addAttributeToSelect('*');
        $jm = '';
        if (count($collection)) {
            $product = $collection->getFirstItem();
            try {
                $jm = $product
//                    ->getData($this->config->getAttributeCodeUnit())
                    ->getResource()->getAttribute($this->config->getAttributeCodeUnit())->getFrontend()->getValue($product);
                if ($jm == 'No' || $jm == 'Nie') {
                    $jm = '';
                }
            } catch (\Exception $ex) {
                // problem z odczytem jedn miary
            }

            return $jm;
        }
        throw new \Exception("getJednMiaryForProduct : Product sku=$sku doesn't exist");
    }

}