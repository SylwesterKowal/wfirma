<?php

namespace Kowal\WFirma\lib;

use Magento\Framework\App\ResourceConnection;

class Query
{
    public $store_id = 0;

    /**
     * @var ResourceConnection
     */
    protected $resourceConnection;

    public function __construct(
        ResourceConnection $resourceConnection
    )
    {
        $this->resourceConnection = $resourceConnection;
    }


    /**
     * @return mixed
     */
    private function _getReadConnection()
    {
        return $this->_getConnection('core_read');
    }


    /**
     * @param string $type
     * @return mixed
     */
    private function _getConnection($type = 'core_read')
    {
        return $this->resourceConnection->getConnection($type);
    }

    /**
     * @param $tableName
     * @return mixed
     */
    private function _getTableName($tableName)
    {
        return $this->resourceConnection->getTableName($tableName);
    }


    public function setExported($invoice_id, $wFirmaId)
    {
        $connection = $this->_getConnection('core_write');

        $sql1 = "UPDATE " . $this->_getTableName('sales_invoice') . " so
	                   SET
	                   so.wfirma = ?
					   WHERE
					   so.entity_id = ?";
        $connection->query($sql1, array($wFirmaId, $invoice_id));

        $sql2 = "UPDATE " . $this->_getTableName('sales_invoice_grid') . " sog
	                   SET
	                   sog.wfirma = ?
					   WHERE
					   sog.entity_id = ?";
        $connection->query($sql2, array($wFirmaId, $invoice_id));

        $sql3 = "UPDATE " . $this->_getTableName('sales_order') . " so
	                   SET
	                   so.wfirma_wz_fullnumber = ?
					   WHERE
					   so.entity_id = ?";
        $connection->query($sql1, array($wFirmaId, $invoice_id));

        $sql2 = "UPDATE " . $this->_getTableName('sales_invoice_grid') . " sog
	                   SET
	                   sog.wfirma = ?
					   WHERE
					   sog.entity_id = ?";
        $connection->query($sql2, array($wFirmaId, $invoice_id));
    }

    /**
     * @param $order_id
     * @return void
     */
    public function setInvoicePdf($filename, $oder_id)
    {
        $connection = $this->_getConnection('core_write');

        $sql1 = "UPDATE " . $this->_getTableName('sales_order') . " so
	                   SET
	                   so.invoice_pdf = ?
					   WHERE
					   so.entity_id = ?";
        $connection->query($sql1, array($filename, $oder_id));
    }

    public function getAttribute($sku, $code, $store_id = 0)
    {
        $product_id = $this->_getIdFromSku($sku);
        $attributeId = $this->_getAttributeId($code);
        return $this->getAttributeValue($product_id, $attributeId, $code, $store_id);
    }

    /**
     * @param $attributeCode
     * @return mixed
     */
    private function _getAttributeId($attributeCode)
    {
        $connection = $this->_getReadConnection();
        $sql = "SELECT attribute_id FROM " . $this->_getTableName('eav_attribute') . " WHERE entity_type_id = ? AND attribute_code = ?";
        return $connection->fetchOne(
            $sql,
            [
                $this->_getEntityTypeId('catalog_product'),
                $attributeCode
            ]
        );
    }

    /**
     * @param $entityTypeCode
     * @return mixed
     */
    private function _getEntityTypeId($entityTypeCode)
    {
        $connection = $this->_getConnection('core_read');
        $sql = "SELECT entity_type_id FROM " . $this->_getTableName('eav_entity_type') . " WHERE entity_type_code = ?";
        return $connection->fetchOne(
            $sql,
            [
                $entityTypeCode
            ]
        );
    }

    public function getSKUbyAttribute($attribyte_code, $value)
    {
        try {
            $connection = $this->_getConnection('core_read');
            $sql = "SELECT e.sku 
                FROM " . $this->_getTableName('catalog_product_entity') . " e 
                LEFT JOIN " . $this->_getTableName('catalog_product_entity_varchar') . " v1 
                    ON e.entity_id = v1.entity_id AND v1.store_id = 0 
                    AND v1.attribute_id = (SELECT attribute_id FROM  " . $this->_getTableName('eav_attribute') . " 
                                            WHERE attribute_code = '$attribyte_code' 
                                            AND entity_type_id = (SELECT entity_type_id 
                                                                    FROM " . $this->_getTableName('eav_entity_type') . " 
                                                                    WHERE entity_type_code = 'catalog_product')) 
                    WHERE v1.value = ?";
            return $connection->fetchOne($sql, [$value]);
        } catch (Exception $e) {
            return $e->getTraceAsString() . "\r";
        }
    }

    private function getAttributeValue($productId, $attributeId, $attributeKey, $store_id = 0)
    {
        try {
            $attribute_type = $this->_getAttributeType($attributeKey);

            if ($attribute_type == 'static') return false;

            $connection = $this->_getConnection('core_write');

            $sql = "SELECT value FROM " . $this->_getTableName('catalog_product_entity_' . $attribute_type) . " cped
			WHERE  cped.attribute_id = ? and cped.store_id = ?
			AND cped.entity_id = ?";
            return $connection->fetchOne($sql, array($attributeId, $store_id, $productId));

        } catch (Exception $e) {
            return $e->getMessage() . ' ' . $attributeKey;
        }
    }

    function _getAttributeType($attribute_code = 'price')
    {
        $connection = $this->_getConnection('core_read');
        $sql = "SELECT backend_type
				FROM " . $this->_getTableName('eav_attribute') . "
			WHERE
				entity_type_id = ?
				AND attribute_code = ?";
        $entity_type_id = $this->_getEntityTypeId('catalog_product');
        return $connection->fetchOne($sql, array($entity_type_id, $attribute_code));
    }

    public function _getIdFromSku($sku)
    {
        $connection = $this->_getConnection('core_read');
        $sql = "SELECT entity_id FROM " . $this->_getTableName('catalog_product_entity') . " WHERE sku = ?";
        return $connection->fetchOne(
            $sql,
            [
                $sku
            ]
        );
    }

    public function checkIfSkuExists($sku, $producent = '')
    {
        $connection = $this->_getConnection('core_read');
        $sql = "SELECT sku, type_id FROM " . $this->_getTableName('catalog_product_entity') . " WHERE sku IN (?,?)";
        return $connection->fetchRow($sql, [trim($sku), trim($producent) . '_' . trim($sku)]);
    }

    public function checkIfOrderExists($orderId, $status = "'processing'", $metody_dostawy = '')
    {
        $connection = $this->_getConnection('core_read');

        $sql = "SELECT so.entity_id  FROM " . $this->_getTableName('sales_order') . " as so  
        WHERE so.increment_id = ? 
        AND so.status IN ({$status})
        {$metody_dostawy}
        ";
        return $connection->fetchRow($sql, [$orderId]);
    }


    public function checkIfSkuExistsInOrder($orderId, $sku, $status = "'processing'")
    {
        if ($order = $this->checkIfOrderExists($orderId, $status)) {

            $connection = $this->_getConnection('core_read');
            $sql = "SELECT item_id  FROM " . $this->_getTableName('sales_order_item') . " WHERE order_id = ? AND sku = ?";
            return $connection->fetchRow($sql, [$order['entity_id'], $sku]);
        } else {
            return false;
        }
    }
    public function checkIfEntityIdExistsInOrder($orderId, $entity_id, $status = "'processing'")
    {
        if ($order = $this->checkIfOrderExists($orderId, $status)) {

            $connection = $this->_getConnection('core_read');
            $sql = "SELECT item_id  FROM " . $this->_getTableName('sales_order_item') . " WHERE order_id = ? AND item_id = ?";
            return $connection->fetchRow($sql, [$order['entity_id'], $entity_id]);
        } else {
            return false;
        }
    }

    public function getOrdersBySku($sku, $days = 30, $statusy_zamowien = "'processing'", $metody_dostawy = "", $limit = "LIMIT 5")
    {
        $connection = $this->_getConnection('core_read');
        $sql = "select so.increment_id as increment_id 
                        from " . $this->_getTableName('sales_order_item') . " as soi
                                 join " . $this->_getTableName('sales_order') . " as so on (so.entity_id = soi.order_id)
                        where soi.sku = ? 
                        AND so.created_at BETWEEN NOW() - INTERVAL ? DAY 
                        AND NOW() 
                        AND so.state = 'processing' 
                        AND so.status IN (" . $statusy_zamowien . ")
                        {$metody_dostawy}
                        GROUP BY so.increment_id 
                        ORDER BY so.created_at ASC
                        {$limit};";
        return array_keys($connection->fetchAssoc($sql, [$sku, $days]));
    }

    public function getOrderByItemId($item_id,  $statusy_zamowien = "'processing'", $metody_dostawy = '')
    {
        $connection = $this->_getConnection('core_read');


        $sql = "select so.increment_id as order_id, soi.sku as sku
                        from " . $this->_getTableName('sales_order_item') . " as soi
                                 join " . $this->_getTableName('sales_order') . " as so on (so.entity_id = soi.order_id)
                        where soi.item_id = ?  
                        AND so.state = 'processing' 
                        AND so.status IN (" . $statusy_zamowien . ")
                        {$metody_dostawy}
                        ";
        return $connection->fetchRow($sql, [$item_id]);
    }

    public function getOrderData($order_id)
    {
        $connection = $this->_getConnection('core_read');

        $sql = "select so.entity_id, so.store_id, si.created_at as invoice_created_at, so.created_at as order_created_at
                        from " . $this->_getTableName('sales_order') . " as so 
                        LEFT JOIN " . $this->_getTableName('sales_invoice') . " si ON so.entity_id = si.order_id
                        where so.increment_id = ? LIMIT 1";
        return $connection->fetchRow($sql, [$order_id]);
    }

    public function getwFirmaDocsIds($order_id)
    {
        $connection = $this->_getConnection('core_read');

        $sql = "select so.entity_id, so.store_id, so.increment_id, so.wfirma_wz_id, so.wfirma_wz_fullnumber, 
                so.wfirma_pw_produckja_id, so.wfirma_pw_produckja_fullnumber, so.wfirma_pw_rezerwacja_id, so.wfirma_pw_rezerwacja_fullnumber,
                so.wfirma_rw_rezerwacja_id, so.wfirma_rw_rezerwacja_fullnumber,
                so.wfirma_pz_id, so.wfirma_pz_fullnumber
                        from " . $this->_getTableName('sales_order') . " as so 
                        where so.increment_id = ? LIMIT 1";
        return $connection->fetchRow($sql, [$order_id]);
    }

    public function getOrdersByEan($ean_code, $ean, $days = 30, $statusy_zamowien = "'processing'")
    {
        $connection = $this->_getConnection('core_read');
        if ($sku = $this->getSKUbyAttribute($ean_code, $ean)) {
            return $this->getOrdersBySku($sku, $days, $statusy_zamowien);
        } else {
            return false;
        }
    }

    public function changeOrderItemProductionStatus($order_increment_id, $sku, $status)
    {
        $connection = $this->_getConnection('core_write');
        if ($order = $this->checkIfOrderExists($order_increment_id)) {
            $sql = "UPDATE " . $this->_getTableName('sales_order_item') . " t
                SET t.status_realizacji = ?
                WHERE t.sku = ? AND t.order_id = ? AND t.parent_item_id IS NULL AND t.product_type != 'bundle'
                OR
                t.sku = ? AND t.order_id = ? AND t.parent_item_id = (SELECT t2.item_id FROM sales_order_item as t2 WHERE t2.item_id = t.parent_item_id AND t2.product_type = 'bundle' )
                ;";
            $connection->query($sql, [$status, $sku, $order['entity_id'], $sku, $order['entity_id']]);
        }
    }

    public function resetOrderItemQty($order_increment_ids, $status)
    {
        $connection = $this->_getConnection('core_write');
        if(is_array($order_increment_ids)) {
            foreach ($order_increment_ids as $increment_id) {
                if ($order = $this->checkIfOrderExists($increment_id, $status)) {
                    $sql = "UPDATE " . $this->_getTableName('sales_order_item') . " t
                SET t.qty_production = ?,
                    t.qty_reserved = ?,
                    t.qty_pack = ?,
                    t.qty_production_completed = ?,
                    t.qty_reserved_completed = ?
                WHERE t.order_id = ? AND t.parent_item_id IS NULL AND t.product_type != 'bundle'
                OR
                t.order_id = ? AND t.parent_item_id = (SELECT t2.item_id FROM sales_order_item as t2 WHERE t2.item_id = t.parent_item_id AND t2.product_type = 'bundle' )
                ;";
                    $connection->query($sql, [null, null, null, null, null, $order['entity_id'], $order['entity_id']]);
                }
            }
        }
    }

    public function updateOrderItemProductionQty($order_increment_id, $sku, $qty_production, $status)
    {
        $connection = $this->_getConnection('core_write');
        if ($order = $this->checkIfOrderExists($order_increment_id, $status)) {
            $sql = "UPDATE " . $this->_getTableName('sales_order_item') . " t
                SET t.qty_production = ?
                WHERE t.sku = ? AND t.order_id = ? AND t.parent_item_id IS NULL AND t.product_type != 'bundle' AND t.qty_ordered > (CASE WHEN t.qty_reserved IS NULL THEN 0 ELSE t.qty_reserved END + CASE WHEN t.qty_production IS NULL THEN 0 ELSE t.qty_production END )
                OR
                t.sku = ? AND t.order_id = ? AND t.parent_item_id = (SELECT t2.item_id FROM sales_order_item as t2 WHERE t2.item_id = t.parent_item_id AND t2.product_type = 'bundle' ) AND t.qty_ordered > (CASE WHEN t.qty_reserved IS NULL THEN 0 ELSE t.qty_reserved END + CASE WHEN t.qty_production IS NULL THEN 0 ELSE t.qty_production END )
                ;";
            $connection->query($sql, [$qty_production, $sku, $order['entity_id'], $sku, $order['entity_id']]);
        }
    }

    public function updateOrderItemReservedQty($order_increment_id, $sku, $qty_reserved, $status)
    {
        $connection = $this->_getConnection('core_write');
        if ($order = $this->checkIfOrderExists($order_increment_id, $status)) {
            $sql = "UPDATE " . $this->_getTableName('sales_order_item') . " t
                SET t.qty_reserved = ?
                WHERE t.sku = ? AND t.order_id = ? AND t.parent_item_id IS NULL AND t.product_type != 'bundle' AND t.qty_ordered > (CASE WHEN t.qty_reserved IS NULL THEN 0 ELSE t.qty_reserved END + CASE WHEN t.qty_production IS NULL THEN 0 ELSE t.qty_production END )
                OR
                t.sku = ? AND t.order_id = ? AND t.parent_item_id = (SELECT t2.item_id FROM sales_order_item as t2 WHERE t2.item_id = t.parent_item_id AND t2.product_type = 'bundle' ) AND t.qty_ordered > (CASE WHEN t.qty_reserved IS NULL THEN 0 ELSE t.qty_reserved END + CASE WHEN t.qty_production IS NULL THEN 0 ELSE t.qty_production END )
                ;";
            $connection->query($sql, [$qty_reserved, $sku, $order['entity_id'], $sku, $order['entity_id']]);

        }
    }

    public function pakujOrderItemQty($order_increment_id, $sku, $item_id, $status = "'processing'")
    {
        $connection = $this->_getConnection('core_write');
        if ($entity_id = $this->checkIfSkuExistsInOrder($order_increment_id, $sku, $status)) {
            if ($order = $this->checkIfOrderExists($order_increment_id, $status)) {
                $sql = "UPDATE " . $this->_getTableName('sales_order_item') . " t
                SET t.qty_pack = CASE
                          WHEN t.qty_pack IS NULL THEN t.qty_completed
                          ELSE t.qty_pack + t.qty_completed
                        END
                WHERE t.item_id = ? AND t.order_id = ? AND t.parent_item_id IS NULL  AND t.product_type = 'simple'
                OR t.sku = ? AND t.order_id = ? AND t.parent_item_id IS NULL  AND t.product_type = 'configurable'
                OR t.sku = ? AND t.order_id = ? AND t.parent_item_id = (SELECT t2.item_id FROM sales_order_item as t2 WHERE t2.item_id = t.parent_item_id AND t2.product_type = 'bundle' )
                ;";
                $connection->query($sql, [$item_id, $order['entity_id'], $sku, $order['entity_id'], $sku, $order['entity_id']]);
                //, t.qty_completed = 0, t.qty_production_completed = 0, t.qty_reserved_completed = 0
                $this->updateOrderItemStatus($sku, $order['entity_id']);
            }
        }
    }

    public function pakujScanedOrderItemQty($order_increment_id, $sku, $status = "'processing'")
    {

        $connection = $this->_getConnection('core_write');
        if ($entity_id = $this->checkIfSkuExistsInOrder($order_increment_id, $sku, $status)) {
            if ($order = $this->checkIfOrderExists($order_increment_id, $status)) {

                $sql = "UPDATE " . $this->_getTableName('sales_order_item') . " t
                SET t.qty_pack = CASE
                          WHEN t.qty_pack IS NULL THEN 1
                          ELSE t.qty_pack + 1
                        END
                WHERE t.sku = ? AND t.order_id = ? AND t.parent_item_id IS NULL AND t.product_type != 'bundle' AND CASE
                          WHEN t.qty_pack IS NULL THEN 0
                          ELSE t.qty_pack 
                        END < t.qty_completed
                        OR
                        t.sku = ? AND t.order_id = ? AND t.parent_item_id = (SELECT t2.item_id FROM sales_order_item as t2 WHERE t2.item_id = t.parent_item_id AND t2.product_type = 'bundle' ) AND CASE
                          WHEN t.qty_pack IS NULL THEN 0
                          ELSE t.qty_pack 
                        END < t.qty_completed
                        ;";
                $connection->query($sql, [$sku, $order['entity_id'], $sku, $order['entity_id']]);
                //, t.qty_completed = 0, t.qty_production_completed = 0, t.qty_reserved_completed = 0
                $this->updateOrderItemStatus($sku, $order['entity_id']);
            }
        }
    }

    public function resetPakujOrderItemQty($order_increment_id, $sku, $item_id, $status = "'processing'")
    {
        $connection = $this->_getConnection('core_write');
        if ($entity_id = $this->checkIfSkuExistsInOrder($order_increment_id, $sku, $status)) {
            if ($order = $this->checkIfOrderExists($order_increment_id, $status)) {
                $sql = "UPDATE " . $this->_getTableName('sales_order_item') . " t
                SET t.qty_pack = 0
                WHERE t.item_id = ? AND t.order_id = ? AND t.parent_item_id IS NULL  AND t.product_type = 'simple'
                OR t.sku = ? AND t.order_id = ? AND t.parent_item_id IS NULL  AND t.product_type = 'configurable'
                OR t.sku = ? AND t.order_id = ? AND t.parent_item_id = (SELECT t2.item_id FROM sales_order_item as t2 WHERE t2.item_id = t.parent_item_id AND t2.product_type = 'bundle' )
                ;";
                $connection->query($sql, [$item_id, $order['entity_id'], $sku, $order['entity_id'], $sku, $order['entity_id']]);
                //, t.qty_completed = 0, t.qty_production_completed = 0, t.qty_reserved_completed = 0
                $this->updateOrderItemStatus($sku, $order['entity_id']);
            }
        }
    }

    public function updateOrderItemQtyCompleted($order_increment_id, $sku,  $item_id, $type = "produkcja", $dzialanie = 'plus', $status = "'processing'")
    {
        $connection = $this->_getConnection('core_write');
        $column = ($type == "produkcja") ? "qty_production_completed" : "qty_reserved_completed";
        $column_plan = ($type == "produkcja") ? "qty_production" : "qty_reserved";
        if ($item_id_ = $this->checkIfSkuExistsInOrder($order_increment_id, $sku, $status)) {
            if ($order = $this->checkIfOrderExists($order_increment_id, $status)) {
                if ($dzialanie == 'plus') {
                    $sql = "UPDATE " . $this->_getTableName('sales_order_item') . " t
                    SET t.{$column} = CASE
                              WHEN t.{$column} IS NULL THEN 1
                              WHEN t.{$column_plan} IS NOT NULL AND t.{$column} < t.{$column_plan} THEN t.{$column} + 1
                              WHEN t.{$column_plan} IS NULL AND t.{$column} < t.qty_ordered THEN t.{$column} + 1
                            END
                    WHERE 
                    t.item_id = ? AND t.order_id = ? AND t.{$column} < CASE WHEN t.{$column_plan} IS NULL THEN t.qty_ordered ELSE t.{$column_plan} END AND t.qty_ordered > t.qty_pack AND t.parent_item_id IS NULL AND t.product_type = 'simple' 
                    OR t.item_id = ? AND t.order_id = ? AND t.parent_item_id IS NULL  AND t.product_type = 'simple'
                    OR t.sku = ? AND t.order_id = ? AND t.{$column} < CASE WHEN t.{$column_plan} IS NULL THEN t.qty_ordered ELSE t.{$column_plan} END AND t.qty_ordered > t.qty_pack AND t.parent_item_id IS NULL AND t.product_type = 'configurable' 
                    OR t.sku = ? AND t.order_id = ? AND t.parent_item_id IS NULL  AND t.product_type = 'configurable'
                    OR t.sku = ? AND t.order_id = ? AND t.{$column} < CASE WHEN t.{$column_plan} IS NULL THEN t.qty_ordered ELSE t.{$column_plan} END AND t.qty_ordered > t.qty_pack AND t.parent_item_id = (SELECT t2.item_id FROM sales_order_item as t2 WHERE t2.item_id = t.parent_item_id AND t2.product_type = 'bundle' ) 
                    OR t.sku = ? AND t.order_id = ? AND t.parent_item_id = (SELECT t2.item_id FROM sales_order_item as t2 WHERE t2.item_id = t.parent_item_id AND t2.product_type = 'bundle' ) 
                    ;";
                    $connection->query($sql, [$item_id, $order['entity_id'], $item_id, $order['entity_id'] ,$sku, $order['entity_id'], $sku, $order['entity_id'], $sku, $order['entity_id'], $sku, $order['entity_id']]);


                    $this->updateOrderItemStatus($sku, $order['entity_id']);

                } else if ($dzialanie == 'minus') {
                    // Odejmowanie kompletacji
                    $sql = "UPDATE " . $this->_getTableName('sales_order_item') . " t
                    SET t.{$column} = CASE
                              WHEN t.{$column} IS NULL THEN 0
                              WHEN t.{$column} > 0 THEN t.{$column} - 1
                            END
                    WHERE 
                    t.item_id = ? AND t.order_id = ? AND t.{$column} > 0 AND t.parent_item_id IS NULL AND t.product_type = 'simple' 
                    OR t.item_id = ? AND t.order_id = ? AND t.{$column} IS NOT NULL AND t.parent_item_id IS NULL AND t.product_type = 'simple'
                    OR t.sku = ? AND t.order_id = ? AND t.{$column} > 0 AND t.parent_item_id IS NULL AND t.product_type = 'configurable' 
                    OR t.sku = ? AND t.order_id = ? AND t.{$column} IS NOT NULL AND t.parent_item_id IS NULL AND t.product_type = 'configurable'
                    OR t.sku = ? AND t.order_id = ? AND t.{$column} > 0 AND t.parent_item_id = (SELECT t2.item_id FROM sales_order_item as t2 WHERE t2.item_id = t.parent_item_id AND t2.product_type = 'bundle' ) 
                    OR t.sku = ? AND t.order_id = ? AND t.{$column} IS NOT NULL AND t.parent_item_id = (SELECT t2.item_id FROM sales_order_item as t2 WHERE t2.item_id = t.parent_item_id AND t2.product_type = 'bundle' ) 
                    ;";
                    $connection->query($sql, [$item_id, $order['entity_id'], $item_id, $order['entity_id'], $sku, $order['entity_id'], $sku, $order['entity_id'], $sku, $order['entity_id'], $sku, $order['entity_id']]);

                    $this->updateOrderItemStatus($sku, $order['entity_id']);

                }

                $sql = "UPDATE " . $this->_getTableName('sales_order_item') . " t
                    SET t.qty_completed = CASE WHEN t.qty_production_completed IS NULL THEN 0 ELSE t.qty_production_completed END + CASE WHEN t.qty_reserved_completed IS NULL THEN 0 ELSE t.qty_reserved_completed END 
                    WHERE 
                    t.item_id = ? AND t.order_id = ? AND t.{$column} > 0 AND t.parent_item_id IS NULL AND t.product_type = 'simple' 
                    OR t.item_id = ? AND t.order_id = ? AND t.{$column} IS NOT NULL AND t.parent_item_id IS NULL AND t.product_type = 'simple'
                    OR t.sku = ? AND t.order_id = ? AND t.{$column} > 0 AND t.parent_item_id IS NULL AND t.product_type = 'configurable' 
                    OR t.sku = ? AND t.order_id = ? AND t.{$column} IS NOT NULL AND t.parent_item_id IS NULL AND t.product_type = 'configurable'
                    OR t.sku = ? AND t.order_id = ? AND t.{$column} > 0 AND t.parent_item_id = (SELECT t2.item_id FROM sales_order_item as t2 WHERE t2.item_id = t.parent_item_id AND t2.product_type = 'bundle' ) 
                    OR t.sku = ? AND t.order_id = ? AND t.{$column} IS NOT NULL AND t.parent_item_id = (SELECT t2.item_id FROM sales_order_item as t2 WHERE t2.item_id = t.parent_item_id AND t2.product_type = 'bundle' ) 
                    ;";
                $connection->query($sql, [$item_id, $order['entity_id'], $item_id, $order['entity_id'],$sku, $order['entity_id'], $sku, $order['entity_id'], $sku, $order['entity_id'], $sku, $order['entity_id']]);

            }
        }
    }

    public function updateOrderItemStatus($sku, $order_id)
    {
        $connection = $this->_getConnection('core_write');
        $sql = "UPDATE " . $this->_getTableName('sales_order_item') . " t
                    SET t.status_realizacji = CASE
                              WHEN t.qty_completed IS NULL OR t.qty_completed = 0 THEN 'W produkcji'
                              WHEN t.qty_completed > 0 AND t.qty_completed <= t.qty_ordered AND t.qty_pack < t.qty_ordered THEN 'W kompletacji'
                              WHEN t.qty_completed = t.qty_ordered AND t.qty_pack = t.qty_ordered THEN 'Do wysylki'
                            END
                    WHERE t.sku = ? AND t.order_id = ? AND t.parent_item_id IS NULL  AND t.product_type != 'bundle'
                    OR
                    t.sku = ? AND t.order_id = ? AND t.parent_item_id = (SELECT t2.item_id FROM sales_order_item as t2 WHERE t2.item_id = t.parent_item_id AND t2.product_type = 'bundle' )
                    ;";
        $connection->query($sql, [$sku, $order_id, $sku, $order_id]);
    }

    public function updateOrderReservationIds($order_id, $wfirma_rw_rezerwacja_id, $wfirma_pw_rezerwacja_id, $wfirma_rw_rezerwacja_fullnumber, $wfirma_pw_rezerwacja_fullnumber)
    {
        $connection = $this->_getConnection('core_write');
        $sql = "UPDATE " . $this->_getTableName('sales_order') . " t
                    SET t.wfirma_rw_rezerwacja_id = ?, 
                        t.wfirma_pw_rezerwacja_id = ?,
                        t.wfirma_rw_rezerwacja_fullnumber = ?, 
                        t.wfirma_pw_rezerwacja_fullnumber = ?
                    WHERE t.entity_id = ?;";
        $connection->query($sql, [$wfirma_rw_rezerwacja_id, $wfirma_pw_rezerwacja_id, $wfirma_rw_rezerwacja_fullnumber, $wfirma_pw_rezerwacja_fullnumber, $order_id]);

        $sql = "UPDATE " . $this->_getTableName('sales_order_grid') . " t
                SET t.wfirma_rw_rezerwacja_fullnumber = ?, t.wfirma_pw_rezerwacja_fullnumber = ?
                WHERE entity_id = ?;";
        $connection->query($sql, [$order_id, $wfirma_rw_rezerwacja_fullnumber, $wfirma_pw_rezerwacja_fullnumber]);
    }

    public function checkOrderReservationIds($orderId)
    {
            $connection = $this->_getConnection('core_read');
            $sql = "SELECT wfirma_rw_rezerwacja_id, wfirma_pw_rezerwacja_id FROM " . $this->_getTableName('sales_order') . " WHERE entity_id = ? ";
            return $connection->fetchRow($sql, [$orderId]);

    }

    public function updateOrderProductionId($order_id, $wfirma_pw_produckja_id, $wfirma_pw_produckja_fullnumber)
    {
        $connection = $this->_getConnection('core_write');
        $sql = "UPDATE " . $this->_getTableName('sales_order') . " t
                    SET t.wfirma_pw_produckja_id = ?, t.wfirma_pw_produckja_fullnumber = ?  
                    WHERE t.entity_id = ?;";
        $connection->query($sql, [$wfirma_pw_produckja_id, $wfirma_pw_produckja_fullnumber, $order_id]);

        $sql = "UPDATE " . $this->_getTableName('sales_order_grid') . " t
                SET t.wfirma_pw_produckja_fullnumber = ?
                WHERE entity_id = ?;";
        $connection->query($sql, [$order_id, $wfirma_pw_produckja_fullnumber]);
    }

    public function updateOrderKompletacjaStockDate($order_id, $wfirma_rw_kompletacja_fullnumber, $wfirma_pw_kompletacja_fullnumber)
    {
        $connection = $this->_getConnection('core_write');
        $sql = "UPDATE " . $this->_getTableName('sales_order') . " t
                    SET t.wfirma_rw_kompletacja_fullnumber = ?, t.wfirma_pw_kompletacja_fullnumber = ?  
                    WHERE t.entity_id = ?;";
        $connection->query($sql, [$wfirma_rw_kompletacja_fullnumber, $wfirma_pw_kompletacja_fullnumber, $order_id]);
    }

    public function checkOrderProductionId($orderId)
    {
        $connection = $this->_getConnection('core_read');
        $sql = "SELECT wfirma_pw_produckja_id FROM " . $this->_getTableName('sales_order') . " WHERE entity_id = ?";
        return $connection->fetchRow($sql, [$orderId]);

    }

    public function updateOrderWZ($order_id, $wfirma_wz_id, $wfirma_wz_fullnumber)
    {
        $connection = $this->_getConnection('core_write');
        $sql = "UPDATE " . $this->_getTableName('sales_order') . " t
                    SET t.wfirma_wz_id = ?, t.wfirma_wz_fullnumber = ?  
                    WHERE t.entity_id = ?;";
        $connection->query($sql, [$wfirma_wz_id, $wfirma_wz_fullnumber, $order_id]);

        $sql = "UPDATE " . $this->_getTableName('sales_order_grid') . " t
                SET t.wfirma_wz_fullnumber = ?
                WHERE entity_id = ?;";
        $connection->query($sql, [$wfirma_wz_fullnumber, $order_id]);
    }

    public function updateFVAdvanceFinal($order_id, $wfirma_fv_zal_id, $wfirma_fv_zal_fullnumber, $wfirma_fv_kon_id, $wfirma_fv_kon_fullnumber)
    {
        $connection = $this->_getConnection('core_write');
        $sql = "UPDATE " . $this->_getTableName('sales_order') . " t
                    SET t.wfirma_fv_zal_id = ?, t.wfirma_fv_zal_fullnumber = ?, t.wfirma_fv_kon_id = ?, t.wfirma_fv_kon_fullnumber = ? 
                    WHERE t.entity_id = ?;";
        $connection->query($sql, [$wfirma_fv_zal_id, $wfirma_fv_zal_fullnumber, $wfirma_fv_kon_id, $wfirma_fv_kon_fullnumber , $order_id]);

        $sql = "UPDATE " . $this->_getTableName('sales_order_grid') . " t
                SET t.wfirma_fv_zal_fullnumber = ?, t.wfirma_fv_kon_fullnumber = ?
                WHERE entity_id = ?;";
        $connection->query($sql, [$wfirma_fv_zal_fullnumber,$wfirma_fv_kon_fullnumber, $order_id]);
    }


    public function updateOrderFV($order_id,$fv='FV VAT')
    {
        $connection = $this->_getConnection('core_write');
        $sql = "UPDATE " . $this->_getTableName('sales_order') . " t
                    SET t.wfirma_fv = ? 
                    WHERE t.entity_id = ?;";
        $connection->query($sql, [$fv,$order_id]);

        $sql = "UPDATE " . $this->_getTableName('sales_order_grid') . " t
                SET t.wfirma_fv = ?
                WHERE entity_id = ?;";
        $connection->query($sql, [$fv, $order_id]);
    }

    public function updateOrderWZPartner($order_id, $wfirma_wz_id, $wfirma_wz_fullnumber)
    {
        $connection = $this->_getConnection('core_write');
        $sql = "UPDATE " . $this->_getTableName('sales_order') . " t
                    SET t.wfirma_wz_partner_id = ?, t.wfirma_wz_partner_fullnumber = ?  
                    WHERE t.entity_id = ?;";
        $connection->query($sql, [$wfirma_wz_id, $wfirma_wz_fullnumber, $order_id]);


    }

    public function updateErrOrderWZ($order_id)
    {
        $connection = $this->_getConnection('core_write');
        $sql = "UPDATE " . $this->_getTableName('sales_order') . " t
                    SET t.wfirma_wz_err = ? 
                    WHERE t.entity_id = ?;";
        $connection->query($sql, ['ERR', $order_id]);

        $sql = "UPDATE " . $this->_getTableName('sales_order_grid') . " t
                SET t.wfirma_wz_err = ?
                WHERE entity_id = ?;";
        $connection->query($sql, ['ERR', $order_id]);
    }

    public function updateProFormaDescription($order_id)
    {
        $connection = $this->_getConnection('core_write');
        $sql = "UPDATE " . $this->_getTableName('sales_order') . " t
                    SET t.wfirma_pf_update_description = ? 
                    WHERE t.entity_id = ?;";
        $connection->query($sql, ['YES', $order_id]);
    }

    public function updateOrderPZ($order_id, $wfirma_pz_id, $wfirma_pz_fullnumber)
    {
        $connection = $this->_getConnection('core_write');
        $sql = "UPDATE " . $this->_getTableName('sales_order') . " t
                    SET t.wfirma_pz_id = ?, t.wfirma_pz_fullnumber = ?  
                    WHERE t.entity_id = ?;";
        $connection->query($sql, [$wfirma_pz_id, $wfirma_pz_fullnumber, $order_id]);

        $sql = "UPDATE " . $this->_getTableName('sales_order_grid') . " t
                SET t.wfirma_pz_fullnumber = ?
                WHERE entity_id = ?;";
        $connection->query($sql, [$wfirma_pz_fullnumber, $order_id]);
    }

    /**
     * Zapisanie domunentów wFirma powiązanych z zleceneim produkcyjnym
     * @param $params
     * @return void
     */
    public function saveWfirmaDocsIdsFromZlecenie($params)
    {
        $connection = $this->_getConnection('core_write');
        $sql = "INSERT INTO " . $this->_getTableName('kowal_wfirma_zlecenie_docs') . " (zlecenie_id, store_id, type, wf_id, fullnumber)
                VALUES (?, ?, ?, ?, ?);";
        $connection->query($sql, [
            $params['zlecenie_id'],
            $params['store_id'],
            $params['type'],
            $params['wf_id'],
            $params['fullnumber']
        ]);
    }

    /**
     * Lista dokument z wFirmy dla zlecenia
     */
    public function getWfirmaDocs($zlecenie_id)
    {
        $connection = $this->_getConnection('core_read');
        $sql = "SELECT  *  FROM " . $this->_getTableName('kowal_wfirma_zlecenie_docs') . " WHERE zlecenie_id = ?";
        return $connection->fetchAll($sql, [$zlecenie_id]);
    }

    public function deleteWfirmaDocs($zlecenie_id)
    {
        $connection = $this->_getConnection('core_write');
        $sql = "DELETE FROM " . $this->_getTableName('kowal_wfirma_zlecenie_docs') . " WHERE zlecenie_id = ?";
        return $connection->fetchAll($sql, [$zlecenie_id]);
    }

    public function getKosztNowy($sku, $store_id){
        $connection = $this->_getConnection('core_read');
        $nazwa_bazowa = $this->getAttribute($sku,'nazwa_bazowa',0);
        $sql = "SELECT  koszt_nowy  FROM " . $this->_getTableName('kowal_koszty_koszty') . " WHERE store_id = ? AND nazwa_bazowa = ?";
        if($koszt = $connection->fetchOne($sql, [$store_id, $nazwa_bazowa])){
            return $koszt;
        }else{
            $sql = "SELECT  koszt_nowy  FROM " . $this->_getTableName('kowal_koszty_koszty') . " WHERE store_id = ? AND nazwa_bazowa = ?";
            return $connection->fetchOne($sql, [0, $nazwa_bazowa]);
        }
    }
}
