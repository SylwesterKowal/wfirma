<?php
/**
 * Created by PhpStorm.
 * User: piotrr
 * Date: 20.11.2018
 * Time: 09:45
 */

namespace Kowal\WFirma\lib;

class InvoiceContent extends Entity
{
    protected $moduleName = 'invoicecontents';
    protected $entityName = 'invoicecontent';

    public function setData($goodId, $count, $price, $vatCode = null)
    {
        $this->data = [
            'invoicecontent' => [
                'good' => [
                    'id' => $goodId
                ],
                'price' => $price,
                'count' => $count
            ]
        ];

        if( isset($vatCode['country']) && $vatCode['country'] != 'PL'){
            $this->data['invoicecontent']['vat_code']['id'] = $vatCode['vat_code'];
        }else{
            $this->data['invoicecontent']['vat'] = $vatCode['vat'];
        }


    }
}