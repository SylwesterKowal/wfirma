<?php
/**
 * Created by PhpStorm.
 * User: piotrr
 * Date: 20.11.2018
 * Time: 09:45
 */

namespace Kowal\WFirma\lib;

class Invoice extends Entity
{
    protected $moduleName = 'invoices';
    protected $entityName = 'invoice';

    public function setData(
        $idContractor,
        $date,
        $disposalDate,
        $paymentDate,
        $invoiceContentsArray,
        $totalPaid,
        $type = "normal",
        $seria = null,
        $currency = 'PLN',
        $order_id = '',
        $advance_invoice_id = null,
        $contractor_data = null
    )
    {
        if ($type == 'normal') {
            $this->data = [
                'invoice' => [
                    'type' => $type,
                    'series' => [
                        'id' => $this->config->getSeries(0, $seria),
                    ],
                    'alreadypaid_initial' => (($this->config->isAutoPayment()) ? round($totalPaid, 2) : 0),
                    'contractor' => [
                        'id' => $idContractor
                    ],
                    'paymentmethod' => 'transfer', // cash, transfer, compensation, cod, payment_card
                    'date' => $date,
                    'auto_send' => $this->config->sendInvoiceByWfirma(),
                    'disposaldate' => $disposalDate,
                    'paymentDate' => $paymentDate,
                    'invoicecontents' => $invoiceContentsArray,
                    'price_type' => 'brutto',
                    'currency' => $currency,
                    'id_external' => $order_id,
                    'description' => $order_id
                ]
            ];
        } else if ($type == 'proforma') {
            $this->data = [
                'invoice' => [
                    'type' => $type,
                    'series' => [
                        'id' => $this->config->getSeries(0, $seria),
                    ],
                    'alreadypaid_initial' => (($this->config->isAutoPayment()) ? round($totalPaid, 2) : 0),
                    'contractor' => [
                        'id' => $idContractor
                    ],
                    'paymentmethod' => 'transfer', // cash, transfer, compensation, cod, payment_card
                    'date' => $date,
                    'auto_send' => $this->config->sendInvoiceByWfirma(),
                    'disposaldate' => $disposalDate,
                    'paymentDate' => $paymentDate,
                    'invoicecontents' => $invoiceContentsArray,
                    'price_type' => 'brutto',
                    'currency' => $currency,
                    'id_external' => $order_id,
                    'description' => $order_id
                ]
            ];
        } else if ($type == 'advance') {
            $this->data = [
                'invoice' => [
                    'type' => $type,
                    "proforma_id" => $advance_invoice_id,
                    'series' => [
                        'id' => '41565457' //$this->config->getSeries(0, $seria),
                    ],
                    'alreadypaid_initial' => (($this->config->isAutoPayment()) ? round($totalPaid, 2) : 0),
                    'contractor' => [
                        'id' => $idContractor
                    ],
                    'paymentmethod' => 'transfer', // cash, transfer, compensation, cod, payment_card
                    'date' => $date,
                    'auto_send' => $this->config->sendInvoiceByWfirma(),
                    'disposaldate' => $disposalDate,
                    'paymentDate' => $paymentDate,
                    'invoicecontents' => $invoiceContentsArray,
                    'price_type' => 'brutto',
                    'currency' => $currency,
                    'id_external' => $order_id,
                    'description' => $order_id
                ]
            ];
        } else if ($type == 'final') {
            $this->data = [
                'invoice' => [
                    'type' => $type,
                    'advance_invoices' => [
                        'id' => $advance_invoice_id
                    ],
                    'series' => [
                        'id' => '41565457' //$this->config->getSeries(0, $seria),
                    ],
                    'alreadypaid_initial' => (($this->config->isAutoPayment()) ? round($totalPaid, 2) : 0),
                    'contractor' => [
                        'id' => $idContractor
                    ],
                    'paymentmethod' => 'transfer', // cash, transfer, compensation, cod, payment_card
                    'date' => $date,
                    'auto_send' => $this->config->sendInvoiceByWfirma(),
                    'disposaldate' => $disposalDate,
                    'paymentDate' => $paymentDate,
                    'invoicecontents' => $invoiceContentsArray,
                    'price_type' => 'brutto',
                    'currency' => $currency,
                    'id_external' => $order_id,
                    'description' => $order_id
                ]
            ];
        } else {
            $this->data = [
                'invoice' => [
                    'type' => $type,
                    'series' => [
                        'id' => $this->config->getSeriesParagon(),
                    ],
                    'alreadypaid_initial' => (($this->config->isAutoPayment() && $totalPaid > 0) ? round($totalPaid, 2) : 0),

                    'paymentmethod' => 'transfer', // cash, transfer, compensation, cod, payment_card
                    'date' => $date,
                    'auto_send' => $this->config->sendInvoiceByWfirma(),
                    'disposaldate' => $disposalDate,
                    'paymentDate' => $paymentDate,
                    'invoicecontents' => $invoiceContentsArray,
                    'price_type' => 'brutto',
                    'description' => $order_id
                ]
            ];
        }

    }

    public function update($data)
    {
        $this->data = $data;
    }

    public function setContractorData($name = null, $city = null, $nip = null, $street = null, $email = null, $country = null, $phone = null)
    {
        $this->data['invoice']['contractor']['name'] = $name;
        $this->data['invoice']['contractor']['city'] = $city;
        $this->data['invoice']['contractor']['nip'] = $nip;
        $this->data['invoice']['contractor']['street'] = $street;
        $this->data['invoice']['contractor']['email'] = $email;
        $this->data['invoice']['contractor']['country'] = $country;
        $this->data['invoice']['contractor']['phone'] = $phone;

    }
}