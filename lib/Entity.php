<?php
/**
 * Created by PhpStorm.
 * User: piotrr
 * Date: 20.11.2018
 * Time: 11:53
 */

namespace Kowal\WFirma\lib;

abstract class Entity
{
    protected $api = null;
    protected $data = null;
    protected $findData = null;
    protected $moduleName;
    protected $actionName;
    protected $entityName;
    protected $aditional_parameters = "";

    public function __construct(
        \Kowal\WFirma\lib\Api    $api,
        \Kowal\WFirma\lib\Config $config,
        \Magento\Framework\App\Cache\TypeListInterface $cachTypeList
    )
    {
        $this->api = $api;
        $this->config = $config;
        $this->cachTypeList = $cachTypeList;
    }

    public function setActionName($name){
        $this->actionName = $name;
    }
    public function setAditionalParameters($param){
        $this->aditional_parameters = $param;
    }

    /**
     * @param $aditional_parameters "&param=value"
     * @return mixed
     * @throws \Exception
     */
    public function add($fullnumber = false, $store_id = 0)
    {
        $current_cred = $this->config->getCredentials($store_id);
        if($new_cred = $this->api->setCredentials( $current_cred )){
            $this->saveNewCred($current_cred, $new_cred);
        }


        $this->actionName = (!is_null($this->actionName)) ? $this->actionName : $this->moduleName;

        $this->api->setAditionalParameters($this->aditional_parameters);
        $response = $this->api->send(["$this->moduleName" => $this->data], $this->actionName . '/add',"json");

        if(isset($response[$this->moduleName][0][$this->entityName]["id"])) {
            if ($fullnumber) {

                return [
                    'id' => $response[$this->moduleName][0][$this->entityName]["id"],
                    'fullnumber' => ((isset($response[$this->moduleName][0][$this->entityName]["fullnumber"])) ? $response[$this->moduleName][0][$this->entityName]["fullnumber"] : ""),
                ];
            } else {
                return $response[$this->moduleName][0][$this->entityName]["id"];
            }
        }else{
            return false;
        }

    }

    private function saveNewCred($current_cred, $new_cred){
        if(isset($new_cred['AUTHORIZATION_TOKEN']) && $new_cred['AUTHORIZATION_TOKEN'] != $current_cred['AUTHORIZATION_TOKEN']){
            $this->config->setConfig('wfirma/wfirmaapi/access_token',$new_cred['AUTHORIZATION_TOKEN']);
            $this->config->setConfig('wfirma/wfirmaapi/refresh_token',$new_cred['REFRESH_TOKEN']);
            $this->config->setConfig('wfirma/wfirmaapi/timestamp_token',$new_cred['TIMESTAMP_TOKEN']);
            $this->cachTypeList->cleanType('config');
        }
    }

    public function get($id, $store_id = 0)
    {

        $current_cred = $this->config->getCredentials($store_id);
        if($new_cred = $this->api->setCredentials( $current_cred )){
            $this->saveNewCred($current_cred, $new_cred);
        }

        $this->actionName = (!is_null($this->actionName)) ? $this->actionName : $this->moduleName;

        $this->api->setAditionalParameters($this->aditional_parameters);

        $response = $this->api->send(["$this->moduleName" => $this->data], $this->actionName . '/get/'.$id);


        if(isset($response[$this->moduleName][0][$this->entityName]["id"])) {
            return $response[$this->moduleName][0];
        }else{
            return null;
        }
    }

    public function edit($id, $store_id = 0)
    {
        $current_cred = $this->config->getCredentials($store_id);
        if($new_cred = $this->api->setCredentials( $current_cred )){
            $this->saveNewCred($current_cred, $new_cred);
        }

        $this->actionName = (!is_null($this->actionName)) ? $this->actionName : $this->moduleName;
        $this->api->setAditionalParameters($this->aditional_parameters);

        $response = $this->api->send(["$this->moduleName" => $this->data], $this->actionName . '/edit/'.$id);



        if(isset($response[$this->moduleName][0][$this->entityName]["id"])) {
            return $response[$this->moduleName][0];
        }else{
            return null;
        }
    }

    public function delete($id, $actionName, $store_id = 0)
    {
        $current_cred = $this->config->getCredentials($store_id);
        if($new_cred = $this->api->setCredentials( $current_cred )){
            $this->saveNewCred($current_cred, $new_cred);
        }
        $this->api->setAditionalParameters($this->aditional_parameters);
        $response = $this->api->delete( $actionName . '/delete/'.(string)$id);
        return $response;
    }

    public function findOne($store_id = 0)
    {
        $current_cred = $this->config->getCredentials($store_id);
        if($new_cred = $this->api->setCredentials( $current_cred )){
            $this->saveNewCred($current_cred, $new_cred);
        }
        $this->api->setAditionalParameters("");
        $response = $this->api->send(["$this->moduleName" => [$this->findData]], $this->moduleName . '/find');
        if (isset($response[$this->moduleName][0][$this->entityName]["id"])) {

            file_put_contents('__goog_info.txt',print_r($response,true));
            return $response[$this->moduleName][0][$this->entityName]["id"];
        }
        return null;

    }

    public function findAll($store_id = 0)
    {
        try {
            $current_cred = $this->config->getCredentials($store_id);
            if($new_cred = $this->api->setCredentials( $current_cred )){
                $this->saveNewCred($current_cred, $new_cred);
            }
            $this->api->setAditionalParameters("");
            $response = $this->api->send(["$this->moduleName" => [$this->findData]], $this->moduleName . '/find');

            if (isset($response[$this->moduleName][0][$this->entityName]["id"])) {
                return $response[$this->moduleName];
            }
            return null;
        } catch (\Exception $e) {
            return [];
        }

    }

    public function setFindData($findData)
    {
        $this->findData = $findData;
    }

    public function getData()
    {
        return $this->data;
    }

    public function createNew()
    {
//$class = \get_class($this);
//        $new = new $class;
//        $new->data = null;
        $new = clone $this;
        return $new;
    }

    public function download($invoice_id, $store_id = 0)
    {
        $current_cred = $this->config->getCredentials($store_id);
        if($new_cred = $this->api->setCredentials( $current_cred )){
            $this->saveNewCred($current_cred, $new_cred);
        }
        $response = $this->api->send(["$this->moduleName" => $this->data], $this->moduleName . '/download/' . $invoice_id, 'download');
        return $response;
    }


}