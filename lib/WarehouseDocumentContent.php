<?php
/**
 * Created by PhpStorm.
 * User: piotrr
 * Date: 20.11.2018
 * Time: 09:45
 */

namespace Kowal\WFirma\lib;

class WarehouseDocumentContent extends Entity
{
    protected $moduleName = 'warehouse_document_contents';
    protected $entityName = 'warehouse_document_content';

    public function setData($goodId, $count, $warehouse, $price = null)
    {
        $this->data = [
            'warehouse_document_content' => [
                'good' => [
                    'id' => $goodId
                ],
                'unit_count' => $count
            ]
        ];

        if(!is_null($price)) $this->data['warehouse_document_content']['price'] = $price;
    }
}