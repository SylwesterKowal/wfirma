<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class DownloadDocs extends Command
{

    const STORE_ID = "store_id";
    const TEST = "option";

    private $arrayDocs = [];
    private $store_id = 0;


    public function __construct(
        \Psr\Log\LoggerInterface                                   $logger,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        \Magento\Sales\Api\OrderRepositoryInterface                $orderRepository,
        \Kowal\WFirma\Helper\WarehouseDocumentData                 $warehouseDocumentData,

        \Kowal\WFirma\Helper\Validate                              $validate,
        \Kowal\WFirma\Helper\Firma                                 $firma,
        \Kowal\WFirma\lib\Config                                   $config,
        \Kowal\WFirma\lib\Query                                    $query,
        \Kowal\WFirma\lib\Contractor                               $contractor,
        \Kowal\WFirma\lib\WarehouseDocument                        $warehouseDocument,
        \Kowal\Email\Helper\Send                                   $send
    )
    {
        $this->logger = $logger;
        $this->orderCollectionFactory = $orderCollectionFactory;
        $this->orderRepository = $orderRepository;
        $this->warehouseDocumentData = $warehouseDocumentData;
        $this->config = $config;
        $this->query = $query;
        $this->validate = $validate;
        $this->firma = $firma;
        $this->contractor = $contractor;
        $this->warehouseDocument = $warehouseDocument;
        $this->send = $send;


        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(
        InputInterface  $input,
        OutputInterface $output
    )
    {

        if ($this->store_id = $input->getArgument(self::STORE_ID)) {

        } else {
            $this->store_id = 0;
        }

        $this->Doc = $this->warehouseDocument->createNew();
        $limit = 20;
        $condition = ['parameters' => ['page' => 1, 'limit' => $limit]];
        $this->Doc->setFindData($condition);

        if ($docs = $this->Doc->findAll($this->store_id)) {
            $pages = round((int)$docs['parameters']['total'] / $limit, 0);
            echo "PAGE 1\n";
            foreach ($docs as $keyContr => $doc) {
                if (isset($doc['warehouse_document']['description']) && $doc['warehouse_document']['invoice']['id'] == 0) {
                    $this->saveToArray($doc);

                    if ($option = $input->getOption(self::TEST)) {
                        $output->writeln(print_r($doc, true));
                    }
                }

            }

            if ($option = $input->getOption(self::TEST)) {
                $this->saveToCsvFile();
                return 1;
            }

            for ($i = 2; $i <= $pages; $i++) {
                $condition = ['parameters' => ['page' => $i, 'limit' => $limit]];
                $this->Doc->setFindData($condition);
                if ($docs = $this->Doc->findAll($this->store_id)) {

                    echo "PAGE $i\n";
                    foreach ($docs as $keyContr => $doc) {
                        if (isset($doc['warehouse_document']['description']) && $doc['warehouse_document']['invoice']['id'] == 0) {
                            $this->saveToArray($doc);
                        }
                    }
                }
            }

        }

        $this->saveToCsvFile();
        return 1;
    }

    protected function saveToArray($doc)
    {
        $order_increment_id = $doc['warehouse_document']['description'];

        if ($orderData = $this->query->getwFirmaDocsIds($order_increment_id)) {
            $orderData['wfirma_doc_id'] = $doc['warehouse_document']['id'];
            $orderData['wfirma_doc_fullnumber'] = $doc['warehouse_document']['fullnumber'];
            $orderData['wfirma_doc_type'] = $doc['warehouse_document']['type'];
            $orderData['wfirma_doc_magazyn_id'] = $doc['warehouse_document']['warehouse']['id'];
            $orderData['wfirma_doc_magazyn_name'] = $this->getMagazynName($orderData);
            $this->arrayDocs[] = $orderData;
            echo $doc['warehouse_document']['id'] . '/' . $doc['warehouse_document']['fullnumber'] . PHP_EOL;
        } else {
            $orderData['entity_id'] = null;
            $orderData['store_id'] = null;
            $orderData['increment_id'] = null;
            $orderData['wfirma_wz_id'] = null;
            $orderData['wfirma_wz_fullnumber'] = null;
            $orderData['wfirma_pw_produckja_id'] = null;
            $orderData['wfirma_pw_produckja_fullnumber'] = null;
            $orderData['wfirma_pw_rezerwacja_id'] = null;
            $orderData['wfirma_pw_rezerwacja_fullnumber'] = null;
            $orderData['wfirma_rw_rezerwacja_id'] = null;
            $orderData['wfirma_rw_rezerwacja_fullnumber'] = null;
            $orderData['wfirma_pz_id'] = null;
            $orderData['wfirma_pz_fullnumber'] = null;
            $orderData['wfirma_doc_id'] = $doc['warehouse_document']['id'];
            $orderData['wfirma_doc_fullnumber'] = $doc['warehouse_document']['fullnumber'];
            $orderData['wfirma_doc_type'] = $doc['warehouse_document']['type'];
            $orderData['wfirma_doc_magazyn_id'] = $doc['warehouse_document']['warehouse']['id'];
            $orderData['wfirma_doc_magazyn_name'] = $this->getMagazynName($orderData);
            $this->arrayDocs[] = $orderData;
        }
    }

    private function getMagazynName($orderData)
    {
        switch ($orderData['wfirma_doc_magazyn_id']) {
            case '651931':
                return 'GŁÓWNY';
            case '656961':
                return 'KOMPLETACJA';
            case '656967':
                return 'REZERWACJA';
            case '672882':
                return 'GLOWNY';
            case '690544':
                return 'KOMPLETACJA';
            case '682912':
                return 'REZERWACJA';
            case '697171':
                return 'EVENT';

        }

    }

    private function saveToCsvFile()
    {
        $file = fopen("__wfirma_docs_{$this->store_id}.csv", "w");
        $heders = [
            'entity_id',
            'store_id',
            'increment_id',
            'wfirma_wz_id',
            'wfirma_wz_fullnumber',
            'wfirma_pw_produckja_id',
            'wfirma_pw_produckja_fullnumber',
            'wfirma_pw_rezerwacja_id',
            'wfirma_pw_rezerwacja_fullnumber',
            'wfirma_rw_rezerwacja_id',
            'wfirma_rw_rezerwacja_fullnumber',
            'wfirma_pz_id',
            'wfirma_pz_fullnumber',
            'wfirma_doc_id',
            'wfirma_doc_fullnumber',
            'wfirma_doc_type',
            'wfirma_doc_magazyn_id',
            'wfirma_doc_magazyn_name'
        ];
        fputcsv($file, $heders);

        foreach ($this->arrayDocs as $row) {
            fputcsv($file, $row);
        }
        fclose($file);
    }


    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName("kowal_wfirma:downloaddocs");
        $this->setDescription("Download Docs deom WFirma");
        $this->setDefinition([
            new InputArgument(self::STORE_ID, InputArgument::OPTIONAL, "STORE ID"),
            new InputOption(self::TEST, "-t", InputOption::VALUE_NONE, "Test")
        ]);
        parent::configure();
    }
}