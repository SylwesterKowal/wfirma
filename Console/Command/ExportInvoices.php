<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ExportInvoices extends Command
{

    const NAME_ARGUMENT = "name";
    const NAME_OPTION = "option";

    /**
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Kowal\WFirma\lib\ExportInvoices $exportInvoices
     */
    public function __construct(
        \Psr\Log\LoggerInterface         $logger,
        \Kowal\WFirma\lib\ExportInvoices $exportInvoices

    )
    {
        $this->logger = $logger;
        $this->exportInvoices = $exportInvoices;

        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(
        InputInterface  $input,
        OutputInterface $output
    )
    {
        $order_id = $input->getArgument(self::NAME_ARGUMENT);
        $option = $input->getOption(self::NAME_OPTION);
        $output->writeln("TEST for order " . $order_id);
        $this->exportInvoices->execute($order_id);
        $output->writeln("Koniec ");
        return 1;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName("kowal_wfirma:exportinvoices");
        $this->setDescription("Export FV do wFirma");
        $this->setDefinition([
            new InputArgument(self::NAME_ARGUMENT, InputArgument::OPTIONAL, "Name"),
            new InputOption(self::NAME_OPTION, "-a", InputOption::VALUE_NONE, "Option functionality")
        ]);
        parent::configure();
    }
}
