<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ExportOrders extends Command
{

    const ORDER_ID = "order_id";
    const NAME_OPTION = "option";

    protected $varCodes;

    protected $wywolanieReczne = false;
    protected $przyjecieDoKompletacjiPodFakture = false;

    /**
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory
     * @param \Magento\Sales\Api\OrderRepositoryInterface $orderRepository
     * @param \Kowal\WFirma\Helper\WarehouseDocumentData $warehouseDocumentData
     * @param \Kowal\WFirma\lib\WarehouseDocument $warehouseDocument
     * @param \Kowal\WFirma\Helper\Validate $validate
     * @param \Kowal\WFirma\Helper\Firma $firma
     * @param \Kowal\WFirma\lib\Config $config
     * @param \Kowal\WFirma\lib\Query $query
     * @param \Kowal\WFirma\lib\Contractor $contractor
     * @param \Kowal\Email\Helper\Send $send
     * @param \Kowal\WFirma\lib\Invoice $invoiceFactory
     * @param \Kowal\WFirma\Service\CountriesTax $countriesTax
     * @param \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory
     * @param \Kowal\WFirma\lib\Good $goodFactory
     * @param \Kowal\WFirma\lib\InvoiceContent $invoiceContentFactory
     * @param \Kowal\WFirma\lib\VatCodes $vatCodes
     * @param \Kowal\WFirma\lib\Mapping $mapping
     * @param \Kowal\WFirma\lib\InvoiceDownload $invoiceDownloadFactory
     * @param \Magento\Framework\Filesystem\DirectoryList $directoryList
     * @param \Magento\Framework\Filesystem\Io\File $file
     * @param \Kowal\WFirma\Model\ResourceModel\Seria\CollectionFactory $seriaCollection
     * @param \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
     * @param \Magento\Sales\Api\ShipmentRepositoryInterface $shipmentRepository
     */
    public function __construct(
        \Psr\Log\LoggerInterface                                       $logger,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory     $orderCollectionFactory,
        \Magento\Sales\Api\OrderRepositoryInterface                    $orderRepository,
        \Kowal\WFirma\Helper\WarehouseDocumentData                     $warehouseDocumentData,
        \Kowal\WFirma\Helper\Validate                                  $validate,
        \Kowal\WFirma\Helper\Firma                                     $firma,
        \Kowal\WFirma\lib\Config                                       $config,
        \Kowal\WFirma\lib\Query                                        $query,
        \Kowal\WFirma\lib\Contractor                                   $contractor,
        \Kowal\Email\Helper\Send                                       $send,
        \Kowal\WFirma\lib\Invoice                                      $invoiceFactory,
        \Kowal\WFirma\Service\CountriesTax                             $countriesTax,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Kowal\WFirma\lib\Good                                         $goodFactory,
        \Kowal\WFirma\lib\InvoiceContent                               $invoiceContentFactory,
        \Kowal\WFirma\lib\VatCodes                                     $vatCodes,
        \Kowal\WFirma\lib\Mapping                                      $mapping,
        \Kowal\WFirma\lib\InvoiceDownload                              $invoiceDownloadFactory,
        \Magento\Framework\Filesystem\DirectoryList                    $directoryList,
        \Magento\Framework\Filesystem\Io\File                          $file,
        \Kowal\WFirma\Model\ResourceModel\Seria\CollectionFactory      $seriaCollection,
        \Magento\Framework\Api\SearchCriteriaBuilder                   $searchCriteriaBuilder,
        \Magento\Sales\Api\ShipmentRepositoryInterface                 $shipmentRepository
    )
    {
        $this->logger = $logger;
        $this->orderCollectionFactory = $orderCollectionFactory;
        $this->orderRepository = $orderRepository;
        $this->warehouseDocumentData = $warehouseDocumentData;
        $this->config = $config;
        $this->query = $query;
        $this->validate = $validate;
        $this->firma = $firma;
        $this->contractor = $contractor;
        $this->send = $send;
        $this->invoiceFactory = $invoiceFactory;
        $this->countriesTax = $countriesTax;
        $this->productCollectionFactory = $productCollectionFactory;
        $this->goodFactory = $goodFactory;
        $this->invoiceContentFactory = $invoiceContentFactory;
        $this->vatCodesFactory = $vatCodes;
        $this->mapping = $mapping;
        $this->invoiceDownloadFactory = $invoiceDownloadFactory;
        $this->directoryList = $directoryList;
        $this->file = $file;
        $this->seriaCollection = $seriaCollection;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->shipmentRepository = $shipmentRepository;

        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(
        InputInterface  $input,
        OutputInterface $output
    )
    {

        $option = $input->getOption(self::NAME_OPTION);
        switch ($option) {
            case '-f':
                $this->przyjecieDoKompletacjiPodFakture = true;
                break;
        }


        if ($order_id = $input->getArgument(self::ORDER_ID)) {

            $this->wywolanieReczne = true;

            $order = $this->orderRepository->get($order_id);


            $firmaModel = $this->firma->getFirmaByStoreId($order->getStoreId());

            //file_put_contents("__dokumenty.txt", 'PARtNER: ' . PHP_EOL . print_r($firmaModel->getData(), true) . PHP_EOL);

            $billingAddress = $order->getBillingAddress();
            $nip = (string)$billingAddress->getVatId();


            $this->przeniesienieRezerwacjiDoKompletacji($order);
            $this->przyjecieProdukcjiDoKompletacji($order);


            if ($firmaModel->getPartner()) {

                $this->dokumentWZBezReerwacji($order); // dla magazynu Producenta
                $this->dokumentPZ($order); // dla magazynu Partnera
                if (!$this->validate->Nip($nip)) {
                    $this->dokumentWZ($order, $order->getStoreId()); // dla klienta ostatecznego RMG
                } else {
                    if ($this->containsWord((string)$order->getCouponCode(), 'WYMIANA') || $order->getStoreId() == 19) {
                        $this->query->updateOrderFV($order->getEntityId(), 'INNE');
                        $this->dokumentWZ($order, $order->getStoreId());
                        // dla magazynu Partnera
                    } else {
                        $this->query->updateOrderFV($order->getEntityId());
                        $this->przesuniecieStanowDoDatyWysylkiNaMagazynieKompletacji($order, $order->getStoreId());
                        $this->setInvoiceFromMagentoInvoice($order, $order->getStoreId());
                    }
                }
            } else {
                // dla magazynu Producenta
                if (!$this->validate->Nip($nip)) {
                    $this->dokumentWZ($order);
                    $output->writeln("TEST order_id " . $order_id . ' | ' . $order->getIncrementId());
                } else {
                    if ($this->containsWord((string)$order->getCouponCode(), 'WYMIANA') || $order->getStoreId() == 19) {
                        $this->query->updateOrderFV($order->getEntityId(), 'INNE');
                        $this->dokumentWZ($order, $order->getStoreId());
                        // dla magazynu Partnera
                    } else {
                        $this->query->updateOrderFV($order->getEntityId());
                        $this->przesuniecieStanowDoDatyWysylkiNaMagazynieKompletacji($order, $order->getStoreId());
                        $this->setInvoiceFromMagentoInvoice($order);
                    }
                }
            }

        } else {
            $now = new \DateTime();
            $now->modify('-3 months'); // odejmij 3 miesiące
            $fromdate = $now->format('Y-m-01 H:i:s'); // ustaw pierwszy dzień miesiąca

            $collection = $this->getOrdersCollection([
//                'wfirma_wz_id' => ['null' => true, 'eq' => ''],
                'wfirma_wz_err' => ['null' => true, 'eq' => ''],
//                'status' => ['in' => $this->config->getOrderStatusDoExportuDoWFirma()],
                'state' => ['in' => ['processing', 'complete']],
                'created_at' => ['gteq' => $fromdate]
            ]);

            // Dodaj sortowanie według daty `created_at` od najstarszych do najnowszych
            $collection->setOrder('created_at', 'ASC');

            foreach ($collection as $order) {
                if ($order->getBaseSubtotalInvoiced() > 0 &&
                    $order->getBaseSubtotalInvoiced() == $order->getbaseSubtotalRefunded()) continue; // refundet

                echo $order->getEntityId() . "/" . $order->getIncrementId() . PHP_EOL;

                $firmaModel = $this->firma->getFirmaByStoreId($order->getStoreId());

                $billingAddress = $order->getBillingAddress();
                $nip = (!is_null($billingAddress)) ? (string)$billingAddress->getVatId() : null;

                $this->przeniesienieRezerwacjiDoKompletacji($order);
                $this->przyjecieProdukcjiDoKompletacji($order);

                if ($firmaModel->getPartner()) {

                    $this->dokumentWZBezReerwacji($order); // dla magazynu Producenta
                    $this->dokumentPZ($order); // dla magazynu Partnera
                    if (!$this->validate->Nip($nip)) {
                        $this->dokumentWZ($order, $order->getStoreId());
                        // dla magazynu Partnera
                    } else {
                        if ($this->containsWord((string)$order->getCouponCode(), 'WYMIANA') || $order->getStoreId() == 19) {
                            $this->query->updateOrderFV($order->getEntityId(), 'INNE');
                            $this->dokumentWZ($order, $order->getStoreId());
                            // dla magazynu Partnera
                        } else {
                            $this->query->updateOrderFV($order->getEntityId());
                            $this->setInvoiceFromMagentoInvoice($order, $order->getStoreId());
                        }
                    } // dla magazynu Partnera
                } else {
                    // dla magazynu Producenta
                    if (!$this->validate->Nip($nip)) {
                        $this->dokumentWZ($order);
                    } else {
                        if ($this->containsWord((string)$order->getCouponCode(), 'WYMIANA') || $order->getStoreId() == 19) {
                            $this->dokumentWZ($order, $order->getStoreId());
                            $this->query->updateOrderFV($order->getEntityId(), 'INNE');
                            // dla magazynu Partnera
                        } else {
                            $this->query->updateOrderFV($order->getEntityId());
                            $this->setInvoiceFromMagentoInvoice($order);
                        }
                    }
                }
            }
        }
        return 1;
    }

    function isSendedOrder($order)
    {
        $statuses = $this->config->getOrderStatusDoExportuDoWFirma();
        if (in_array($order->getStatus(), $statuses)) {
            return true;
        } else {
            return false;
        }
    }

    function containsWord($text, $word)
    {
        // Użyj wyrażenia regularnego do wyszukiwania słowa "WYMIANA"
        if (preg_match("/\b" . preg_quote($word, '/') . "\b/", $text)) {
            return true;
        } else {
            return false;
        }
    }


    public function getVatCodes($store_id = 0)
    {
        try {
            $limit = 20;
            $condition = ['parameters' => ['page' => 1, 'limit' => $limit]];
            $this->vatCodesFactory->setFindData($condition);
            $vat_codes_ = [];


            if ($vat_codes = $this->vatCodesFactory->findAll($store_id)) {

                $pages = round((int)$vat_codes['parameters']['total'] / $limit, 0) + 1;

                foreach ($vat_codes as $keyContr => $vat_code) {
                    if (isset($vat_code['vat_code']['code'])) {
                        $vat_codes_[] = ["id" => (string)$vat_code['vat_code']['id'], 'code' => (string)$vat_code['vat_code']['code'], 'label' => (string)$vat_code['vat_code']['label'], 'rate' => (string)$vat_code['vat_code']['rate'], 'type' => (string)$vat_code['vat_code']['type'], 'country_id' => (string)$vat_code['vat_code']['declaration_country']['id']];
                    }
                }
                for ($i = 2; $i <= $pages; $i++) {
                    $condition = ['parameters' => ['page' => $i, 'limit' => $limit]];
                    $this->vatCodesFactory->setFindData($condition);
                    if ($vat_codes = $this->vatCodesFactory->findAll($store_id)) {

                        foreach ($vat_codes as $keyContr => $vat_code) {
                            if (isset($vat_code['vat_code']['code'])) {
                                $vat_codes_[] = ["id" => (string)$vat_code['vat_code']['id'], 'code' => (string)$vat_code['vat_code']['code'], 'label' => (string)$vat_code['vat_code']['label'], 'rate' => (string)$vat_code['vat_code']['rate'], 'type' => (string)$vat_code['vat_code']['type'], 'country_id' => (string)$vat_code['vat_code']['declaration_country']['id']];
                            }
                        }
                    }
                }
            }


            return $vat_codes_;
        } catch (\Exception $e) {
            $this->logger->critical($e->getMessage());
            return [];
        }
    }

    public function setInvoiceFromMagentoInvoice($order, $store_id = 0)
    {
        if (!$magazyn_kompletacji_id = $this->config->getMagazynKompletacji($store_id)) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __("Brak zdefiniowanego magazynu kompletacji w ustawieniach modułu wFirma")
            );
        }

        // data wygenerowania dokumentu wysyłki
        $date_delivery = $this->getShippmentDate($order->getEntityId(), $order->getCreatedAt());

        if ($order->getWfirmaWzId() && $order->getWfirmaPfUpdateDescription() != 'YES') {
            $this->updateFVProForma($order, $date_delivery);
        }

        if ($this->isSendedOrder($order) && $this->config->czyWystawiacFakturyProformaZamiastFvvat($store_id)) {

            #TODO gdy juz bedzie API obsługiwać FV zaliczową i końcową, ale puki co robimy aktualizację PROFORMA zapisując datę wysyłki w opisie.


//                if (!$order->getWfirmaFvZalId()) {
//                    $typ = 'advance'; // $this->setInvoiceType($order, $store_id)
//                    $wFinvoice->setData($contractorId, $date_created, $date_delivery, $date_created, $invoiceContentArray, $wartoscFaktury, $typ, $this->setInvoiceSeria($order, $store_id), $order->getOrderCurrencyCode(), $order->getIncrementId(), $wFinvoiceProForma['id']);
//                    $wFinvoiceAdvande = $wFinvoice->add(true, $store_id);
//                }
//
//
//                if (!$order->getWfirmaFvKonId()) {
//                    $typ = 'final'; // $this->setInvoiceType($order, $store_id)
//                    $wFinvoice->setData($contractorId, $date_delivery, $date_delivery, $date_created, $invoiceContentArray, $wartoscFaktury, $typ, $this->setInvoiceSeria($order, $store_id), $order->getOrderCurrencyCode(), $order->getIncrementId(), $wFinvoiceAdvande['id']);
//                    $wFinvoiceFinal = $wFinvoice->add(true, $store_id);
//                    $this->downloadInvoicePdf($order, $wFinvoiceFinal['id'], $store_id);
//                    $this->query->updateFVAdvanceFinal($order->getEntityId(), $wFinvoiceAdvande['id'], $wFinvoiceAdvande['fullnumber'], $wFinvoiceFinal['id'], $wFinvoiceFinal['fullnumber']);
//                }


        }

        if ($order->getWfirmaWzId()) return; // jeśli już wystawiono FV PROF lub WZ to pomiń

        $billingAddress = $order->getBillingAddress();
        $nip = $billingAddress->getVatId();
        $czy_wystawiac_tylko_dla_firm = $this->config->getConfigValue('wfirma/options/czy_wystawiac_tylko_dla_firm', $order->getStoreId());


        $wFinvoice = $this->invoiceFactory->createNew();
//        $contractorId = $this->getContractor($order, $store_id);

        if (!$contractorData = $this->getContractorByEmail($order)) {
            if ($contractor = $this->createContractor($order)) {
                $contractorId = (isset($contractor['id'])) ? $contractor['id'] : null;
            } else {
                $contractorId = ($store_id == 16) ? 115882373 : 115882354;
            }

        } else if (isset($contractorData['id'])) {
            $contractorId = $contractorData['id'];
            #TODO mozna porównać dane klient z wFirma z danymi w Magento
        } else {
            $contractorId = ($store_id == 16) ? 115882373 : 115882354;
        }

        try {
            $invoiceContentArray = $this->getInvoiceContentsArray($order, true, $store_id);


            $date_created = date('Y-m-d', strtotime($order->getCreatedAt()));

            $wFinvoice->setAditionalParameters("&warehouse_id=" . $magazyn_kompletacji_id);

            $wartoscFaktury = $order->getGrandTotal();

            $Invoice_type = $this->setInvoiceType($order, $store_id);

            $wFinvoice->setData($contractorId, $date_created, $date_delivery, $date_created, $invoiceContentArray, $wartoscFaktury, $Invoice_type, $this->setInvoiceSeria($order, $store_id), $order->getOrderCurrencyCode(), $order->getIncrementId());
            $customerData = $this->getContractorData($order);
            $wFinvoice->setContractorData($customerData['name'] , $customerData['city'], $customerData['nip'], $customerData['street'], $customerData['email'], $customerData['country'], $customerData['phone']);

            $wFinvoiceProForma = $wFinvoice->add(true, $store_id);
            $this->query->updateOrderWZ($order->getEntityId(), $wFinvoiceProForma['id'], $wFinvoiceProForma['fullnumber']);


        } catch (\Exception $e) {
            $this->logger->critical('Function setInvoiceFromMagentoInvoice: ' . $e->getMessage() . PHP_EOL . print_r($invoiceContentArray, true));
            echo 'Function setInvoiceFromMagentoInvoice: ' . $e->getMessage() . PHP_EOL . print_r($invoiceContentArray, true);
            $this->query->updateErrOrderWZ($order->getEntityId());
        }
    }

    protected function updateFVProForma($order, $date_delivery)
    {
        if (!$magazyn_kompletacji_id = $this->config->getMagazynKompletacji($order->getStoreId())) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __("Brak zdefiniowanego magazynu kompletacji w ustawieniach modułu wFirma")
            );
        }

        try {

//            $invoice = $this->invoiceFactory->get($order->getWfirmaWzId(), $order->getStoreId());
//
//            $currentDescription = isset($invoice['invoice']['description']) ? $invoice['invoice']['description'] : '';


            // Sprawdzamy czy fv prof RMG poprzez udział w promocji SM99999999
            $appliedRuleIds = explode(",", (string)$order->getAppliedRuleIds());
            if (in_array(479, $appliedRuleIds)) {
                $couponCode = (string)$order->getCouponCode();
                $couponCodePrefix = substr($couponCode, 0, 2);
                $voucherValue = ($couponCodePrefix == 'SM') ? 19 : 20.12;
                $voucher = " | Zapłacono ".$voucherValue." zł voucherem nr.: " . $couponCode;
            } else {
                $voucher = "";
            }

            // Dodaj aktualną datę do opisu
            $newDescription = $order->getIncrementId() . $voucher . ' | ' . "Wysłana dnia: $date_delivery";

            // Zaktualizuj fakturę
            $invoiceData = array(
                'invoice' => array(
                    'id' => $order->getWfirmaWzId(),
                    'description' => $newDescription
                )
            );
            $this->invoiceFactory->update($invoiceData);
            $this->invoiceFactory->edit($order->getWfirmaWzId(), $order->getStoreId());

            $this->query->updateProFormaDescription($order->getEntityId());

        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }

    private function getShippmentDate($order_id, $created_at)
    {
        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter('order_id', $order_id, 'eq')
            ->create();

        // Get list of shippments
        $shippList = $this->shipmentRepository->getList($searchCriteria);

        // faktura
        $shipDate = null;
        foreach ($shippList->getItems() as $shipment) {
            $shipDate = date('Y-m-d', strtotime($shipment->getCreatedAt()));
            break;
        }
        return (is_null($shipDate)) ? date('Y-m-d', strtotime($created_at)) : $shipDate;
    }

    private function setInvoiceSeria($order, $store_id = 0)
    {
        $seria = null;
        if (!$this->config->getConfigValue('wfirma/options/multiseria', $store_id)) {
            return $seria;
        } else {
            $attr = 'get' . $this->config->getConfigValue('wfirma/options/multiseria_order_attr_name', $store_id);
            if ($seriaCodeVallue = $order->{$attr}()) {
                $seriaCollection = $this->seriaCollection->create();
                $seriaCollection->addFieldToFilter('order_attr_value', ['eq' => $seriaCodeVallue]);
                foreach ($seriaCollection as $seria_) {
                    if ($seria = $seria_->getWfirmaSeria()) {
                        return $seria;
                    }
                    return null;
                }

            } else {
                return $seria;
            }
        }
    }

    public function downloadInvoicePdf($order, $wf_invoice_id, $store_id = 0)
    {
        if (!$this->config->sendInvoiceByStore($store_id)) return;

        $invoiceDownload = $this->invoiceDownloadFactory->createNew();
        $invoiceDownload->setData('invoice');
        if ($download = $invoiceDownload->download($wf_invoice_id, $store_id)) {
            $filename = $order->getIncrementId() . "_invoice.pdf";

            $filepath = $this->getFileName($filename);

            @file_put_contents($filepath, $download);

            // $this->query->setInvoicePdf($this->setInvoicePdfUrl($filename), $this->invoice->getId());
        }
    }

    public function getFileName($filename)
    {
        $this->var = $this->directoryList->getPath('var');

        if (!file_exists($this->var . DIRECTORY_SEPARATOR . 'invoices')) {
            $this->file->mkdir($this->var . DIRECTORY_SEPARATOR . 'invoices', 0775);
        }
        return $this->var . DIRECTORY_SEPARATOR . 'invoices' . DIRECTORY_SEPARATOR . $filename;
    }

    public function getInvoiceContentsArray($order, $orderItemsFromInvoice = false, $store_id = 0)
    {
        $orderItems = $order->getAllItems();
        $this->varCodes = $this->getVatCodes($order->getStoreId());

        $invoiceContentsArray = [];
        $znizka = 0.0;
        $itemsArray = [];

        $canShip = false;
        foreach ($orderItems as $item) {
            if ($item->getParentItemId() && $item->getParentItem() && $item->getParentItem()->getProductType() == 'configurable') {
                continue;
            }
            if ($item->getProductType() == \Magento\Catalog\Model\Product\Type::TYPE_BUNDLE) {
                continue;
            }


            $countryId = is_null($order->getShippingAddress()) ? "" : $order->getShippingAddress()->getCountryId();
            $tax = (int)$this->countriesTax->getTaxRate('standard', $countryId);
            $good = $this->goodFactory->createNew();
            $znizka = (float)$item->getDiscountAmount() / (int)$item->getQtyOrdered();
            $price = $item->getPrice() - $znizka;
            $ean = $this->getEanForProduct($item->getSku());
            if ($goodId = $this->getGoodByEAN($item->getSku(), $store_id)) {

            } else {
                $good->setData($item->getSku(), $item->getName(), 'szt.', '0', ceil($price * 100) / 100, $tax, "{$item->getSku()}");
                $goodId = $good->add($store_id);
            }


            $invoiceContent = $this->invoiceContentFactory->createNew();
            $invoiceContent->setData($goodId, $item->getQtyOrdered(), $price, $this->getVatCodeId($order, $item, $tax));
            $invoiceContentsArray[] = $invoiceContent->getData();

        }

        try {
            if ($order->getShippingInclTax() > 0) {
                $invoiceContent = $this->invoiceContentFactory->createNew();
                $invoiceContent->setData($this->getShippingGood($order, $store_id), 1, $order->getShippingInclTax(), $this->getVatCodeId($order, null, $tax));
                $invoiceContentsArray[] = $invoiceContent->getData();
            }

        } catch
        (\Exception $e) {
            $this->logger->critical($e->getMessage());
        }

        return $invoiceContentsArray;
    }

    public function getShippingGood($order, $store_id = 0)
    {
        try {
//            if ($goodId = $this->warehouseDocumentData->getGoodBySKU('delivery', $store_id)) {
//                 return $goodId;
//            } else {

            if ($goodId = $this->getGoodByEAN('delivery', $store_id)) {
                return $goodId;
            } else {
                $tax = '23';
                $good = $this->goodFactory->createNew();
                $shippingMethodMagento = $order->getShippingMethod();
                $name = $this->mapping->getShippmentTypeForERP($shippingMethodMagento);
                $good->setData('delivery', $name, 'szt.', '0', $order->getShippingInclTax(), $tax, '', 'service');
                $goodId = $good->add(false, $store_id);
                return $goodId;
            }
//            }
        } catch (\Exception $e) {
            $this->logger->critical($e->getMessage());
        }
    }

    public function getVatCodeId($order, $item, $tax)
    {
        $billingAddress = $order->getBillingAddress();
        $country = $billingAddress->getCountryId();
        $vat_code_id = null;
        if ($country != 'PL') {
            foreach ($this->varCodes as $vatCode) {
                if (isset($vatCode['country_id']) && (int)$vatCode['country_id'] == $country) {
                    $vat_code_id = $vatCode['id'];
                }
            }
            if (is_null($vat_code_id) && !is_null($item)) {
                foreach ($this->varCodes as $vatCode) {
                    if (isset($vatCode['rate']) && (int)$vatCode['rate'] == (int)$item->getTaxPercent()) {
                        $vat_code_id = $vatCode['id'];
                    }
                }
            }
        }
        return ['country' => $country, 'vat_code' => $vat_code_id, 'vat' => $tax];
    }

    public function getGoodByEAN($ean, $store_id = 0)
    {
        $good = $this->goodFactory->createNew();
        $good->setFindData(['parameters' => ['conditions' => ['condition' => [
            'field' => 'code',
            'operator' => 'eq',
            'value' => $ean
        ]]]]);
        return ($good->findOne($store_id));
    }

    private function getEanForProduct($sku)
    {
        $collection = $this->productCollectionFactory->create();
        $collection->addAttributeToFilter('sku', $sku);
        $collection->addAttributeToSelect('*');
        $ean = '';
        if (count($collection)) {
            $product = $collection->getFirstItem();
            try {
                $ean = $product->getData($this->config->getAttributeCodeEAN());
            } catch (\Exception $ex) {
                // problem z odczytem EAN
            }

            return $ean;
        }
        throw new \Exception("getEanForProduct : Product sku=$sku doesn't exist");
    }


    public function getContractor($order, $store_id = 0)
    {
        if ($this->setInvoiceType($order) != 'normal') return null;
        $billingAddress = $order->getBillingAddress();

        $contractor = $this->contractor->createNew();
        $company = (trim((string)$billingAddress->getCompany()));
        $name = (($company) ? ($company . " ") : '') . $billingAddress->getFirstname() . ' ' . $billingAddress->getLastname();
        $postcode = $billingAddress->getPostcode();
        $city = $billingAddress->getCity();
        $street = implode(', ', $billingAddress->getStreet());
        $email = $order->getCustomerEmail();
//        $email = 'testtest@meus.net.pl';
        $country = $billingAddress->getCountryId();
        $contractor->setData($name, $street, $postcode, $city, $country, $billingAddress->getVatId(), $billingAddress->getTelephone(), $email);
        $contractorId = $contractor->add($store_id);
        return $contractorId;
    }

    private function setInvoiceType($order, $store_id = 0)
    {
        if ($this->config->czyWystawiacFakturyProformaZamiastFvvat($store_id)) return 'proforma';

        $type = "normal";
        if (!$this->config->czyWystawiacFakturyOrazParagony($store_id)) return $type;

        $billingAddress = $order->getBillingAddress();
//        $company = (trim($billingAddress->getCompany()));
        $nip = $billingAddress->getVatId();
        if (!empty($nip)) {
            return $type;
        } else {
            return 'receipt_normal';
        }
    }


    public function getOrdersCollection(array $filters = [])
    {

        $collection = $this->orderCollectionFactory->create()
            ->addAttributeToSelect('*');

        foreach ($filters as $field => $condition) {

            $collection->addFieldToFilter($field, $condition);
        }

        return $collection;
    }

    private function przeniesienieRezerwacjiDoKompletacji($order)
    {
        if ($order->getWfirmaRwRezerwacjaId()) return; // jeśli już wystawiono RW to pomiń
        if (!$this->isSendedOrder($order)) return; // jeśli nie jest zakończona produkcja

        $data_reserved = [];
        // Przygotowanie danych
        foreach ($order->getAllItems() as $item) {
            if ($item->getParentItemId() && $item->getParentItem() && $item->getParentItem()->getProductType() == 'configurable') {
                continue;
            }
            if (!$item->getParentItemId() && $item->getProductType() == 'bundle') {
                continue;
            }
            $qty = (int)$item->getQtyOrdered();
            if ($qty > 0 && $qty >= $item->getQtyReserved() && $item->getQtyReserved() > 0) {
                $data_reserved[] = ["sku" => $item->getSku(), "qty" => $item->getQtyReserved(), 'price' => $this->query->getKosztNowy($item->getSku(), 0)];
            }
        }
        if (count($data_reserved)) {
            if (!$magazyn_kompletacji_id = $this->config->getMagazynKompletacji($order->getStoreId())) {
                throw new \Magento\Framework\Exception\LocalizedException(
                    __("Brak zdefiniowanego magazynu kompletacji w ustawieniach modułu wFirma")
                );
            }
            if (!$magazyn_rezerwacji_id = $this->config->getMagazynRezerwacji($order->getStoreId())) {
                throw new \Magento\Framework\Exception\LocalizedException(
                    __("Brak zdefiniowanego magazynu rezerwacji w ustawieniach modułu wFirma")
                );
            }
            try {
                // data wygenerowania dokumentu wysyłki
                $date = $this->getShippmentDate($order->getEntityId(), $order->getCreatedAt());
                if ($wfirma_rw_rezerwacja = $this->warehouseDocumentData->setData($date, $date, $data_reserved, "RW", $magazyn_rezerwacji_id, "warehouse_document_r_w", null, null, $order->getIncrementId(), $order->getIncrementId(), true, $order->getStoreId())) {
                    sleep(1);
                    if ($wfirma_pw_rezerwacja = $this->warehouseDocumentData->setData($date, $date, $data_reserved, "PW", $magazyn_kompletacji_id, "warehouse_document_p_w", null, null, $order->getIncrementId(), $order->getIncrementId(), true, $order->getStoreId())) {
                        // zapisać numery ID do sales_order
                        $this->query->updateOrderReservationIds($order->getEntityId(), $wfirma_rw_rezerwacja['id'], $wfirma_pw_rezerwacja['id'], $wfirma_rw_rezerwacja['fullnumber'], $wfirma_pw_rezerwacja['fullnumber']);
//                        file_put_contents("__dokumenty.txt", 'REZERWACJA: ' . PHP_EOL . print_r([$order->getEntityId(), $wfirma_rw_rezerwacja['id'], $wfirma_pw_rezerwacja['id'], $wfirma_rw_rezerwacja['fullnumber'], $wfirma_pw_rezerwacja['fullnumber']], true) . PHP_EOL, FILE_APPEND);
                    }
                }

            } catch (\Exception $e) {
                $this->logger->critical('Function przeniesienieRezerwacjiDoKompletacji: ' . $e->getMessage() . PHP_EOL . print_r($data_reserved, true));
                echo 'Function przeniesienieRezerwacjiDoKompletacji: ' . $e->getMessage() . PHP_EOL . print_r($data_reserved, true);

                // uzupełniam braki tylko przy manualnym uruchomieniu
                if ($this->wywolanieReczne) {
                    $this->warehouseDocumentData->setData($date, $date, $data_reserved, "PW", $magazyn_rezerwacji_id, "warehouse_document_p_w", null, null, $order->getIncrementId(), $order->getIncrementId(), true, $order->getStoreId());
                }
//                $this->send->email($message = $e->getMessage(), $domain = 'smmash.pl');
//                throw new \Magento\Framework\Exception\LocalizedException(
//                    __($e->getMessage().PHP_EOL.print_r($data_reserved,true))
//                );
            }
        }
    }

    private function przyjecieProdukcjiDoKompletacji($order)
    {
        if ($order->getWfirmaPwProduckjaId()) return; // jeśli już wystawiono PW to pomiń
        if (!$this->isSendedOrder($order)) return; // jeśli nie jest zakończona produkcja

        $data_production = [];
        // Przygotowanie danych
        foreach ($order->getAllItems() as $item) {
            if ($item->getParentItemId() && $item->getParentItem() && $item->getParentItem()->getProductType() == 'configurable') {
                continue;
            }
            if (!$item->getParentItemId() && $item->getProductType() == 'bundle') {
                continue;
            }
            $qty = (int)$item->getQtyOrdered();
            if ($qty > 0 && $qty >= $item->getQtyProduction() && $item->getQtyProduction() > 0) {
                $data_production[] = ["sku" => $item->getSku(), "qty" => $item->getQtyProduction(), 'price' => $this->query->getKosztNowy($item->getSku(), 0)];
            }
        }
        if (count($data_production)) {
            if (!$magazyn_kompletacji_id = $this->config->getMagazynKompletacji()) {
                throw new \Magento\Framework\Exception\LocalizedException(
                    __("Brak zdefiniowanego magazynu kompletacji w ustawieniach modułu wFirma")
                );
            }


            // data wygenerowania dokumentu wysyłki
            $date = $this->getShippmentDate($order->getEntityId(), $order->getCreatedAt());

            try {
                sleep(1);
                if ($wfirma_pw_produckja = $this->warehouseDocumentData->setData($date, $date, $data_production, "PW", $magazyn_kompletacji_id, "warehouse_document_p_w", null, null, $order->getIncrementId(), $order->getIncrementId(), true)) {
                    // zapisać numery ID do sales_order
                    $this->query->updateOrderProductionId($order->getEntityId(), $wfirma_pw_produckja['id'], $wfirma_pw_produckja['fullnumber']);

                }
            } catch (\Exception $e) {
                $this->logger->critical($e->getMessage());
                throw new \Magento\Framework\Exception\LocalizedException(
                    __('Function przyjecieProdukcjiDoKompletacji: ' . $e->getMessage())
                );
            }
        }
    }

    private function przesuniecieStanowDoDatyWysylkiNaMagazynieKompletacji($order, $store_id = 0)
    {

        if (!$this->przyjecieDoKompletacjiPodFakture) return; // jeśli nie wykonano polecenie ręcznie z opcją = -f to pomin
        if (!$this->isSendedOrder($order)) return; // jeśli nie jest zakończona produkcja

        $data_items = [];
        // Przygotowanie danych
        foreach ($order->getAllItems() as $item) {
            if ($item->getParentItemId() && $item->getParentItem() && $item->getParentItem()->getProductType() == 'configurable') {
                continue;
            }
            if (!$item->getParentItemId() && $item->getProductType() == 'bundle') {
                continue;
            }
            $qty = (int)$item->getQtyOrdered();
            if ($qty > 0) {
                $data_items[] = ["sku" => $item->getSku(), "qty" => $item->getQtyOrdered(), 'price' => $this->query->getKosztNowy($item->getSku(), $store_id)];
            }
        }
        if (count($data_items)) {
            if (!$magazyn_kompletacji_id = $this->config->getMagazynKompletacji($store_id)) {
                throw new \Magento\Framework\Exception\LocalizedException(
                    __("Brak zdefiniowanego magazynu kompletacji w ustawieniach modułu wFirma")
                );
            }

            $date_current = date('Y-m-d');
            // data wygenerowania dokumentu wysyłki
            $date = $this->getShippmentDate($order->getEntityId(), $order->getCreatedAt());

            try {
                if ($wfirma_rw_kompletacja = $this->warehouseDocumentData->setData($date_current, $date_current, $data_items, "RW", $magazyn_kompletacji_id, "warehouse_document_r_w", null, null, $order->getIncrementId(), $order->getIncrementId(), true, $store_id)) {
                    $wfirma_pw_kompletacja = $this->warehouseDocumentData->setData($date, $date, $data_items, "PW", $magazyn_kompletacji_id, "warehouse_document_p_w", null, null, $order->getIncrementId(), $order->getIncrementId(), true, $store_id);

                    echo print_r(['RW' => $wfirma_rw_kompletacja, 'PW' => $wfirma_pw_kompletacja], true);
                    // zapisać numery ID do sales_order
                    if (isset($wfirma_rw_kompletacja['fullnumber']) && isset($wfirma_pw_kompletacja['fullnumber'])) {
                        $this->query->updateOrderKompletacjaStockDate($order->getEntityId(), $wfirma_rw_kompletacja['fullnumber'], $wfirma_pw_kompletacja['fullnumber']);
                    }
                }
            } catch (\Exception $e) {
                $this->logger->critical($e->getMessage());

                $wfirma_pw_kompletacja = $this->warehouseDocumentData->setData($date, $date, $data_items, "PW", $magazyn_kompletacji_id, "warehouse_document_p_w", null, null, $order->getIncrementId(), $order->getIncrementId(), true, $store_id);
                if ( isset($wfirma_pw_kompletacja['fullnumber'])) {
                    $this->query->updateOrderKompletacjaStockDate($order->getEntityId(), null, $wfirma_pw_kompletacja['fullnumber']);
                }
                throw new \Magento\Framework\Exception\LocalizedException(
                    __('Function przesuniecieStanowDoDatyWysylkiNaMagazynieKompletacji: ' . $e->getMessage())
                );
            }
        }
    }

    private function dokumentWZ($order, $store_id = 0)
    {
        if ($order->getWfirmaWzId()) return; // jeśli już wystawiono FV lub WZ to pomiń
        if (!$this->isSendedOrder($order)) return; // jeśli nie jest zakończona produkcja

        $data_ = [];
        // Przygotowanie danych
        foreach ($order->getAllItems() as $item) {
            if ($item->getParentItemId() && $item->getParentItem() && $item->getParentItem()->getProductType() == 'configurable') {
                continue;
            }
            if (!$item->getParentItemId() && $item->getProductType() == 'bundle') {
                continue;
            }
            $qty = (int)$item->getQtyOrdered();

            if ($qty > 0) {
                $data_[] = ["sku" => $item->getSku(), "qty" => $qty];
            }

        }
        if (count($data_)) {
            if (!$magazyn_kompletacji_id = $this->config->getMagazynKompletacji($store_id)) {
                throw new \Magento\Framework\Exception\LocalizedException(
                    __("Brak zdefiniowanego magazynu kompletacji w ustawieniach modułu wFirma")
                );
            }


            // data wygenerowania dokumentu wysyłki
            $date = $this->getShippmentDate($order->getEntityId(), $order->getCreatedAt());

            if (!$contractorData = $this->getContractorByEmail($order)) {
                if ($contractor = $this->createContractor($order)) {
                    $contractor_id = (isset($contractor['id'])) ? $contractor['id'] : null;
                } else {
                    $contractor_id = ($store_id == 16) ? 115882373 : 115882354;
                }

            } else if (isset($contractorData['id'])) {
                $contractor_id = $contractorData['id'];
                #TODO mozna porównać dane klient z wFirma z danymi w Magento
            } else {
                $contractor_id = ($store_id == 16) ? 115882373 : 115882354;
            }
            try {
                if ($wfirma_wz = $this->warehouseDocumentData->setData($date, $date, $data_, "WZ", $magazyn_kompletacji_id, "warehouse_document_w_z", $contractor_id, 'netto', $order->getIncrementId(), $order->getIncrementId(), true, $store_id)) {
                    // zapisać numery ID do sales_order
                    $this->query->updateOrderWZ($order->getEntityId(), $wfirma_wz['id'], $wfirma_wz['fullnumber']);

//                    file_put_contents("__dokumenty.txt", 'WZ u Partnera po wyst. PZ: ' . PHP_EOL . print_r([$order->getEntityId(), $wfirma_wz['id'], $wfirma_wz['fullnumber'], $contractor_id], true) . PHP_EOL, FILE_APPEND);

                    $order->addCommentToStatusHistory(
                        __('Generowanie wydania %1.', $wfirma_wz['fullnumber'])
                    )->setIsCustomerNotified(false)->save();
                }
            } catch (\Exception $e) {
                $this->logger->critical('Function dokumentWZ: ' . $e->getMessage() . PHP_EOL . print_r($data_, true));
                echo 'Function dokumentWZ: ' . $e->getMessage() . PHP_EOL . print_r($data_, true);


                $curent_date = date("Y-m-d");
                if($rw__ = $this->warehouseDocumentData->setData($curent_date, $curent_date, $data_, "RW", $magazyn_kompletacji_id, "warehouse_document_r_w", null, null, $order->getIncrementId(), $order->getIncrementId(), true, $order->getStoreId())){
                    $this->warehouseDocumentData->setData($date, $date, $data_, "PW", $magazyn_kompletacji_id, "warehouse_document_p_w", null, null, $order->getIncrementId(), $order->getIncrementId(), true, $order->getStoreId());
                }else{
                    // Jeśli nie uda się przesunąć stanów na kompletacji to wówczas bład do WZ
                    $this->query->updateErrOrderWZ($order->getEntityId());
                }
                if ($this->wywolanieReczne) {
                    // Wywołując ręcznie przyjmuję na astan brakujace prdukty
                    $this->warehouseDocumentData->setData($date, $date, $data_, "PW", $magazyn_kompletacji_id, "warehouse_document_p_w", null, null, $order->getIncrementId(), $order->getIncrementId(), true, $order->getStoreId());
                }

//                throw new \Magento\Framework\Exception\LocalizedException(
//                    __($e->getMessage().PHP_EOL.print_r($data_,true))
//                );
            }
        }
    }

    private function dokumentWZBezReerwacji($order)
    {
        if ($order->getWfirmaWzPartnerId()) return; // jeśli już wystawiono WZ to pomiń
        if (!$this->isSendedOrder($order)) return; // jeśli nie jest zakończona produkcja

        $data_ = [];
        // Przygotowanie danych
        foreach ($order->getAllItems() as $item) {
            if ($item->getParentItemId() && $item->getParentItem() && $item->getParentItem()->getProductType() == 'configurable') {
                continue;
            }
            if (!$item->getParentItemId() && $item->getProductType() == 'bundle') {
                continue;
            }
            $qty = (int)$item->getQtyOrdered();

            if ($qty > 0 && $qty >= $item->getQtyProduction() && $item->getQtyProduction() > 0) {
//                $data_[] = ["sku" => $item->getSku(), "qty" => $item->getQtyProduction(), 'price' => $this->query->getKosztNowy($item->getSku(), $order->getStoreId())];
                $data_[] = ["sku" => $item->getSku(), "qty" => $item->getQtyProduction()];
            }

        }
        if (count($data_)) {
            if (!$magazyn_kompletacji_id = $this->config->getMagazynKompletacji()) {
                throw new \Magento\Framework\Exception\LocalizedException(
                    __("Brak zdefiniowanego magazynu kompletacji w ustawieniach modułu wFirma")
                );
            }


            // data wygenerowania dokumentu wysyłki
            $date = $this->getShippmentDate($order->getEntityId(), $order->getCreatedAt());

            if (!$contractorData = $this->getContractorByNIPBezRezerwacji($this->config->getNipCompanyDocs($order->getStoreId()), 0)) {
                throw new \Magento\Framework\Exception\LocalizedException(
                    __("Brak zdefiniowanego konta firmy Partnera do dok. WZ.")
                );
            } else {
                $contractor_id = $contractorData['id'];
                #TODO mozna porównać dane klient z wFirma z danymi w Magento
            }
            try {

                if ($wfirma_wz = $this->warehouseDocumentData->setData($date, $date, $data_, "WZ", $magazyn_kompletacji_id, "warehouse_document_w_z", $contractor_id, 'netto', $order->getIncrementId(), $order->getIncrementId(), true)) {
                    // zapisać numery ID do sales_order
                    $this->query->updateOrderWZPartner($order->getEntityId(), $wfirma_wz['id'], $wfirma_wz['fullnumber']);
//                    file_put_contents("__dokumenty.txt", '1. WZ DLA PARTNERA: ' . PHP_EOL . print_r([$order->getEntityId(), $wfirma_wz['id'], $wfirma_wz['fullnumber']], true) . PHP_EOL, FILE_APPEND);
                    $order->addCommentToStatusHistory(
                        __('Generowanie wydania dla partnera %1.', $wfirma_wz['fullnumber'])
                    )->setIsCustomerNotified(false)->save();
                }
            } catch (\Exception $e) {
                $this->logger->critical('Function dokumentWZBezReerwacji: ' . $e->getMessage() . PHP_EOL . print_r($data_, true));


                $curent_date = date("Y-m-d");
                if($rw__ = $this->warehouseDocumentData->setData($curent_date, $curent_date, $data_, "RW", $magazyn_kompletacji_id, "warehouse_document_r_w", null, null, $order->getIncrementId(), $order->getIncrementId(), true, $order->getStoreId())){
                    $this->warehouseDocumentData->setData($date, $date, $data_, "PW", $magazyn_kompletacji_id, "warehouse_document_p_w", null, null, $order->getIncrementId(), $order->getIncrementId(), true, $order->getStoreId());
                }else{
                    $this->query->updateErrOrderWZ($order->getEntityId());
                }



//                throw new \Magento\Framework\Exception\LocalizedException(
//                    __('Function dokumentWZBezReerwacji: ' . $e->getMessage() . PHP_EOL . print_r($data_, true))
//                );
            }
        }
    }

    private function dokumentPZ($order)
    {
        if ($order->getWfirmaPzId()) return; // jeśli już wystawiono PZ to pomiń
        if (!$this->isSendedOrder($order)) return; // jeśli nie jest zakończona produkcja

        $data_ = [];
        // Przygotowanie danych
        foreach ($order->getAllItems() as $item) {
            if ($item->getParentItemId() && $item->getParentItem() && $item->getParentItem()->getProductType() == 'configurable') {
                continue;
            }
            if (!$item->getParentItemId() && $item->getProductType() == 'bundle') {
                continue;
            }


            $qty = (int)$item->getQtyOrdered();
            if ($qty > 0 && $qty >= $item->getQtyProduction() && $item->getQtyProduction() > 0) {
                $data_[] = ["sku" => $item->getSku(), "qty" => $item->getQtyProduction(), 'price' => $this->query->getKosztNowy($item->getSku(), $order->getStoreId())];
            }
        }
        if (count($data_)) {
            if (!$magazyn_kompletacji_id = $this->config->getMagazynKompletacji($order->getStoreId())) {
                throw new \Magento\Framework\Exception\LocalizedException(
                    __("Brak zdefiniowanego magazynu kompletacji w ustawieniach modułu wFirma")
                );
            }


            // data wygenerowania dokumentu wysyłki
            $date = $this->getShippmentDate($order->getEntityId(), $order->getCreatedAt());

//            if (!$contractorData = $this->getContractorByEmailBezRezerwacji($this->config->getEmailCompanyDocs(0), $order->getStoreId())) {}
            if (!$contractorData = $this->getContractorByNipBezRezerwacji($this->config->getNipCompanyDocs(0), $order->getStoreId())) {
                throw new \Magento\Framework\Exception\LocalizedException(
                    __("Brak zdefiniowanego konta firmy Partnera do dok. WZ lub PZ ")
                );
            } else {
                $contractor_id = $contractorData['id'];
                #TODO mozna porównać dane klient z wFirma z danymi w Magento
            }
            try {

                if ($wfirma_pz = $this->warehouseDocumentData->setData($date, $date, $data_, "PZ", $magazyn_kompletacji_id, "warehouse_document_p_z", $contractor_id, 'netto', $order->getIncrementId(), $order->getIncrementId(), true, $order->getStoreId())) {
                    // zapisać numery ID do sales_order
                    $this->query->updateOrderPZ($order->getEntityId(), $wfirma_pz['id'], $wfirma_pz['fullnumber']);

                    $order->addCommentToStatusHistory(
                        __('Generowanie przyjęcia %1.', $wfirma_pz['fullnumber'])
                    )->setIsCustomerNotified(false)->save();
                }
            } catch (\Exception $e) {
                $this->logger->critical('Function dokumentPZ: ' . $e->getMessage());
                echo 'Function dokumentPZ: ' . $e->getMessage();
                throw new \Magento\Framework\Exception\LocalizedException(
                    __($e->getMessage())
                );
            }
        }
    }

    private function getContractorByEmail($order)
    {
        $condition = ['parameters' => ['conditions' => ['condition' => [
            'field' => 'email',
            'operator' => 'eq',
            'value' => $order->getCustomerEmail()
        ]]]];
        $this->contractor->setFindData($condition);
        if ($contractors = $this->contractor->findAll($order->getStoreId())) {
            foreach ($contractors as $keyContr => $contractor) {
                if (isset($contractor['contractor']['id'])) {
                    return $contractor['contractor'];
                }
            }
        } else {
            return false;
        }
    }


    private function getContractorByEmailBezRezerwacji($email, $store_id)
    {
        $condition = ['parameters' => ['conditions' => ['condition' => [
            'field' => 'email',
            'operator' => 'eq',
            'value' => $email
        ]]]];
        $this->contractor->setFindData($condition);
        if ($contractors = $this->contractor->findAll($store_id)) {
            // file_put_contents("_contractor.txt", print_r($contractors,true));
            foreach ($contractors as $keyContr => $contractor) {
                if (isset($contractor['contractor']['id'])) {
                    return $contractor['contractor'];
                }
            }
        } else {
            return false;
        }
    }

    private function getContractorByNIPBezRezerwacji($nip, $store_id)
    {
        $condition = ['parameters' => ['conditions' => ['condition' => [
            'field' => 'nip',
            'operator' => 'eq',
            'value' => $nip
        ]]]];
        $this->contractor->setFindData($condition);
        if ($contractors = $this->contractor->findAll($store_id)) {
            // file_put_contents("_contractor.txt", print_r($contractors,true));
            foreach ($contractors as $keyContr => $contractor) {
                if (isset($contractor['contractor']['id'])) {
                    return $contractor['contractor'];
                }
            }
        } else {
            return false;
        }
    }


    private function getProducent($order)
    {
        $firmaModel = $this->firma->getProducent();
        $email = ($firmaModel->getPartner()) ? $firmaModel->getPartnerEmail() : '';
        $condition = ['parameters' => ['conditions' => ['condition' => [
            'field' => 'email',
            'operator' => 'eq',
            'value' => $email
        ]]]];
        $this->contractor->setFindData($condition);
        if ($contractors = $this->contractor->findAll($order->getStoreId())) {
            // file_put_contents("_contractor.txt", print_r($contractors,true));
            foreach ($contractors as $keyContr => $contractor) {
                if (isset($contractor['contractor']['id'])) {
                    return $contractor['contractor'];
                }
            }
        } else {
            return false;
        }
    }

    public function createContractor($order)
    {

        $contractor = $this->contractor->createNew();

        $billingAddress = $order->getBillingAddress();

        $company = (trim((string)$billingAddress->getCompany()));
        $name = (($company) ? ($company . " ") : '') . $billingAddress->getFirstname() . ' ' . $billingAddress->getLastname();
        $postcode = (!empty($billingAddress->getPostcode())) ? str_replace(" ", "", $billingAddress->getPostcode()) : "00-000";
        $city = (!empty($billingAddress->getCity())) ? $billingAddress->getCity() : "-";
        $street = implode(', ', $billingAddress->getStreet());
        $email = $order->getCustomerEmail();
        $country = (!empty($billingAddress->getCountryId())) ? $billingAddress->getCountryId() : "PL";;

        $contractor->setData($name, $street, $postcode, $city, $country, $billingAddress->getVatId(), $billingAddress->getTelephone(), $email);
        $contractorId = $contractor->add(true, $order->getStoreId());
        return $contractorId;
    }

    public function getContractorData($order)
    {
        $billingAddress = $order->getBillingAddress();
        $company = (trim((string)$billingAddress->getCompany()));
        $name = (($company) ? ($company . " ") : '') . $billingAddress->getFirstname() . ' ' . $billingAddress->getLastname();
        $postcode = (!empty($billingAddress->getPostcode())) ? str_replace(" ", "", $billingAddress->getPostcode()) : "00-000";
        $city = (!empty($billingAddress->getCity())) ? $billingAddress->getCity() : "-";
        $street = implode(', ', $billingAddress->getStreet());
        $email = $order->getCustomerEmail();
        $country = (!empty($billingAddress->getCountryId())) ? $billingAddress->getCountryId() : "PL";;
        return ['name' => $name, 'postcode' => $postcode, 'city' => $city , 'street' => $street, 'email' => $email, 'country' => $country];
    }


    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName("kowal_wfirma:exportorders");
        $this->setDescription("Export Orders to wFirma as WZ");
        $this->setDefinition([
            new InputArgument(self::ORDER_ID, InputArgument::OPTIONAL, "Order ID TEST"),
            new InputOption(self::NAME_OPTION, "-f", InputOption::VALUE_NONE, "Przenieść stany do fv z datą wysyłki (RW/PW na kompletacji")
        ]);
        parent::configure();
    }
}
