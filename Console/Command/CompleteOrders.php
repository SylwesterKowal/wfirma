<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;


class CompleteOrders extends Command
{

    const ORDER_ID = "order_id";
    const NAME_OPTION = "option";

    public function __construct(
        \Psr\Log\LoggerInterface                                   $logger,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        \Kowal\WFirma\Model\CreateShipment                         $createShipment,
        \Kowal\WFirma\Model\CreateInvoice                          $createInvoice,
        \Kowal\WFirma\lib\Config                                   $config,
        \Magento\Framework\App\State                               $state
    )
    {
        $this->logger = $logger;
        $this->orderCollectionFactory = $orderCollectionFactory;
        $this->createShipment = $createShipment;
        $this->createInvoice = $createInvoice;
        $this->config = $config;
        $this->state = $state;
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(
        InputInterface  $input,
        OutputInterface $output
    )
    {
        try {
            $this->state->getAreaCode();
        } catch (\Magento\Framework\Exception\LocalizedException $ex) {
            $this->state->setAreaCode(\Magento\Framework\App\Area::AREA_ADMINHTML);
        }

        $option = $input->getOption(self::NAME_OPTION);


        if ($order_id = $input->getArgument(self::ORDER_ID)) {
            $output->writeln("TEST order_id " . $order_id);
            $this->createShipment->createShipmentForOrder($order_id);
            $this->createInvoice->execute($order_id);
        } else {

            $now = new \DateTime();
            $fromdate = $now->format('2024-01-01 00:00:00');
//            $todate=$now->format('2024-01-16 H:i:s');
//            $collection = $this->getOrdersCollection(['status' => ['eq' => $this->config->getOrderStatusDoWysylki()], 'created_at'=> ['lteq' => $todate], 'created_at' => ['gteq' => $fromdate]]);
            $collection = $this->getOrdersCollection(['status' => ['eq' => $this->config->getOrderStatusDoWysylki()], 'created_at' => ['gteq' => $fromdate]]);
            foreach ($collection as $order) {
                $this->createShipment->createShipmentForOrder($order->getEntityId());
            }

            $collection = $this->getOrdersCollection(['status' => ['in' => implode(',',$this->config->getOrderStatusDoExportuDoWFirma())], 'created_at' => ['gteq' => $fromdate]]);
            foreach ($collection as $order) {
                $this->createInvoice->execute($order->getEntityId());
            }


            /**
             * Zamówienia z kodem rabatowym 100%
             */
            $collection = $this->getOrdersCollection(['state' => ['eq' => 'new' ], 'status' => ['eq' => 'pending' ], 'created_at' => ['gteq' => $fromdate]]);
            /* join with payment table */
            $joinQuery = $collection->getSelect()
                ->join(["sop" => "sales_order_payment"],
                    'main_table.entity_id = sop.parent_id',
                    array('method')
                )
                ->where('sop.method = ?','free' );
            $freeOrdersCollection = $collection->load($joinQuery);
            foreach ($freeOrdersCollection as $order) {
                $this->createInvoice->execute($order->getEntityId(),'processing','processing');
            }
        }

        return 1;
    }

    public function getOrdersCollection(array $filters = [])
    {

        $collection = $this->orderCollectionFactory->create()
            ->addAttributeToSelect('*');

        foreach ($filters as $field => $condition) {

            $collection->addFieldToFilter($field, $condition);
        }


        return $collection;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName("kowal_wfirma:completeorders");
        $this->setDescription("Zamyka zamówienia generując Fv i  Wysyłkę");
        $this->setDefinition([
            new InputArgument(self::ORDER_ID, InputArgument::OPTIONAL, "Test order_id"),
            new InputOption(self::NAME_OPTION, "-a", InputOption::VALUE_NONE, "Option functionality")
        ]);
        parent::configure();
    }
}