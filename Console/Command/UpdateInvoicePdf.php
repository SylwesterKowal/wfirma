<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateInvoicePdf extends Command
{

    const ORDER_ID = "order_id";
    const BY_WZ_ID = "option";

    public function __construct(
        \Psr\Log\LoggerInterface                                   $logger,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        \Magento\Sales\Api\OrderRepositoryInterface                $orderRepository,
        \Magento\Framework\Filesystem\DirectoryList                $directoryList,
        \Magento\Framework\Filesystem\Io\File                      $file,
        \Kowal\WFirma\lib\InvoiceDownload                          $invoiceDownloadFactory,
        \Kowal\WFirma\lib\Config                                   $config,
        \Kowal\WFirma\lib\Query                                    $query,

    )
    {
        $this->logger = $logger;
        $this->orderCollectionFactory = $orderCollectionFactory;
        $this->orderRepository = $orderRepository;
        $this->invoiceDownloadFactory = $invoiceDownloadFactory;
        $this->config = $config;
        $this->query = $query;
        $this->directoryList = $directoryList;
        $this->file = $file;

        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(
        InputInterface  $input,
        OutputInterface $output
    )
    {
        if ($order_id = $input->getArgument(self::ORDER_ID)) {
            $order = $this->orderRepository->get($order_id);
            $this->downloadInvoicePdf($order, $order->getWfirmaWzId(), $order->getStoreId());
        } else {
            $now = new \DateTime();
            $fromdate = $now->format('2024-03-01 H:i:s');

            $collection = $this->getOrdersCollection([
                'wfirma_fv' => ['eq' => 'FV VAT'],
                'created_at' => ['gteq' => $fromdate]
            ]);

            foreach ($collection as $order) {
                $this->downloadInvoicePdf($order, $order->getWfirmaWzId(), $order->getStoreId());
            }
        }
        return 1;
    }

    public function downloadInvoicePdf($order, $wf_invoice_id, $store_id = 0)
    {
        if (!$this->config->sendInvoiceByStore($store_id)) return;

        $invoiceDownload = $this->invoiceDownloadFactory->createNew();
        $invoiceDownload->setData('invoice');
        if ($download = $invoiceDownload->download($wf_invoice_id, $store_id)) {
            $filename = $order->getIncrementId() . "_invoice.pdf";

            $filepath = $this->getFileName($filename);

            @file_put_contents($filepath, $download);

            // $this->query->setInvoicePdf($this->setInvoicePdfUrl($filename), $this->invoice->getId());
        }
    }

    public function getFileName($filename)
    {
        $this->var = $this->directoryList->getPath('var');

        if (!file_exists($this->var . DIRECTORY_SEPARATOR . 'invoices')) {
            $this->file->mkdir($this->var . DIRECTORY_SEPARATOR . 'invoices', 0775);
        }
        return $this->var . DIRECTORY_SEPARATOR . 'invoices' . DIRECTORY_SEPARATOR . $filename;
    }

    protected function getOrdersCollection(array $filters = [])
    {
        $collection = $this->orderCollectionFactory->create()
            ->addAttributeToSelect('*');

        foreach ($filters as $field => $condition) {

            $collection->addFieldToFilter($field, $condition);
        }

        return $collection;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName("kowal_wfirma:updateinvoicepdf");
        $this->setDescription("Update Wz");
        $this->setDefinition([
            new InputArgument(self::ORDER_ID, InputArgument::OPTIONAL, "Order ID"),
            new InputOption(self::BY_WZ_ID, "-w", InputOption::VALUE_NONE, "Wyszukiwanie wg WZ z wFirma")
        ]);
        parent::configure();
    }
}