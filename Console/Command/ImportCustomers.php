<?php
/**
 * Copyright ©  kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ImportCustomers extends Command
{

    const NAME_ARGUMENT = "name";
    const NAME_OPTION = "option";

    public function __construct(
        \Kowal\WFirma\lib\Contractor $contractor,
        \Kowal\WFirma\lib\Config $config,
        \Magento\Customer\Model\Customer $customer,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Customer\Model\AddressFactory $addressFactory,
        \Magento\Framework\Encryption\EncryptorInterface $encryptor,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository

    )
    {
        try {
            $this->contractor = $contractor;
            $this->customer = $customer;
            $this->customerFactory = $customerFactory;
            $this->addressFactory = $addressFactory;
            $this->config = $config;
            $this->encryptor = $encryptor;
            $this->customerRepository = $customerRepository;
            parent::__construct();
        } catch (\Exception $ex) {
            echo $ex->getMessage();
        }

    }

    /**
     * {@inheritdoc}
     */
    protected function execute(
        InputInterface $input,
        OutputInterface $output
    )
    {
        $name = $input->getArgument(self::NAME_ARGUMENT);
        $option = $input->getOption(self::NAME_OPTION);
        $output->writeln("Hello " . $name);

        // przykłąd 1
//        $condition = ['parameters' => ['conditions' => ['condition' => [
//            'field' => 'email',
//            'operator' => 'eq',
//            'value' => 'bwa@aaaa24.eu'
//        ]]]];
//        $this->contractor->setFindData($condition);

        try {
            $limit = 20;
            $condition = ['parameters' => ['page' => 1, 'limit' => $limit]];
            $this->contractor->setFindData($condition);

            if ($contractors = $this->contractor->findAll()) {
                $pages = round((int)$contractors['parameters']['total'] / $limit, 0) + 1;
                echo "PAGE 1\n";
                foreach ($contractors as $keyContr => $contractor) {
                    $this->checkEmail($contractor);

                }
                for ($i = 2; $i <= $pages; $i++) {
                    $condition = ['parameters' => ['page' => $i, 'limit' => $limit]];
                    $this->contractor->setFindData($condition);
                    if ($contractors = $this->contractor->findAll()) {
                        echo "PAGE $i\n";
                        foreach ($contractors as $keyContr => $contractor) {
                            if (isset($contractor['contractor']['email']) && !empty($contractor['contractor']['email'])) {
                                $this->checkEmail($contractor);
                            }
                        }
                    }
                }
            }
            return true;
        } catch (\Exception $ex) {
            echo $ex->getMessage();
            return true;
        }
    }

    public function customerExists($email, $websiteId = null)
    {
        $customer = $this->customer;
        if ($websiteId) {
            $customer->setWebsiteId($websiteId);
        }
        $customer->loadByEmail($email);
        if ($customer->getId()) {
            return true;
        }

        return false;
    }

    private function checkEmail($contractor)
    {
        try {
            if (isset($contractor['contractor']['email']) && !empty($contractor['contractor']['email'])) {
                if ($websitesIds = explode(",", $this->config->getWebsitesIds())) {
                    if (is_array($websitesIds)) {
                        $emails = explode(",", $contractor['contractor']['email']);
                        echo $contractor['contractor']['email'] . PHP_EOL;
                        if (isset($emails[0])) {
                            foreach ($websitesIds as $website_id) {
                                if (!$this->customerExists($emails[0], $website_id)) {
                                    $this->saveCustomer($website_id, $contractor);
                                }
                            }
                        }
                    }
                }
            }
        } catch (\Exception $ex) {
            echo $ex->getMessage();
        }
    }

    private function saveCustomer($websiteId, $contractor)
    {
        $name = $contractor['contractor']['name'];
        $name_ = explode(" ", $contractor['contractor']['name']);
        $first_name = (isset($name_[0])) ? $name_[0] : " ";
        $last_name = (isset($name_[1])) ? $name_[1] : " ";
        $nip = $contractor['contractor']['nip'];
        $street = $contractor['contractor']['street'];
        $zip = $contractor['contractor']['zip'];
        $city = $contractor['contractor']['city'];
        $country = $contractor['contractor']['country'];

        $different_contact_address = $contractor['contractor']['different_contact_address'];
        $contact_name = $contractor['contractor']['contact_name'];
        $name__ = explode(" ", $contact_name);
        $contact_first_name = (isset($name__[0])) ? $name__[0] : " ";
        $contact_last_name = (isset($name__[1])) ? $name__[1] : " ";
        $contact_street = $contractor['contractor']['contact_street'];
        $contact_zip = $contractor['contractor']['contact_zip'];
        $contact_city = $contractor['contractor']['contact_city'];
        $contact_country = $contractor['contractor']['contact_country'];
        $contact_person = $contractor['contractor']['contact_person'];

        $phone = (!empty($contractor['contractor']['phone'])) ? $contractor['contractor']['phone'] : '+48 ';
        $emails = explode(",", $contractor['contractor']['email']);
        $email = $emails[0];


        $customer = $this->customerFactory->create();
        $customer->setWebsiteId($websiteId);
        $customer->setEmail($email);
        $customer->setFirstname($first_name);
        $customer->setLastname($last_name);
        $customer->save();


        $address = $this->addressFactory->create();
        if (!$different_contact_address) {
            $address->setCustomerId($customer->getId())
                ->setFirstname($first_name)
                ->setLastname($last_name)
                ->setCountryId($country)
                ->setPostcode($zip)
                ->setCity($city)
                ->setTelephone($phone)
                ->setFax('')
                ->setCompany($name)
                ->setVatId($nip)
                ->setStreet($street)
                ->setIsDefaultBilling('1')
                ->setIsDefaultShipping('1')
                ->setSaveInAddressBook('1');

            $address->save();
        } else {
            $address->setCustomerId($customer->getId())
                ->setFirstname($first_name)
                ->setLastname($last_name)
                ->setCountryId($country)
                ->setPostcode($zip)
                ->setCity($city)
                ->setTelephone($phone)
                ->setFax('')
                ->setCompany($name)
                ->setVatId($nip)
                ->setStreet($street)
                ->setIsDefaultBilling('1')
                ->setIsDefaultShipping('0')
                ->setSaveInAddressBook('1');

            $address->save();

            $s_address = $this->addressFactory->create();
            $s_address->setCustomerId($customer->getId())
                ->setFirstname($contact_first_name)
                ->setLastname($contact_last_name)
                ->setCountryId($contact_country)
                ->setPostcode($contact_zip)
                ->setCity($contact_city)
                ->setTelephone($phone)
                ->setFax('')
                ->setCompany($contact_name)
                ->setVatId($nip)
                ->setStreet($contact_street)
                ->setIsDefaultBilling('0')
                ->setIsDefaultShipping('1')
                ->setSaveInAddressBook('1');

            $s_address->save();
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName("kowal_wfirma:importcustomers");
        $this->setDescription("Import Customers");
        $this->setDefinition([
            new InputArgument(self::NAME_ARGUMENT, InputArgument::OPTIONAL, "Name"),
            new InputOption(self::NAME_OPTION, "-a", InputOption::VALUE_NONE, "Option functionality")
        ]);
        parent::configure();
    }
}
