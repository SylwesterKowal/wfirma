<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class DeleteDocs extends Command
{

    const STORE_ID = "store_id";
    const TEST = "option";

    private $arrayDocs = [];
    private $store_id = 0;


    public function __construct(
        \Psr\Log\LoggerInterface                                   $logger,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        \Magento\Sales\Api\OrderRepositoryInterface                $orderRepository,
        \Kowal\WFirma\Helper\WarehouseDocumentData                 $warehouseDocumentData,

        \Kowal\WFirma\Helper\Validate                              $validate,
        \Kowal\WFirma\Helper\Firma                                 $firma,
        \Kowal\WFirma\lib\Config                                   $config,
        \Kowal\WFirma\lib\Query                                    $query,
        \Kowal\WFirma\lib\Contractor                               $contractor,
        \Kowal\WFirma\lib\WarehouseDocument                        $warehouseDocument,
        \Kowal\Email\Helper\Send                                   $send
    )
    {
        $this->logger = $logger;
        $this->orderCollectionFactory = $orderCollectionFactory;
        $this->orderRepository = $orderRepository;
        $this->warehouseDocumentData = $warehouseDocumentData;
        $this->config = $config;
        $this->query = $query;
        $this->validate = $validate;
        $this->firma = $firma;
        $this->contractor = $contractor;
        $this->warehouseDocument = $warehouseDocument;
        $this->send = $send;


        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(
        InputInterface  $input,
        OutputInterface $output
    )
    {

        if ($this->store_id = $input->getArgument(self::STORE_ID)) {

        } else {
            $this->store_id = 0;
        }

        // Path to the CSV file
        $filename = '/var/www/virtual/smmash.pl/htdocs/var/delete_wz_smm_test.csv';

// Open the file for reading
        if (($handle = fopen($filename, "r")) !== FALSE) {
            // Początek pomiaru czasu
            $start = microtime(true);

            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {

                $store_id = $data[0];
                $wfirma_doc_id = $data[1];
                $magazyn_kompletacji = $data[2];

                $doc_name = 'warehouse_document_w_z';
                $this->new_doc = $this->warehouseDocument->createNew();
                $this->new_doc->setActionName($doc_name);
                $this->new_doc->setAditionalParameters("&warehouse_id=" . $magazyn_kompletacji);
                $respons = $this->new_doc->delete($wfirma_doc_id, $doc_name, $this->store_id);
                $this->logger->critical('DELETE WZ: ' . $respons);
            }
            $end = microtime(true);
            $time = $end - $start;
            $output->writeln("Czas wykonania skryptu: " . $time . " sekund");
            // Close the handle
            fclose($handle);
        }


        return 1;
    }


    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName("kowal_wfirma:deletedocs");
        $this->setDescription("Delete Docs from WFirma");
        $this->setDefinition([
            new InputArgument(self::STORE_ID, InputArgument::OPTIONAL, "STORE ID"),
            new InputOption(self::TEST, "-t", InputOption::VALUE_NONE, "Test")
        ]);
        parent::configure();
    }
}