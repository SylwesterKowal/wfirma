<?php
/**
 * Copyright ©  kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ImportProducts extends Command
{

    const SKU_ARGUMENT = "sku";
    const NAME_OPTION = "option";

    private $stany_csv = [];

    public function __construct(
        \Kowal\WFirma\lib\Good                                       $good,
        \Kowal\WFirma\lib\Config                                     $config,
        \Magento\Framework\App\State                                 $state,
        \Magento\Catalog\Model\ProductFactory                        $productFactory,
        \Kowal\WFirma\Model\ResourceModel\Magazyny\CollectionFactory $magazynyCollection,
        \Magento\InventoryApi\Api\Data\SourceItemInterfaceFactory    $sourceItemFactory,
        \Magento\InventoryApi\Api\SourceItemsSaveInterface           $sourceItemsSaveInterface
    )
    {
        try {
            $this->good = $good;
            $this->state = $state;
            $this->productFactory = $productFactory;
            $this->config = $config;
            $this->magazynyMapowanie = $magazynyCollection;
            $this->sourceItemFactory = $sourceItemFactory;
            $this->sourceItemsSaveInterface = $sourceItemsSaveInterface;

            parent::__construct();
        } catch (\Exception $ex) {
            echo $ex->getMessage();
        }

    }

    /**
     * {@inheritdoc}
     */
    protected function execute(
        InputInterface  $input,
        OutputInterface $output
    )
    {
        $this->state->setAreaCode(\Magento\Framework\App\Area::AREA_ADMINHTML); // or \Magento\Framework\App\Area::AREA_ADMINHTML, AREA_FRONTEND depending on your needs

        $test_sku = $input->getArgument(self::SKU_ARGUMENT);
        $option = $input->getOption(self::NAME_OPTION);
        if (!empty($test_sku)) $output->writeln("DEMO SKU: " . $test_sku);

        $this->mapowanieMagazynow = $this->getMapowanieMagazynow();
        $this->aktywneMagazynywMagento = explode(",",$this->config->getAllActiveMagentoSources());


        $limit = 20;
        $condition = ['parameters' => ['page' => 1, 'limit' => $limit]];
        $this->good->setFindData($condition);

        if ($products = $this->good->findAll()) {
            $pages = round((int)$products['parameters']['total'] / $limit, 0);
            echo "PAGE 1\n";
            foreach ($products as $keyContr => $product) {
                $this->saveToCsv($product);
                $this->isProductExist($product, $test_sku);
            }
            for ($i = 2; $i <= $pages; $i++) {
                $condition = ['parameters' => ['page' => $i, 'limit' => $limit]];
                $this->good->setFindData($condition);
                if ($products = $this->good->findAll()) {
                    echo "PAGE $i\n";
                    foreach ($products as $keyContr => $product) {
                        $this->saveToCsv($product);
                        $this->isProductExist($product, $test_sku);
                    }
                }
            }

        }


        $this->saveArrayToCsv();


        return 1;
    }

    private function saveArrayToCsv()
    {
        $fp = fopen('__stany_magazynowe.csv', 'w');
        foreach ($this->stany_csv as $fields) {
            fputcsv($fp, $fields);
        }
        fclose($fp);
    }

    private function getMapowanieMagazynow()
    {
        $magazyny = [];
        $magazynyMapowanie = $this->magazynyMapowanie->create();
        foreach ($magazynyMapowanie as $mapowanie) {
            $magazyny_wfirma = explode(",", $mapowanie['mag_wfirma']);
            foreach ($magazyny_wfirma as $mag_wf) {
                $magazyny[$mag_wf] = $mapowanie['mag_magento'];
            }
        }
        return $magazyny;
    }


    private function isProductExist($product, $test_sku = null)
    {
        if (isset($product['good']['code']) && !empty($product['good']['code'])) {

            if (!empty($test_sku) && $test_sku != $product['good']['code']) return false;

            $product_ = $this->productFactory->create();
            if ($product_id = $product_->getIdBySku($product['good']['code'])) {
                $product_->load($product_id);
                $this->updateProduct($product_, $product, $test_sku);
                return true;
            } else {
                $this->addProduct($product);
                return false;
            }
        }
    }

    private function saveToCsv($product){
        if (isset($product['good']['warehouse_good_parcels']) && is_array($product['good']['warehouse_good_parcels'])) {
            $sku = $product['good']['code'];
            foreach ($product['good']['warehouse_good_parcels'] as $wf_mag) {
                $wf_mag_id = $wf_mag['warehouse_good_parcel']['warehouse']['id'];
                $wf_mag_stock = (float)$wf_mag['warehouse_good_parcel']['count'];

                $this->stany_csv[] = ['sku' => $sku,'kod_magazynu' =>$wf_mag_id,'stan_magazynowy'=> $wf_mag_stock];
            }
        }
        return $this;
    }

    private function addProduct($product)
    {

        if (!$this->config->isEnableAddNewProduct()) return;

        echo "add " . $product['good']['code'] . " / " . $product['good']['name'] . "\n";
        $name = $product['good']['name'];
        $description = $product['good']['description'];
        $sku = $product['good']['code'];
        $netto = $this->getDefaultPrice($product, 'netto');
        $brutto = $this->getDefaultPrice($product, 'brutto');
        $count = $product['good']['count'];

        $_product = $this->productFactory->create();
        $_product->setName($name);
        $_product->setDescription($description);
        $_product->setTypeId('simple');
        $_product->setAttributeSetId(4);
        $_product->setSku($sku);
        $_product->setWebsiteIds(explode(",", $this->config->getWebsitesIds()));
        $_product->setVisibility(4);
        $_product->setPrice((($this->config->checkCatalogPriceIncludingTax(0)) ? $brutto : $netto));
//        $_product->setImage('/simpeproduct/test.jpg');
//        $_product->setSmallImage('/simpeproduct/test.jpg');
//        $_product->setThumbnail('/simpeproduct/test.jpg');
        $_product->setStockData(array(
                'use_config_manage_stock' => 0, //'Use config settings' checkbox
                'manage_stock' => 1, //manage stock
                'min_sale_qty' => 1, //Minimum Qty Allowed in Shopping Cart
                'max_sale_qty' => 1000, //Maximum Qty Allowed in Shopping Cart
                'is_in_stock' => (($count > 0) ? 1 : 0), //Stock Availability
                'qty' => $count //qty
            )
        );

        $_product->save();
    }

    private function getDefaultPrice($product, $type = 'netto')
    {
        if (isset($product['good']['good_prices'])) {
            if (is_array($product['good']['good_prices'])) {
                foreach ($product['good']['good_prices'] as $good_price) {
                    if ($good_price['good_price']['is_default'] == 1) {
                        return $good_price['good_price'][$type];
                    }
                }
            }
        } else {
            return $product['good'][$type];
        }
    }

    private function updateProduct($product_, $product, $test_sku = null)
    {
        if (empty($test_sku)) {
            if (!$product_->getUpdateFromWfirma()) return; // nie aktualizujemy produktów w Magento
        }
        echo "update " . $product['good']['code'] . "\n";

        $netto = $this->getDefaultPrice($product, 'netto');
        $brutto = $this->getDefaultPrice($product, 'brutto');
        $count = $product['good']['count'];

        if ($this->config->isUpdatePriceEnabel()) $product_->setPrice((($this->config->checkCatalogPriceIncludingTax()) ? $brutto : $netto));
        if ($this->config->isUpdateStockEnabel()) {
            if (!$this->config->isMultiSourceEnabled()) {

                $product_->setStockData(array(
                        'is_in_stock' => (($count > 0) ? 1 : 0), //Stock Availability
                        'qty' => $count //qty
                    )
                );
            }
        }
        if ($this->config->isUpdatePriceEnabel() || $this->config->isUpdateStockEnabel()) {
            $product_->save();
        }
        if ($this->config->isMultiSourceEnabled() && $this->config->isUpdateStockEnabel()){
            $this->saveStock($product);
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName("kowal_wfirma:importproducts");
        $this->setDescription("Import Products");
        $this->setDefinition([
            new InputArgument(self::SKU_ARGUMENT, InputArgument::OPTIONAL, "SKU"),
            new InputOption(self::NAME_OPTION, "-a", InputOption::VALUE_NONE, "Option functionality")
        ]);
        parent::configure();
    }

    private function saveStock($product)
    {
        $stany_magazynowe = [];
        if (is_array($product['good']['warehouse_good_parcels'])) {
            foreach ($product['good']['warehouse_good_parcels'] as $wf_mag) {
                $wf_mag_id = $wf_mag['warehouse_good_parcel']['warehouse']['id'];
                $wf_mag_stock = (float)$wf_mag['warehouse_good_parcel']['count'];
                if (array_key_exists($wf_mag_id, $this->mapowanieMagazynow)) {
                    $magazyn_magento = $this->mapowanieMagazynow[$wf_mag_id];
                    $stany_magazynowe[$magazyn_magento] = (isset($stany_magazynowe[$magazyn_magento])) ?  (float)$stany_magazynowe[$magazyn_magento] + $wf_mag_stock : $wf_mag_stock;
                }else{
                    echo "BRAK MAPOWANIA MAGAZYNU wFirma: ".$wf_mag_id;
                }
            }
        }
        echo print_r($stany_magazynowe,true);
        try {
            if(is_array($stany_magazynowe) && is_array($this->aktywneMagazynywMagento)) {

                foreach ($this->aktywneMagazynywMagento as $mag_mag){
                    if(!array_key_exists($mag_mag,$stany_magazynowe)){
                        $stany_magazynowe[$mag_mag] = 0; // ustawiamy stan zerowy na magazynie do którego nie cnie ma w wFirma, ale jest aktywny w Magento. (konfiguracja modułu wFirma)
                    }
                }
                foreach ($stany_magazynowe as $mag => $ilosc) {
                    $sourceItem = $this->sourceItemFactory->create();
                    $sourceItem->setSourceCode($mag);
                    $sourceItem->setSku($product['good']['code']);
                    $sourceItem->setQuantity($ilosc);
                    $sourceItem->setStatus(1);
                    $this->sourceItemsSaveInterface->execute([$sourceItem]);
                }
            }
        } catch (\Exception $e) {
            // display error message
            echo $e->getMessage();
        }
    }
}
