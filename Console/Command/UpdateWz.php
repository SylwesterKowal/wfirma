<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateWz extends Command
{

    const ORDER_ID = "order_id";
    const BY_WZ_ID = "option";
    const BY_FV_PROF = "proformy";

    public function __construct(
        \Psr\Log\LoggerInterface                                   $logger,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        \Magento\Sales\Api\OrderRepositoryInterface                $orderRepository,
        \Kowal\WFirma\Helper\WarehouseDocumentData                 $warehouseDocumentData,

        \Kowal\WFirma\Helper\Validate                              $validate,
        \Kowal\WFirma\Helper\Firma                                 $firma,
        \Kowal\WFirma\lib\Config                                   $config,
        \Kowal\WFirma\lib\Query                                    $query,
        \Kowal\WFirma\lib\Contractor                               $contractor,
        \Kowal\WFirma\lib\WarehouseDocument                        $warehouseDocument,
        \Kowal\WFirma\lib\Invoice                                  $invoiceDocument,
        \Kowal\Email\Helper\Send                                   $send
    )
    {
        $this->logger = $logger;
        $this->orderCollectionFactory = $orderCollectionFactory;
        $this->orderRepository = $orderRepository;
        $this->warehouseDocumentData = $warehouseDocumentData;
        $this->config = $config;
        $this->query = $query;
        $this->validate = $validate;
        $this->firma = $firma;
        $this->contractor = $contractor;
        $this->warehouseDocument = $warehouseDocument;
        $this->invoiceDocument = $invoiceDocument;
        $this->send = $send;


        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(
        InputInterface  $input,
        OutputInterface $output
    )
    {
        if ($option = $input->getOption(self::BY_WZ_ID)) {
            // Odczytujemy dokumenty WZ z wFirma i aktualizujemy


            $this->wzDoc = $this->warehouseDocument->createNew();
            $limit = 20;
            $condition = ['parameters' => ['page' => 1, 'limit' => $limit]];
            $this->wzDoc->setFindData($condition);

            if (!$magazyn_kompletacji_id = $this->config->getMagazynKompletacji(0)) {
                throw new \Magento\Framework\Exception\LocalizedException(
                    __("Brak zdefiniowanego magazynu kompletacji w ustawieniach modułu wFirma")
                );
            }

            if ($docsWz = $this->wzDoc->findAll(0)) {
                $pages = round((int)$docsWz['parameters']['total'] / $limit, 0);
                echo "PAGE 1\n";
                foreach ($docsWz as $keyContr => $wz) {
                    if (isset($wz['warehouse_document']['description']) && $wz['warehouse_document']['invoice']['id'] == 0) {
                        $this->updateWzFromwFrima($wz, $magazyn_kompletacji_id);
                    }

                }
                for ($i = 2; $i <= $pages; $i++) {
                    $condition = ['parameters' => ['page' => $i, 'limit' => $limit]];
                    $this->wzDoc->setFindData($condition);
                    if ($docsWz = $this->wzDoc->findAll(0)) {

                        echo "PAGE $i\n";
                        foreach ($docsWz as $keyContr => $wz) {
                            if (isset($wz['warehouse_document']['description']) && $wz['warehouse_document']['invoice']['id'] == 0) {
                                $this->updateWzFromwFrima($wz, $magazyn_kompletacji_id);
                            }
                        }
                    }
                }
            }

        } else if ($option = $input->getOption(self::BY_FV_PROF)) {
            // Odczytujemy dokumenty PROF z wFirma i aktualizujemy


            $this->invoices = $this->invoiceDocument->createNew();
            $limit = 20;
            $condition = ['parameters' => [
                'page' => 1,
                'limit' => $limit,
                'conditions' => [
                    'condition' => [
                        'field' => 'type',
                        'operator' => 'eq',
                        'value' => 'proforma'
                    ]
                ]]];
            $this->invoices->setFindData($condition);

            if (!$magazyn_kompletacji_id = $this->config->getMagazynKompletacji(0)) {
                throw new \Magento\Framework\Exception\LocalizedException(
                    __("Brak zdefiniowanego magazynu kompletacji w ustawieniach modułu wFirma")
                );
            }

            if ($invoices = $this->invoices->findAll(0)) {
                $pages = round((int)$invoices['parameters']['total'] / $limit, 0);
                echo "PAGE 1\n";
                foreach ($invoices as $keyContr => $invoice) {
                    if (isset($invoice['warehouse_document']['description']) && $invoice['warehouse_document']['invoice']['id'] == 0) {
                        //$this->updateWzFromwFrima($invoice, $magazyn_kompletacji_id);
                    }

                }
                for ($i = 2; $i <= $pages; $i++) {
                    $condition = ['parameters' => ['page' => $i, 'limit' => $limit]];
                    $this->invoices->setFindData($condition);
                    if ($invoices = $this->invoices->findAll(0)) {

                        echo "PAGE $i\n";
                        foreach ($invoices as $keyContr => $invoice) {
                            if (isset($invoice['warehouse_document']['description']) && $invoice['warehouse_document']['invoice']['id'] == 0) {
//                                    $this->updateWzFromwFrima($invoice, $magazyn_kompletacji_id);
                            }
                        }
                    }
                }

            }

        } else {
            // Odczytujemy zamówienia z Magento i aktualizujmy WZ

            if ($order_id = $input->getArgument(self::ORDER_ID)) {
                $output->writeln("TEST order_id " . $order_id);
                $order = $this->orderRepository->get($order_id);
                $this->updateWz($order);


            } else {
                $now = new \DateTime();
                $fromdate = $now->format('2024-03-01 H:i:s');

                $collection = $this->getOrdersCollection([
                    'wfirma_wz_id' => ['null' => false, 'neq' => ''],
                    'created_at' => ['gteq' => $fromdate]
                ]);

                foreach ($collection as $order) {
                    if ($order->getBaseSubtotalInvoiced() > 0 &&
                        $order->getBaseSubtotalInvoiced() == $order->getbaseSubtotalRefunded()) continue; // refundet
                    $output->writeln($order->getIncrementId());
                    $this->updateWz($order);
                }
            }
        }
        return 1;
    }

    protected
    function updateWzFromwFrima($wz, $magazyn_kompletacji_id)
    {

        $order_increment_id = $wz['warehouse_document']['description'];
        if ($order_increment_id != '2000007582') {
            return;
        }

        file_put_contents("__wz.txt", print_r($wz, true));

        $orderData = $this->query->getOrderData($order_increment_id);

        $date = (!empty($orderData['invoice_created_at'])) ? $orderData['invoice_created_at'] : $orderData['order_created_at'];

        $uwz = [];
        foreach ($wz['warehouse_document']['warehouse_document_contents'] as $key => $item) {
            $uwz['warehouse_document']['created'] = $date;
            $uwz['warehouse_document']['warehouse_document_contents'][$key]['warehouse_document_content']['created'] = $date;
            $uwz['warehouse_document']['warehouse_document_contents'][$key]['warehouse_document_content']['id'] = $item['warehouse_document_content']['id'];
            $uwz['warehouse_document']['warehouse_document_contents'][$key]['warehouse_document_content']['price'] = $this->query->getKosztNowy($item['warehouse_document_content']['classification'], $orderData['store_id']);
        }

        $this->edit_doc = $this->warehouseDocument->createNew();
        $this->edit_doc->update($uwz);
        $this->edit_doc->setActionName('warehouse_document_w_z');
        $this->edit_doc->setAditionalParameters("&warehouse_id=" . $magazyn_kompletacji_id);
        $wz = $this->edit_doc->edit($wz['warehouse_document']['id'], 0);
        echo $wz['warehouse_document']['fullnumber'] . PHP_EOL;
    }

    protected
    function updateWz($order)
    {
        if (!$magazyn_kompletacji_id = $this->config->getMagazynKompletacji($order->getStoreId())) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __("Brak zdefiniowanego magazynu kompletacji w ustawieniach modułu wFirma")
            );
        }

        $this->get_doc = $this->warehouseDocument->createNew();
        $this->get_doc->setActionName('warehouse_document_w_z');
        $this->get_doc->setAditionalParameters("&warehouse_id=" . $magazyn_kompletacji_id);
        if ($wz = $this->get_doc->get($order->getWfirmaWzId(), $order->getStoreId())) {
            echo print_r($wz, true);
            $uwz = [];
            foreach ($wz['warehouse_document']['warehouse_document_contents'] as $key => $item) {
                $uwz['warehouse_document']['warehouse_document_contents'][$key]['warehouse_document_content']['id'] = $item['warehouse_document_content']['id'];
                $uwz['warehouse_document']['warehouse_document_contents'][$key]['warehouse_document_content']['price'] = $this->query->getKosztNowy($item['warehouse_document_content']['classification'], $order->getStoreId());
            }
            $this->edit_doc = $this->warehouseDocument->createNew();
            $this->edit_doc->update($uwz);
            $this->edit_doc->setActionName('warehouse_document_w_z');
            $this->edit_doc->setAditionalParameters("&warehouse_id=" . $magazyn_kompletacji_id);
            $wz = $this->edit_doc->edit($order->getWfirmaWzId(), $order->getStoreId());
            echo $wz['warehouse_document']['fullnumber'] . PHP_EOL;
        }


    }


    protected
    function getOrdersCollection(array $filters = [])
    {
        $collection = $this->orderCollectionFactory->create()
            ->addAttributeToSelect('*');

        foreach ($filters as $field => $condition) {

            $collection->addFieldToFilter($field, $condition);
        }

        return $collection;
    }

    /**
     * {@inheritdoc}
     */
    protected
    function configure()
    {
        $this->setName("kowal_wfirma:updatewz");
        $this->setDescription("Update Wz");
        $this->setDefinition([
            new InputArgument(self::ORDER_ID, InputArgument::OPTIONAL, "Order ID"),
            new InputOption(self::BY_WZ_ID, "-w", InputOption::VALUE_NONE, "Wyszukiwanie wg WZ z wFirma"),
            new InputOption(self::BY_FV_PROF, "-p", InputOption::VALUE_NONE, "Wyszukiwanie wg WZ z wFirma")
        ]);
        parent::configure();
    }
}