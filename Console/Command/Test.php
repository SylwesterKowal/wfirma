<?php


namespace Kowal\WFirma\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class Test extends Command
{
    const NAME_ARGUMENT = "name";
    const NAME_OPTION = "option";

    public function __construct(
        \Kowal\WFirma\lib\Contractor $contractor,
        \Kowal\WFirma\lib\Good $good,
        \Kowal\WFirma\lib\Invoice $invoice,
        \Kowal\WFirma\lib\InvoiceContent $invoiceContent,
        \Kowal\WFirma\lib\ExportOrder $exportOrder,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Magento\Sales\Api\InvoiceRepositoryInterface $invoiceRepository,
        \Magento\Framework\App\State $state
    )
    {
        try {
            $this->contractor = $contractor;
            $this->good = $good;
            $this->invoice = $invoice;
            $this->invoiceContent = $invoiceContent;
            $this->exportOrder = $exportOrder;
            $this->orderRepository = $orderRepository;
            $this->invoiceRepository = $invoiceRepository;
            $this->state = $state;
            parent::__construct();
        } catch(\Exception $ex) {
            echo $ex->getMessage();
        }

    }

    /**
     * {@inheritdoc}
     */
    protected function execute(
        InputInterface $input,
        OutputInterface $output
    ) {

        $output->writeln("Eksport zamowien do WFirma");

//        $this->good->setFindData(['parameters'=> ['conditions'=>['condition'=>[
//            'field' => 'code',
//            'operator' => 'eq',
//            'value' => '5903260227550'
//        ]]]]);
//        var_dump($this->good->findOne());die();

//        $this->good->setData('produkt','szt.', '5','4.00','10.00','23');
//        $goodId = $this->good->add();
//        $this->contractor->setData('Jan Kowalski', 'Kwiatowa 3', '12-123', 'Miasto', 'PL');
//        $contractorId = $this->contractor->add();
//        $this->invoiceContent->setData($goodId, 1);
//        $this->invoice->setData($contractorId, '2018-11-20','2018-11-20','2018-11-20',[$this->invoiceContent->getData(), $this->invoiceContent->getData()]);
//        $invoiceId = $this->invoice->add();
//        var_dump($invoiceId);

        $ids = $input->getArgument('ids');
        switch($input->getOption('is-invoice')) {
            case 'yes':
                $is_invoice= true;
                break;
            case 'no':
                $is_invoice = false;
                break;
            default:
                $output->writeln("Nieznana wartosc opcji is-invoice: " . $input->getOption('is-invoice') . " (dozwolone yes lub no)");
                break;
        }
        $csv_file = false;
//        $csv_file = '/home/meus/public_html/FV1020-2019.csv';
//        $csv_file = (string)$input->getOption('csv-file');
        foreach($ids as $id) {
            try {
                if($csv_file) {
                    if($is_invoice) {
                        $this->invoice = $this->invoiceRepository->get($id);
                        $invoiceId = $this->exportOrder->exportToFile($this->invoice, $csv_file, true);
                    } else {
                        $order = $this->orderRepository->get($id);
                        $invoiceId = $this->exportOrder->exportToFile($order, $csv_file);
                    }
                } else {
                    if($is_invoice) {
                        $this->invoice = $this->invoiceRepository->get($id);
                        $invoiceId = $this->exportOrder
                            ->setInvoiceMagento($this->invoice)
                            ->getInvoiceFromMagentoInvoice();
//                        var_dump($invoiceId);
                    } else {
                        $order = $this->orderRepository->get($id);
                        $invoiceId = $this->exportOrder->getInvoice($order);
                    }
                }


            } catch (\Exception $ex) {
                $output->writeln("Zamowienie $id : " . $ex->getMessage());
                var_dump($ex->getTraceAsString());
            }
        }
        $output->writeln("Zakonczono");
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName("kowal:wfirma");
        $this->setDescription("");
        $this->addArgument(
            'ids',
            InputArgument::IS_ARRAY,
            'Order or invoices ids'
        );

        $this->addOption(
            'is-invoice', 'i',
            InputOption::VALUE_OPTIONAL,
            'Ids are invoice ids or order ids (yes/no)',
            'no'
        );
//        $this->addOption(
//            'csv-file', 'c',
//            InputOption::VALUE_OPTIONAL,
//            'Ids are invoice ids or order ids (yes/no)',
//            'no'
//        );
        parent::configure();
    }
}

	
	