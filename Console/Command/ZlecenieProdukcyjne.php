<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;


class ZlecenieProdukcyjne extends Command
{

    const ORDER_ID = "order_id";
    const NAME_OPTION = "option";

    protected $orders;
    protected $_objectManager;

    public function __construct(
        \Psr\Log\LoggerInterface                                   $logger,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        \Kowal\WFirma\Model\CreateShipment                         $createShipment,
        \Kowal\WFirma\Model\CreateInvoice                          $createInvoice,
        \Kowal\WFirma\lib\Config                                   $config,
        \Magento\Framework\App\State                               $state,
        \Kowal\WFirma\Helper\ZpmData                               $zpmData,
        \Magento\Sales\Model\OrderFactory                          $orderFactory,
        \Kowal\WFirma\lib\Query                                    $query,
        \Kowal\WFirma\Controller\Adminhtml\Kompletacja\SaveNew     $saveNew,
        \Magento\Catalog\Model\ProductRepository                   $productRepository,
        \Magento\Framework\App\Request\DataPersistorInterface      $dataPersistor,
        \Kowal\WFirma\Helper\WarehouseDocumentData                 $warehouseDocumentData
    )
    {
        $this->logger = $logger;
        $this->orderCollectionFactory = $orderCollectionFactory;
        $this->createShipment = $createShipment;
        $this->createInvoice = $createInvoice;
        $this->config = $config;
        $this->state = $state;
        $this->zpmData = $zpmData;
        $this->orderFactory = $orderFactory;
        $this->query = $query;
        $this->saveNew = $saveNew;
        $this->productRepository = $productRepository;
        $this->dataPersistor = $dataPersistor;
        $this->warehouseDocumentData = $warehouseDocumentData;
        $this->_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(
        InputInterface  $input,
        OutputInterface $output
    )
    {
        try {
            $this->state->getAreaCode();
        } catch (\Magento\Framework\Exception\LocalizedException $ex) {
            $this->state->setAreaCode(\Magento\Framework\App\Area::AREA_ADMINHTML);
        }

        $option = $input->getOption(self::NAME_OPTION);


        if ($order_id = $input->getArgument(self::ORDER_ID)) {
            $output->writeln("TEST order_id " . $order_id);
            $collection = $this->getOrdersCollection(['status' => ['eq' => 'processing'], 'entity_id' => ['eq' => $order_id]]);
            $collection->setPageSize(1);

            $this->createZpm($collection);
        } else {

            $now = new \DateTime();
            $fromdate = $now->format('2024-01-01 00:00:00');
            $collection = $this->getOrdersCollection(['status' => ['eq' => 'processing'], 'created_at' => ['gteq' => $fromdate]]);
            $collection->setPageSize(2);

            $this->createZpm($collection);
        }
        return 1;
    }

    private function createZpm($collection){
        if ($this->orders = $this->zpmData->setData($collection)) {

            foreach ($this->orders as $order) {
                if ($order_ = $this->getOrderByIncrementId($order['increment_id'])) {
                    $this->changeOrderStatus($order_, $order['order_status_to_set']);
                }
            }

            if($zlecenie_id = $this->saveDokuments()) {

                /**
                 * Przesunięcie Rezerwacji jeśli są
                 */

                if ($rw_wfirma = $this->createRW()) {
                    if ($pw_wfirma = $this->createPW()) {

                    }
                } else {
                    $pw_wfirma = false;
                }


                $this->savewFirmaDocs($zlecenie_id, $pw_wfirma, $rw_wfirma);

                $csv_content = $this->saveArrayToCsv($this->getZlecenieProdukcyjneData($zlecenie_id));

                $modelZlecenie = $this->_objectManager->create(\Kowal\WFirma\Model\Zlecenie::class)->load($zlecenie_id);
                $modelZlecenie->setOpis($csv_content);
                $modelZlecenie->setStatus('wygenerowane');
                $modelZlecenie->save();
            }
        }
    }

    private  function saveArrayToCsv($array)
    {
//        $fp = fopen($this->path_to_file, 'w');
        $fp = fopen('php://temp', 'rw');
        foreach ($array as $fields) {
            fputcsv($fp, $fields);
        }
        rewind($fp);
        $csv = stream_get_contents($fp);
        fclose($fp);
        return $csv;
    }

    private function getZlecenieProdukcyjneData($zlecenie_id)
    {
        $data = [];
        $data_zlecenia = date('Y-m-d');
        $data[] = ['ZLECENIE PRODUKCUJNE:', $zlecenie_id, null, null, null, null];
        foreach ($this->orders as $order) {
            foreach ($order['products'] as $product) {
                if ($product['quantity_produced'] > 0) {
                    $data[] = [$data_zlecenia, $order['increment_id'], $product['sku'], $product['quantity_produced'], $product['name'], $product['parametry']];
                }
            }
        }



        $data[] = ['', '', '', '', '', ''];
        $data[] = ['--------', '---------', '----------', '--------', '---------', '----------'];
        $data[] = ['', '', '', '', '', ''];
        $data[] = ['Produkty do przesunięcia', '', null, null, null, null];
        foreach ($this->orders as $order) {
            foreach ($order['products'] as $product) {
                if ($product['quantity_reserved'] > 0) {
                    $data[] = [$data_zlecenia, $order['increment_id'], $product['sku'], $product['quantity_reserved'], $product['name'], $product['parametry']];
                }
            }
        }

        return $data;
    }

    private function saveDokuments()
    {
        $data_zlecenie = [];

        foreach ($this->orders as $order) {
            foreach ($order['products'] as $product) {
                if ($product['quantity_produced'] > 0) {
                    $data_zlecenie[] = ['order_id' => $order['increment_id'], 'sku' => $product['sku'], 'qty' => $product['quantity_produced'], 'parametry' => $product['parametry'], 'typ_zlecenia' => 'produkcja'];
                }
            }
        }

        foreach ($this->orders as $order) {
            foreach ($order['products'] as $product) {
                if ($product['quantity_reserved'] > 0) {
                    $data_zlecenie[] = ['order_id' => $order['increment_id'], 'sku' => $product['sku'], 'qty' => $product['quantity_reserved'], 'parametry' => $product['parametry'], 'typ_zlecenia' => 'przesuniecie'];
                }
            }
        }

        $zlecenie_id = $this->saveZlecenie($data_zlecenie);

        return $zlecenie_id;
    }

    private function saveZlecenie($data)
    {
        if (is_array($data)) {
            $modelZlecenie = $this->_objectManager->create(\Kowal\WFirma\Model\Zlecenie::class)->load(null);
            $dataZ = [];
            $dataZ['data_zlecenia'] = date('Y-m-d');
//            $dataZ['wf_pw_id'] = (isset($wFirmaPw['id'])) ? $wFirmaPw['id'] : "";
//            $dataZ['wf_rw_id'] = (isset($wFirmaRw['id'])) ? $wFirmaRw['id'] : "";
//            $dataZ['wf_pw_fullnumber'] = (isset($wFirmaPw['fullnumber'])) ? $wFirmaPw['fullnumber'] : "";
//            $dataZ['wf_rw_fullnumber'] = (isset($wFirmaRw['fullnumber'])) ? $wFirmaRw['fullnumber'] : "";
            $modelZlecenie->setData($dataZ);


            try {
                $modelZlecenie->save();

                // czyscimy poprzednie zapisy produkcji i rezerwacji
                $orders_to_reset = [];
                foreach ($data as $zlecenieItem) {
                    $orders_to_reset[] = $zlecenieItem['order_id'];
                }
                $this->query->resetOrderItemQty($orders_to_reset, $this->saveNew->getOrderStatusToFilter());

                foreach ($data as $zlecenieItem) {
                    $productName = "";
                    if ($this->query->checkIfSkuExists($zlecenieItem['sku'])) {
                        if ($product = $this->productRepository->get($zlecenieItem['sku'])) {
                            $productName = $product->getNazwaBazowa();
                        }
                    }
                    $modelItems = $this->_objectManager->create(\Kowal\WFirma\Model\ZlecenieItems::class)->load(null);
                    $dataItens = [
                        'zlecenie_id' => $modelZlecenie->getId(),
                        'sku' => $zlecenieItem['sku'],
                        "nazwa" => $productName,
                        'ilosc' => $zlecenieItem['qty'],
                        'order_id' => $zlecenieItem['order_id'],
                        "parametry" => $zlecenieItem['parametry'],
                        "typ_zlecenia" => $zlecenieItem['typ_zlecenia'],
                        "data_zlecenia" => $dataZ['data_zlecenia']
                    ];
                    $modelItems->setData($dataItens);
                    $modelItems->save();


                    if ($zlecenieItem['typ_zlecenia'] == "produkcja") {
                        $this->query->updateOrderItemProductionQty($zlecenieItem['order_id'], $zlecenieItem['sku'], $zlecenieItem['qty'], $this->saveNew->getOrderStatusToFilter());
                    }

                    if ($zlecenieItem['typ_zlecenia'] == "przesuniecie") {

                        $this->query->updateOrderItemReservedQty($zlecenieItem['order_id'], $zlecenieItem['sku'], $zlecenieItem['qty'], $this->saveNew->getOrderStatusToFilter());
                    }
                }

                $this->dataPersistor->clear('kowal_wfirma_zlecenie');
                return $modelZlecenie->getId();

            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the Zlecenie.'));
            }
            return false;
        }else{
            return false;
        }

    }

    /**
     * $wFirmaPw = ['doc' => $rw_wfirma, 'store_id' => $store_id, 'type' => 'RW']
     * $rw_wfirma = ['id' => '999999', 'fullnumber' => 'PW 999/']
     * @param $zlecenie_id
     * @param $wFirmaPw
     * @param $wFirmaRw
     * @return void
     */
    private function savewFirmaDocs($zlecenie_id, $wFirmaPw, $wFirmaRw){
        $modelZlecenie = $this->_objectManager->create(\Kowal\WFirma\Model\Zlecenie::class)->load($zlecenie_id);

        if(is_array($wFirmaPw)){
            foreach ($wFirmaPw as $doc){
                $this->query->saveWfirmaDocsIdsFromZlecenie([
                    'zlecenie_id' => $zlecenie_id,
                    'store_id' => $doc['store_id'],
                    'type' => $doc['type'],
                    'wf_id' => $doc['doc']['id'],
                    'fullnumber' => $doc['doc']['fullnumber'],
                    'order_id' => $doc['order_id']
                ]);
                $modelZlecenie->setWfPwFullnumber( $doc['doc']['fullnumber'] );
                $modelZlecenie->setWfPwId( $doc['doc']['id'] );
            }
        }
        if(is_array($wFirmaRw)){
            foreach ($wFirmaRw as $doc){
                $this->query->saveWfirmaDocsIdsFromZlecenie([
                    'zlecenie_id' => $zlecenie_id,
                    'store_id' => $doc['store_id'],
                    'type' => $doc['type'],
                    'wf_id' => $doc['doc']['id'],
                    'fullnumber' => $doc['doc']['fullnumber'],
                    'order_id' => $doc['order_id']
                ]);
                $modelZlecenie->setWfRwFullnumber( $doc['doc']['fullnumber'] );
                $modelZlecenie->setWfRwId( $doc['doc']['id'] );
            }
        }

        $modelZlecenie->save();
    }

    /**
     * Poniważ nie mamy MM rozchdowuję RW towar dostępny na magazynie głównym, który będzie przyjęty do mag. rezerwacji
     * @param $order
     * @return bool|mixed
     */
    private function createRW()
    {

        $docs_rw = [];

        foreach ($this->orders as $order) {

//            file_put_contents("__order_.txt",print_r($order,true),FILE_APPEND);

            $store_id = (isset($order['store_id'])) ? $order['store_id'] : 0;
            if ($magazyny = $this->config->getMagazynGlownyWFirma($store_id)) {
                $magazynyArray = explode(",", $magazyny);
                if (is_array($magazynyArray)) {
                    $magazyn_id = reset($magazynyArray);
                } else {
                    $magazyn_id = $magazynyArray;
                }
                $data_ = [];
                foreach ($order['products'] as $product) {
                    if ($product['quantity_reserved'] > 0) {
                        // MM (nasze PW/RW ) - po cenach z wFirmy
//                        $data_[] = ["sku" => $product['sku'], "qty" => $product['quantity_reserved'], 'price' => $this->query->getKosztNowy($product['sku'], 0)];
                        $data_[] = ["sku" => $product['sku'], "qty" => $product['quantity_reserved']];
                    }
                }
                if (count($data_)) {
                    $date = $order['created_at'];
                    $description = $order['increment_id'];

//                    file_put_contents("__order_.txt",print_r(['magazyn_id'=>$magazyn_id,'store_id'=>$store_id,'description' => $description],true),FILE_APPEND);

                    if ($rw_wfirma = $this->warehouseDocumentData->setData($date, $date, $data_, "RW", $magazyn_id, "warehouse_document_r_w", null, null, null, $description, true, $store_id)) {
                        $docs_rw[] = ['doc' => $rw_wfirma, 'store_id' => $store_id, 'type' => 'RW', 'order_id' => $order['increment_id']];
                    }
                }
            }
        }

        return (count($docs_rw)) ? $docs_rw : false;
    }

    /**
     * Przyjęcie PW na magazyn rezerwacji towarów dostępnych na magazynie głównym. Z magazynu rezerwacji beda pobrane
     * gdy będzie przyjmowany towar z produkcji oraz z magazynu głownego i przeworzony do kompletacji zamówień
     * @param $order
     * @return false|mixed
     */
    private function createPW()
    {
        $docs_pw = [];
        foreach ($this->orders as $order) {
            $store_id = (isset($order['store_id'])) ? $order['store_id'] : 0;
            if ($magazyn_id = $this->config->getMagazynRezerwacji($store_id)) {
                $data_ = [];
                foreach ($order['products'] as $product) {
                    if ($product['quantity_reserved'] > 0) {
                        // MM (nasze PW/RW ) - po cenach z wFirmy
//                        $data_[] = ["sku" => $product['sku'], "qty" => $product['quantity_reserved'], 'price' => $this->query->getKosztNowy($product['sku'], 0)];
                        $data_[] = ["sku" => $product['sku'], "qty" => $product['quantity_reserved']];
                    }
                }

                if (count($data_)) {
                    $date = $order['created_at'];
                    $description = $order['increment_id'];
                    if ($pw_wfirma = $this->warehouseDocumentData->setData($date, $date, $data_, "PW", $magazyn_id, "warehouse_document_p_w", null, null, null, $description, true, $store_id)) {
                        $docs_pw[] = ['doc' => $pw_wfirma, 'store_id' => $store_id, 'type' => 'PW', 'order_id' => $order['increment_id']];
                    }
                }
            }
        }


        return (count($docs_pw)) ? $docs_pw : false;
    }

    public function getOrderByIncrementId($orderIncrementId)
    {
        return $this->orderFactory->create()->loadByIncrementId($orderIncrementId);
    }

    public function changeOrderStatus($order, $status)
    {

        $orderStateProcessing = \Magento\Sales\Model\Order::STATE_PROCESSING;
        $order->setState($orderStateProcessing);
        $order->setStatus($status);
        $order->save();

    }


    public function getOrdersCollection(array $filters = [])
    {

        $collection = $this->orderCollectionFactory->create()
            ->addAttributeToSelect('*');

        foreach ($filters as $field => $condition) {

            $collection->addFieldToFilter($field, $condition);
        }


        return $collection;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName("kowal_wfirma:zlecenieprodukcyjne");
        $this->setDescription("Zlecenie Produkcyjne");
        $this->setDefinition([
            new InputArgument(self::ORDER_ID, InputArgument::OPTIONAL, "Test order_id"),
            new InputOption(self::NAME_OPTION, "-a", InputOption::VALUE_NONE, "Option functionality")
        ]);
        parent::configure();
    }
}