<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ExportProducts extends Command
{

    const NAME_ARGUMENT = "name";
    const NAME_OPTION = "option";

    /**
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Kowal\WFirma\lib\ExportInvoices $exportInvoices
     */
    public function __construct(
        \Psr\Log\LoggerInterface         $logger,
        \Kowal\WFirma\lib\ExportProoducts $exportProoducts

    )
    {
        $this->logger = $logger;
        $this->exportProoducts = $exportProoducts;

        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(
        InputInterface  $input,
        OutputInterface $output
    )
    {
        $name = $input->getArgument(self::NAME_ARGUMENT);
        if($reset = $input->getOption(self::NAME_OPTION)){
            $reset = true;
        }else{
            $reset = false;
        }
        $output->writeln("Start " . $name);
        $this->exportProoducts->execute($reset);
        $output->writeln("Koniec " . $name);
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName("kowal_wfirma:exportproducts");
        $this->setDescription("Export Products do wFirma");
        $this->setDefinition([
            new InputArgument(self::NAME_ARGUMENT, InputArgument::OPTIONAL, "Name"),
            new InputOption(self::NAME_OPTION, "-r", InputOption::VALUE_NONE, "Resetuj aktualne mapowanie produktów")
        ]);
        parent::configure();
    }
}
