<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Cron;

class ExportInvoice
{

    protected $logger;
    protected $config;
    protected $exportInvoices;

    /**
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Kowal\WFirma\lib\ExportInvoices $exportInvoices
     */
    public function __construct(
        \Psr\Log\LoggerInterface         $logger,
        \Kowal\WFirma\lib\ExportInvoices $exportInvoices,
        \Kowal\WFirma\lib\Config         $config

    )
    {
        $this->logger = $logger;
        $this->config = $config;
        $this->exportInvoices = $exportInvoices;
    }

    /**
     * Execute the cron
     *
     * @return void
     */
    public function execute()
    {
        if (!$this->config->getEnabled()) return;
        $this->exportInvoices->execute();
    }

}