<?php
/**
 * Copyright © ssss All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Rewrite\Magento\Sales\Block\Order\Invoice;

class Items extends \Magento\Sales\Block\Order\Invoice\Items
{

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry                      $registry,
        \Kowal\WFirma\lib\Encrypt                        $encrypt,
        \Magento\Store\Model\StoreManagerInterface       $storeManager,
        array                                            $data = []
    )
    {
        $this->_coreRegistry = $registry;
        $this->encrypt = $encrypt;
        $this->storeManager = $storeManager;
        parent::__construct($context, $registry, $data);
    }

    public function getPrintInvoiceUrl($invoice_id)
    {
        return $this->getBaseUrl()."invoice/pdf/download/id/".$this->getLink($invoice_id);

    }

    public function getLink($invoice_id)
    {
        return $this->encrypt->encrypt($invoice_id);
    }

    public function getBaseUrl()
    {
        return $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_WEB);
    }
}