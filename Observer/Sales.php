<?php

namespace Kowal\WFirma\Observer;

class Sales implements \Magento\Framework\Event\ObserverInterface
{
    public function __construct()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->exportOrder = $objectManager->create('Kowal\WFirma\lib\ExportOrder');
        $this->directoryList = $objectManager->get('\Magento\Framework\Filesystem\DirectoryList');
        $this->config = $objectManager->get('Kowal\WFirma\lib\Config');
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        try {
            file_put_contents($this->directoryList->getPath('var') . '/tmp/___export_order_to_wfirma'.'.txt', '');
            if($this->config->getEnabled()) {
                $this->order = $observer->getEvent()->getOrder();
                $status = $this->order->getStatus();
                if ($status == 'complete' && strlen($this->order->getBillingAddress()->getVatId())) {
                    file_put_contents($this->directoryList->getPath('var') . '/tmp/___export_order_to_wfirma' . '.txt', date('d-m-Y H:i:s') . ' : Order Magento :' . $this->order->getId() . "\r\n", FILE_APPEND);

                    $invoiceId = $this->exportOrder->getInvoice($this->order);
                    file_put_contents($this->directoryList->getPath('var') . '/tmp/___export_order_to_wfirma' . '.txt', date('d-m-Y H:i:s') . ' : Invoice WFirma : ' . $invoiceId, FILE_APPEND);
                }
            }
        } catch (\Exception $ex) {
            file_put_contents($this->directoryList->getPath('var') . '/tmp/___export_order_to_wfirma'.'.txt', date('d-m-Y H:i:s') . $ex->getMessage() . "\r\n");
        }

        return $this;
    }
}