<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Observer\Backend\Sales;

class OrderShipmentSaveAfter implements \Magento\Framework\Event\ObserverInterface
{
    public function __construct(
        \Kowal\WFirma\lib\Config                                     $config,
        \Kowal\WFirma\Helper\WarehouseDocumentData                   $warehouseDocumentData,
        \Kowal\WFirma\Model\ResourceModel\Magazyny\CollectionFactory $magazynyCollection
    )
    {
//        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->config = $config;
        $this->warehouseDocumentData = $warehouseDocumentData;
        $this->magazynyMapowanie = $magazynyCollection;
    }

    /**
     * Execute observer
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(
        \Magento\Framework\Event\Observer $observer
    )
    {
        if ($this->config->getEnabledStockByRw()) {
            $shipment = $observer->getEvent()->getShipment();
            $order = $shipment->getOrder();

            // $this->createRW($shipment, $order);
            #TODO sprawdzić czy są rezerwacje i przesunąć produkty z rezerwacji do kompletacji.
            #TODO sprawdzić czt jest produkcja i przyjąc na magazyn kompletacji PW
            #TODO wystawić WZ

        }
    }

    private function createRW($shipment, $order)
    {
        if ($magazyn_id = $this->config->getMagazynKompletacji()) {
            $data_ = [];
            foreach ($shipment->getItems() as $item) {
//                if ($item->getOrderItem()->getParentItemId()) {
//                    continue;
//                }

                if ( $item->getOrderItem()->getParentItemId() && $item->getOrderItem()->getParentItem() && $item->getOrderItem()->getParentItem()->getProductType() == 'configurable') {
                    continue;
                }
                if (!$item->getOrderItem()->getParentItemId() && $item->getOrderItem()->getProductType() == 'bundle') {
                    continue;
                }

                $product_sku = $item->getSku();
                $qtyShipped = $item->getQty();
                $data_[] = ["sku" => $product_sku, "qty" => $qtyShipped];
            }



            if (count($data_)) {
                $date = date('Y-m-d');
                if ($id = $this->warehouseDocumentData->setData($date, $date, $data_, "RW", $magazyn_id, "warehouse_document_r_w", null, null, $order->getIncrementId(), $order->getIncrementId())) {
                    return $id;
                } else {
                    return false;
                }
            }
        }
        return false;
    }

    private function getDefaultMagazynwFirma()
    {
        $magazyny = [];
        $magazynyMapowanie = $this->magazynyMapowanie->create();
        foreach ($magazynyMapowanie as $mapowanie) {
            $magazyny_wfirma = explode(",", $mapowanie['mag_wfirma']);
            if ($mapowanie['mag_magento'] == 'default') {
                foreach ($magazyny_wfirma as $mag_wf) {
                    return $mag_wf;
                }
            }
        }
    }
}