<?php

namespace Kowal\WFirma\Service;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\State;

class CountriesTax
{


    /**
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Tax\Model\Calculation $taxCalculation
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Tax\Model\Calculation                     $taxCalculation
    )
    {
        $this->scopeConfig = $scopeConfig;
        $this->taxCalculation = $taxCalculation;


    }

    /**
     * @param $productTaxClassId
     * @param $countryCode
     * @param $customerTaxClassId
     * @return mixed
     */
    public function getTaxRate($productTaxClassId, $countryCode = null, $customerTaxClassId = null)
    {

        $taxType = "standard";


        return $this->getRateByCountryCode($countryCode, $taxType);

        if (is_null($customerTaxClassId)) $customerTaxClassId = $this->scopeConfig->getValue('tax/classes/default_customer_tax_class');
        if (is_null($countryCode)) $countryCode = $this->scopeConfig->getValue(\Magento\Shipping\Model\Config::XML_PATH_ORIGIN_COUNTRY_ID);

        return (int)$this->taxCalculation->getRate(
            new \Magento\Framework\DataObject(
                [
                    'country_id' => $countryCode,
                    'customer_class_id' => $customerTaxClassId,
                    'product_class_id' => $productTaxClassId
                ]
            )
        );
    }

    /**
     * @return array
     */
    public function ueCountries()
    {
        return [
            'AT' => ['standard' => 20, 'lowered' => 13],
            'BE' => ['standard' => 21, 'lowered' => 6],
            'BG' => ['standard' => 20, 'lowered' => 9],
            'HR' => ['standard' => 25, 'lowered' => 13],
            'CY' => ['standard' => 19, 'lowered' => 5],
            'CZ' => ['standard' => 21, 'lowered' => 15],
            'DK' => ['standard' => 25, 'lowered' => 25],
            'EE' => ['standard' => 20, 'lowered' => 9],
            'FI' => ['standard' => 24, 'lowered' => 14],
            'FR' => ['standard' => 20, 'lowered' => 10],
            'DE' => ['standard' => 19, 'lowered' => 7],
            'GR' => ['standard' => 24, 'lowered' => 13],
            'HU' => ['standard' => 27, 'lowered' => 18],
            'IE' => ['standard' => 23, 'lowered' => 13.50],
            'IT' => ['standard' => 22, 'lowered' => 10],
            'LV' => ['standard' => 21, 'lowered' => 12],
            'LT' => ['standard' => 21, 'lowered' => 9],
            'LU' => ['standard' => 17, 'lowered' => 8],
            'MT' => ['standard' => 18, 'lowered' => 5],
            'NL' => ['standard' => 21, 'lowered' => 9],
            'PL' => ['standard' => 23, 'lowered' => 8],
            'PT' => ['standard' => 23, 'lowered' => 13],
            'RO' => ['standard' => 19, 'lowered' => 9],
            'SK' => ['standard' => 20, 'lowered' => 10],
            'SI' => ['standard' => 22, 'lowered' => 9.5],
            'ES' => ['standard' => 21, 'lowered' => 10],
            'SE' => ['standard' => 25, 'lowered' => 12],
            'GB' => ['standard' => 20, 'lowered' => 5],
            'CH' => ['standard' => 23, 'lowered' => 13],
            'IM' => ['standard' => 23, 'lowered' => 13]
        ];
    }

    /**
     * @param $countryCode
     * @return int|mixed
     */
    public function getRateByCountryCode($countryCode, $taxType = 'standard')
    {
        $list = $this->ueCountries();
        $code = strtoupper(trim($countryCode));
        if (array_key_exists($code, $list)) {
            return (isset($list[$code][$taxType])) ? $list[$code][$taxType] : 23;
        } else {
            return 23;
        }
    }

}