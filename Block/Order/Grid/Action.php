<?php

namespace Kowal\WFirma\Block\Order\Grid;

use Magento\Sales\Model\Order;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;

/**
 *
 */
class Action extends Template
{
    /**
     * Order ID
     */
    const ORDER_ID = 'order_id';

    /**
     * @param Context $context
     * @param array $data
     */
    public function __construct(Context $context, array $data = [])
    {
        parent::__construct($context, $data);
    }

    /**
     * Return url for repay
     *
     * @param Order $order
     *
     * @return string
     */
    public function getOrderRepayUrl(Order $order)
    {
        return $this->getUrl('sales/order/repayview', [static::ORDER_ID => $order->getId()]);
    }

    /**
     * @param Order $order
     *
     * @return string
     */
    public function getViewUrl(Order $order)
    {
        return $this->getUrl('sales/order/view', [static::ORDER_ID => $order->getId()]);
    }

    /**
     * @param Order $order
     *
     * @return string
     */
    public function getReorderUrl(Order $order)
    {
        return $this->getUrl('sales/order/reorder', [static::ORDER_ID => $order->getId()]);
    }
}
