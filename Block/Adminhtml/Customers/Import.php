<?php
/**
 * Copyright ©  kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Block\Adminhtml\Customers;

class Import extends \Magento\Backend\Block\Template
{

    /**
     * Constructor
     *
     * @param \Magento\Backend\Block\Template\Context  $context
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
    }

    public function importCustomers(){
//        exec('php bin/magento kowal_wfirma:importcustomers', $out);

        return "Wykonać polecenie Conlole: php bin/magento kowal_wfirma:importcustomers";
    }
}

