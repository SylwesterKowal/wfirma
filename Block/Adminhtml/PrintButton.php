<?php

namespace Kowal\WFirma\Block\Adminhtml;

class PrintButton extends \Magento\Backend\Block\Widget\Container
{
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $coreRegistry = null;

    /**
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Sales\Api\InvoiceRepositoryInterface $invoiceRepository
     * @param \Kowal\WFirma\lib\Encrypt $encrypt
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Widget\Context         $context,
        \Magento\Framework\Registry                   $registry,
        \Magento\Sales\Api\InvoiceRepositoryInterface $invoiceRepository,
        \Kowal\WFirma\lib\Encrypt                     $encrypt,
        array                                         $data = []
    )
    {
        $this->encrypt = $encrypt;
        $this->coreRegistry = $registry;
        $this->invoiceRepository = $invoiceRepository;
        parent::__construct($context, $data);
    }

    protected function _construct()
    {
        $this->addButton(
            'wfirma_invoice_print_button',
            [
                'label' => 'Drukuj Fakturę',
                'class' => 'print-invoice',
                'onclick' => 'setLocation(\'' . $this->getPrintInvoiceUrl() . '\')'
            ]
        );
        parent::_construct();
    }

    /**
     * @return string
     */
    public function getPrintInvoiceUrl()
    {
        $this->invoice = $this->getMagentoInvoice();
        return $this->getUrl($this->setInvoicePdfUrl());
    }

    /**
     * @return integer
     */
    public function getInvoiceId()
    {
        return $this->getRequest()->getParam('invoice_id');
    }

    public function getMagentoInvoice()
    {
        $invoiceId = $this->getInvoiceId();
        return $this->invoiceRepository->get($invoiceId);
    }

    public function setInvoicePdfUrl()
    {
        $this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $storeManager = $this->objectManager->get('\Magento\Store\Model\StoreManagerInterface');
        $baseUrl = $storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_WEB);

        return $baseUrl . '/invoice/pdf/download/id/' . $this->encrypt->encrypt($this->invoice->getId());
    }
}
