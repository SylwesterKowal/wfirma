<?php

namespace Kowal\WFirma\Block\Adminhtml;

class OrderButton extends \Magento\Backend\Block\Widget\Container
{
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $coreRegistry = null;

    /**
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Kowal\WFirma\lib\Encrypt $encrypt
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Magento\Framework\Registry           $registry,
        \Kowal\WFirma\lib\Encrypt             $encrypt,
        array                                 $data = []
    )
    {
        $this->coreRegistry = $registry;
        $this->encrypt = $encrypt;
        parent::__construct($context, $data);
    }

    protected function _construct()
    {
        $this->addButton(
            'wfirma_invoice_button',
            [
                'label' => 'Wyślij do WFirma',
                'class' => 'send-to-wfirma',
                'onclick' => 'setLocation(\'' . $this->getExportWfirmaUrl() . '\')'
            ]
        );
        parent::_construct();
    }

    /**
     * @return string
     */
    public function getExportWfirmaUrl()
    {
        return $this->getUrl('kowal_wfirma/invoice/export/invoice_id/' . $this->getInvoiceId());
    }

    /**
     * @return integer
     */
    public function getInvoiceId()
    {
        return $this->getRequest()->getParam('invoice_id');
    }
}
