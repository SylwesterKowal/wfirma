<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Block\Adminhtml\Zpm;

class Mass extends \Magento\Backend\Block\Template
{
    public $formKey;
    public $magazynyWfirma;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Data\Form\FormKey $formKey
     * @param \Kowal\WFirma\Model\Config\Source\MagazynyWfirma $magazynyWfirma
     * @param \Magento\Framework\Session\SessionManagerInterface $session
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context            $context,
        \Magento\Framework\Data\Form\FormKey               $formKey,
        \Kowal\WFirma\Model\Config\Source\MagazynyWfirma   $magazynyWfirma,
        \Magento\Framework\Session\SessionManagerInterface $session,
        \Kowal\WFirma\lib\Config                           $config,
        array                                              $data = []
    )
    {
        parent::__construct($context, $data);
        $this->formKey = $formKey;
        $this->magazynyWfirma = $magazynyWfirma;
        $this->session = $session;
        $this->config = $config;
    }

    public function getOrders()
    {
        $this->session->start();
        $orders = $this->session->getZpmOrders();
//        $this->session->unsZpmOrder();
        return $orders;
    }

    public function getMagazynKompletacji()
    {
        $this->config->getMagazynKompletacji();
    }
}
