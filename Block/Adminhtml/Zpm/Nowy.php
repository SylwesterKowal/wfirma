<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Block\Adminhtml\Zpm;

class Nowy extends \Magento\Backend\Block\Template
{
    public $formKey;
    public $magazynyWfirma;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Data\Form\FormKey $formKey
     * @param \Kowal\WFirma\Model\Config\Source\MagazynyWfirma $magazynyWfirma
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context            $context,
        \Magento\Framework\Data\Form\FormKey               $formKey,
        \Kowal\WFirma\Model\Config\Source\MagazynyWfirma   $magazynyWfirma,
        array                                              $data = []
    )
    {
        parent::__construct($context, $data);
        $this->formKey = $formKey;
        $this->magazynyWfirma = $magazynyWfirma;
    }

}
