<?php
/**
 * Copyright ©  kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Block\Adminhtml\Kompletacja;

class View extends \Magento\Backend\Block\Template
{

    /**
     * Constructor
     *
     * @param \Magento\Backend\Block\Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context                              $context,
        \Magento\Framework\Registry                                          $coreRegistry,
        \Kowal\WFirma\Model\ResourceModel\KompletacjaItems\CollectionFactory $collectionFactoryKompletacjaItems,
        array                                                                $data = []
    )
    {
        $this->coreRegistry = $coreRegistry;
        $this->collectionFactoryKompletacjaItems = $collectionFactoryKompletacjaItems;
        parent::__construct($context, $data);
    }

    public function getItems()
    {
        $kompletacja_id = $this->coreRegistry->registry('kowal_wfirma_kompletacja')->getId();
        $kompletacjaCollection = $this->collectionFactoryKompletacjaItems->create();
        $kompletacjaCollection->addFieldToFilter('kompletacja_id', array('eq' => $kompletacja_id));

        return $kompletacjaCollection->addFieldToSelect('*');
    }
}

