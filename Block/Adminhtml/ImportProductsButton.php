<?php
/**
 * Copyright ©  kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Block\Adminhtml;

class ImportProductsButton extends \Magento\Backend\Block\Widget\Container
{

    /**
     * ImportButton constructor.
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Magento\Framework\Registry $registry,
        array $data = []
    ) {
        $this->coreRegistry = $registry;
        parent::__construct($context, $data);
    }

    protected function _construct()
    {
        $this->addButton(
            'wfirma_import_products_button',
            [
                'label'   => 'Import/Aktualizuj z wFrima',
                'class'   => 'import-products-from-wfirma',
                'onclick' => 'setLocation(\'' . $this->getImportProductsFromWfirmaUrl() . '\')'
            ]
        );
        parent::_construct();
    }

    public function getImportProductsFromWfirmaUrl()
    {
        return $this->getUrl('kowal_wfirma/products/import');
    }
}

