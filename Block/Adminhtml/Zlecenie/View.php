<?php
/**
 * Copyright ©  kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Block\Adminhtml\Zlecenie;

class View extends \Magento\Backend\Block\Template
{

    /**
     * Constructor
     *
     * @param \Magento\Backend\Block\Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context                           $context,
        \Magento\Framework\Registry                                       $coreRegistry,
        \Kowal\WFirma\Model\ResourceModel\ZlecenieItems\CollectionFactory $collectionFactoryZlecenieItems,
        array                                                             $data = []
    )
    {
        $this->coreRegistry = $coreRegistry;
        $this->collectionFactoryZlecenieItems = $collectionFactoryZlecenieItems;
        parent::__construct($context, $data);
    }

    public function getItems()
    {
        $zlecenie_id = $this->coreRegistry->registry('kowal_wfirma_zlecenie')->getId();
        $zlecenieCollection = $this->collectionFactoryZlecenieItems->create();
        $zlecenieCollection->addFieldToFilter('zlecenie_id', array('eq' => $zlecenie_id));

        return $zlecenieCollection->addFieldToSelect('*');
    }
}

