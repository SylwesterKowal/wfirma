<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Block\Adminhtml\Mm;

class Nowy extends \Magento\Backend\Block\Template
{
    public $formKey;
    public $magazynyWfirma;
    /**
     * Constructor
     *
     * @param \Magento\Backend\Block\Template\Context  $context
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Data\Form\FormKey    $formKey,
        \Kowal\WFirma\Model\Config\Source\MagazynyWfirma $magazynyWfirma,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->formKey = $formKey;
        $this->magazynyWfirma = $magazynyWfirma;
    }
}

