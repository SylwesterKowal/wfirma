<?php
/**
 * Copyright ©  kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Block\Adminhtml;

class ImportButton extends \Magento\Backend\Block\Widget\Container
{

    /**
     * ImportButton constructor.
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Magento\Framework\Registry $registry,
        array $data = []
    ) {
        $this->coreRegistry = $registry;
        parent::__construct($context, $data);
    }

    protected function _construct()
    {
        $this->addButton(
            'wfirma_import_customer_button',
            [
                'label'   => 'Import z wFirma',
                'class'   => 'import-from-wfirma',
                'onclick' => 'setLocation(\'' . $this->getImportFromWfirmaUrl() . '\')'
            ]
        );
        parent::_construct();
    }

    public function getImportFromWfirmaUrl()
    {
        return $this->getUrl('kowal_wfirma/customers/import');
    }
}

