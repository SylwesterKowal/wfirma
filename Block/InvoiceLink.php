<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Block;

use Kowal\WFirma\lib\Encrypt;

class InvoiceLink extends \Magento\Framework\View\Element\Template
{

    protected $encrypt;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Kowal\WFirma\lib\Encrypt                        $encrypt,
        \Magento\Store\Model\StoreManagerInterface       $storeManager,
        \Magento\Framework\View\Element\Template\Context $context,
        array                                            $data = []
    )
    {
        $this->encrypt = $encrypt;
        $this->storeManager = $storeManager;
        parent::__construct($context, $data);
    }

    /**
     * @return string
     */
    public function getLink()
    {
        return $this->encrypt->encrypt($this->getData('invoice_id'));
    }

    public function getBaseUrl()
    {

        return $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_WEB);
    }
}