<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Controller\Redirect;

class Uri extends \Magento\Framework\App\Action\Action
{
    protected $callBackUrl;
    protected $authUrl;
    protected $accessTokenUrl;
    protected $authorizationCode;
    protected $timestampToken;
    protected $refreshToken;

    public function __construct(
        \Magento\Framework\App\Action\Context          $context,
        \Magento\Framework\App\RequestInterface        $request,
        \Magento\Framework\Controller\ResultFactory    $resultFactory,
        \Kowal\WFirma\lib\Config                       $config,
        \Magento\Framework\App\Cache\TypeListInterface $cachTypeList
    )
    {
        $this->request = $request;
        $this->resultFactory = $resultFactory;
        $this->config = $config;
        $this->cachTypeList = $cachTypeList;
        parent::__construct($context);
    }

    protected function _isAllowed()
    {
        return true;
    }

    /**
     * Execute view action
     *
     * @return ResultInterface
     */
    public function execute()
    {

        /** @var Redirect $response */
        $response = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_REDIRECT);

        $authParam = $this->config->getCredentials();
        $this->callBackUrl = $authParam['CALLBACK_URL'];
        $this->authUrl = $authParam['AUTH_URL'];
        $this->accessTokenUrl = $authParam['ACCESS_TOKEN_URL'];
        $this->clientId = $authParam['CLIENT_ID'];
        $this->clientSecret = $authParam['CLIENT_SECRET'];

        if ($this->authorizationCode = $this->request->getParam('code')) {

            $this->getToken();

            $this->config->setConfig('wfirma/wfirmaapi/authorization_code', $this->authorizationCode);
            $this->config->setConfig('wfirma/wfirmaapi/access_token',$this->authorizationToken);
            $this->config->setConfig('wfirma/wfirmaapi/refresh_token',$this->refreshToken);
            $this->config->setConfig('wfirma/wfirmaapi/timestamp_token',time());
            $this->cachTypeList->cleanType('config');
            echo $this->authorizationCode;
        }

    }

    function getToken(){
        $curl = curl_init();

        $rurl = $this->accessTokenUrl."?oauth_version=2";
        $data = [
            'grant_type' => 'authorization_code',
            'code' => $this->authorizationCode,
            'redirect_uri' => $this->callBackUrl,
            'client_id' => $this->clientId,
            'client_secret' => $this->clientSecret
        ];

        $params = array(
            CURLOPT_URL =>  $rurl,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_NOBODY => false,
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "content-type: application/x-www-form-urlencoded",
                "accept: *",
                "accept-encoding: gzip, deflate",
            ),
            CURLOPT_POSTFIELDS => http_build_query($data)
        );

        curl_setopt_array($curl, $params);

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {

            echo "cURL Error #01: " . $err ;
        } else {
            $response = json_decode($response, true);

            if(array_key_exists("access_token", $response)){
                $this->timestampToken = time();
                $this->authorizationToken = $response['access_token'];
                $this->refreshToken = $response['refresh_token'];

            }
            if(array_key_exists("error", $response)) echo "cURL Error: ".$response["error_description"];

        }
    }

}

