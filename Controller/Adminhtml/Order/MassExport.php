<?php
namespace Kowal\WFirma\Controller\Adminhtml\Order;

use Magento\Backend\App\Action;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Shell;
use Kowal\WFirma\Model\CliPhpResolver;
use Magento\Ui\Component\MassAction\Filter;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory;

class MassExport extends Action
{
    protected $filter;
    protected $collectionFactory;
    protected $shell;
    protected $cliPhpResolver;

    public function __construct(
        CliPhpResolver $cliPhpResolver,
        Shell $shell,
        Action\Context $context,
        Filter $filter,
        CollectionFactory $collectionFactory
    ) {
        $this->cliPhpResolver = $cliPhpResolver;
        $this->shell = $shell;
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $collection = $this->filter->getCollection($this->collectionFactory->create());
        $messages = [];
        \Magento\Framework\App\ObjectManager::getInstance()->get(\Psr\Log\LoggerInterface::class)->debug("POLECENIE START");
        foreach ($collection as $order) {
            $entityId = $order->getEntityId();
            try {
                // Pobierz ścieżkę do PHP
                $phpPath = $this->cliPhpResolver->getExecutablePath();
                $phpPath = '/usr/bin/php8.1'; // dla smmash

                // Utwórz komendę CLI
                $command = sprintf(
                    '%s %s kowal_wfirma:exportorders %s -f > /dev/null &',
                    $phpPath,
                    BP . '/bin/magento',
                    $entityId
                );

//                \Magento\Framework\App\ObjectManager::getInstance()->get(\Psr\Log\LoggerInterface::class)->debug("POLECENIE ");

                // Wykonaj komendę CLI
                $this->shell->execute($command);

                $messages[] = 'Zamówienie ID ' . $entityId . ': Proces eksportu został uruchomiony w tle.';
            } catch (LocalizedException $e) {
                $messages[] = 'Zamówienie ID ' . $entityId . ': Wystąpił błąd - ' . $e->getMessage();
            } catch (\Exception $e) {
                $messages[] = 'Zamówienie ID ' . $entityId . ': Wystąpił nieoczekiwany błąd - ' . $e->getMessage();
            }
        }

        if (!empty($messages)) {
            foreach ($messages as $message) {
                $this->messageManager->addSuccessMessage($message);
            }
        }

        $resultRedirect = $this->resultRedirectFactory->create();
        return $resultRedirect->setPath('sales/order/index');
    }
}