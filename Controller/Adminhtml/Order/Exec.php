<?php
namespace Kowal\WFirma\Controller\Adminhtml\Order;

use Magento\Backend\App\Action;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Shell;
use Kowal\WFirma\Model\CliPhpResolver;
use Magento\Sales\Api\OrderRepositoryInterface;

class Exec extends Action
{
    protected $orderRepository;
    protected $shell;
    protected $cliPhpResolver;

    public function __construct(
        CliPhpResolver $cliPhpResolver,
        Shell $shell,
        Action\Context $context,
        OrderRepositoryInterface $orderRepository
    ) {
        $this->cliPhpResolver = $cliPhpResolver;
        $this->shell = $shell;
        $this->orderRepository = $orderRepository;
        parent::__construct($context);
    }

    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $orderId = $this->getRequest()->getParam('order_id');

        try {
            // Pobierz zamówienie na podstawie entity_id
            $order = $this->orderRepository->get($orderId);

            // Pobierz ścieżkę do PHP
            $phpPath = $this->cliPhpResolver->getExecutablePath();

            // Utwórz i wykonaj komendę CLI
            $this->shell->execute(
                $phpPath . ' %s kowal_wfirma:exportorders %s -f > /dev/null &',
                [BP . '/bin/magento', $order->getEntityId()]
            );

            $this->messageManager->addSuccessMessage(__('Proces eksportu zamówienia ID %1 został uruchomiony w tle.', $order->getEntityId()));
        } catch (LocalizedException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__('Wystąpił nieoczekiwany błąd: %1', $e->getMessage()));
        }

        return $resultRedirect->setPath('sales/order/view', ['order_id' => $orderId]);
    }
}