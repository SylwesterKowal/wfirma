<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Controller\Adminhtml\Magazyny;

use Magento\Framework\Exception\LocalizedException;

class Save extends \Magento\Backend\App\Action
{

    protected $dataPersistor;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
    ) {
        $this->dataPersistor = $dataPersistor;
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();
        if ($data) {
            $id = $this->getRequest()->getParam('magazyny_id');
        
            $model = $this->_objectManager->create(\Kowal\WFirma\Model\Magazyny::class)->load($id);
            if (!$model->getId() && $id) {
                $this->messageManager->addErrorMessage(__('This Magazyny no longer exists.'));
                return $resultRedirect->setPath('*/*/');
            }

            if(isset($data['mag_wfirma'])) $data['mag_wfirma'] = implode(",",$data['mag_wfirma']);
//            file_put_contents("/home/m2tande/public_html/pub/_warehouses.txt",print_r($data,true));
            $model->setData($data);
        
            try {
                $model->save();
                $this->messageManager->addSuccessMessage(__('You saved the Magazyny.'));
                $this->dataPersistor->clear('kowal_wfirma_magazyny');
        
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['magazyny_id' => $model->getId()]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the Magazyny.'));
            }
        
            $this->dataPersistor->set('kowal_wfirma_magazyny', $data);
            return $resultRedirect->setPath('*/*/edit', ['magazyny_id' => $this->getRequest()->getParam('magazyny_id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }
}

