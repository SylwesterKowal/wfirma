<?php


namespace Kowal\WFirma\Controller\Adminhtml\Invoice;

class Export extends \Magento\Backend\App\Action
{

    protected $resultPageFactory;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Framework\Controller\ResultFactory $resultFactory
     * @param \Magento\Framework\App\RequestInterface $request
     * @param \Magento\Sales\Api\InvoiceRepositoryInterface $invoiceRepository
     * @param \Kowal\WFirma\lib\ExportOrder $exportOrder
     * @param \Kowal\WFirma\lib\ExportInvoices $exportInvoices
     * @param \Magento\Sales\Model\Order\Email\Sender\InvoiceSender $invoiceSender
     * @param \Kowal\WFirma\lib\Encrypt $encrypt
     * @param \Kowal\WFirma\lib\Query $query
     */
    public function __construct(
        \Magento\Backend\App\Action\Context                   $context,
        \Magento\Framework\View\Result\PageFactory            $resultPageFactory,
        \Magento\Framework\Message\ManagerInterface           $messageManager,
        \Psr\Log\LoggerInterface                              $logger,
        \Magento\Framework\Controller\ResultFactory           $resultFactory,
        \Magento\Framework\App\RequestInterface               $request,
        \Magento\Sales\Api\InvoiceRepositoryInterface         $invoiceRepository,
        \Kowal\WFirma\lib\ExportOrder                         $exportOrder,
        \Kowal\WFirma\lib\ExportInvoices                      $exportInvoices,
        \Magento\Sales\Model\Order\Email\Sender\InvoiceSender $invoiceSender,
        \Kowal\WFirma\lib\Encrypt                             $encrypt,
        \Kowal\WFirma\lib\Query                               $query
    )
    {
        $this->resultPageFactory = $resultPageFactory;
        $this->messageManager = $messageManager;
        $this->logger = $logger;
        $this->resultFactory = $resultFactory;
        $this->request = $request;
        $this->invoiceRepository = $invoiceRepository;
        $this->exportOrder = $exportOrder;
        $this->exportInvoices = $exportInvoices;
        $this->invoiceSender = $invoiceSender;
        $this->encrypt = $encrypt;
        $this->query = $query;
        parent::__construct($context);
    }


    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var Redirect $response */
        $response = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_REDIRECT);
        try {
            $msg = $this->invoiceToWfirma();

            $this->messageManager->addSuccess($msg);
        } catch (LocalizedException $e) {
            $this->logger->critical($e->getMessage());
            $this->messageManager->addErrorMessage($e->getMessage());
        } catch (\Exception $e) {
            $this->logger->critical($e->getMessage());
            $this->messageManager->addErrorMessage(
                __('Błąd połączenia z wFirma') . PHP_EOL . $e->getMessage()
            );

        }
        $response->setPath($this->_redirect->getRefererUrl());
        return $response;
        // return $this->resultPageFactory->create();
    }

    public function invoiceToWfirma()
    {
        $msg = '';
        try {

            $this->invoice = $this->getMagentoInvoice();
            $this->order = $this->invoice->getOrder();

            $vatId = $this->order->getBillingAddress()->getVatId();


            $this->exportOrder->setVatCodes($this->exportInvoices->getVatCodes());

            if($invoicewFrirma = $this->exportOrder
                ->setInvoiceMagento($this->invoice)
                ->getInvoiceFromMagentoInvoice()) {

                $this->query->setExported($this->invoice->getId(), $invoicewFrirma['id']);
                $this->query->updateOrderWZ($this->order->getEntityId(), $invoicewFrirma['id'], $invoicewFrirma['fullnumber']);

                $msg = "Zakończono poprawnie, Numer faktury w WFirma: {$invoicewFrirma['fullnumber']}. <br/> 
                    <a href=\"/invoice/pdf/download/id/{$this->encrypt->encrypt($this->invoice->getId())}\"> Pobierz fakturę </a>";

                $this->invoiceSender->send($this->invoice);
            }else{
                $msg = "Nie można wystawić FV.";
            }

            return $msg;

        } catch (\Exception $ex) {
            $msg = ($ex->getMessage());
            throw new \Exception($msg);
        }

        return $msg;
    }

    public function getMagentoInvoice()
    {
        $invoiceId = $this->request->getParam('invoice_id');
        return $this->invoiceRepository->get($invoiceId);
    }
}
