<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Controller\Adminhtml\Pz;

class Save extends \Magento\Backend\App\Action
{

    protected $resultPageFactory;
    protected $jsonHelper;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Kowal\WFirma\Helper\WarehouseDocumentData $warehouseDocumentData
     */
    public function __construct(
        \Magento\Backend\App\Action\Context        $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Json\Helper\Data        $jsonHelper,
        \Psr\Log\LoggerInterface                   $logger,
        \Kowal\WFirma\Helper\WarehouseDocumentData $warehouseDocumentData,
        \Kowal\WFirma\lib\Config                   $config,
        \Kowal\WFirma\lib\Contractor               $contractor,
        \Kowal\WFirma\lib\Query                    $query
    )
    {
        $this->resultPageFactory = $resultPageFactory;
        $this->jsonHelper = $jsonHelper;
        $this->logger = $logger;
        $this->config = $config;
        $this->contractor = $contractor;
        $this->warehouseDocumentData = $warehouseDocumentData;
        $this->query = $query;
        parent::__construct($context);
    }

    protected function _isAllowed()
    {
        return true;
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        try {
            $data = $this->getRequest()->getPostValue();
            if ($data) {
                $data_ = $this->getArrayForCsv();
                $magazyn_id = $this->getRequest()->getParam('magazyn', null);
                $store_id = $this->getRequest()->getParam('store', null);
                $date = date('Y-m-d');

                if (!$contractorData = $this->getContractorByEmail($this->config->getEmailCompanyDocsPz(0), $store_id)) {
                    throw new \Magento\Framework\Exception\LocalizedException(
                        __("Brak zdefiniowanego konta firmy Partnera do dok. PZ. ")
                    );
                } else {
                    $contractor_id = $contractorData['id'];
                }

                if ($id = $this->warehouseDocumentData->setData($date, $date, $data_, "PZ", $magazyn_id, "warehouse_document_p_z", $contractor_id, null, null, null, false, $store_id)) {
                    return $this->jsonResponse("Dokument PZ " . $id . " dodany!");
                }
            }
            return $this->jsonResponse("Błąd dodawania dokumentu PZ");
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            return $this->jsonResponse($e->getMessage());
        } catch (\Exception $e) {
            $this->logger->critical($e);
            return $this->jsonResponse($e->getMessage());
        }
    }

    private function getContractorByEmail($email, $store_id)
    {
        $condition = ['parameters' => ['conditions' => ['condition' => [
            'field' => 'email',
            'operator' => 'eq',
            'value' => $email
        ]]]];
        $this->contractor->setFindData($condition);
        if ($contractors = $this->contractor->findAll($store_id)) {
            // file_put_contents("_contractor.txt", print_r($contractors,true));
            foreach ($contractors as $keyContr => $contractor) {
                if (isset($contractor['contractor']['id'])) {
                    return $contractor['contractor'];
                }
            }
        } else {
            return false;
        }
    }

    /**
     * Create json response
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function jsonResponse($response = '')
    {
        return $this->getResponse()->representJson(
            $this->jsonHelper->jsonEncode($response)
        );
    }

    private function getArrayForCsv()
    {
        $data = $this->getRequest()->getParam('import', null);

        if ($data) {
            $data = array_filter(array_map('trim', explode(PHP_EOL, $data)));

            $data = array_map(
                function ($row) {
                    if (preg_match("/\t/", $row)) {
                        $row = explode('	', $row);
                    } else {
                        $row = explode(',', $row);
                    }
                    $row['sku'] = $row[0] ?? '';
                    unset($row[0]);

                    $row['qty'] = $row[1] ?? '';
                    unset($row[1]);

                    if (isset($row[2])) {
                        $row['price'] = $row[2] ?? '';
                        unset($row[2]);
                    }else{
                        $row['price'] = $this->query->getKosztNowy($row['sku'], $this->getRequest()->getParam('store', null));
                    }

                    return $row;
                },
                $data
            );
        }

        return $data;
    }
}

