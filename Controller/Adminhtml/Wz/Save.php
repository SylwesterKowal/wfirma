<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Controller\Adminhtml\Wz;

class Save extends \Magento\Backend\App\Action
{

    protected $resultPageFactory;
    protected $jsonHelper;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Kowal\WFirma\Helper\WarehouseDocumentData $warehouseDocumentData
     */
    public function __construct(
        \Magento\Backend\App\Action\Context        $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Json\Helper\Data        $jsonHelper,
        \Psr\Log\LoggerInterface                   $logger,
        \Kowal\WFirma\Helper\WarehouseDocumentData $warehouseDocumentData
    )
    {
        $this->resultPageFactory = $resultPageFactory;
        $this->jsonHelper = $jsonHelper;
        $this->logger = $logger;
        $this->warehouseDocumentData = $warehouseDocumentData;
        parent::__construct($context);
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        try {
            $data = $this->getRequest()->getPostValue();
            if ($data) {
                $data_ = $this->getArrayForCsv();
                $magazyn_id = $this->getRequest()->getParam('magazyn', null);
                $date = date('Y-m-d');

                if($id = $this->warehouseDocumentData->setData($date, $date, $data_, "WZ", $magazyn_id, "warehouse_document_w_z")){
                    return $this->jsonResponse("Dokument WZ ".$id." dodany!");
                }
            }
            return $this->jsonResponse("Błąd dodawania dokumentu WZ");

        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            return $this->jsonResponse($e->getMessage());
        } catch (\Exception $e) {
            $this->logger->critical($e);
            return $this->jsonResponse($e->getMessage());
        }
    }

    /**
     * Create json response
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function jsonResponse($response = '')
    {
        return $this->getResponse()->representJson(
            $this->jsonHelper->jsonEncode($response)
        );
    }

    private function getArrayForCsv()
    {
        $data = $this->getRequest()->getParam('import', null);

        if ($data) {
            $data = array_filter(array_map('trim', explode(PHP_EOL, $data)));

            $data = array_map(
                function ($row) {
                    if (preg_match("/\t/", $row)) {
                        $row = explode('	', $row);
                    } else {
                        $row = explode(',', $row);
                    }
                    $row['sku'] = $row[0] ?? '';
                    unset($row[0]);

                    $row['qty'] = $row[1] ?? '';
                    unset($row[1]);

                    return $row;
                },
                $data
            );
        }

        return $data;
    }
}

