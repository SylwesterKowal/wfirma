<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Controller\Adminhtml\Zlecenie;

class Delete extends \Kowal\WFirma\Controller\Adminhtml\Zlecenie
{

    public function __construct(
        \Magento\Backend\App\Action\Context                               $context,
        \Kowal\WFirma\Model\ResourceModel\ZlecenieItems\CollectionFactory $collectionFactoryZlecenieItems,
        \Magento\Framework\Registry                                       $coreRegistry,
        \Kowal\WFirma\lib\WarehouseDocument                               $warehouseDocument,
        \Magento\Sales\Model\OrderFactory                                 $orderFactory,
        \Kowal\WFirma\lib\Config                                          $config,
        \Kowal\WFirma\lib\Query                                           $query
    )
    {
        $this->collectionFactoryZlecenieItems = $collectionFactoryZlecenieItems;
        $this->warehouseDocument = $warehouseDocument;
        $this->orderFactory = $orderFactory;
        $this->config = $config;
        $this->query = $query;
        parent::__construct($context, $coreRegistry);
    }

    protected function _isAllowed()
    {
        return true;
    }

    /**
     * Delete action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        // check if we know what should be deleted
        $id = $this->getRequest()->getParam('zlecenie_id');
        if ($id) {
            try {
                // init model and delete
                $model = $this->_objectManager->create(\Kowal\WFirma\Model\Zlecenie::class);
                $model->load($id);

                $modelItem = $this->_objectManager->create(\Kowal\WFirma\Model\ZlecenieItems::class);
                if ($items = $this->getItems($id)) {
                    foreach ($items as $item) {
                        if ($modelItem->load($item->getZlecenieitemsId())) {
                            $order = $this->getOrderByIncrementId($modelItem->getOrderId());
                            if ($order->getStatus() != $this->config->getOrderStatusDoZleceniaProd()) {
                                $order->setStatus($this->config->getOrderStatusDoZleceniaProd());
                                $order->save();
                            }
                            $modelItem->delete();
                        }
                    }
                }

                if ($wFirmaDocs = $this->query->getWfirmaDocs($id)) {
                    foreach ($wFirmaDocs as $wFirmaDoc) {
                        switch ($wFirmaDoc['type']) {
                            case 'PW':
                                $doc_name = 'warehouse_document_p_w';
                                $this->new_doc = $this->warehouseDocument->createNew();
                                $this->new_doc->setActionName($doc_name);
                                $magazyn_rezerwacji = $this->config->getMagazynRezerwacji($wFirmaDoc['store_id']);
                                $this->new_doc->setAditionalParameters("&warehouse_id=" . $magazyn_rezerwacji);
                                $respons = $this->new_doc->delete($wFirmaDoc['wf_id'], $doc_name, $wFirmaDoc['store_id']);
                                break;
                            case 'RW':
                                $doc_name = 'warehouse_document_r_w';
                                $this->new_doc = $this->warehouseDocument->createNew();
                                $this->new_doc->setActionName('warehouse_document_r_w');
                                $magazyn_glowny = $this->config->getMagazynGlownyWFirma($wFirmaDoc['store_id']);
                                $this->new_doc->setAditionalParameters("&warehouse_id=" . $magazyn_glowny);
                                $respons = $this->new_doc->delete($wFirmaDoc['wf_id'], $doc_name, $wFirmaDoc['store_id']);
                                break;
                        }

                        sleep(1);
                    }
                    $this->query->deleteWfirmaDocs($id);
                }
//                if ($pw_id = $model->getWfPwId()) {
//                    $this->new_doc = $this->warehouseDocument->createNew();
//                    $this->new_doc->setActionName('warehouse_document_p_w');
//                    $magazyn_rezerwacji = $this->config->getMagazynRezerwacji();
//                    $this->new_doc->setAditionalParameters("&warehouse_id=".$magazyn_rezerwacji);
//                    $respons = $this->new_doc->delete($pw_id,'warehouse_document_p_w');
//
//                }
//                sleep(6);
//                if ($rw_id = $model->getWfRwId()) {
//                    $this->new_doc = $this->warehouseDocument->createNew();
//                    $this->new_doc->setActionName('warehouse_document_r_w');
//                    $magazyn_glowny = $this->config->getMagazynGlownyWFirma();
//                    $this->new_doc->setAditionalParameters("&warehouse_id=".$magazyn_glowny);
//                    $respons = $this->new_doc->delete($rw_id, 'warehouse_document_r_w');
//
//                }

                $model->delete();

                // display success message
                $this->messageManager->addSuccessMessage(__('You deleted the Zlecenie.'));
                // go to grid
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                // display error message
                $this->messageManager->addErrorMessage($e->getMessage());
                // go back to edit form
                return $resultRedirect->setPath('*/*/edit', ['zlecenie_id' => $id]);
            }
        }
        // display error message
        $this->messageManager->addErrorMessage(__('We can\'t find a Zlecenie to delete.'));
        // go to grid
        return $resultRedirect->setPath('*/*/');
    }

    public function getItems($zlecenie_id)
    {
        $zlecenieCollection = $this->collectionFactoryZlecenieItems->create();
        $zlecenieCollection->addFieldToFilter('zlecenie_id', array('eq' => $zlecenie_id));

        return $zlecenieCollection->addFieldToSelect('*');
    }

    public function getOrderByIncrementId($orderIncrementId)
    {
        return $this->orderFactory->create()->loadByIncrementId($orderIncrementId);
    }
}

