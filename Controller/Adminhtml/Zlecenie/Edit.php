<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Controller\Adminhtml\Zlecenie;

class Edit extends \Kowal\WFirma\Controller\Adminhtml\Zlecenie
{

    protected $resultPageFactory;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context              $context,
        \Magento\Framework\Registry                      $coreRegistry,
        \Magento\Framework\View\Result\PageFactory       $resultPageFactory,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
        \Magento\Framework\Controller\Result\RawFactory  $resultRawFactory
    )
    {
        $this->resultPageFactory = $resultPageFactory;
        $this->fileFactory = $fileFactory;
        $this->resultRawFactory = $resultRawFactory;
        parent::__construct($context, $coreRegistry);
    }

    protected function _isAllowed()
    {
        return true;
    }

    /**
     * Edit action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        // 1. Get ID and create model
        $id = $this->getRequest()->getParam('zlecenie_id');
        $model = $this->_objectManager->create(\Kowal\WFirma\Model\Zlecenie::class);

        // 2. Initial checking
        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->messageManager->addErrorMessage(__('This Zlecenie no longer exists.'));
                /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }

        }

        $this->fileFactory->create(
            'zlecenie_' . $id . '.csv',
            $model->getOpis(),
            \Magento\Framework\App\Filesystem\DirectoryList::MEDIA
        );
        $resultRaw = $this->resultRawFactory->create();
        return $resultRaw;

        $this->_coreRegistry->register('kowal_wfirma_zlecenie', $model);

        // 3. Build edit form
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $this->initPage($resultPage)->addBreadcrumb(
            $id ? __('Edit Zlecenie') : __('New Zlecenie'),
            $id ? __('Edit Zlecenie') : __('New Zlecenie')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Zlecenies'));
        $resultPage->getConfig()->getTitle()->prepend($model->getId() ? __('Edit Zlecenie %1', $model->getId()) : __('New Zlecenie'));
        return $resultPage;
    }
}

