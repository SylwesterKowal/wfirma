<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Controller\Adminhtml\Mm;

class Save extends \Magento\Backend\App\Action
{

    protected $resultPageFactory;
    protected $jsonHelper;

    /**
     * Constructor
     *
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     */
    public function __construct(
        \Magento\Backend\App\Action\Context        $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Json\Helper\Data        $jsonHelper,
        \Psr\Log\LoggerInterface                   $logger,
        \Kowal\WFirma\Helper\WarehouseDocumentData $warehouseDocumentData
    )
    {
        $this->resultPageFactory = $resultPageFactory;
        $this->jsonHelper = $jsonHelper;
        $this->logger = $logger;
        $this->warehouseDocumentData = $warehouseDocumentData;
        parent::__construct($context);
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        try {
            $data = $this->getRequest()->getPostValue();
            if ($data) {
                $data_ = $this->getArrayForCsv();
                $z_magazyn_id = $this->getRequest()->getParam('zmagazynu', null);
                $do_magazyn_id = $this->getRequest()->getParam('domagazynu', null);
                $date = date('Y-m-d');
                if($id_rw = $this->warehouseDocumentData->setData($date, $date, $data_, "RW", $z_magazyn_id, "warehouse_document_r_w")){
                    if($id_pw = $this->warehouseDocumentData->setData($date, $date, $data_, "PW", $do_magazyn_id, "warehouse_document_p_w")){
                        return $this->jsonResponse("Dokument RW i PW dodany!");
                    }else{
                        return $this->jsonResponse("Błąd przyjęcia stany w magazynie.".print_r($id_pw,true));
                    }
                }else{
                    return $this->jsonResponse("Błąd zmniejszenia stany w magazynie.".print_r($id_rw,true));
                }
            }
            return $this->jsonResponse("Błąd dodawania dokumentu MM");
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            return $this->jsonResponse($e->getMessage());
        } catch (\Exception $e) {
            $this->logger->critical($e);
            return $this->jsonResponse($e->getMessage());
        }
    }

    /**
     * Create json response
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function jsonResponse($response = '')
    {
        return $this->getResponse()->representJson(
            $this->jsonHelper->jsonEncode($response)
        );
    }

    private function getArrayForCsv()
    {
        $data = $this->getRequest()->getParam('import', null);

        if ($data) {
            $data = array_filter(array_map('trim', explode(PHP_EOL, $data)));

            $data = array_map(
                function ($row) {
                    if (preg_match("/\t/", $row)) {
                        $row = explode('	', $row);
                    } else {
                        $row = explode(',', $row);
                    }
                    $row['sku'] = $row[0] ?? '';
                    unset($row[0]);

                    $row['qty'] = $row[1] ?? '';
                    unset($row[1]);

                    return $row;
                },
                $data
            );
        }

        return $data;
    }
}

