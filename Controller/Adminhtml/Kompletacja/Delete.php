<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Controller\Adminhtml\Kompletacja;

class Delete extends \Kowal\WFirma\Controller\Adminhtml\Kompletacja
{

    public function __construct(
        \Magento\Backend\App\Action\Context                                  $context,
        \Kowal\WFirma\Model\ResourceModel\KompletacjaItems\CollectionFactory $collectionFactoryKompletacjaItems,
        \Magento\Framework\Registry                                          $coreRegistry
    )
    {
        $this->collectionFactoryKompletacjaItems = $collectionFactoryKompletacjaItems;
        parent::__construct($context, $coreRegistry);
    }

    protected function _isAllowed()
    {
        return true;
    }

    /**
     * Delete action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        // check if we know what should be deleted
        $id = $this->getRequest()->getParam('kompletacja_id');
        if ($id) {
            try {
                // init model and delete
                $model = $this->_objectManager->create(\Kowal\WFirma\Model\Kompletacja::class);
                $model->load($id);

                $modelItem = $this->_objectManager->create(\Kowal\WFirma\Model\KompletacjaItems::class);
                if ($items = $this->getItems($id)) {
                    foreach ($items as $item) {
                        if ($modelItem->load($item->getKompletacjaitemsId())) {
                            #TODO przenieść stany magazynowy z Kompletacji do Mg. Głównego.
                            # Można by zapisywac dokument z wFirmy podczas tworzenia kompletacji i teraz przy jej usuwaniu
                            # usuwać również dokument wFirma zamiast tworzyć RW i PW
                            $modelItem->delete();
                        }
                    }
                }
                $model->delete();

                // display success message
                $this->messageManager->addSuccessMessage(__('You deleted the Kompletacja.'));
                // go to grid
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                // display error message
                $this->messageManager->addErrorMessage($e->getMessage());
                // go back to edit form
                return $resultRedirect->setPath('*/*/edit', ['kompletacja_id' => $id]);
            }
        }
        // display error message
        $this->messageManager->addErrorMessage(__('We can\'t find a Kompletacja to delete.'));
        // go to grid
        return $resultRedirect->setPath('*/*/');
    }

    public function getItems($kompletacja_id)
    {
        $kompletacjaCollection = $this->collectionFactoryKompletacjaItems->create();
        $kompletacjaCollection->addFieldToFilter('kompletacja_id', array('eq' => $kompletacja_id));

        return $kompletacjaCollection->addFieldToSelect('*');
    }
}

