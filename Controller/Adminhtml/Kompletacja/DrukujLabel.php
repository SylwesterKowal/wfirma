<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Controller\Adminhtml\Zpm;


class DrukujLabel extends \Magento\Backend\App\Action
{
    /**
     * @param \Magento\Backend\App\Action\Context $context
     */
    public function __construct(
        \Magento\Backend\App\Action\Context                   $context,
        \Magento\Sales\Model\OrderFactory                     $orderFactory,
        \Magento\Framework\App\Response\Http\FileFactory      $fileFactory,
        \Magento\Framework\Controller\Result\RawFactory       $resultRawFactory,
        \Psr\Log\LoggerInterface                              $logger
    )
    {

        $this->orderFactory = $orderFactory;
        $this->fileFactory = $fileFactory;
        $this->resultRawFactory = $resultRawFactory;
        $this->logger = $logger;

        parent::__construct($context);
    }

    protected function _isAllowed()
    {
        return true;
    }


    public
    function execute()
    {
        try {

            $data = $this->getRequest()->getPostValue();


                if ($order_ = $this->orderFactory->create()->loadByIncrementId($data['order_id'])) {
                    $this->fileFactory->create(
                        $this->fileName,
                        $order_->getShipmentLabel(),
                        \Magento\Framework\App\Filesystem\DirectoryList::MEDIA
                    );
                    $resultRaw = $this->resultRawFactory->create();
                    return $resultRaw;

                }







        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            return $this->jsonResponse($e->getMessage());
        } catch (\Exception $e) {
            $this->logger->critical($e);
            return $this->jsonResponse($e->getMessage());
        }
    }
}