<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Controller\Adminhtml\Kompletacja;

use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\App\Response\Http;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\View\Result\PageFactory;
use Psr\Log\LoggerInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Session\SessionManagerInterface;
use Kowal\WFirma\lib\Query;
use Kowal\WFirma\lib\Config;
use Kowal\WFirma\Controller\Adminhtml\Kompletacja\SaveNew;
use Magento\Catalog\Model\ProductRepository;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory as OrderCollectionFactory;
use Magento\Sales\Model\OrderFactory;
use Magento\Backend\Model\Auth\Session as AdminSession;

class Pakuj extends SaveNew implements HttpPostActionInterface
{

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;
    /**
     * @var Json
     */
    protected $serializer;
    /**
     * @var LoggerInterface
     */
    protected $logger;
    /**
     * @var Http
     */
    protected $http;

    /**
     * @param PageFactory $resultPageFactory
     * @param Json $json
     * @param LoggerInterface $logger
     * @param Http $http
     * @param RequestInterface $request
     * @param SessionManagerInterface $session
     * @param Query $query
     * @param Config $config
     * @param ProductRepository $productRepository
     * @param OrderCollectionFactory $orderCollectionFactory
     * @param OrderFactory $orderFactory
     * @param AdminSession $adminSession
     */
    public function __construct(
        PageFactory             $resultPageFactory,
        Json                    $json,
        LoggerInterface         $logger,
        Http                    $http,
        RequestInterface        $request,
        SessionManagerInterface $session,
        Query                   $query,
        Config                  $config,
        ProductRepository       $productRepository,
        OrderCollectionFactory  $orderCollectionFactory,
        OrderFactory            $orderFactory,
        AdminSession            $adminSession
    )
    {
        $this->resultPageFactory = $resultPageFactory;
        $this->serializer = $json;
        $this->logger = $logger;
        $this->http = $http;
        $this->request = $request;
        $this->session = $session;
        $this->query = $query;
        $this->config = $config;
        $this->orderFactory = $orderFactory;
        $this->adminSession = $adminSession;
        parent::__construct($resultPageFactory, $json, $logger, $http, $request, $session, $query, $config, $productRepository, $orderCollectionFactory, $orderFactory, $adminSession);
    }

    protected function _isAllowed()
    {
        return true;
    }


    /**
     * Execute view action
     *
     * @return ResultInterface
     */
    public function execute()
    {
        try {

            $request = $this->request->getPostValue();
            $pakowanie_skanerem = (isset($request['pakowanie_skanerem'])) ? explode("#", $request['pakowanie_skanerem']) : null;
            if (isset($request['type']) && $request['type'] == 'reset') {
                $this->query->resetPakujOrderItemQty($request['order_id'], $request['sku'], $request['item_id'], $this->getOrderStatusToFilter());
                if ($order = $this->getOrderByIncrementId($request['order_id'])) {
                    $this->changeOrderStatus($order, $this->config->getOrderStatusDoPakowania());
                }
                $order_id = $request['order_id'];
                $sku = $request['sku'];
            } else if (isset($request['type']) && $request['type'] == 'pakuj_skanerem') {
                $ean_code = $this->config->getAttributeCodeEAN();
                if ($sku = $this->query->getSKUbyAttribute($ean_code, $request['pakowanie_skanerem'])) {
                    $this->query->pakujScanedOrderItemQty($request['order_id'], $sku, $this->getOrderStatusToFilter());
                } else if (isset($pakowanie_skanerem[0]) && isset($pakowanie_skanerem[1])) {
                    $this->query->pakujScanedOrderItemQty($pakowanie_skanerem[0], $pakowanie_skanerem[1], $this->getOrderStatusToFilter());
                    $order_id = $pakowanie_skanerem[0];
                    $sku = $pakowanie_skanerem[1];
                } else {
                    $this->query->pakujScanedOrderItemQty($request['order_id'], $request['pakowanie_skanerem'], $this->getOrderStatusToFilter());
                    $order_id = $request['order_id'];
                    $sku = $request['pakowanie_skanerem'];
                }
            } else if (isset($request['type']) && $request['type'] == 'pakuj') {
                $this->query->pakujOrderItemQty($request['order_id'], $request['sku'], $request['item_id'], $this->getOrderStatusToFilter());
                $order_id = $request['order_id'];
                $sku = $request['sku'];
            }


            $this->session->start();

            if ($increment_ids = $this->session->getKolekcjaItems()) {
                $lastOrder = $this->session->getKolekcjaLastOrder();
                if ($display_type = $this->config->getSposobWyswietlaniaKompletacji()) {
                    $wyswietl_zamowienia = ($display_type == 'wszystkie') ? $increment_ids : [$lastOrder];
                } else {
                    $wyswietl_zamowienia = [$lastOrder];
                }

                $output = $this->resultPageFactory->create()->getLayout()
                    ->createBlock('Kowal\WFirma\Block\Adminhtml\Kompletacja\View')
                    ->setAttribute('items', $this->groupByOrders($wyswietl_zamowienia, $sku, $request['order_id']))
                    ->setAttribute('printerName', $this->getPrinterName())
                    ->setTemplate("Kowal_WFirma::kompletacja/new.phtml")
                    ->toHtml();

            } else if($order_id){
                $output = $this->resultPageFactory->create()->getLayout()
                    ->createBlock('Kowal\WFirma\Block\Adminhtml\Kompletacja\View')
                    ->setAttribute('items', $this->groupByOrders([$order_id], $sku, $request['order_id']))
                    ->setAttribute('printerName', $this->getPrinterName())
                    ->setTemplate("Kowal_WFirma::kompletacja/new.phtml")
                    ->toHtml();
                $this->session->setKolekcjaItems([$order_id]);
            }else{
                $output = '...';
            }

            return $this->jsonResponse($output);


        } catch (LocalizedException $e) {
            return $this->jsonResponse($e->getMessage());
        } catch (\Exception $e) {
            $this->logger->critical($e);
            return $this->jsonResponse($e->getMessage());
        }
    }

    /**
     * Create json response
     *
     * @return ResultInterface
     */
    public function jsonResponse($response = '')
    {
        $this->http->getHeaders()->clearHeaders();
        $this->http->setHeader('Content-Type', 'application/json');
        return $this->http->setBody(
            $this->serializer->serialize($response)
        );
    }
}

