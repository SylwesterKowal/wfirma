<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Controller\Adminhtml\Kompletacja;

use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\App\Response\Http;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\View\Result\PageFactory;
use Psr\Log\LoggerInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Session\SessionManagerInterface;
use Kowal\WFirma\lib\Query;
use Kowal\WFirma\lib\Config;
use Kowal\WFirma\Controller\Adminhtml\Kompletacja\SaveNew;
use Magento\Catalog\Model\ProductRepository;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory as OrderCollectionFactory;
use Magento\Sales\Model\OrderFactory;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Convert\Order as CconvertOrder;
use Magento\Backend\Model\Auth\Session as AdminSession;

class Wyslij extends SaveNew implements HttpPostActionInterface
{

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;
    /**
     * @var Json
     */
    protected $serializer;
    /**
     * @var LoggerInterface
     */
    protected $logger;
    /**
     * @var Http
     */
    protected $http;

    /**
     * @param PageFactory $resultPageFactory
     * @param Json $json
     * @param LoggerInterface $logger
     * @param Http $http
     * @param RequestInterface $request
     * @param SessionManagerInterface $session
     * @param Query $query
     * @param Config $config
     * @param ProductRepository $productRepository
     * @param OrderCollectionFactory $orderCollectionFactory
     * @param OrderFactory $orderFactory
     * @param CconvertOrder $convertOrder
     * @param AdminSession $adminSession
     */
    public function __construct(
        PageFactory             $resultPageFactory,
        Json                    $json,
        LoggerInterface         $logger,
        Http                    $http,
        RequestInterface        $request,
        SessionManagerInterface $session,
        Query                   $query,
        Config                  $config,
        ProductRepository       $productRepository,
        OrderCollectionFactory  $orderCollectionFactory,
        OrderFactory            $orderFactory,
        CconvertOrder           $convertOrder,
        AdminSession            $adminSession,
        Order                   $order
    )
    {
        $this->resultPageFactory = $resultPageFactory;
        $this->serializer = $json;
        $this->logger = $logger;
        $this->http = $http;
        $this->request = $request;
        $this->session = $session;
        $this->query = $query;
        $this->config = $config;
        $this->orderFactory = $orderFactory;
        $this->convertOrder = $convertOrder;
        $this->adminSession = $adminSession;
        $this->order = $order;
        parent::__construct($resultPageFactory, $json, $logger, $http, $request, $session, $query, $config, $productRepository, $orderCollectionFactory, $orderFactory, $adminSession);
    }

    protected function _isAllowed()
    {
        return true;
    }

    /**
     * Execute view action
     *
     * @return ResultInterface
     */
    public function execute()
    {
        try {
            $this->session->start();
            $request = $this->request->getPostValue();
            if (isset($request['order_id']) && !empty($request['order_id'])) {
                if ($order = $this->order->load($request['order_id'])) {
//                    $this->createShipment($order);

                    $this->changeOrderStatus($order, $this->config->getOrderStatusDoWysylki());
                }
            }


            if ($increment_ids = $this->session->getKolekcjaItems()) {
                $lastOrder = $this->session->getKolekcjaLastOrder();

                if ($display_type = $this->config->getSposobWyswietlaniaKompletacji()) {
                    $wyswietl_zamowienia = ($display_type == 'wszystkie') ? $increment_ids : $lastOrder;
                } else {
                    $wyswietl_zamowienia = $lastOrder;
                }


                $output = $this->resultPageFactory->create()->getLayout()
                    ->createBlock('Kowal\WFirma\Block\Adminhtml\Kompletacja\View')
                    ->setAttribute('items', $this->groupByOrders($wyswietl_zamowienia))
                    ->setAttribute('printerName', $this->getPrinterName())
                    ->setTemplate("Kowal_WFirma::kompletacja/new.phtml")
                    ->toHtml();
            } else {
                $output = "";
            }

            return $this->jsonResponse($output);


        } catch (LocalizedException $e) {
            return $this->jsonResponse($e->getMessage() . (string)$request['order_id']);
        } catch (\Exception $e) {
            $this->logger->critical($e);
            return $this->jsonResponse($e->getMessage() . (string)$request['order_id']);
        }
    }


    private
    function createShipment($order)
    {
        if (!$order->canShip()) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('You can\'t create an shipment.')
            );
        }

        // Initialize the order shipment object
//        $convertOrder = $this->_objectManager->create('Magento\Sales\Model\Convert\Order');
        $shipment = $this->convertOrder->toShipment($order);


        foreach ($order->getAllItems() as $orderItem) {
            // Check if order item has qty to ship or is virtual
            if (!$orderItem->getQtyToShip() || $orderItem->getIsVirtual()) {
                continue;
            }

            // Create shipment item with qty
            $shipmentItem = $this->convertOrder->itemToShipmentItem($orderItem)->setQty($orderItem->getQtyToShip());

            // Add shipment item to shipment
            $shipment->addItem($shipmentItem);

        }

        // Register shipment
        $shipment->register();

        $shipment->getOrder()->setIsInProcess(true);

        try {
            // Save created shipment and order
            $shipment->save();
            $shipment->getOrder()->save();

            // Send email
//                $this->_objectManager->create('Magento\Shipping\Model\ShipmentNotifier')
//                    ->notify($shipment);

//                $shipment->save();

            return true;
        } catch (\Exception $e) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __($e->getMessage())
            );
        }

    }

    /**
     * Create json response
     *
     * @return ResultInterface
     */
    public function jsonResponse($response = '')
    {
        $this->http->getHeaders()->clearHeaders();
        $this->http->setHeader('Content-Type', 'application/json');
        return $this->http->setBody(
            $this->serializer->serialize($response)
        );
    }


}

