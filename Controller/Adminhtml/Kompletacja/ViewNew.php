<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Controller\Adminhtml\Kompletacja;

use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\Response\Http;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\View\Result\PageFactory;
use Psr\Log\LoggerInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Session\SessionManagerInterface;
use Kowal\WFirma\lib\Query;
use Kowal\WFirma\lib\Config;
use Kowal\WFirma\Controller\Adminhtml\Kompletacja\SaveNew;
use Magento\Catalog\Model\ProductRepository;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory as OrderCollectionFactory;
use Magento\Sales\Model\OrderFactory;
use Magento\Backend\Model\Auth\Session as AdminSession;

class ViewNew extends SaveNew implements HttpGetActionInterface
{
    protected $curent_sku;

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;
    /**
     * @var Json
     */
    protected $serializer;
    /**
     * @var LoggerInterface
     */
    protected $logger;
    /**
     * @var Http
     */
    protected $http;

    /**
     * Constructor
     *
     * @param PageFactory $resultPageFactory
     * @param Json $json
     * @param LoggerInterface $logger
     * @param Http $http
     */
    public function __construct(
        PageFactory             $resultPageFactory,
        Json                    $json,
        LoggerInterface         $logger,
        Http                    $http,
        RequestInterface        $request,
        SessionManagerInterface $session,
        Query                   $query,
        Config                  $config,
        ProductRepository       $productRepository,
        OrderCollectionFactory  $orderCollectionFactory,
        OrderFactory            $orderFactory,
        AdminSession            $adminSession,
        \Magento\Framework\Registry $coreRegistry
    )
    {
        $this->resultPageFactory = $resultPageFactory;
        $this->serializer = $json;
        $this->logger = $logger;
        $this->http = $http;
        $this->request = $request;
        $this->session = $session;
        $this->query = $query;
        $this->config = $config;
        $this->orderFactory = $orderFactory;
        $this->adminSession = $adminSession;
        $this->_coreRegistry = $coreRegistry;
        parent::__construct($resultPageFactory, $json, $logger, $http, $request, $session, $query, $config, $productRepository, $orderCollectionFactory, $orderFactory, $adminSession);
    }

    protected function _isAllowed()
    {
        return true;
    }

    /**
     * Execute view action
     *
     * @return ResultInterface
     */
    public function execute()
    {
        try {
            $this->session->start();
            $request = $this->request->getPostValue();

            // ID#SKU albo ID#SKU#OrderID albo OrderID#SKU
            $item = (isset($request['szukana_wartosc'])) ? explode("#", $request['szukana_wartosc']) : null;
            $filtr_detal = (isset($request['filtr_detal'])) ?  $request['filtr_detal'] : false;
            $filtr_pozostale = (isset($request['filtr_pozostale'])) ? $request['filtr_pozostale'] : false;
            $limit = (isset($request['bez_limitu']) && $request['bez_limitu'] == "1") ? '' : 'LIMIT 5';

            file_put_contents("_filtr.txt",print_r([$filtr_detal,$filtr_pozostale],true));
            file_put_contents("_limit.txt",print_r([$limit],true));

            if (isset($request['szukana_wartosc']) && trim($request['szukana_wartosc']) != '') {
                $ean_code = $this->config->getAttributeCodeEAN();
                $sku = $this->query->getSKUbyAttribute($ean_code, $request['szukana_wartosc']);
                $order_id = null;
                if ($orders = $this->query->getOrdersBySku($sku, 190, $this->getOrderStatusToFilter(), $this->getOrderShippingToFilter($filtr_detal, $filtr_pozostale),$limit)){
                    $wyswietl_zamowienia = $orders;
                    $order_id = null;
                } else if ($orders = $this->query->getOrdersBySku($request['szukana_wartosc'], 190, $this->getOrderStatusToFilter(), $this->getOrderShippingToFilter($filtr_detal, $filtr_pozostale), $limit)) {
                    $wyswietl_zamowienia = $orders;
                    $sku = $request['szukana_wartosc'];
                    $order_id = null;
                } else if ($order = $this->query->checkIfOrderExists($request['szukana_wartosc'], $this->getOrderStatusToFilter(), $this->getOrderShippingToFilter($filtr_detal, $filtr_pozostale))) {
                    $wyswietl_zamowienia = [$request['szukana_wartosc']];
                    $order_id = $request['szukana_wartosc'];
                    $sku = null;
                } else if (isset($item[0]) && isset($item[1])) {
                    if ($order = $this->query->getOrderByItemId($item[0], $this->getOrderStatusToFilter(), $this->getOrderShippingToFilter($filtr_detal, $filtr_pozostale))) {
                       file_put_contents("__order_by_itemid.txt",print_r($order,true));
                        $wyswietl_zamowienia = [$order['order_id']];
                        $sku = $order['sku'];
                        $order_id = $order['order_id'];
                    }else {
                        $wyswietl_zamowienia = $orders;
                        $order_id = null;
                        $wyswietl_zamowienia[(string)$item[0]] = (string)$item[0];
                        $sku = (string)$item[1];
                        $order_id = (string)$item[0];
                    }
                } else if( !empty($sku) && $this->czyDrukowacAutomatycznieKodQR($sku, $order_id)) {
                    $wyswietl_zamowienia = false;
                } else if( $this->query->checkIfSkuExists($request['szukana_wartosc']) && $this->czyDrukowacAutomatycznieKodQR($request['szukana_wartosc'], $order_id)) {
                    $wyswietl_zamowienia = false;
                    $sku = $request['szukana_wartosc'];
                }else{
                    $wyswietl_zamowienia = false;
                }
                if ($wyswietl_zamowienia) {
                    // wyswietlaamy wyniki - lista zamówień
                    $output = $this->resultPageFactory->create()->getLayout()
                        ->createBlock('Kowal\WFirma\Block\Adminhtml\Kompletacja\View')
                        ->setAttribute('items', $this->groupByOrders($wyswietl_zamowienia, $sku, $order_id))
                        ->setAttribute('printerName', $this->getPrinterName())
                        ->setAttribute('czyDrukowacAutomatycznieKodQR', $this->czyDrukowacAutomatycznieKodQR($sku, $order_id))
                        ->setAttribute('liveLabelAttr', ['sku'=>$sku,'order_id'=>$order_id,'ean' =>$this->query->getAttribute($sku,$ean_code)])
                        ->setTemplate("Kowal_WFirma::kompletacja/new.phtml")
                        ->toHtml();
                    $this->session->setKolekcjaLastOrder($wyswietl_zamowienia);
                } else {
                    // wyswietlamy puste wyniki aby wydrukować etykietę dla ytkowników PROD1, PROD2
                    $output = $this->resultPageFactory->create()->getLayout()
                        ->createBlock('Kowal\WFirma\Block\Adminhtml\Kompletacja\View')
                        ->setAttribute('items', false)
                        ->setAttribute('printerName', $this->getPrinterName())
                        ->setAttribute('czyDrukowacAutomatycznieKodQR', $this->czyDrukowacAutomatycznieKodQR($sku, $order_id))
                        ->setAttribute('liveLabelAttr', ['sku'=>$sku,'order_id'=>$order_id,'ean' =>$this->query->getAttribute($sku,$ean_code)])
                        ->setTemplate("Kowal_WFirma::kompletacja/new.phtml")
                        ->toHtml();
                }

            } else if ($increment_ids = $this->session->getKolekcjaItems()) {
                // Wyświetlamy ostatnio wczytane zamówienia
                $lastOrder = $this->session->getKolekcjaLastOrder();
                if ($display_type = $this->config->getSposobWyswietlaniaKompletacji()) {
                    $wyswietl_zamowienia = ($display_type == 'wszystkie') ? $increment_ids : $lastOrder;
                } else {
                    $wyswietl_zamowienia = $lastOrder;
                }

                $output = $this->resultPageFactory->create()->getLayout()
                    ->createBlock('Kowal\WFirma\Block\Adminhtml\Kompletacja\View')
                    ->setAttribute('items', $this->groupByOrders($wyswietl_zamowienia))
                    ->setAttribute('printerName', $this->getPrinterName())
                    ->setTemplate("Kowal_WFirma::kompletacja/new.phtml")
                    ->toHtml();
            } else {
                $output = "brak zamówień";
            }

            return $this->jsonResponse($output);

        } catch (LocalizedException $e) {
            return $this->jsonResponse($e->getMessage());
        } catch (\Exception $e) {
            $this->logger->critical($e);
            return $this->jsonResponse($e->getMessage());
        }
    }

    /**
     * Create json response
     *
     * @return ResultInterface
     */
    public function jsonResponse($response = '')
    {
        $this->http->getHeaders()->clearHeaders();
        $this->http->setHeader('Content-Type', 'application/json');
        return $this->http->setBody(
            $this->serializer->serialize($response)
        );
    }

}

