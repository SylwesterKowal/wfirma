<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Controller\Adminhtml\Kompletacja;

use Magento\Framework\Exception\LocalizedException;

class Save extends \Magento\Backend\App\Action
{

    protected $dataPersistor;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
     */
    public function __construct(
        \Magento\Backend\App\Action\Context                   $context,
        \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor,
        \Magento\Framework\Session\SessionManagerInterface    $sessionManager,
        \Magento\Catalog\Model\ProductRepository              $productRepository,
        \Kowal\WFirma\Helper\WarehouseDocumentData            $warehouseDocumentData,
        \Kowal\WFirma\Helper\ZpmData                          $zpmData,
        \Kowal\WFirma\lib\Config                              $config,
        \Kowal\WFirma\lib\Query                               $query
    )
    {
        $this->dataPersistor = $dataPersistor;
        $this->sessionManager = $sessionManager;
        $this->productRepository = $productRepository;
        $this->warehouseDocumentData = $warehouseDocumentData;
        $this->zpmData = $zpmData;
        $this->config = $config;
        $this->query = $query;
        parent::__construct($context);
    }

    protected function _isAllowed()
    {
        return true;
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();
        if ($data) {
            $id = $this->getRequest()->getParam('kompletacja_id');

            $model = $this->_objectManager->create(\Kowal\WFirma\Model\Kompletacja::class)->load($id);
            if (!$model->getId() && $id) {
                $this->messageManager->addErrorMessage(__('This Kompletacja no longer exists.'));
                return $resultRedirect->setPath('*/*/');
            }



            try {


                $this->sessionManager->start();
                $items = $this->sessionManager->getKolekcjaItems();
                $this->sessionManager->unsKolekcjaItems();

                $data['wf_pw_id'] = '';
                $data['wf_rw_id'] = '';
                $data['wf_mm_id'] = '';
                #TODO trzeba je najpierw pomniejszyć o produkty z magazynu rezerwacji wFirma.
                // Odczyt stanu magazynowego z rezerwacji
                if ($stockRezerwacje = $this->zpmData->getGoodsStocksRezerwacje($items)) {
                    if ($itemsRezerwacje = $this->calculateRezerwacjeFromStocks($items, $stockRezerwacje)) { // produkty do przesuniecia z Rezerwacji do Kompletacji
                        $data['wf_rw_id'] = $this->createRWFromRezerwacje($itemsRezerwacje);
                        $this->updateItemQtyCompletedAndStatus($itemsRezerwacje);
                    }

                    if ($itemsProdukcja = $this->calculateProductionFromStocks($items, $stockRezerwacje)) {
                        $data['wf_pw_id'] = $this->createPW($itemsProdukcja); // przyjecie z produkcji pomniejszone o rezerwacje
                        $this->updateItemQtyCompletedAndStatus($itemsProdukcja);
                    }else{
                        $data['wf_pw_id'] = $this->createPW($items); // przyjęcie produktów z produkcji do magazynu kompletacji WFIRMA wcałości
                        $this->updateItemQtyCompletedAndStatus($items);
                    }
                } else {
                    $data['wf_pw_id'] =  $this->createPW($items); // przyjęcie produktów z produkcji do magazynu kompletacji WFIRMA wcałości
                    $this->updateItemQtyCompletedAndStatus($items);
                }


                $model->setData($data);
                $model->save();

                $this->dataPersistor->clear('kowal_wfirma_kompletacja');

                foreach ($items as $item) {
                    if ($product = $this->productRepository->get($item['sku'])) {
                        $modelItems = $this->_objectManager->create(\Kowal\WFirma\Model\KompletacjaItems::class)->load(null);
                        $dataItens = ['kompletacja_id' => $model->getId(), 'sku' => $item['sku'], "nazwa" => $product->getName(), 'ilosc' => $item['ilosc'], 'order_id' => $item['order_id'], "z_magazynu" => 'produkcja', "do_magazynu" => 'kompletacja'];
                        $modelItems->setData($dataItens);
                        $modelItems->save();
                    }
                }
                $this->messageManager->addSuccessMessage(__('You saved the Kompletacja.'));


                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['kompletacja_id' => $model->getId()]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the Kompletacja. ').$e->getMessage());
            }

            $this->dataPersistor->set('kowal_wfirma_kompletacja', $data);
            return $resultRedirect->setPath('*/*/edit', ['kompletacja_id' => $this->getRequest()->getParam('kompletacja_id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }

    private function updateItemQtyCompletedAndStatus($items){
        foreach ($items as  $item) {
            $this->query->updateOrderItemQtyCompleted($item['order_id'],$item['sku'],  $item['item_id'], $item['ilosc'],'produkcja');
        }
    }

    private function createPW($items)
    {
        if ($magazyn_id = $this->config->getMagazynKompletacji()) {
            $data_ = [];
            foreach ($items as $product) {
                $data_[] = ["sku" => $product['sku'], "qty" => $product['ilosc']];
            }

            if (count($data_)) {
                $date = date('Y-m-d');
                if ($id = $this->warehouseDocumentData->setData($date, $date, $data_, "PW", $magazyn_id, "warehouse_document_p_w", null, null, null, null)) {
                    return $id;
                } else {
                    return false;
                }
            }
        }
        return false;
    }

    private function createRWFromRezerwacje($items)
    {
        if ($magazyn_id = $this->config->getMagazynRezerwacji()) {
            $data_ = [];
            foreach ($items as $product) {
                $data_[] = ["sku" => $product['sku'], "qty" => $product['ilosc']];
            }

            if (count($data_)) {
                $date = date('Y-m-d');
                if ($id = $this->warehouseDocumentData->setData($date, $date, $data_, "RW", $magazyn_id, "warehouse_document_r_w", null, null, null, null)) {
                    return $id;
                } else {
                    return false;
                }
            }
        }
        return false;
    }


    private function calculateRezerwacjeFromStocks($items, $stockRezerwacje)
    {
        $itemsRezerwacje = [];

        foreach ($items as $item) {
            if (array_key_exists($item['sku'], $stockRezerwacje)) {
                if ($stockRezerwacje[$item['sku']] > 0) {
                    if ($stockRezerwacje[$item['sku']] < $item['ilosc']) {
                        $item['ilosc'] = $stockRezerwacje[$item['sku']];
                    }
                    $itemsRezerwacje[] = $item;
                }
            }
        }
        return $itemsRezerwacje;
    }

    private function calculateProductionFromStocks($items, $stockRezerwacje)
    {
        $itemsProduction = [];

        foreach ($items as $item) {
            if (array_key_exists($item['sku'], $stockRezerwacje)) {
                if ($stockRezerwacje[$item['sku']] > 0) {
                    if ($stockRezerwacje[$item['sku']] < $item['ilosc']) {
                        $item['ilosc'] = $item['ilosc'] - $stockRezerwacje[$item['sku']];
                        $itemsProduction[] = $item;
                    }
                } else {
                    $itemsProduction[] = $item;
                }
            }
        }
        return $itemsProduction;
    }

}

