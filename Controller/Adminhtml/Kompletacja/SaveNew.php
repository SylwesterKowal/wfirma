<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Controller\Adminhtml\Kompletacja;

use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\App\Response\Http;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\View\Result\PageFactory;
use Psr\Log\LoggerInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Session\SessionManagerInterface;
use Kowal\WFirma\lib\Query;
use Kowal\WFirma\lib\Config;
use Magento\Catalog\Model\ProductRepository;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory as OrderCollectionFactory;
use Magento\Sales\Model\OrderFactory;
use Magento\Backend\Model\Auth\Session as AdminSession;


class SaveNew implements HttpPostActionInterface
{

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;
    /**
     * @var Json
     */
    protected $serializer;
    /**
     * @var LoggerInterface
     */
    protected $logger;
    /**
     * @var Http
     */
    protected $http;

    /**
     * @param PageFactory $resultPageFactory
     * @param Json $json
     * @param LoggerInterface $logger
     * @param Http $http
     * @param RequestInterface $request
     * @param SessionManagerInterface $session
     * @param Query $query
     * @param Config $config
     * @param ProductRepository $productRepository
     * @param OrderCollectionFactory $orderCollectionFactory
     * @param OrderFactory $orderFactory
     * @param AdminSession $adminSession
     */
    public function __construct(
        PageFactory             $resultPageFactory,
        Json                    $json,
        LoggerInterface         $logger,
        Http                    $http,
        RequestInterface        $request,
        SessionManagerInterface $session,
        Query                   $query,
        Config                  $config,
        ProductRepository       $productRepository,
        OrderCollectionFactory  $orderCollectionFactory,
        OrderFactory            $orderFactory,
        AdminSession            $adminSession
    )
    {
        $this->resultPageFactory = $resultPageFactory;
        $this->serializer = $json;
        $this->logger = $logger;
        $this->http = $http;
        $this->request = $request;
        $this->session = $session;
        $this->query = $query;
        $this->config = $config;
        $this->productRepository = $productRepository;
        $this->orderCollectionFactory = $orderCollectionFactory;
        $this->orderFactory = $orderFactory;
        $this->adminSession = $adminSession;
    }

    protected function _isAllowed()
    {
        return true;
    }

    /**
     * Execute view action
     *
     * @return ResultInterface
     */
    public function execute()
    {
        try {
            $request = $this->request->getPostValue();
//            return $this->jsonResponse(print_r($this->validatedParams(),true));

            if (trim($request['new_kopletacja']) != '') {
                $items = $this->validatedParams();

                $output = $this->resultPageFactory->create()->getLayout()
                    ->createBlock('Kowal\WFirma\Block\Adminhtml\Kompletacja\View')
                    ->setAttribute('items', $items)
                    ->setAttribute('printerName', $this->getPrinterName())
                    ->setTemplate("Kowal_WFirma::kompletacja/new.phtml")
                    ->toHtml();
            }

            return $this->jsonResponse($output);

        } catch (LocalizedException $e) {
            return $this->jsonResponse($e->getMessage());
        } catch (\Exception $e) {
            $this->logger->critical($e);
            return $this->jsonResponse($e->getMessage());
        }
    }

    /**
     * Create json response
     *
     * @return ResultInterface
     */
    public function jsonResponse($response = '')
    {
        $this->http->getHeaders()->clearHeaders();
        $this->http->setHeader('Content-Type', 'application/json');
        return $this->http->setBody(
            $this->serializer->serialize($response)
        );
    }

    private function validatedParams()
    {
        $request = $this->request->getPostValue();
        $item = explode("#", $request['new_kopletacja']);

        if (trim($request['new_kopletacja']) === '') {
            throw new LocalizedException(__('Wprowadź poprawną strukturę [numer_zamówienia]#[sku]#[ilosc].'));
        }

        if (\strpos($request['new_kopletacja'], '#') === false) {
            throw new LocalizedException(__('Wprowadź poprawną strukturę [numer_zamówienia]#[sku]#[ilosc].'));
        }

        if (!isset($item[0]) || !isset($item[1])) {
            throw new LocalizedException(__('Wprowadź poprawną strukturę [numer_zamówienia]#[sku]#[ilosc].'));
        }

        if (!$this->query->checkIfOrderExists($item[0], $this->getOrderStatusToFilter())) {
            throw new LocalizedException(__('Nie ma takiego numeru zamówienia w sklepie'));
        }

        if (!$this->query->checkIfSkuExists($item[1])) {
            throw new LocalizedException(__('Nie ma takiego SKU w sklepie'));
        }

        if (!$this->query->checkIfSkuExistsInOrder($item[0], $item[1], $this->getOrderStatusToFilter())) {
            throw new LocalizedException(__('Nie ma takiego SKU: ' . $item[1] . ' w tym zamówieniu: ' . $item[0]));
        }

        $this->session->start();
        $increment_ids = $this->session->getKolekcjaItems();
//        if(!$lastOrder = $this->session->getKolekcjaLastOrder()){
        $lastOrder = [(string)$item[0]];
//        }

        $increment_ids[(string)$item[0]] = (string)$item[0];

        if ($display_type = $this->config->getSposobWyswietlaniaKompletacji()) {
            $wyswietl_zamowienia = ($display_type == 'wszystkie') ? $increment_ids : $lastOrder;
        } else {
            $wyswietl_zamowienia = $lastOrder;
        }
        $this->session->setKolekcjaItems($increment_ids);


        $typ = (isset($request['typ']) && !empty($request['typ'])) ? $request['typ'] : 'produkcja';
        $dzialanie = (isset($request['dzialanie']) && !empty($request['dzialanie'])) ? $request['dzialanie'] : 'plus';

        // Dodanie modyfikacji po entity_id ze wzgleduu na kilkukrotne wystepowanie SKU w jednym zamowieniau głownie Xcreator
        $item_id = (isset($item[2])) ? $item[2] : null;
        // Dodaje 1 do kompletacji po skanowaniu głównym
        $this->query->updateOrderItemQtyCompleted($item[0], $item[1], $item_id, $typ, $dzialanie, $this->getOrderStatusToFilter());


        return $this->groupByOrders($wyswietl_zamowienia, $item[1], $item[0]);
    }

    protected function groupByOrders($increment_ids, $sku_wyroznione = null, $order_id_wyroznione = null)
    {
        $data = [];
        if ($orders = $this->getInvoiceCollection($increment_ids)) {
            $this->session->setKolekcjaLastOrder($increment_ids);
            foreach ($orders as $order) {
                $orderItems = $order->getAllItems();
                $status_kompletacji = true;
                $status_pakowania = true;
                $items = [];
                $mozna_wysylac = true;
                foreach ($orderItems as $item) {
                    if ($item->getParentItemId() && $item->getParentItem() && $item->getParentItem()->getProductType() == 'configurable') {
                        continue;
                    }
                    if ($item->getProductType() == 'bundle') {
                        continue;
                    }
                    if ($this->query->checkIfSkuExists($item->getSku())) {
                        $product = $this->productRepository->get($item->getSku());
                    }
//                    else if($this->query->checkIfSkuExists(rtrim($item->getSku(),'-FBM'))) {
//
//                        $product = $this->productRepository->get(rtrim($item->getSku(),'-FBM'));
//                    }
//                    else{
//                        $ean_code = $this->config->getAttributeCodeEAN();
//                        if($sku = $this->query->getSKUbyAttribute($ean_code,$item->getEan())) {
//                            $product = $this->productRepository->get($sku);
//                        }
//                    }

                    $wyroznij = false;
                    if ($sku_wyroznione == $item->getSku() && $order_id_wyroznione == $order->getIncrementId()) {
                        $wyroznij = true;
                    } else if ($sku_wyroznione == $item->getSku() && empty($order_id_wyroznione)) {
                        $wyroznij = true;
                    }

                    if (isset($items[$item->getSku()])) {
                        $items[$item->getItemId()] = [
                            'qty_ordered' => (int)$item->getQtyOrdered() + (int)$items[$item->getSku()]['qty_ordered'],
                            'qty_completed' => (int)$item->getQtyCompleted() + (int)$items[$item->getSku()]['qty_completed'],
                            'qty_production' => (int)$item->getQtyProduction() + (int)$items[$item->getSku()]['qty_production'],
                            'qty_production_completed' => (int)$item->getQtyProductionCompleted() + (int)$items[$item->getSku()]['qty_production_completed'],
                            'qty_reserved' => (int)$item->getQtyReserved() + (int)$items[$item->getSku()]['qty_reserved'],
                            'qty_reserved_completed' => (int)$item->getQtyReservedCompleted() + (int)$items[$item->getSku()]['qty_reserved_completed'],
                            'qty_pack' => (int)$item->getQtyPack() + (int)$items[$item->getSku()]['qty_pack'],
                            'product_name' => $item->getName(),
                            'sku' => $item->getSku(),
                            'item_id' => $item->getItemId(),
                            'ean' => (isset($product)) ? $product->getEan() : "",
                            'nazwa_bazowa' => (isset($product)) ? $product->getNazwaBazowa() : "",
                            'wyroznij' => $wyroznij,
                            'bledny_produkt' => (isset($product)) ? "" : "brak-sku",
                            'product_type' => ($item->getParentItem() && $item->getParentItem()->getProductType() == 'bundle') ? 'bundle' : ''
                        ];
                        $status_kompletacji = ($status_kompletacji && $items[$item->getSku()]['qty_pack'] == $items[$item->getSku()]['qty_completed']) ? true : false;
                        $status_pakowania = ($status_kompletacji && $items[$item->getSku()]['qty_pack'] == $items[$item->getSku()]['qty_ordered']) ? true : false;
                    } else {
                        $items[$item->getItemId()] = [
                            'qty_ordered' => (int)$item->getQtyOrdered(),
                            'qty_completed' => (int)$item->getQtyCompleted(),
                            'qty_production' => (int)$item->getQtyProduction(),
                            'qty_production_completed' => (int)$item->getQtyProductionCompleted(),
                            'qty_reserved' => (int)$item->getQtyReserved(),
                            'qty_reserved_completed' => (int)$item->getQtyReservedCompleted(),
                            'qty_pack' => (int)$item->getQtyPack(),
                            'product_name' => $item->getName(),
                            'sku' => $item->getSku(),
                            'item_id' => $item->getItemId(),
                            'ean' => (isset($product)) ? $product->getEan() : "",
                            'nazwa_bazowa' => (isset($product)) ? $product->getNazwaBazowa() : "",
                            'wyroznij' => $wyroznij,
                            'bledny_produkt' => (isset($product)) ? "" : "brak-sku",
                            'product_type' => ($item->getParentItem() && $item->getParentItem()->getProductType() == 'bundle') ? 'bundle' : ''
                        ];
                        $status_kompletacji = ($status_kompletacji && $item->getQtyOrdered() == $item->getQtyCompleted()) ? true : false;
                        $status_pakowania = ($status_kompletacji && $item->getQtyOrdered() == $item->getQtyPack()) ? true : false;
                    }
                    if ($items[$item->getItemId()]['qty_production'] == 0 && $items[$item->getItemId()]['qty_reserved'] == 0) $items[$item->getItemId()]['qty_production'] = $items[$item->getItemId()]['qty_ordered'];
                }
                $data[$order->getIncrementId()]['items'] = $items;
                $data[$order->getIncrementId()]['status_pakowania'] = $status_pakowania;
                $data[$order->getIncrementId()]['status_kompletacji'] = $status_kompletacji;
                $data[$order->getIncrementId()]['order_id'] = $order->getId();
                $data[$order->getIncrementId()]['order_status'] = $order->getStatus();
                $data[$order->getIncrementId()]['order_status_label'] = $order->getStatusLabel();
                $data[$order->getIncrementId()]['order_state'] = $order->getState();
                $data[$order->getIncrementId()]['data_zakupu'] = $order->getCreatedAt();
                $data[$order->getIncrementId()]['shipment_label'] = $order->getShipmentLabel();
            }
        }
        return $data;
    }

    protected function getPrinterName()
    {
        $curentUsername = $this->adminSession->getUser()->getUsername();
        $usernameDrukarka1 = (string)$this->config->getDrukarka1Username();
        $usernameDrukarka2 = (string)$this->config->getDrukarka2Username();
        $DrukarkaNazwa1 = (string)$this->config->getDrukarka1Nazwa();
        $DrukarkaNazwa2 = (string)$this->config->getDrukarka2Nazwa();
        switch ($curentUsername) {
            case $usernameDrukarka1:
                return $DrukarkaNazwa1;
            case $usernameDrukarka2:
                return $DrukarkaNazwa2;
            default:
                return $DrukarkaNazwa1;
        }
    }

    protected function czyDrukowacAutomatycznieKodQR($sku, $order_id)
    {
        $curentUsername = $this->adminSession->getUser()->getUsername();
        $usernameDrukarka1 = (string)$this->config->getDrukarka1Username();
        $usernameDrukarka2 = (string)$this->config->getDrukarka2Username();
        if ($curentUsername == $usernameDrukarka1 && !empty($order_id) && !empty($sku) || $curentUsername == $usernameDrukarka2 && !empty($order_id) && !empty($sku)) {
            return 1;
        } else if ($curentUsername == $usernameDrukarka1 && !empty($sku) || $curentUsername == $usernameDrukarka2 && !empty($sku)) {
            return 2;
        } else {
            return 0;
        }
    }


    private function getInvoiceCollection(array $increment_ids = [])
    {
        $collection = $this->orderCollectionFactory->create()
            ->addAttributeToSelect('*');
        $collection->addFieldToFilter('increment_id', ['in' => $increment_ids]);
        $collection->addFieldToFilter('status', ['in' => $this->getOrderStatusToFilter(true)]);
        $collection->setOrder('created_at', 'ASC');

        return $collection;
    }

    public
    function changeOrderStatus($order, $status)
    {

        $orderStateProcessing = \Magento\Sales\Model\Order::STATE_PROCESSING;
        $order->setState($orderStateProcessing);
        $order->setStatus($status);
        $order->save();

    }

    public function getOrderByIncrementId($orderIncrementId)
    {
        return $this->orderFactory->create()->loadByIncrementId($orderIncrementId);
    }

    public function getOrderStatusToFilter($array = false)
    {
        if ($array) {
            return [/*$this->config->getOrderStatusDoZleceniaProd(), $this->config->getOrderStatusDoWysylki(),*/ $this->config->getOrderStatusDoProdukcji(), $this->config->getOrderStatusDoPakowania()];
        } else {
//            return "'{$this->config->getOrderStatusDoZleceniaProd()}','{$this->config->getOrderStatusDoWysylki()}','{$this->config->getOrderStatusDoProdukcji()}','{$this->config->getOrderStatusDoPakowania()}'";
            return "'{$this->config->getOrderStatusDoProdukcji()}','{$this->config->getOrderStatusDoPakowania()}'";
        }

    }

    public function getOrderShippingToFilter($detal = 'true', $pozostale = 'false')
    {
        $metody =  $this->config->getOrderDeliveryFilterPozostale();
        $m = explode(",", $metody);
        $filtr = '';
        if ($detal == 'true' && $pozostale == 'true') {
            $filtr = '';
        }
        if ($detal == 'true' && $pozostale == 'false') {
            if(is_array($m)) {
                $filtr = " AND so.shipping_method NOT IN ('" . implode("','", $m) . "','amstorepickup_amstorepickup') ";
            }else{
                $filtr = " AND so.shipping_method NOT IN ('".$m."','amstorepickup_amstorepickup') ";
            }
        }
        if ($detal == 'false' && $pozostale == 'true') {
            if(is_array($m)) {
                $filtr = " AND so.shipping_method IN ('" . implode("','", $m) . "') ";
            }else{
                $filtr = " AND so.shipping_method IN ('".$m."') ";
            }
        }
        if ($detal == 'false' && $pozostale == 'false') {
            $filtr = " AND so.shipping_method IN ('-') ";
        }




        file_put_contents("_method.txt", $filtr);
        return $filtr;
    }


}

