<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Controller\Adminhtml\Zpm;


class SaveMass extends \Magento\Backend\App\Action
{

    protected $resultPageFactory;
    protected $jsonHelper;
    protected $orders;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Kowal\WFirma\Helper\WarehouseDocumentData $warehouseDocumentData
     * @param \Magento\Framework\App\Response\Http\FileFactory $fileFactory
     * @param \Magento\Framework\Filesystem\DirectoryList $directoryList
     * @param \Magento\Framework\Filesystem\Io\File $file
     * @param \Magento\Framework\Controller\Result\RawFactory $resultRawFactory
     * @param \Magento\Framework\Session\SessionManagerInterface $session
     * @param \Magento\Sales\Model\OrderFactory $orderFactory
     * @param \Magento\Sales\Model\Convert\Order $convertOrder
     * @param \Magento\Catalog\Model\ProductRepository $productRepository
     * @param \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
     * @param \Kowal\WFirma\lib\Config $config
     * @param \Kowal\WFirma\lib\Query $query
     */
    public function __construct(
        \Magento\Backend\App\Action\Context                    $context,
        \Magento\Framework\View\Result\PageFactory             $resultPageFactory,
        \Magento\Framework\Json\Helper\Data                    $jsonHelper,
        \Psr\Log\LoggerInterface                               $logger,
        \Kowal\WFirma\Helper\WarehouseDocumentData             $warehouseDocumentData,
        \Magento\Framework\App\Response\Http\FileFactory       $fileFactory,
        \Magento\Framework\Filesystem\DirectoryList            $directoryList,
        \Magento\Framework\Filesystem\Io\File                  $file,
        \Magento\Framework\Controller\Result\RawFactory        $resultRawFactory,
        \Magento\Framework\Session\SessionManagerInterface     $session,
        \Magento\Sales\Model\OrderFactory                      $orderFactory,
        \Magento\Sales\Model\Convert\Order                     $convertOrder,
        \Magento\Catalog\Model\ProductRepository               $productRepository,
        \Magento\Framework\App\Request\DataPersistorInterface  $dataPersistor,
        \Kowal\WFirma\lib\Config                               $config,
        \Kowal\WFirma\lib\Query                                $query,
        \Kowal\WFirma\Controller\Adminhtml\Kompletacja\SaveNew $saveNew
    )
    {
        $this->resultPageFactory = $resultPageFactory;
        $this->jsonHelper = $jsonHelper;
        $this->logger = $logger;
        $this->warehouseDocumentData = $warehouseDocumentData;

        $this->fileFactory = $fileFactory;
        $this->directoryList = $directoryList;
        $this->file = $file;
        $this->resultRawFactory = $resultRawFactory;
        $this->session = $session;
        $this->orderFactory = $orderFactory;
        $this->convertOrder = $convertOrder;
        $this->productRepository = $productRepository;
        $this->dataPersistor = $dataPersistor;
        $this->config = $config;
        $this->query = $query;
        $this->saveNew = $saveNew;
        $this->fileName = 'zlecenie_produkcyjne_' . date('Ymd_His') . '.csv';

        parent::__construct($context);
    }

    protected function _isAllowed()
    {
        return true;
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public
    function execute()
    {
        try {

            $this->session->start();
            $this->orders = $this->session->getZpmOrders();
            $this->session->unsZpmOrders();

            $data = $this->getRequest()->getPostValue();


            foreach ($this->orders as $order) {
                if ($order_ = $this->getOrderByIncrementId($order['increment_id'])) {
                    $this->changeOrderStatus($order_, $order['order_status_to_set']);
                }
            }

            $zlecenie_id = $this->saveDokuments();

            /**
             * Przesunięcie Rezerwacji jeśli są
             */

            if ($rw_wfirma = $this->createRW()) {
                if ($pw_wfirma = $this->createPW()) {

                }
            }else{
                $pw_wfirma = false;
            }


            $this->savewFirmaDocs($zlecenie_id, $pw_wfirma, $rw_wfirma);

            $csv_content = $this->saveArrayToCsv($this->getZlecenieProdukcyjneData($zlecenie_id));

            $modelZlecenie = $this->_objectManager->create(\Kowal\WFirma\Model\Zlecenie::class)->load($zlecenie_id);
            $modelZlecenie->setOpis($csv_content);
            $modelZlecenie->save();

            $this->fileFactory->create(
                $this->fileName,
                $csv_content,
                \Magento\Framework\App\Filesystem\DirectoryList::MEDIA
            );

            $resultRaw = $this->resultRawFactory->create();
            return $resultRaw;

        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            return $this->jsonResponse($e->getMessage());
        } catch (\Exception $e) {
            $this->logger->critical($e);
            return $this->jsonResponse($e->getMessage());
        }
    }

    private function saveZlecenie($data)
    {
        if (is_array($data)) {
            $modelZlecenie = $this->_objectManager->create(\Kowal\WFirma\Model\Zlecenie::class)->load(null);
            $dataZ = [];
            $dataZ['data_zlecenia'] = date('Y-m-d');
//            $dataZ['wf_pw_id'] = (isset($wFirmaPw['id'])) ? $wFirmaPw['id'] : "";
//            $dataZ['wf_rw_id'] = (isset($wFirmaRw['id'])) ? $wFirmaRw['id'] : "";
//            $dataZ['wf_pw_fullnumber'] = (isset($wFirmaPw['fullnumber'])) ? $wFirmaPw['fullnumber'] : "";
//            $dataZ['wf_rw_fullnumber'] = (isset($wFirmaRw['fullnumber'])) ? $wFirmaRw['fullnumber'] : "";
            $modelZlecenie->setData($dataZ);


            try {
                $modelZlecenie->save();

                // czyscimy poprzednie zapisy produkcji i rezerwacji
                $orders_to_reset = [];
                foreach ($data as $zlecenieItem) {
                    $orders_to_reset[] = $zlecenieItem['order_id'];
                }
                $this->query->resetOrderItemQty($orders_to_reset, $this->saveNew->getOrderStatusToFilter());

                foreach ($data as $zlecenieItem) {
                    $productName = "";
                    if ($this->query->checkIfSkuExists($zlecenieItem['sku'])){
                        if ($product = $this->productRepository->get($zlecenieItem['sku'])) {
                            $productName = $product->getNazwaBazowa();
                        }
                    }
                    $modelItems = $this->_objectManager->create(\Kowal\WFirma\Model\ZlecenieItems::class)->load(null);
                    $dataItens = [
                        'zlecenie_id' => $modelZlecenie->getId(),
                        'sku' => $zlecenieItem['sku'],
                        "nazwa" => $productName,
                        'ilosc' => $zlecenieItem['qty'],
                        'order_id' => $zlecenieItem['order_id'],
                        "parametry" => $zlecenieItem['parametry'],
                        "typ_zlecenia" => $zlecenieItem['typ_zlecenia'],
                        "data_zlecenia" => $dataZ['data_zlecenia']
                    ];
                    $modelItems->setData($dataItens);
                    $modelItems->save();




                    if ($zlecenieItem['typ_zlecenia'] == "produkcja") {
                        $this->query->updateOrderItemProductionQty($zlecenieItem['order_id'], $zlecenieItem['sku'], $zlecenieItem['qty'], $this->saveNew->getOrderStatusToFilter());
                    }

                    if ($zlecenieItem['typ_zlecenia'] == "przesuniecie") {

                        $this->query->updateOrderItemReservedQty($zlecenieItem['order_id'], $zlecenieItem['sku'], $zlecenieItem['qty'], $this->saveNew->getOrderStatusToFilter());
                    }
                }


                $this->dataPersistor->clear('kowal_wfirma_zlecenie');
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the Zlecenie.'));
            }
        }
        return $modelZlecenie->getId();
    }

    /**
     * $wFirmaPw = ['doc' => $rw_wfirma, 'store_id' => $store_id, 'type' => 'RW']
     * $rw_wfirma = ['id' => '999999', 'fullnumber' => 'PW 999/']
     * @param $zlecenie_id
     * @param $wFirmaPw
     * @param $wFirmaRw
     * @return void
     */
    private function savewFirmaDocs($zlecenie_id, $wFirmaPw, $wFirmaRw){
        if(is_array($wFirmaPw)){
            foreach ($wFirmaPw as $doc){
                $this->query->saveWfirmaDocsIdsFromZlecenie([
                    'zlecenie_id' => $zlecenie_id,
                    'store_id' => $doc['store_id'],
                    'type' => $doc['type'],
                    'wf_id' => $doc['doc']['id'],
                    'fullnumber' => $doc['doc']['fullnumber'],
                    'order_id' => $doc['order_id']
                ]);
            }
        }
        if(is_array($wFirmaRw)){
            foreach ($wFirmaRw as $doc){
                $this->query->saveWfirmaDocsIdsFromZlecenie([
                    'zlecenie_id' => $zlecenie_id,
                    'store_id' => $doc['store_id'],
                    'type' => $doc['type'],
                    'wf_id' => $doc['doc']['id'],
                    'fullnumber' => $doc['doc']['fullnumber'],
                    'order_id' => $doc['order_id']
                ]);
            }
        }
    }

    /**
     * chyba do usuniecia
     * @param $data
     * @return mixed
     */
    private function saveKompletacja($data)
    {
        if (is_array($data)) {
            $modelKompletacja = $this->_objectManager->create(\Kowal\WFirma\Model\Kompletacja::class)->load(null);
            $dataZ = [];
            $dataZ['data_kompletacji'] = date('Y-m-d');
            $modelKompletacja->setData($dataZ);


            try {
                $modelKompletacja->save();

                foreach ($data as $item) {
                    if ($product = $this->productRepository->get($item['sku'])) {
                        $modelItems = $this->_objectManager->create(\Kowal\WFirma\Model\KompletacjaItems::class)->load(null);
                        $dataItens = ['kompletacja_id' => $modelKompletacja->getId(), 'sku' => $item['sku'], "nazwa" => $product->getName(), 'ilosc' => $item['qty'], 'order_id' => $item['order_id'], "z_magazynu" => $this->getRequest()->getParam('magazyn', null), "do_magazynu" => $this->getRequest()->getParam('magazyn_kompletacja', null)];
                        $modelItems->setData($dataItens);
                        $modelItems->save();

                        $this->query->changeOrderItemProductionStatus($item['order_id'], $item['sku'], 'Do przeniesienia');
                    }
                }

                $this->dataPersistor->clear('kowal_wfirma_kompletacja');


            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the Kompletacja.'));
            }
        }
        return $modelKompletacja->getId();
    }

    /**
     * Create json response
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public
    function jsonResponse($response = '')
    {
        return $this->getResponse()->representJson(
            $this->jsonHelper->jsonEncode($response)
        );
    }

    public function getOrderByIncrementId($orderIncrementId)
    {
        return $this->orderFactory->create()->loadByIncrementId($orderIncrementId);
    }


    private
    function getFileName($i)
    {
        $this->var = $this->directoryList->getPath('var');

        if (!file_exists($this->var . DIRECTORY_SEPARATOR . 'tmp' . DIRECTORY_SEPARATOR . 'produkcja')) {
            $this->file->mkdir($this->var . DIRECTORY_SEPARATOR . 'tmp' . DIRECTORY_SEPARATOR . 'produkcja', 0775);
        }
        $this->path_to_file = $this->var . DIRECTORY_SEPARATOR . 'tmp' . DIRECTORY_SEPARATOR . 'produkcja' . DIRECTORY_SEPARATOR . 'zlecenie_produkcyjne_' . date('Ymd_His') . '_' . $i . '.csv';
        return $this;
    }

    private
    function saveArrayToCsv($array)
    {
//        $fp = fopen($this->path_to_file, 'w');
        $fp = fopen('php://temp', 'rw');
        foreach ($array as $fields) {
            fputcsv($fp, $fields);
        }
        rewind($fp);
        $csv = stream_get_contents($fp);
        fclose($fp);
        return $csv;
    }

    private function getRwData()
    {

        $data = [];
        foreach ($this->orders as $order) {
            foreach ($order['products'] as $product) {
                if ($product['quantity_reserved'] > 0) {
                    $data[] = ["sku" => $product['sku'], "qty" => $product['quantity_reserved']];
                }
            }
        }
        return $data;
    }

    private function getZlecenieProdukcyjneData($zlecenie_id)
    {
        $data = [];
        $data_zlecenia = date('Y-m-d');
        $data[] = ['ZLECENIE PRODUKCUJNE:', $zlecenie_id, null, null, null, null];
        foreach ($this->orders as $order) {
            foreach ($order['products'] as $product) {
                if ($product['quantity_produced'] > 0) {
                    $data[] = [$data_zlecenia, $order['increment_id'], $product['sku'], $product['quantity_produced'], $product['name'], $product['parametry']];
                }
            }
        }



        $data[] = ['', '', '', '', '', ''];
        $data[] = ['--------', '---------', '----------', '--------', '---------', '----------'];
        $data[] = ['', '', '', '', '', ''];
        $data[] = ['Produkty do przesunięcia', '', null, null, null, null];
        foreach ($this->orders as $order) {
            foreach ($order['products'] as $product) {
                if ($product['quantity_reserved'] > 0) {
                    $data[] = [$data_zlecenia, $order['increment_id'], $product['sku'], $product['quantity_reserved'], $product['name'], $product['parametry']];
                }
            }
        }

        return $data;
    }

    private function saveDokuments()
    {
        $data_zlecenie = [];

        foreach ($this->orders as $order) {
            foreach ($order['products'] as $product) {
                if ($product['quantity_produced'] > 0) {
                    $data_zlecenie[] = ['order_id' => $order['increment_id'], 'sku' => $product['sku'], 'qty' => $product['quantity_produced'], 'parametry' => $product['parametry'], 'typ_zlecenia' => 'produkcja'];
                }
            }
        }

        foreach ($this->orders as $order) {
            foreach ($order['products'] as $product) {
                if ($product['quantity_reserved'] > 0) {
                    $data_zlecenie[] = ['order_id' => $order['increment_id'], 'sku' => $product['sku'], 'qty' => $product['quantity_reserved'], 'parametry' => $product['parametry'], 'typ_zlecenia' => 'przesuniecie'];
                }
            }
        }

        $zlecenie_id = $this->saveZlecenie($data_zlecenie);



        return $zlecenie_id;
    }

    /**
     * Poniważ nie mamy MM rozchdowuję RW towar dostępny na magazynie głównym, który będzie przyjęty do mag. rezerwacji
     * @param $order
     * @return bool|mixed
     */
    private function createRW()
    {

        $docs_rw = [];

        foreach ($this->orders as $order) {

//            file_put_contents("__order_.txt",print_r($order,true),FILE_APPEND);

            $store_id = (isset($order['store_id'])) ? $order['store_id'] : 0;
            if ($magazyny = $this->config->getMagazynGlownyWFirma($store_id)) {
               $magazynyArray = explode(",", $magazyny);
                if(is_array($magazynyArray)) {
                    $magazyn_id = reset($magazynyArray);
                }else{
                    $magazyn_id = $magazynyArray;
                }
                $data_ = [];
                foreach ($order['products'] as $product) {
                    if ($product['quantity_reserved'] > 0) {
                        // MM (nasze PW/RW ) - po cenach z wFirmy
//                        $data_[] = ["sku" => $product['sku'], "qty" => $product['quantity_reserved'], 'price' => $this->query->getKosztNowy($product['sku'], 0)];
                        $data_[] = ["sku" => $product['sku'], "qty" => $product['quantity_reserved']];
                    }
                }
                if (count($data_)) {
                    $date = $order['created_at'];
                    $description = $order['increment_id'];

//                    file_put_contents("__order_.txt",print_r(['magazyn_id'=>$magazyn_id,'store_id'=>$store_id,'description' => $description],true),FILE_APPEND);

                    if ($rw_wfirma = $this->warehouseDocumentData->setData($date, $date, $data_, "RW", $magazyn_id, "warehouse_document_r_w", null, null, null, $description, true, $store_id)) {
                        $docs_rw[] = ['doc' => $rw_wfirma, 'store_id' => $store_id, 'type' => 'RW', 'order_id' => $order['increment_id']];
                    }
                }
            }
        }

        return  (count($docs_rw)) ? $docs_rw : false;
    }

    /**
     * Przyjęcie PW na magazyn rezerwacji towarów dostępnych na magazynie głównym. Z magazynu rezerwacji beda pobrane
     * gdy będzie przyjmowany towar z produkcji oraz z magazynu głownego i przeworzony do kompletacji zamówień
     * @param $order
     * @return false|mixed
     */
    private function createPW()
    {
        $docs_pw = [];
        foreach ($this->orders as $order) {
            $store_id = (isset($order['store_id'])) ? $order['store_id'] : 0;
            if ($magazyn_id = $this->config->getMagazynRezerwacji($store_id)) {
                $data_ = [];
                foreach ($order['products'] as $product) {
                    if ($product['quantity_reserved'] > 0) {
                        // MM (nasze PW/RW ) - po cenach z wFirmy
//                        $data_[] = ["sku" => $product['sku'], "qty" => $product['quantity_reserved'], 'price' => $this->query->getKosztNowy($product['sku'], 0)];
                        $data_[] = ["sku" => $product['sku'], "qty" => $product['quantity_reserved']];
                    }
                }

                if (count($data_)) {
                    $date = $order['created_at'];
                    $description = $order['increment_id'];
                    if ($pw_wfirma = $this->warehouseDocumentData->setData($date, $date, $data_, "PW", $magazyn_id, "warehouse_document_p_w", null, null, null, $description, true, $store_id)) {
                        $docs_pw[] = ['doc' => $pw_wfirma, 'store_id' => $store_id, 'type' => 'PW','order_id' => $order['increment_id']];
                    }
                }
            }
        }


        return  (count($docs_pw)) ? $docs_pw : false;
    }

    /**
     * @param $order
     * @param $order_data
     * @return bool|void
     */
    private
    function createShipment($order, $order_data)
    {
        if (!$order->canShip()) {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('You can\'t create an shipment.')
            );
        }
        $items_with_stock = [];
        foreach ($order_data['products'] as $product) {
            if ($product['quantity_reserved'] > 0) {
                $items_with_stock[$product['sku']] = $product['quantity_reserved'];
            }
        }

        // Initialize the order shipment object
//        $convertOrder = $this->_objectManager->create('Magento\Sales\Model\Convert\Order');
        $shipment = $this->convertOrder->toShipment($order);

        if (count($items_with_stock)) {

            foreach ($order->getAllItems() as $orderItem) {
                // Check if order item has qty to ship or is virtual
                if (!$orderItem->getQtyToShip() || $orderItem->getIsVirtual()) {
                    continue;
                }

                if (array_key_exists($orderItem->getSku(), $items_with_stock) && $items_with_stock[$orderItem->getSku()] <= $orderItem->getQtyToShip()) {
                    $qtyShipped = $items_with_stock[$orderItem->getSku()];
                    // Create shipment item with qty
                    $shipmentItem = $this->convertOrder->itemToShipmentItem($orderItem)->setQty($qtyShipped);

                    // Add shipment item to shipment
                    $shipment->addItem($shipmentItem);
                }
            }

            // Register shipment
            $shipment->register();

            $shipment->getOrder()->setIsInProcess(true);

            try {
                // Save created shipment and order
                $shipment->save();
                $shipment->getOrder()->save();

                // Send email
//                $this->_objectManager->create('Magento\Shipping\Model\ShipmentNotifier')
//                    ->notify($shipment);

//                $shipment->save();

                return true;
            } catch (\Exception $e) {
                throw new \Magento\Framework\Exception\LocalizedException(
                    __($e->getMessage())
                );
            }
        }
    }


    public
    function changeOrderStatus($order, $status)
    {

        $orderStateProcessing = \Magento\Sales\Model\Order::STATE_PROCESSING;
        $order->setState($orderStateProcessing);
        $order->setStatus($status);
        $order->save();

    }
}

