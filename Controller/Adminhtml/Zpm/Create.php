<?php
/**
 * Copyright © Kowal All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Controller\Adminhtml\Zpm;

class Create extends \Magento\Sales\Controller\Adminhtml\Order\AbstractMassAction implements \Magento\Framework\App\Action\HttpPostActionInterface
{
    /**
     * Authorization level of a basic admin session
     */
    const ADMIN_RESOURCE = 'Magento_Sales::cancel';
    /**
     * @var OrderManagementInterface|\Magento\Sales\Api\OrderManagementInterface|null
     */
    private $orderManagement;

    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Ui\Component\MassAction\Filter $filter
     * @param \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $collectionFactory
     * @param \Kowal\WFirma\Helper\ZpmData $zpmData
     * @param \Magento\Framework\Session\SessionManagerInterface $session
     */
    public function __construct(
        \Magento\Backend\App\Action\Context                        $context,
        \Magento\Ui\Component\MassAction\Filter                    $filter,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $collectionFactory,
        \Kowal\WFirma\Helper\ZpmData                               $zpmData,
        \Magento\Framework\Session\SessionManagerInterface         $session
    )
    {
        parent::__construct($context, $filter);
        $this->collectionFactory = $collectionFactory;
        $this->zpmData = $zpmData;
        $this->session = $session;
    }

    protected function _isAllowed()
    {
//        return $this->_authorization->isAllowed('Kowal_WFirma::create');
        return true;
    }

    /**
     * Cancel selected orders
     *
     * @param AbstractCollection $collection
     * @return \Magento\Backend\Model\View\Result\Redirect
     */
    protected function massAction(\Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection $collection)
    {


        if ($orders = $this->zpmData->setData($collection)) {
            $this->session->start();
            $this->session->setZpmOrders($orders);
        }


        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath('kowal_wfirma/Zpm/Mass');
        return $resultRedirect;
    }


    public function getCountryname($countryCode)
    {
        $country = $this->countryFactory->create()->loadByCode($countryCode);
        return $country->getName();
    }
}

