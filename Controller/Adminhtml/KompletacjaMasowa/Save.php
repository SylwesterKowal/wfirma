<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Controller\Adminhtml\KompletacjaMasowa;


class Save extends \Magento\Backend\App\Action
{

    protected $resultPageFactory;
    protected $jsonHelper;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Kowal\WFirma\Helper\WarehouseDocumentData $warehouseDocumentData
     */
    public function __construct(
        \Magento\Backend\App\Action\Context        $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Json\Helper\Data        $jsonHelper,
        \Psr\Log\LoggerInterface                   $logger,
        \Magento\Sales\Model\Order                 $order,
        \Kowal\WFirma\lib\Config                   $config,
        \Kowal\WFirma\lib\Query                    $query
    )
    {
        $this->resultPageFactory = $resultPageFactory;
        $this->jsonHelper = $jsonHelper;
        $this->logger = $logger;
        $this->order = $order;
        $this->config = $config;
        $this->query = $query;
        parent::__construct($context);
    }

    protected function _isAllowed()
    {
        return true;
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        try {
            $data = $this->getRequest()->getPostValue();
            if ($data) {
                $data_ = $this->getArrayForCsv();
                foreach ($data_ as $order_id) {

                    if (isset($order_id['id']) && !empty($order_id['id'])) {
                        if ($order_ = $this->query->checkIfOrderExists($order_id['id'], "'{$this->config->getOrderStatusDoProdukcji()}','{$this->config->getOrderStatusDoPakowania()}'")) {
                            if ($order = $this->order->load($order_['entity_id'])) {
//                    $this->createShipment($order);

                                $this->changeOrderStatus($order, $this->config->getOrderStatusDoWysylki());


                            }
                        }
                    }
                }
                return $this->jsonResponse("Zamówienia wysłane!");
            }
            return $this->jsonResponse("Błąd wysyłania zamówień".print_r($order_,true));
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            return $this->jsonResponse($e->getMessage());
        } catch (\Exception $e) {
            $this->logger->critical($e);
            return $this->jsonResponse($e->getMessage());
        }
    }

    /**
     * Create json response
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function jsonResponse($response = '')
    {
        return $this->getResponse()->representJson(
            $this->jsonHelper->jsonEncode($response)
        );
    }

    private function getArrayForCsv()
    {
        $data = $this->getRequest()->getParam('import', null);

        if ($data) {
            $data = array_filter(array_map('trim', explode(PHP_EOL, $data)));

            $data = array_map(
                function ($row) {
                    if (preg_match("/\t/", $row)) {
                        $row = explode('	', $row);
                    } else {
                        $row = explode(',', $row);
                    }
                    $row['id'] = $row[0] ?? '';
                    unset($row[0]);


                    return $row;
                },
                $data
            );
        }

        return $data;
    }

    public
    function changeOrderStatus($order, $status)
    {

        $orderStateProcessing = \Magento\Sales\Model\Order::STATE_PROCESSING;
        $order->setState($orderStateProcessing);
        $order->setStatus($status);
        $order->save();

    }

}

