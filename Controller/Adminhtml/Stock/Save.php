<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Controller\Adminhtml\Stock;

class Save extends \Magento\Backend\App\Action
{

    protected $resultPageFactory;
    protected $jsonHelper;
    protected $good;
    protected $logger;
    protected $resultRawFactory;
    protected $fileFactory;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Kowal\WFirma\lib\Good $good
     * @param \Magento\Framework\Controller\Result\RawFactory $resultRawFactory
     * @param \Magento\Framework\App\Response\Http\FileFactory $fileFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context              $context,
        \Magento\Framework\View\Result\PageFactory       $resultPageFactory,
        \Magento\Framework\Json\Helper\Data              $jsonHelper,
        \Psr\Log\LoggerInterface                         $logger,
        \Kowal\WFirma\lib\Good                           $good,
        \Magento\Framework\Controller\Result\RawFactory  $resultRawFactory,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory
    )
    {
        $this->resultPageFactory = $resultPageFactory;
        $this->jsonHelper = $jsonHelper;
        $this->logger = $logger;
        $this->good = $good;
        $this->resultRawFactory = $resultRawFactory;
        $this->fileFactory = $fileFactory;

        parent::__construct($context);
    }

    protected function _isAllowed()
    {
        return true;
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        try {
            $data = $this->getRequest()->getPostValue();
            if ($data) {

                $magazyn_id = $this->getRequest()->getParam('magazyn', null);
                $store_id = $this->getRequest()->getParam('store', null);
                $date = date('Y-m-d');

                $this->fileName = 'stan_magazynu_' . $magazyn_id . '_' . $store_id . '_' . date('Ymd_His') . '.csv';

                $stockArray = $this->getGoodsStocks($magazyn_id, $store_id);

                $csv_content = $this->saveArrayToCsv($stockArray);

                $this->fileFactory->create(
                    $this->fileName,
                    $csv_content,
                    \Magento\Framework\App\Filesystem\DirectoryList::MEDIA
                );

                $resultRaw = $this->resultRawFactory->create();
                return $resultRaw;
            }
            return $this->jsonResponse("Błąd odczytu sanów magazynowych");
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            return $this->jsonResponse($e->getMessage());
        } catch (\Exception $e) {
            $this->logger->critical($e);
            return $this->jsonResponse($e->getMessage());
        }
    }


    public function getGoodsStocks($magazyny_wfirma, $store_id)
    {
        $stocks = [];

        try {

            $good = $this->good->createNew();

            $limit = 500;
            $good->setFindData(['parameters' => [
                'page' => 1,
                'limit' => $limit
            ]]);


            if ($products = $good->findAll($store_id)) {

                if (isset($products['parameters']['total'])) {


                    $pages = round((int)$products['parameters']['total'] / $limit, 0) + 1;


                    foreach ($products as $keyContr => $product) {
                        if (isset($product['good']['code']) && isset($stocks[$product['good']['code']])) $stocks[$product['good']['code']] = 0;
                        if (isset($product['good']['code']) && isset($product['good']['warehouse_good_parcels'])) {

                            foreach ($product['good']['warehouse_good_parcels'] as $warehouse_good_parcel) {
                                if (isset($warehouse_good_parcel['warehouse_good_parcel']['warehouse']['id']) &&  $warehouse_good_parcel['warehouse_good_parcel']['warehouse']['id'] == $magazyny_wfirma) {
                                    if (isset($stocks[$product['good']['code']])) {
                                        $curent_stock = (int)$stocks[$product['good']['code']];
                                    } else {
                                        $curent_stock = 0;
                                    }
                                    if(isset($warehouse_good_parcel['warehouse_good_parcel']['count'])) {
                                        $stocks[$product['good']['code']] = $curent_stock + (int)$warehouse_good_parcel['warehouse_good_parcel']['count'];
                                    }
                                }
                            }
                        }
                    }

                    for ($i = 2; $i <= $pages; $i++) {
                        $condition = ['parameters' => ['page' => $i, 'limit' => $limit]];
                        $this->good->setFindData($condition);
                        if ($products = $this->good->findAll($store_id)) {
                            if (is_array($products)) {
                                foreach ($products as $keyContr => $product) {

                                    if (isset($product['good']['code']) && isset($stocks[$product['good']['code']])) $stocks[$product['good']['code']] = 0;
                                    if (isset($product['good']['code']) && isset($product['good']['warehouse_good_parcels'])) {
                                        foreach ($product['good']['warehouse_good_parcels'] as $warehouse_good_parcel) {
                                        if (isset($warehouse_good_parcel['warehouse_good_parcel']['warehouse']['id']) &&  $warehouse_good_parcel['warehouse_good_parcel']['warehouse']['id'] == $magazyny_wfirma) {
                                            if (isset($stocks[$product['good']['code']])) {
                                                $curent_stock = (int)$stocks[$product['good']['code']];
                                            } else {
                                                $curent_stock = 0;
                                            }
                                            if(isset($warehouse_good_parcel['warehouse_good_parcel']['count'])) {
                                                $stocks[$product['good']['code']] = $curent_stock + (int)$warehouse_good_parcel['warehouse_good_parcel']['count'];
                                            }
                                        }
                                        }
                                    }


                                }
                            }
                        }
                    }
                }
            }
            $aStocks = [];
            if (is_array($stocks)) {
                foreach ($stocks as $sku => $stock) {
                    $aStocks[] = ['sku' => $sku, 'stock' => $stock];
                }
            }
            return $aStocks;

        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->logger->critical($e);
            return [];
        } catch (\Exception $e) {
            $this->logger->critical($e);
            return [];
        }
    }

    /**
     * Create json response
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function jsonResponse($response = '')
    {
        return $this->getResponse()->representJson(
            $this->jsonHelper->jsonEncode($response)
        );
    }

    private
    function saveArrayToCsv($array)
    {
//        $fp = fopen($this->path_to_file, 'w');
        $fp = fopen('php://temp', 'rw');
        foreach ($array as $fields) {
            fputcsv($fp, $fields);
        }
        rewind($fp);
        $csv = stream_get_contents($fp);
        fclose($fp);
        return $csv;
    }

}

