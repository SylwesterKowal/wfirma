<?php
/**
 * Copyright ©  kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Controller\Adminhtml\Products;

use Magento\Framework\Controller\ResultFactory;

class Import extends \Magento\Backend\App\Action
{

    protected $resultPageFactory;

    /**
     * Import constructor.
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Framework\UrlFactory $urlFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\UrlFactory $urlFactory
    )
    {
        $this->resultPageFactory = $resultPageFactory;
        $this->urlModel = $urlFactory->create();
        parent::__construct($context);
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);

        exec('php bin/magento kowal_wfirma:importproducts', $out);

        $url = $this->_redirect->getRefererUrl();
        $url = $this->urlModel->getUrl('catalog/product/index', ['_secure' => true]);
        $resultRedirect->setUrl($url);
        return $resultRedirect;
    }
}

