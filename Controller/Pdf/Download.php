<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Controller\Pdf;

use Magento\Framework\App\Filesystem\DirectoryList;

class Download extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Framework\App\Response\Http\FileFactory
     */
    protected $fileFactory;
    protected $resultPageFactory;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Framework\App\Response\Http\FileFactory $fileFactory
     * @param \Magento\Framework\App\RequestInterface $request
     * @param \Magento\Framework\Controller\ResultFactory $resultFactory
     * @param \Magento\Sales\Api\InvoiceRepositoryInterface $invoiceRepository
     * @param \Magento\Framework\Filesystem\Driver\File $fileDriver
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param DirectoryList $directoryList
     */
    public function __construct(
        \Magento\Framework\App\Action\Context            $context,
        \Magento\Framework\View\Result\PageFactory       $resultPageFactory,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
        \Magento\Framework\App\RequestInterface          $request,
        \Magento\Framework\Controller\ResultFactory      $resultFactory,
        \Magento\Sales\Api\InvoiceRepositoryInterface    $invoiceRepository,
        \Magento\Framework\Filesystem\Driver\File        $fileDriver,
        \Magento\Framework\Message\ManagerInterface      $messageManager,
        \Magento\Framework\App\Filesystem\DirectoryList  $directoryList,
        \Kowal\WFirma\lib\Encrypt                        $encrypt
    )
    {
        $this->resultPageFactory = $resultPageFactory;
        $this->fileFactory = $fileFactory;
        $this->request = $request;
        $this->resultFactory = $resultFactory;
        $this->invoiceRepository = $invoiceRepository;
        $this->fileDriver = $fileDriver;
        $this->messageManager = $messageManager;
        $this->directoryList = $directoryList;
        $this->encrypt = $encrypt;
        parent::__construct($context);
    }


    protected function _isAllowed()
    {
        return true;
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var Redirect $response */
        $response = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_REDIRECT);
        if ($invoice_id = $this->request->getParam('id')) {

            $this->invoice = $this->getMagentoInvoice();
            $this->order = $this->invoice->getOrder();
            $downloadName = $this->order->getIncrementId() . '_invoice.pdf';
            $filePath = 'invoices' . DIRECTORY_SEPARATOR . $downloadName;
            $fullpath = $this->directoryList->getPath('var') . '/' . $filePath;
            if ($this->fileDriver->isExists($fullpath)) {

                $content['type'] = 'filename';
                $content['value'] = $filePath;
                $content['rm'] = 0; // If you will set here 1 then, it will remove file from location.
                return $this->fileFactory->create($downloadName, $content, DirectoryList::VAR_DIR);
            } else {
                $this->messageManager->addErrorMessage('Faktura nie jest jeszcze dostępna. Proszę ponowić pobranie faktury za 10 min.');
                $response->setPath($this->_redirect->getRefererUrl());
                return $response;
            }
        } else {
            $response->setPath($this->_redirect->getRefererUrl());
            return $response;
        }


//        return $this->resultPageFactory->create();
    }

    public function getMagentoInvoice()
    {
        $invoiceId = $this->getInvoiceId();
        return $this->invoiceRepository->get($invoiceId);
    }

    public function getInvoiceId()
    {
        return $this->encrypt->decrypt($this->getRequest()->getParam('id'));
    }
}
