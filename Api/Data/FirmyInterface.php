<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Api\Data;

interface FirmyInterface
{

    const NAZWA_FIRMY = 'nazwa_firmy';
    const COMPANY_ID_WFIRMA = 'company_id_wfirma';
    const STORE_ID_MAGENTO = 'store_id_magento';
    const FIRMY_ID = 'firmy_id';
    const PARTNER_EMAIL = 'partner_email';
    const PARTNER = 'partner';

    /**
     * Get firmy_id
     * @return string|null
     */
    public function getFirmyId();

    /**
     * Set firmy_id
     * @param string $firmyId
     * @return \Kowal\WFirma\Firmy\Api\Data\FirmyInterface
     */
    public function setFirmyId($firmyId);

    /**
     * Get nazwa_firmy
     * @return string|null
     */
    public function getNazwaFirmy();

    /**
     * Set nazwa_firmy
     * @param string $nazwaFirmy
     * @return \Kowal\WFirma\Firmy\Api\Data\FirmyInterface
     */
    public function setNazwaFirmy($nazwaFirmy);

    /**
     * Get company_id_wfirma
     * @return string|null
     */
    public function getCompanyIdWfirma();

    /**
     * Set company_id_wfirma
     * @param string $companyIdWfirma
     * @return \Kowal\WFirma\Firmy\Api\Data\FirmyInterface
     */
    public function setCompanyIdWfirma($companyIdWfirma);

    /**
     * Get store_id_magento
     * @return string|null
     */
    public function getStoreIdMagento();

    /**
     * Set store_id_magento
     * @param string $storeIdMagento
     * @return \Kowal\WFirma\Firmy\Api\Data\FirmyInterface
     */
    public function setStoreIdMagento($storeIdMagento);
    /**
     * Get partner
     * @return string|null
     */
    public function getPartner();

    /**
     * Set partner
     * @param string $partner
     * @return \Kowal\WFirma\Firma\Api\Data\FirmaInterface
     */
    public function setPartner($partner);

    /**
     * Get partner_email
     * @return string|null
     */
    public function getPartnerEmail();

    /**
     * Set partner_email
     * @param string $partnerEmail
     * @return \Kowal\WFirma\Firma\Api\Data\FirmaInterface
     */
    public function setPartnerEmail($partnerEmail);
}

