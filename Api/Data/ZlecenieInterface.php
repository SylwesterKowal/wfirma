<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Api\Data;

interface ZlecenieInterface
{

    const STATUS = 'status';
    const OPIS = 'opis';
    const ZLECENIE_ID = 'zlecenie_id';
    const DATA_ZLECENIA = 'data_zlecenia';
    const WF_RW_ID = 'wf_rw_id';
    const WF_PW_ID = 'wf_pw_id';
    const WF_MM_ID = 'wf_mm_id';

    /**
     * Get zlecenie_id
     * @return string|null
     */
    public function getZlecenieId();

    /**
     * Set zlecenie_id
     * @param string $zlecenieId
     * @return \Kowal\WFirma\Zlecenie\Api\Data\ZlecenieInterface
     */
    public function setZlecenieId($zlecenieId);

    /**
     * Get status
     * @return string|null
     */
    public function getStatus();

    /**
     * Set status
     * @param string $status
     * @return \Kowal\WFirma\Zlecenie\Api\Data\ZlecenieInterface
     */
    public function setStatus($status);

    /**
     * Get data_zlecenia
     * @return string|null
     */
    public function getDataZlecenia();

    /**
     * Set data_zlecenia
     * @param string $dataZlecenia
     * @return \Kowal\WFirma\Zlecenie\Api\Data\ZlecenieInterface
     */
    public function setDataZlecenia($dataZlecenia);

    /**
     * Get opis
     * @return string|null
     */
    public function getOpis();

    /**
     * Set opis
     * @param string $opis
     * @return \Kowal\WFirma\Zlecenie\Api\Data\ZlecenieInterface
     */
    public function setOpis($opis);

    /**
     * Get wf_pw_id
     * @return string|null
     */
    public function getWfPwId();

    /**
     * Set wf_pw_id
     * @param string $wfPwId
     * @return \Kowal\WFirma\Zlecenie\Api\Data\ZlecenieInterface
     */
    public function setWfPwId($wfPwId);

    /**
     * Get wf_rw_id
     * @return string|null
     */
    public function getWfRwId();

    /**
     * Set wf_rw_id
     * @param string $wfRwId
     * @return \Kowal\WFirma\Zlecenie\Api\Data\ZlecenieInterface
     */
    public function setWfRwId($wfRwId);

    /**
     * Get wf_mm_id
     * @return string|null
     */
    public function getWfMmId();

    /**
     * Set wf_mm_id
     * @param string $wfMmId
     * @return \Kowal\WFirma\Zlecenie\Api\Data\ZlecenieInterface
     */
    public function setWfMmId($wfMmId);
}

