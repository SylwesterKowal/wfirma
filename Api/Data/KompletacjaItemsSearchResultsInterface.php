<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Api\Data;

interface KompletacjaItemsSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get KompletacjaItems list.
     * @return \Kowal\WFirma\Api\Data\KompletacjaItemsInterface[]
     */
    public function getItems();

    /**
     * Set kompletacja_id list.
     * @param \Kowal\WFirma\Api\Data\KompletacjaItemsInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

