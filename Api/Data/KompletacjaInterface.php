<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Api\Data;

interface KompletacjaInterface
{

    const STATUS = 'status';
    const OPIS = 'opis';
    const DATA_KOMPLETACJI = 'data_kompletacji';
    const KOMPLETACJA_ID = 'kompletacja_id';
    const WF_PW_ID = 'wf_pw_id';
    const WF_RW_ID = 'wf_rw_id';
    const WF_MM_ID = 'wf_mm_id';

    /**
     * Get kompletacja_id
     * @return string|null
     */
    public function getKompletacjaId();

    /**
     * Set kompletacja_id
     * @param string $kompletacjaId
     * @return \Kowal\WFirma\Kompletacja\Api\Data\KompletacjaInterface
     */
    public function setKompletacjaId($kompletacjaId);

    /**
     * Get status
     * @return string|null
     */
    public function getStatus();

    /**
     * Set status
     * @param string $status
     * @return \Kowal\WFirma\Kompletacja\Api\Data\KompletacjaInterface
     */
    public function setStatus($status);

    /**
     * Get data_kompletacji
     * @return string|null
     */
    public function getDataKompletacji();

    /**
     * Set data_kompletacji
     * @param string $dataKompletacji
     * @return \Kowal\WFirma\Kompletacja\Api\Data\KompletacjaInterface
     */
    public function setDataKompletacji($dataKompletacji);

    /**
     * Get opis
     * @return string|null
     */
    public function getOpis();

    /**
     * Set opis
     * @param string $opis
     * @return \Kowal\WFirma\Kompletacja\Api\Data\KompletacjaInterface
     */
    public function setOpis($opis);

    /**
     * Get wf_pw_id
     * @return string|null
     */
    public function getWfPwId();

    /**
     * Set wf_pw_id
     * @param string $wfPwId
     * @return \Kowal\WFirma\Kompletacja\Api\Data\KompletacjaInterface
     */
    public function setWfPwId($wfPwId);

    /**
     * Get wf_rw_id
     * @return string|null
     */
    public function getWfRwId();

    /**
     * Set wf_pw_id
     * @param string $wfRwId
     * @return \Kowal\WFirma\Kompletacja\Api\Data\KompletacjaInterface
     */
    public function setWfRwId($wfRwId);

    /**
     * Get wf_mm_id
     * @return string|null
     */
    public function getWfMmId();

    /**
     * Set wf_mm_id
     * @param string $wfMmId
     * @return \Kowal\WFirma\Kompletacja\Api\Data\KompletacjaInterface
     */
    public function setWfMmId($wfMmId);
}

