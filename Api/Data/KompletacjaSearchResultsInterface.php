<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Api\Data;

interface KompletacjaSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get Kompletacja list.
     * @return \Kowal\WFirma\Api\Data\KompletacjaInterface[]
     */
    public function getItems();

    /**
     * Set status list.
     * @param \Kowal\WFirma\Api\Data\KompletacjaInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

