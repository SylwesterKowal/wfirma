<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Api\Data;

interface ZlecenieItemsSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get ZlecenieItems list.
     * @return \Kowal\WFirma\Api\Data\ZlecenieItemsInterface[]
     */
    public function getItems();

    /**
     * Set zlecenie_id list.
     * @param \Kowal\WFirma\Api\Data\ZlecenieItemsInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

