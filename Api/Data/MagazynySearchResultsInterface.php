<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Api\Data;

interface MagazynySearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get Magazyny list.
     * @return \Kowal\WFirma\Api\Data\MagazynyInterface[]
     */
    public function getItems();

    /**
     * Set mag_wfirma list.
     * @param \Kowal\WFirma\Api\Data\MagazynyInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

