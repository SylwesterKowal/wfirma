<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Api\Data;

interface ZlecenieSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get Zlecenie list.
     * @return \Kowal\WFirma\Api\Data\ZlecenieInterface[]
     */
    public function getItems();

    /**
     * Set status list.
     * @param \Kowal\WFirma\Api\Data\ZlecenieInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

