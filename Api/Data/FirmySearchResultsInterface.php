<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Api\Data;

interface FirmySearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get Firmy list.
     * @return \Kowal\WFirma\Api\Data\FirmyInterface[]
     */
    public function getItems();

    /**
     * Set nazwa_firmy list.
     * @param \Kowal\WFirma\Api\Data\FirmyInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

