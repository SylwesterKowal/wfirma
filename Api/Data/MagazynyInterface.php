<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Api\Data;

interface MagazynyInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const MAGAZYNY_ID = 'magazyny_id';
    const MAG_MAGENTO = 'mag_magento';
    const MAG_WFIRMA = 'mag_wfirma';

    /**
     * Get magazyny_id
     * @return string|null
     */
    public function getMagazynyId();

    /**
     * Set magazyny_id
     * @param string $magazynyId
     * @return \Kowal\WFirma\Api\Data\MagazynyInterface
     */
    public function setMagazynyId($magazynyId);

    /**
     * Get mag_wfirma
     * @return string|null
     */
    public function getMagWfirma();

    /**
     * Set mag_wfirma
     * @param string $magWfirma
     * @return \Kowal\WFirma\Api\Data\MagazynyInterface
     */
    public function setMagWfirma($magWfirma);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Kowal\WFirma\Api\Data\MagazynyExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \Kowal\WFirma\Api\Data\MagazynyExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Kowal\WFirma\Api\Data\MagazynyExtensionInterface $extensionAttributes
    );

    /**
     * Get mag_magento
     * @return string|null
     */
    public function getMagMagento();

    /**
     * Set mag_magento
     * @param string $magMagento
     * @return \Kowal\WFirma\Api\Data\MagazynyInterface
     */
    public function setMagMagento($magMagento);
}

