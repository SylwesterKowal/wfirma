<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Api\Data;

interface KompletacjaItemsInterface
{

    const Z_MAGAZYNU = 'z_magazynu';
    const ORDER_ID = 'order_id';
    const DO_MAGAZYNU = 'do_magazynu';
    const KOMPLETACJA_ID = 'kompletacja_id';
    const KOMPLETACJAITEMS_ID = 'kompletacjaitems_id';
    const NAZWA = 'nazwa';
    const ILOSC = 'ilosc';
    const SKU = 'sku';

    /**
     * Get kompletacjaitems_id
     * @return string|null
     */
    public function getKompletacjaitemsId();

    /**
     * Set kompletacjaitems_id
     * @param string $kompletacjaitemsId
     * @return \Kowal\WFirma\KompletacjaItems\Api\Data\KompletacjaItemsInterface
     */
    public function setKompletacjaitemsId($kompletacjaitemsId);

    /**
     * Get kompletacja_id
     * @return string|null
     */
    public function getKompletacjaId();

    /**
     * Set kompletacja_id
     * @param string $kompletacjaId
     * @return \Kowal\WFirma\KompletacjaItems\Api\Data\KompletacjaItemsInterface
     */
    public function setKompletacjaId($kompletacjaId);

    /**
     * Get sku
     * @return string|null
     */
    public function getSku();

    /**
     * Set sku
     * @param string $sku
     * @return \Kowal\WFirma\KompletacjaItems\Api\Data\KompletacjaItemsInterface
     */
    public function setSku($sku);

    /**
     * Get nazwa
     * @return string|null
     */
    public function getNazwa();

    /**
     * Set nazwa
     * @param string $nazwa
     * @return \Kowal\WFirma\KompletacjaItems\Api\Data\KompletacjaItemsInterface
     */
    public function setNazwa($nazwa);

    /**
     * Get ilosc
     * @return string|null
     */
    public function getIlosc();

    /**
     * Set ilosc
     * @param string $ilosc
     * @return \Kowal\WFirma\KompletacjaItems\Api\Data\KompletacjaItemsInterface
     */
    public function setIlosc($ilosc);

    /**
     * Get order_id
     * @return string|null
     */
    public function getOrderId();

    /**
     * Set order_id
     * @param string $orderId
     * @return \Kowal\WFirma\KompletacjaItems\Api\Data\KompletacjaItemsInterface
     */
    public function setOrderId($orderId);

    /**
     * Get z_magazynu
     * @return string|null
     */
    public function getZMagazynu();

    /**
     * Set z_magazynu
     * @param string $zMagazynu
     * @return \Kowal\WFirma\KompletacjaItems\Api\Data\KompletacjaItemsInterface
     */
    public function setZMagazynu($zMagazynu);

    /**
     * Get do_magazynu
     * @return string|null
     */
    public function getDoMagazynu();

    /**
     * Set do_magazynu
     * @param string $doMagazynu
     * @return \Kowal\WFirma\KompletacjaItems\Api\Data\KompletacjaItemsInterface
     */
    public function setDoMagazynu($doMagazynu);
}

