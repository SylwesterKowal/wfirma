<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Api\Data;

interface ZlecenieItemsInterface
{

    const ORDER_ID = 'order_id';
    const ZLECENIE_ID = 'zlecenie_id';
    const NAZWA = 'nazwa';
    const PARAMETRY = 'parametry';
    const TYP_ZLECENIA = 'typ_zlecenia';
    const DATA_ZLECENIA = 'data_zlecenia';
    const ILOSC = 'ilosc';
    const SKU = 'sku';
    const ZLECENIEITEMS_ID = 'zlecenieitems_id';

    /**
     * Get zlecenieitems_id
     * @return string|null
     */
    public function getZlecenieitemsId();

    /**
     * Set zlecenieitems_id
     * @param string $zlecenieitemsId
     * @return \Kowal\WFirma\ZlecenieItems\Api\Data\ZlecenieItemsInterface
     */
    public function setZlecenieitemsId($zlecenieitemsId);

    /**
     * Get zlecenie_id
     * @return string|null
     */
    public function getZlecenieId();

    /**
     * Set zlecenie_id
     * @param string $zlecenieId
     * @return \Kowal\WFirma\ZlecenieItems\Api\Data\ZlecenieItemsInterface
     */
    public function setZlecenieId($zlecenieId);

    /**
     * Get order_id
     * @return string|null
     */
    public function getOrderId();

    /**
     * Set order_id
     * @param string $orderId
     * @return \Kowal\WFirma\ZlecenieItems\Api\Data\ZlecenieItemsInterface
     */
    public function setOrderId($orderId);

    /**
     * Get sku
     * @return string|null
     */
    public function getSku();

    /**
     * Set sku
     * @param string $sku
     * @return \Kowal\WFirma\ZlecenieItems\Api\Data\ZlecenieItemsInterface
     */
    public function setSku($sku);

    /**
     * Get nazwa
     * @return string|null
     */
    public function getNazwa();

    /**
     * Set nazwa
     * @param string $nazwa
     * @return \Kowal\WFirma\ZlecenieItems\Api\Data\ZlecenieItemsInterface
     */
    public function setNazwa($nazwa);

    /**
     * Get ilosc
     * @return string|null
     */
    public function getIlosc();

    /**
     * Set ilosc
     * @param string $ilosc
     * @return \Kowal\WFirma\ZlecenieItems\Api\Data\ZlecenieItemsInterface
     */
    public function setIlosc($ilosc);

    /**
     * Get parametry
     * @return string|null
     */
    public function getParametry();

    /**
     * Set parametry
     * @param string $parametry
     * @return \Kowal\WFirma\ZlecenieItems\Api\Data\ZlecenieItemsInterface
     */
    public function setParametry($parametry);

    /**
     * Get typ_zlecenia
     * @return string|null
     */
    public function getTypZlecenia();

    /**
     * Set typ_zlecenia
     * @param string $typZlecenia
     * @return \Kowal\WFirma\ZlecenieItems\Api\Data\ZlecenieItemsInterface
     */
    public function setTypZlecenia($typZlecenia);

    /**
     * Get data_zlecenia
     * @return string|null
     */
    public function getDataZlecenia();

    /**
     * Set data_zlecenia
     * @param string $dataZlecenia
     * @return \Kowal\WFirma\ZlecenieItems\Api\Data\ZlecenieItemsInterface
     */
    public function setDataZlecenia($dataZlecenia);
}

