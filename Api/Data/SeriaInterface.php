<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Api\Data;

interface SeriaInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const SERIA_ID = 'seria_id';
    const ORDER_ATTR_NAME = 'order_attr_name';
    const ORDER_ATTR_VALUE = 'order_attr_value';
    const WFIRMA_SERIA = 'wfirma_seria';
    const OPIS = 'Opis';

    /**
     * Get seria_id
     * @return string|null
     */
    public function getSeriaId();

    /**
     * Set seria_id
     * @param string $seriaId
     * @return \Kowal\WFirma\Api\Data\SeriaInterface
     */
    public function setSeriaId($seriaId);

    /**
     * Get order_attr_name
     * @return string|null
     */
    public function getOrderAttrName();

    /**
     * Set order_attr_name
     * @param string $orderAttrName
     * @return \Kowal\WFirma\Api\Data\SeriaInterface
     */
    public function setOrderAttrName($orderAttrName);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Kowal\WFirma\Api\Data\SeriaExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \Kowal\WFirma\Api\Data\SeriaExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Kowal\WFirma\Api\Data\SeriaExtensionInterface $extensionAttributes
    );

    /**
     * Get order_attr_value
     * @return string|null
     */
    public function getOrderAttrValue();

    /**
     * Set order_attr_value
     * @param string $orderAttrValue
     * @return \Kowal\WFirma\Api\Data\SeriaInterface
     */
    public function setOrderAttrValue($orderAttrValue);

    /**
     * Get wfirma_seria
     * @return string|null
     */
    public function getWfirmaSeria();

    /**
     * Set wfirma_seria
     * @param string $wfirmaSeria
     * @return \Kowal\WFirma\Api\Data\SeriaInterface
     */
    public function setWfirmaSeria($wfirmaSeria);

    /**
     * Get Opis
     * @return string|null
     */
    public function getOpis();

    /**
     * Set Opis
     * @param string $opis
     * @return \Kowal\WFirma\Api\Data\SeriaInterface
     */
    public function setOpis($opis);
}

