<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Api\Data;

interface SeriaSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get Seria list.
     * @return \Kowal\WFirma\Api\Data\SeriaInterface[]
     */
    public function getItems();

    /**
     * Set order_attr_name list.
     * @param \Kowal\WFirma\Api\Data\SeriaInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

