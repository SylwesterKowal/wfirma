<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface FirmyRepositoryInterface
{

    /**
     * Save Firmy
     * @param \Kowal\WFirma\Api\Data\FirmyInterface $firmy
     * @return \Kowal\WFirma\Api\Data\FirmyInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Kowal\WFirma\Api\Data\FirmyInterface $firmy
    );

    /**
     * Retrieve Firmy
     * @param string $firmyId
     * @return \Kowal\WFirma\Api\Data\FirmyInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($firmyId);

    /**
     * Retrieve Firmy matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Kowal\WFirma\Api\Data\FirmySearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete Firmy
     * @param \Kowal\WFirma\Api\Data\FirmyInterface $firmy
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Kowal\WFirma\Api\Data\FirmyInterface $firmy
    );

    /**
     * Delete Firmy by ID
     * @param string $firmyId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($firmyId);
}

