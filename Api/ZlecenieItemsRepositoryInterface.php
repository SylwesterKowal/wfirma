<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface ZlecenieItemsRepositoryInterface
{

    /**
     * Save ZlecenieItems
     * @param \Kowal\WFirma\Api\Data\ZlecenieItemsInterface $zlecenieItems
     * @return \Kowal\WFirma\Api\Data\ZlecenieItemsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Kowal\WFirma\Api\Data\ZlecenieItemsInterface $zlecenieItems
    );

    /**
     * Retrieve ZlecenieItems
     * @param string $zlecenieitemsId
     * @return \Kowal\WFirma\Api\Data\ZlecenieItemsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($zlecenieitemsId);

    /**
     * Retrieve ZlecenieItems matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Kowal\WFirma\Api\Data\ZlecenieItemsSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete ZlecenieItems
     * @param \Kowal\WFirma\Api\Data\ZlecenieItemsInterface $zlecenieItems
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Kowal\WFirma\Api\Data\ZlecenieItemsInterface $zlecenieItems
    );

    /**
     * Delete ZlecenieItems by ID
     * @param string $zlecenieitemsId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($zlecenieitemsId);
}

