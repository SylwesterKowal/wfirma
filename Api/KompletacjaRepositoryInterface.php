<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface KompletacjaRepositoryInterface
{

    /**
     * Save Kompletacja
     * @param \Kowal\WFirma\Api\Data\KompletacjaInterface $kompletacja
     * @return \Kowal\WFirma\Api\Data\KompletacjaInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Kowal\WFirma\Api\Data\KompletacjaInterface $kompletacja
    );

    /**
     * Retrieve Kompletacja
     * @param string $kompletacjaId
     * @return \Kowal\WFirma\Api\Data\KompletacjaInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($kompletacjaId);

    /**
     * Retrieve Kompletacja matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Kowal\WFirma\Api\Data\KompletacjaSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete Kompletacja
     * @param \Kowal\WFirma\Api\Data\KompletacjaInterface $kompletacja
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Kowal\WFirma\Api\Data\KompletacjaInterface $kompletacja
    );

    /**
     * Delete Kompletacja by ID
     * @param string $kompletacjaId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($kompletacjaId);
}

