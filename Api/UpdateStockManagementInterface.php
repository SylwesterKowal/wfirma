<?php


namespace Kowal\WFirma\Api;

interface UpdateStockManagementInterface
{

    /**
     * PUT for UpdateStock api
     * @param string $param
     * @return string
     */
    public function putUpdateStock($param);
}