<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface MagazynyRepositoryInterface
{

    /**
     * Save Magazyny
     * @param \Kowal\WFirma\Api\Data\MagazynyInterface $magazyny
     * @return \Kowal\WFirma\Api\Data\MagazynyInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Kowal\WFirma\Api\Data\MagazynyInterface $magazyny
    );

    /**
     * Retrieve Magazyny
     * @param string $magazynyId
     * @return \Kowal\WFirma\Api\Data\MagazynyInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($magazynyId);

    /**
     * Retrieve Magazyny matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Kowal\WFirma\Api\Data\MagazynySearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete Magazyny
     * @param \Kowal\WFirma\Api\Data\MagazynyInterface $magazyny
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Kowal\WFirma\Api\Data\MagazynyInterface $magazyny
    );

    /**
     * Delete Magazyny by ID
     * @param string $magazynyId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($magazynyId);
}

