<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface ZlecenieRepositoryInterface
{

    /**
     * Save Zlecenie
     * @param \Kowal\WFirma\Api\Data\ZlecenieInterface $zlecenie
     * @return \Kowal\WFirma\Api\Data\ZlecenieInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Kowal\WFirma\Api\Data\ZlecenieInterface $zlecenie
    );

    /**
     * Retrieve Zlecenie
     * @param string $zlecenieId
     * @return \Kowal\WFirma\Api\Data\ZlecenieInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($zlecenieId);

    /**
     * Retrieve Zlecenie matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Kowal\WFirma\Api\Data\ZlecenieSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete Zlecenie
     * @param \Kowal\WFirma\Api\Data\ZlecenieInterface $zlecenie
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Kowal\WFirma\Api\Data\ZlecenieInterface $zlecenie
    );

    /**
     * Delete Zlecenie by ID
     * @param string $zlecenieId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($zlecenieId);
}

