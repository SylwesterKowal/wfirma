<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface KompletacjaItemsRepositoryInterface
{

    /**
     * Save KompletacjaItems
     * @param \Kowal\WFirma\Api\Data\KompletacjaItemsInterface $kompletacjaItems
     * @return \Kowal\WFirma\Api\Data\KompletacjaItemsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Kowal\WFirma\Api\Data\KompletacjaItemsInterface $kompletacjaItems
    );

    /**
     * Retrieve KompletacjaItems
     * @param string $kompletacjaitemsId
     * @return \Kowal\WFirma\Api\Data\KompletacjaItemsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($kompletacjaitemsId);

    /**
     * Retrieve KompletacjaItems matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Kowal\WFirma\Api\Data\KompletacjaItemsSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete KompletacjaItems
     * @param \Kowal\WFirma\Api\Data\KompletacjaItemsInterface $kompletacjaItems
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Kowal\WFirma\Api\Data\KompletacjaItemsInterface $kompletacjaItems
    );

    /**
     * Delete KompletacjaItems by ID
     * @param string $kompletacjaitemsId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($kompletacjaitemsId);
}

