<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface SeriaRepositoryInterface
{

    /**
     * Save Seria
     * @param \Kowal\WFirma\Api\Data\SeriaInterface $seria
     * @return \Kowal\WFirma\Api\Data\SeriaInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Kowal\WFirma\Api\Data\SeriaInterface $seria
    );

    /**
     * Retrieve Seria
     * @param string $seriaId
     * @return \Kowal\WFirma\Api\Data\SeriaInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($seriaId);

    /**
     * Retrieve Seria matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Kowal\WFirma\Api\Data\SeriaSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete Seria
     * @param \Kowal\WFirma\Api\Data\SeriaInterface $seria
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Kowal\WFirma\Api\Data\SeriaInterface $seria
    );

    /**
     * Delete Seria by ID
     * @param string $seriaId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($seriaId);
}

