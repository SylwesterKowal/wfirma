<?php
/**
 * Copyright © Kowal All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Setup\Patch\Data;

use Magento\Variable\Model\VariableFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\Patch\PatchRevertableInterface;

class InstallData implements DataPatchInterface
{

    protected $varFActory;

    /**
     * @param VariableFactory $varFactory
     */
    public function __construct(
        VariableFactory $varFactory
    ) {
        $this->varFActory = $varFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function apply()
    {

        $variable = $this->varFActory->create();
        $data = [
            'code' => 'invoicepdf',
            'name' => 'Invoice Pdf link',
            'html_value' => '<a href="{{config path="web/secure/base_url"}}invoice/pdf/download/id/{{trans "%id" id=$invoice.entity_id}}"> Print Invoice </a>',
            'plain_value' => '<a href="{{config path="web/secure/base_url"}}invoice/pdf/download/id/{{trans "%id" id=$invoice.entity_id}}"> Print Invoice </a>',

        ];
        $variable->setData($data);
        $variable->save();
    }

    public function revert()
    {
        $this->moduleDataSetup->getConnection()->startSetup();


        $this->moduleDataSetup->getConnection()->endSetup();
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [

        ];
    }
}