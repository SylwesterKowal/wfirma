<?php


namespace Kowal\WFirma\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

class Firma extends AbstractHelper
{

    public $store_id = 0;
    protected $resourceConnection;
    public $connection_read;
    public $connection_write;


    /**
     * @param \Magento\Framework\App\Helper\Context $context
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context     $context,
        \Kowal\WFirma\Model\ResourceModel\Firmy\CollectionFactory $firmaCollectionFactory
    )
    {
        parent::__construct($context);
        $this->firmaCollectionFactory = $firmaCollectionFactory;
    }

    public function getFirmaByStoreId($store_id){
        $this->collection = $this->firmaCollectionFactory->create();
        $items = $this->collection->getItems();
        foreach ($items as $model) {
            $stores = explode(",",$model->getStoreIdMagento());
            if(in_array($store_id,$stores)){
                return $model;
            }
        }
    }

    public function getProducent(){
        $this->collection = $this->firmaCollectionFactory->create();
        $items = $this->collection->getItems();
        foreach ($items as $model) {
           if(!$model->getPartner){
               return $model;
           }
        }
    }

    public function getFirmy(){
        return $this->firmaCollectionFactory->create()->getItems();
    }
}
