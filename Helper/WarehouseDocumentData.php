<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

class WarehouseDocumentData extends AbstractHelper
{

    public $new_doc;
    public $id;

    /**
     * @param \Magento\Framework\App\Helper\Context $context
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context      $context,
        \Kowal\WFirma\lib\WarehouseDocument        $warehouseDocument,
        \Kowal\WFirma\lib\WarehouseDocumentContent $warehouseDocumentContent,
        \Kowal\WFirma\lib\Good                     $good,
        \Kowal\WFirma\lib\Query                    $query,
        \Magento\Tax\Api\TaxCalculationInterface   $taxCalculation,
        \Magento\Catalog\Model\ProductRepository   $productRepository
    )
    {
        parent::__construct($context);
        $this->warehouseDocument = $warehouseDocument;
        $this->warehouseDocumentContent = $warehouseDocumentContent;
        $this->good = $good;
        $this->query = $query;
        $this->taxCalculation = $taxCalculation;
        $this->productRepository = $productRepository;
    }

    public function setData($date, $disposalDate, $ContentArray_, $type, $magazyn_id, $action = "", $contractor = null, $price_type = null, $parent_id = null, $description = null, $fullnumber = false, $store_id = 0)
    {
        $this->new_doc = $this->warehouseDocument->createNew();
        $_warehouseDocumentContent = $this->getWarehouseDocumentContents($ContentArray_, $magazyn_id, $store_id);
        $this->new_doc->setData($date, $disposalDate, $_warehouseDocumentContent, $type, $contractor, $price_type, $parent_id, $description);
        $this->new_doc->setActionName($action);
        $this->new_doc->setAditionalParameters("&warehouse_id=" . $magazyn_id);
        $this->id = $this->new_doc->add($fullnumber, $store_id);
        //$this->new_doc->get($this->id);
        return $this->id;
    }

    public function getWarehouseDocumentContents($ContentArray_, $magazyn_id, $store_id = 0)
    {
        $warehouseDocumentContents = [];

        foreach ($ContentArray_ as $item) {
            $warehouseDocumentContent = $this->warehouseDocumentContent->createNew();
            $price = (isset($item['price'])) ? $item['price'] : null;
            $price  = (is_null($price)) ? $this->query->getKosztNowy($item['sku'], $store_id) : $price;

            if ($goodId = $this->getGoodBySKU($item['sku'], $store_id)) {
                $warehouseDocumentContent->setData($goodId, $item['qty'], $magazyn_id, $price);
            } else {
                if ($product = $this->productRepository->get($item['sku'])) {
                    $tax = $this->taxCalculation->getCalculatedRate($product->getTaxClassId());
                    $this->good->setData($product->getSku(), $product->getName(), 'szt.', 0, ceil($price* 100) / 100, $tax, "{$product->getSku()}");
                    $goodId = $this->good->add(false, $store_id);
                    $warehouseDocumentContent->setData($goodId, $item['qty'], $magazyn_id, $price);
                }
            }
            $warehouseDocumentContents[] = $warehouseDocumentContent->getData();
        }
        return $warehouseDocumentContents;
    }

    public
    function getGoodBySKU($sku, $store_id = 0)
    {
        if ($this->query->checkIfSkuExists($sku)) {
            if ($wfirma_product_id = $this->query->getAttribute($sku, 'wfirma_product_id',$store_id)) {
//                file_put_contents("_attr.txt", print_r($wfirma_product_id, true));
                return $wfirma_product_id;
            }
        }

        $good = $this->good->createNew();
        $good->setFindData(['parameters' => ['conditions' => ['condition' => [
            'field' => 'code',
            'operator' => 'eq',
            'value' => $sku
        ]]]]);

        return ($good->findOne($store_id));
    }
}

