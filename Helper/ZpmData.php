<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

class ZpmData extends AbstractHelper
{

    protected $stocks = [];
    protected $used_stocks = [];

    /**
     * @param \Magento\Framework\App\Helper\Context $context
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context                      $context,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $collectionFactory,
        \Magento\Store\Model\StoreManagerInterface                 $storeManager,
        \Magento\Catalog\Model\ProductRepository                   $productRepository,
        \Kowal\WFirma\lib\Good                                     $good,
        \Kowal\WFirma\lib\Config                                   $config,
        \Kowal\WFirma\lib\Query                                    $query,
        \Kowal\WFirma\Helper\Firma                                 $firma
    )
    {
        parent::__construct($context);
        $this->collectionFactory = $collectionFactory;
        $this->storeManager = $storeManager;
        $this->productRepository = $productRepository;
        $this->good = $good;
        $this->config = $config;
        $this->query = $query;
        $this->firma = $firma;
        $this->rezerwacje = [];
    }


    /**
     *
     * @param $collection
     * @return array|string
     */
    public function setData($collection = null)
    {
        $orders_data = [];
        $this->rezerwacje = [];

        if (is_null($collection)) return "Lista zamówień jest pusta" . PHP_EOL;


        foreach ($collection as $order) {

            $this->stocks = $this->getGoodsStocks($order);
            file_put_contents("_order_stocks.txt", print_r($this->stocks, true));
            if ($order_data = $this->getOrderData($order)) {

                $orders_data[] = $order_data;

            }
        }


        return $orders_data;
    }


    public function getOrderData($order)
    {
        $store_id = $order->getStoreId();
        $status = $order->getStatus();
        $status_domylsmny = $this->config->getOrderStatusDoZleceniaProd($store_id);

        if ($status != $status_domylsmny) return false;
//            file_put_contents("_order_data.txt",print_r($order->getData(),true));
//            file_put_contents("_order_shipping_data.txt",print_r($order->getShippingAddress()->getData(),true));
        $payment = $order->getPayment();
        $method = $payment->getMethodInstance();
        $paymentTitle = $method->getTitle();
        $paymentCode = $method->getCode();


        $shippingAddress = $order->getShippingAddress();
        $person = str_replace(",", " ", trim($shippingAddress->getFirstname() . ' ' . $shippingAddress->getLastname()));
        $company = str_replace(",", " ", (!empty($shippingAddress->getCompany())) ? trim($shippingAddress->getCompany()) : "");
        $street = $shippingAddress->getStreet()[0];
        $street = str_replace(",", " ", (isset($shippingAddress->getStreet()[1])) ? $street . ' ' . $shippingAddress->getStreet()[1] : $street);

        $billingAddress = $order->getBillingAddress();
        $blling_person = str_replace(",", " ", trim($billingAddress->getFirstname() . ' ' . $billingAddress->getLastname()));
        $blling_company = str_replace(",", " ", (!empty($billingAddress->getCompany())) ? trim($billingAddress->getCompany()) : "");
        $billing_vat_id = $billingAddress->getVatId();
        $blling_street = $billingAddress->getStreet()[0];
        $blling_street = str_replace(",", " ", (isset($billingAddress->getStreet()[1])) ? $blling_street . ' ' . $billingAddress->getStreet()[1] : $blling_street);
//            file_put_contents("_order_billing_data.txt",print_r($billingAddress->getData(),true));

        $orderShipping = (float)$order->getBaseShippingInclTax() + $order->getFee() + $order->getFeeTax();


        $order_data = [
            "increment_id" => $order->getIncrementId(),
            "created_at" => date('Y-m-d', strtotime($order->getCreatedAt())),
            "store_id" => $order->getStoreId(),
            "store_code" => (string)$this->storeManager->getStore($order->getStoreId())->getCode(),
            "date_add" => strtotime(str_replace(",", "", $order->getCreatedAtFormatted(2))),
            "phone" => $billingAddress->getTelephone(),
            "email" => $order->getCustomerEmail(),

            "currency" => $order->getOrderCurrencyCode(),
            "payment_method" => $paymentTitle,
            "delivery_method" => $order->getShippingDescription(),
            "delivery_price" => $orderShipping,
            "delivery_fullname" => $person,
            "delivery_company" => $company,
            "delivery_address" => $street,
            "delivery_city" => $shippingAddress->getCity(),
            "delivery_state" => $shippingAddress->getRegionId(),
            "delivery_postcode" => $shippingAddress->getPostcode(),
            "delivery_country_code" => $shippingAddress->getCountryId(),
            "invoice_fullname" => $blling_person,
            "invoice_company" => $blling_company,
            "invoice_nip" => $billing_vat_id,
            "invoice_address" => $blling_street,
            "invoice_city" => $billingAddress->getCity(),
            "invoice_state" => $billingAddress->getRegion(),
            "invoice_postcode" => $billingAddress->getPostcode(),
            "invoice_country_code" => $billingAddress->getCountryId(),
            "want_invoice" => (empty($billing_vat_id)) ? "0" : "1",
            'order_status_to_set' => $this->setOrderStatus($order),
            'shipment_label' => $order->getShipmentLabel()
        ];

        $orderItems = $order->getAllItems();

        $products = [];
        foreach ($orderItems as $item) {

            if ( $item->getParentItemId() && $item->getParentItem() && $item->getParentItem()->getProductType() == 'configurable') {
                continue;
            }
            if (!$item->getParentItemId() && $item->getProductType() == 'bundle') {
                continue;
            }

            if ($this->query->checkIfSkuExists($item->getSku())) {
                if ($product = $this->productRepository->get($item->getSku())) {

                    $attr = $product->getResource()->getAttribute('size');
                    if ($attr->usesSource()) {
                        $sizeLabel = $attr->getSource()->getOptionText($product->getSize());
                    } else {
                        $sizeLabel = "";
                    }
                    $nazwa_bazowa = $product->getNazwaBazowa();
                } else {
                    $nazwa_bazowa = $item->getName();
                }
            } else {
                $sizeLabel = "";
                $nazwa_bazowa = $item->getName();
            }
            $firmaModel = $this->firma->getFirmaByStoreId($store_id); // grupujemy rezerwacje na firmę
            $quantity_reserved = $this->setReservedQty($item, $firmaModel->getFirmyId());
            file_put_contents("_used_stock.txt", print_r($this->used_stocks, true));
            $products[] = [
                "product_id" => $item->getSku(),
                "name" => str_replace('"', "", (string)$nazwa_bazowa),
                "sku" => $item->getSku(),
                "ean" => $item->getEan(),
                "price_brutto" => str_replace(".", ",", (string)round($item->getPriceInclTax() - ($item->getDiscountAmount() / $item->getQtyOrdered()), 2)),
                "tax_rate" => $item->getTaxPercent(),
                "quantity_ordered" => (int)$item->getQtyOrdered(),
                "quantity_shipped" => (int)$item->getQtyShipped(),
                "quantity_instock" => ((array_key_exists($item->getSku(), $this->stocks)) ? $this->stocks[$item->getSku()] : 0),
                "quantity_reserved" => $quantity_reserved,
                "quantity_produced" => ((int)$item->getQtyOrdered() - $quantity_reserved),
                "weight" => (int)$item->getWeight(),
                'parametry' => $sizeLabel
            ];

        }


        $order_data['products'] = $products;

        return $order_data;
    }

    private function setReservedQty($item, $firma_id)
    {

        if(isset( $this->used_stocks[$firma_id][$item->getSku()])) {
            $this->stocks[$item->getSku()] = ((int)$this->stocks[$item->getSku()] - $this->used_stocks[$firma_id][$item->getSku()] ); // pomniejszam stan pobrany z wFirma o uzyte już w poprzednich zamówieniach na firmę
        }

        if (array_key_exists($item->getSku(), $this->stocks) && (int)$item->getQtyOrdered() > 0 && (int)$this->stocks[$item->getSku()] > 0) {
            if ((int)$this->stocks[$item->getSku()] >= (int)$item->getQtyOrdered() && (int)$this->stocks[$item->getSku()] > 0) {
//                $this->stocks[$item->getSku()] = ((int)$this->stocks[$item->getSku()] - (int)$item->getQtyOrdered()); // pomniejszam stan pobrany z wFirma aby kolejny taki sam produkt nie uwzględniał już tego samego stanu
                $this->used_stocks[$firma_id][$item->getSku()] = (isset($this->used_stocks[$firma_id][$item->getSku()])) ? $this->used_stocks[$firma_id][$item->getSku()] + (int)$item->getQtyOrdered() : (int)$item->getQtyOrdered();
                return (int)$item->getQtyOrdered();
            } else if ((int)$this->stocks[$item->getSku()] > 0 && (int)$this->stocks[$item->getSku()] < (int)$item->getQtyOrdered()) {
                $stock = (int)$this->stocks[$item->getSku()];
//                $this->stocks[$item->getSku()] = 0;
                $this->used_stocks[$firma_id][$item->getSku()] = (isset($this->used_stocks[$firma_id][$item->getSku()])) ? $this->used_stocks[$firma_id][$item->getSku()] + (int)$this->stocks[$item->getSku()] : (int)$this->stocks[$item->getSku()];
                return (int)$stock;
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }


    /**
     * Odczyt stanu magazynowego wFirma dla produkrowanych zamówień
     * @param $collection
     * @return array
     */
    public function getGoodsStocks($order)
    {

        $store_id = $order->getStoreId();
        $stocks = [];
        $skus = [];


        $magazyny_wfirma = explode(",", $this->config->getMagazynGlownyWFirma($store_id));


        $orderItems = $order->getAllItems();
        foreach ($orderItems as $item) {
            $skus[$item->getSku()] = $item->getSku();
        }


        $good = $this->good->createNew();


        $limit = 300;
        $good->setFindData(['parameters' => [
            'page' => 1,
            'limit' => $limit,
            'conditions' => ['condition' =>
                [
                    'field' => 'code',
                    'operator' => 'in',
                    'value' => implode(',', $skus)
                ]
            ]
        ]]);


        if ($products = $good->findAll($store_id)) {

            $reszta = (count($skus) > $limit) ? 1 : 0;
            $pages = round((int)$products['parameters']['total'] / $limit, 0) + 1;
            foreach ($products as $keyContr => $product) {
                if (isset($product['good']['code']) && isset($stocks[$product['good']['code']])) $stocks[$product['good']['code']] = 0;
                if (isset($product['good']['code']) && isset($product['good']['warehouse_good_parcels'])) {
                    foreach ($product['good']['warehouse_good_parcels'] as $warehouse_good_parcel) {
                        if (in_array($warehouse_good_parcel['warehouse_good_parcel']['warehouse']['id'], $magazyny_wfirma)) {
                            if (isset($stocks[$product['good']['code']])) {
                                $curent_stock = (int)$stocks[$product['good']['code']];
                            } else {
                                $curent_stock = 0;
                            }
                            $stocks[$product['good']['code']] = $curent_stock + (int)$warehouse_good_parcel['warehouse_good_parcel']['count'];
                        }
                    }
                }


            }

            for ($i = 2; $i <= $pages; $i++) {
                sleep(2);
                $condition = ['parameters' => ['page' => $i, 'limit' => $limit]];
                $this->good->setFindData($condition);
                if ($products = $this->good->findAll($store_id)) {

                    foreach ($products as $keyContr => $product) {

                        if (isset($product['good']['code']) && isset($stocks[$product['good']['code']])) $stocks[$product['good']['code']] = 0;
                        if (isset($product['good']['code']) && isset($product['good']['warehouse_good_parcels'])) {
                            foreach ($product['good']['warehouse_good_parcels'] as $warehouse_good_parcel) {
                                if (in_array($warehouse_good_parcel['warehouse_good_parcel']['warehouse']['id'], $magazyny_wfirma)) {
                                    if (isset($stocks[$product['good']['code']])) {
                                        $curent_stock = (int)$stocks[$product['good']['code']];
                                    } else {
                                        $curent_stock = 0;
                                    }
                                    $stocks[$product['good']['code']] = $curent_stock + (int)$warehouse_good_parcel['warehouse_good_parcel']['count'];
                                }
                            }
                        }


                    }
                }
            }
        }
        return $stocks;
    }

    /**
     * Odczyt sanu magazynowego Rezerwacje dla produktów przyjmowanych z produkcji
     * Jeśli są na magazynie to zamiast PW ma być MM (RW, PW) do magazynu kompletacji
     * @param $items
     * @return array
     */
    public function getGoodsStocksRezerwacje($items)
    {
        $magazyny_wfirma = $this->config->getMagazynRezerwacji();
        $stocks = [];
        $skus = [];
        foreach ($items as $item) {
            $skus[] = $item['sku'];
        }

        $good = $this->good->createNew();
        $good->setFindData(['parameters' => ['conditions' => ['condition' =>
            [
                'field' => 'code',
                'operator' => 'in',
                'value' => implode(',', $skus)
            ]
        ]]]);
        $products = $good->findAll();

        if (is_array($products)) {
            foreach ($products as $product) {
                if (isset($product['good']['code']) && isset($product['good']['warehouse_good_parcels'])) {
                    foreach ($product['good']['warehouse_good_parcels'] as $warehouse_good_parcel) {
                        if ($warehouse_good_parcel['warehouse_good_parcel']['warehouse']['id'] == $magazyny_wfirma) {
                            if (isset($stocks[$product['good']['code']])) {
                                $curent_stock = $stocks[$product['good']['code']];
                            } else {
                                $curent_stock = 0;
                            }
                            $stocks[$product['good']['code']] = $curent_stock + $warehouse_good_parcel['warehouse_good_parcel']['count'];
                        }
                    }
                }
            }
        }
        return $stocks;
    }

    private function setOrderStatus($order)
    {
        $orderItems = $order->getAllItems();
        $status = $this->config->getOrderStatusDoPakowania($order->getStoreId());
        foreach ($orderItems as $item) {
            if ( $item->getParentItemId() && $item->getParentItem() && $item->getParentItem()->getProductType() == 'configurable') {
                continue;
            }
            if (!$item->getParentItemId() && $item->getProductType() == 'bundle') {
                continue;
            }

            if (array_key_exists($item->getSku(), $this->stocks) && (int)$item->getQtyOrdered() <= $this->stocks[$item->getSku()]) {

            } else {
                $status = $this->config->getOrderStatusDoProdukcji($order->getStoreId());
                break;
            }

        }
        return $status;
    }
}

