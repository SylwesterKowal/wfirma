<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Model;

use Kowal\WFirma\Api\Data\SeriaInterfaceFactory;
use Kowal\WFirma\Api\Data\SeriaSearchResultsInterfaceFactory;
use Kowal\WFirma\Api\SeriaRepositoryInterface;
use Kowal\WFirma\Model\ResourceModel\Seria as ResourceSeria;
use Kowal\WFirma\Model\ResourceModel\Seria\CollectionFactory as SeriaCollectionFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;

class SeriaRepository implements SeriaRepositoryInterface
{

    protected $dataObjectHelper;

    protected $extensibleDataObjectConverter;
    protected $seriaFactory;

    private $collectionProcessor;

    private $storeManager;

    protected $seriaCollectionFactory;

    protected $dataSeriaFactory;

    protected $searchResultsFactory;

    protected $dataObjectProcessor;

    protected $resource;

    protected $extensionAttributesJoinProcessor;


    /**
     * @param ResourceSeria $resource
     * @param SeriaFactory $seriaFactory
     * @param SeriaInterfaceFactory $dataSeriaFactory
     * @param SeriaCollectionFactory $seriaCollectionFactory
     * @param SeriaSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourceSeria $resource,
        SeriaFactory $seriaFactory,
        SeriaInterfaceFactory $dataSeriaFactory,
        SeriaCollectionFactory $seriaCollectionFactory,
        SeriaSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource = $resource;
        $this->seriaFactory = $seriaFactory;
        $this->seriaCollectionFactory = $seriaCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataSeriaFactory = $dataSeriaFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \Kowal\WFirma\Api\Data\SeriaInterface $seria
    ) {
        /* if (empty($seria->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $seria->setStoreId($storeId);
        } */
        
        $seriaData = $this->extensibleDataObjectConverter->toNestedArray(
            $seria,
            [],
            \Kowal\WFirma\Api\Data\SeriaInterface::class
        );
        
        $seriaModel = $this->seriaFactory->create()->setData($seriaData);
        
        try {
            $this->resource->save($seriaModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the seria: %1',
                $exception->getMessage()
            ));
        }
        return $seriaModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function get($seriaId)
    {
        $seria = $this->seriaFactory->create();
        $this->resource->load($seria, $seriaId);
        if (!$seria->getId()) {
            throw new NoSuchEntityException(__('Seria with id "%1" does not exist.', $seriaId));
        }
        return $seria->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->seriaCollectionFactory->create();
        
        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \Kowal\WFirma\Api\Data\SeriaInterface::class
        );
        
        $this->collectionProcessor->process($criteria, $collection);
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }
        
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \Kowal\WFirma\Api\Data\SeriaInterface $seria
    ) {
        try {
            $seriaModel = $this->seriaFactory->create();
            $this->resource->load($seriaModel, $seria->getSeriaId());
            $this->resource->delete($seriaModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the Seria: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($seriaId)
    {
        return $this->delete($this->get($seriaId));
    }
}

