<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Model;

use Kowal\WFirma\Api\Data\MagazynyInterface;
use Kowal\WFirma\Api\Data\MagazynyInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;

class Magazyny extends \Magento\Framework\Model\AbstractModel
{

    protected $dataObjectHelper;

    protected $magazynyDataFactory;

    protected $_eventPrefix = 'kowal_wfirma_magazyny';

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param MagazynyInterfaceFactory $magazynyDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \Kowal\WFirma\Model\ResourceModel\Magazyny $resource
     * @param \Kowal\WFirma\Model\ResourceModel\Magazyny\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        MagazynyInterfaceFactory $magazynyDataFactory,
        DataObjectHelper $dataObjectHelper,
        \Kowal\WFirma\Model\ResourceModel\Magazyny $resource,
        \Kowal\WFirma\Model\ResourceModel\Magazyny\Collection $resourceCollection,
        array $data = []
    ) {
        $this->magazynyDataFactory = $magazynyDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve magazyny model with magazyny data
     * @return MagazynyInterface
     */
    public function getDataModel()
    {
        $magazynyData = $this->getData();
        
        $magazynyDataObject = $this->magazynyDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $magazynyDataObject,
            $magazynyData,
            MagazynyInterface::class
        );
        
        return $magazynyDataObject;
    }
}

