<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Model;

use Kowal\WFirma\Api\Data\KompletacjaInterface;
use Kowal\WFirma\Api\Data\KompletacjaInterfaceFactory;
use Kowal\WFirma\Api\Data\KompletacjaSearchResultsInterfaceFactory;
use Kowal\WFirma\Api\KompletacjaRepositoryInterface;
use Kowal\WFirma\Model\ResourceModel\Kompletacja as ResourceKompletacja;
use Kowal\WFirma\Model\ResourceModel\Kompletacja\CollectionFactory as KompletacjaCollectionFactory;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;

class KompletacjaRepository implements KompletacjaRepositoryInterface
{

    /**
     * @var CollectionProcessorInterface
     */
    protected $collectionProcessor;

    /**
     * @var ResourceKompletacja
     */
    protected $resource;

    /**
     * @var KompletacjaCollectionFactory
     */
    protected $kompletacjaCollectionFactory;

    /**
     * @var KompletacjaInterfaceFactory
     */
    protected $kompletacjaFactory;

    /**
     * @var Kompletacja
     */
    protected $searchResultsFactory;


    /**
     * @param ResourceKompletacja $resource
     * @param KompletacjaInterfaceFactory $kompletacjaFactory
     * @param KompletacjaCollectionFactory $kompletacjaCollectionFactory
     * @param KompletacjaSearchResultsInterfaceFactory $searchResultsFactory
     * @param CollectionProcessorInterface $collectionProcessor
     */
    public function __construct(
        ResourceKompletacja $resource,
        KompletacjaInterfaceFactory $kompletacjaFactory,
        KompletacjaCollectionFactory $kompletacjaCollectionFactory,
        KompletacjaSearchResultsInterfaceFactory $searchResultsFactory,
        CollectionProcessorInterface $collectionProcessor
    ) {
        $this->resource = $resource;
        $this->kompletacjaFactory = $kompletacjaFactory;
        $this->kompletacjaCollectionFactory = $kompletacjaCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->collectionProcessor = $collectionProcessor;
    }

    /**
     * @inheritDoc
     */
    public function save(KompletacjaInterface $kompletacja)
    {
        try {
            $this->resource->save($kompletacja);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the kompletacja: %1',
                $exception->getMessage()
            ));
        }
        return $kompletacja;
    }

    /**
     * @inheritDoc
     */
    public function get($kompletacjaId)
    {
        $kompletacja = $this->kompletacjaFactory->create();
        $this->resource->load($kompletacja, $kompletacjaId);
        if (!$kompletacja->getId()) {
            throw new NoSuchEntityException(__('Kompletacja with id "%1" does not exist.', $kompletacjaId));
        }
        return $kompletacja;
    }

    /**
     * @inheritDoc
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->kompletacjaCollectionFactory->create();
        
        $this->collectionProcessor->process($criteria, $collection);
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model;
        }
        
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * @inheritDoc
     */
    public function delete(KompletacjaInterface $kompletacja)
    {
        try {
            $kompletacjaModel = $this->kompletacjaFactory->create();
            $this->resource->load($kompletacjaModel, $kompletacja->getKompletacjaId());
            $this->resource->delete($kompletacjaModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the Kompletacja: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * @inheritDoc
     */
    public function deleteById($kompletacjaId)
    {
        return $this->delete($this->get($kompletacjaId));
    }
}

