<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Model;

use Kowal\WFirma\Api\Data\KompletacjaItemsInterface;
use Kowal\WFirma\Api\Data\KompletacjaItemsInterfaceFactory;
use Kowal\WFirma\Api\Data\KompletacjaItemsSearchResultsInterfaceFactory;
use Kowal\WFirma\Api\KompletacjaItemsRepositoryInterface;
use Kowal\WFirma\Model\ResourceModel\KompletacjaItems as ResourceKompletacjaItems;
use Kowal\WFirma\Model\ResourceModel\KompletacjaItems\CollectionFactory as KompletacjaItemsCollectionFactory;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;

class KompletacjaItemsRepository implements KompletacjaItemsRepositoryInterface
{

    /**
     * @var KompletacjaItemsInterfaceFactory
     */
    protected $kompletacjaItemsFactory;

    /**
     * @var KompletacjaItemsCollectionFactory
     */
    protected $kompletacjaItemsCollectionFactory;

    /**
     * @var ResourceKompletacjaItems
     */
    protected $resource;

    /**
     * @var CollectionProcessorInterface
     */
    protected $collectionProcessor;

    /**
     * @var KompletacjaItems
     */
    protected $searchResultsFactory;


    /**
     * @param ResourceKompletacjaItems $resource
     * @param KompletacjaItemsInterfaceFactory $kompletacjaItemsFactory
     * @param KompletacjaItemsCollectionFactory $kompletacjaItemsCollectionFactory
     * @param KompletacjaItemsSearchResultsInterfaceFactory $searchResultsFactory
     * @param CollectionProcessorInterface $collectionProcessor
     */
    public function __construct(
        ResourceKompletacjaItems $resource,
        KompletacjaItemsInterfaceFactory $kompletacjaItemsFactory,
        KompletacjaItemsCollectionFactory $kompletacjaItemsCollectionFactory,
        KompletacjaItemsSearchResultsInterfaceFactory $searchResultsFactory,
        CollectionProcessorInterface $collectionProcessor
    ) {
        $this->resource = $resource;
        $this->kompletacjaItemsFactory = $kompletacjaItemsFactory;
        $this->kompletacjaItemsCollectionFactory = $kompletacjaItemsCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->collectionProcessor = $collectionProcessor;
    }

    /**
     * @inheritDoc
     */
    public function save(
        KompletacjaItemsInterface $kompletacjaItems
    ) {
        try {
            $this->resource->save($kompletacjaItems);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the kompletacjaItems: %1',
                $exception->getMessage()
            ));
        }
        return $kompletacjaItems;
    }

    /**
     * @inheritDoc
     */
    public function get($kompletacjaItemsId)
    {
        $kompletacjaItems = $this->kompletacjaItemsFactory->create();
        $this->resource->load($kompletacjaItems, $kompletacjaItemsId);
        if (!$kompletacjaItems->getId()) {
            throw new NoSuchEntityException(__('KompletacjaItems with id "%1" does not exist.', $kompletacjaItemsId));
        }
        return $kompletacjaItems;
    }

    /**
     * @inheritDoc
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->kompletacjaItemsCollectionFactory->create();
        
        $this->collectionProcessor->process($criteria, $collection);
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model;
        }
        
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * @inheritDoc
     */
    public function delete(
        KompletacjaItemsInterface $kompletacjaItems
    ) {
        try {
            $kompletacjaItemsModel = $this->kompletacjaItemsFactory->create();
            $this->resource->load($kompletacjaItemsModel, $kompletacjaItems->getKompletacjaitemsId());
            $this->resource->delete($kompletacjaItemsModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the KompletacjaItems: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * @inheritDoc
     */
    public function deleteById($kompletacjaItemsId)
    {
        return $this->delete($this->get($kompletacjaItemsId));
    }
}

