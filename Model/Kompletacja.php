<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Model;

use Kowal\WFirma\Api\Data\KompletacjaInterface;
use Magento\Framework\Model\AbstractModel;

class Kompletacja extends AbstractModel implements KompletacjaInterface
{

    /**
     * @inheritDoc
     */
    public function _construct()
    {
        $this->_init(\Kowal\WFirma\Model\ResourceModel\Kompletacja::class);
    }

    /**
     * @inheritDoc
     */
    public function getKompletacjaId()
    {
        return $this->getData(self::KOMPLETACJA_ID);
    }

    /**
     * @inheritDoc
     */
    public function setKompletacjaId($kompletacjaId)
    {
        return $this->setData(self::KOMPLETACJA_ID, $kompletacjaId);
    }

    /**
     * @inheritDoc
     */
    public function getStatus()
    {
        return $this->getData(self::STATUS);
    }

    /**
     * @inheritDoc
     */
    public function setStatus($status)
    {
        return $this->setData(self::STATUS, $status);
    }

    /**
     * @inheritDoc
     */
    public function getDataKompletacji()
    {
        return $this->getData(self::DATA_KOMPLETACJI);
    }

    /**
     * @inheritDoc
     */
    public function setDataKompletacji($dataKompletacji)
    {
        return $this->setData(self::DATA_KOMPLETACJI, $dataKompletacji);
    }

    /**
     * @inheritDoc
     */
    public function getOpis()
    {
        return $this->getData(self::OPIS);
    }

    /**
     * @inheritDoc
     */
    public function setOpis($opis)
    {
        return $this->setData(self::OPIS, $opis);
    }


    /**
     * @inheritDoc
     */
    public function getWfPwId()
    {
        return $this->getData(self::WF_PW_ID);
    }

    /**
     * @inheritDoc
     */
    public function setWfPwId($wfPwId)
    {
        return $this->setData(self::WF_PW_ID, $wfPwId);
    }

    /**
     * @inheritDoc
     */
    public function getWfRwId()
    {
        return $this->getData(self::WF_RW_ID);
    }

    /**
     * @inheritDoc
     */
    public function setWfRwId($wfRwId)
    {
        return $this->setData(self::WF_RW_ID, $wfRwId);
    }

    /**
     * @inheritDoc
     */
    public function getWfMmId()
    {
        return $this->getData(self::WF_MM_ID);
    }

    /**
     * @inheritDoc
     */
    public function setWfMmId($wfMmId)
    {
        return $this->setData(self::WF_MM_ID, $wfMmId);
    }
}

