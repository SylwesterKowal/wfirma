<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Model\ResourceModel\ZlecenieItems;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{

    /**
     * @inheritDoc
     */
    protected $_idFieldName = 'zlecenieitems_id';

    /**
     * @inheritDoc
     */
    protected function _construct()
    {
        $this->_init(
            \Kowal\WFirma\Model\ZlecenieItems::class,
            \Kowal\WFirma\Model\ResourceModel\ZlecenieItems::class
        );
    }
}

