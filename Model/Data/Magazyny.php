<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Model\Data;

use Kowal\WFirma\Api\Data\MagazynyInterface;

class Magazyny extends \Magento\Framework\Api\AbstractExtensibleObject implements MagazynyInterface
{

    /**
     * Get magazyny_id
     * @return string|null
     */
    public function getMagazynyId()
    {
        return $this->_get(self::MAGAZYNY_ID);
    }

    /**
     * Set magazyny_id
     * @param string $magazynyId
     * @return \Kowal\WFirma\Api\Data\MagazynyInterface
     */
    public function setMagazynyId($magazynyId)
    {
        return $this->setData(self::MAGAZYNY_ID, $magazynyId);
    }

    /**
     * Get mag_wfirma
     * @return string|null
     */
    public function getMagWfirma()
    {
        return $this->_get(self::MAG_WFIRMA);
    }

    /**
     * Set mag_wfirma
     * @param string $magWfirma
     * @return \Kowal\WFirma\Api\Data\MagazynyInterface
     */
    public function setMagWfirma($magWfirma)
    {
        return $this->setData(self::MAG_WFIRMA, $magWfirma);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Kowal\WFirma\Api\Data\MagazynyExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \Kowal\WFirma\Api\Data\MagazynyExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Kowal\WFirma\Api\Data\MagazynyExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * Get mag_magento
     * @return string|null
     */
    public function getMagMagento()
    {
        return $this->_get(self::MAG_MAGENTO);
    }

    /**
     * Set mag_magento
     * @param string $magMagento
     * @return \Kowal\WFirma\Api\Data\MagazynyInterface
     */
    public function setMagMagento($magMagento)
    {
        return $this->setData(self::MAG_MAGENTO, $magMagento);
    }
}

