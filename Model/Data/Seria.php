<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Model\Data;

use Kowal\WFirma\Api\Data\SeriaInterface;

class Seria extends \Magento\Framework\Api\AbstractExtensibleObject implements SeriaInterface
{

    /**
     * Get seria_id
     * @return string|null
     */
    public function getSeriaId()
    {
        return $this->_get(self::SERIA_ID);
    }

    /**
     * Set seria_id
     * @param string $seriaId
     * @return \Kowal\WFirma\Api\Data\SeriaInterface
     */
    public function setSeriaId($seriaId)
    {
        return $this->setData(self::SERIA_ID, $seriaId);
    }

    /**
     * Get order_attr_name
     * @return string|null
     */
    public function getOrderAttrName()
    {
        return $this->_get(self::ORDER_ATTR_NAME);
    }

    /**
     * Set order_attr_name
     * @param string $orderAttrName
     * @return \Kowal\WFirma\Api\Data\SeriaInterface
     */
    public function setOrderAttrName($orderAttrName)
    {
        return $this->setData(self::ORDER_ATTR_NAME, $orderAttrName);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Kowal\WFirma\Api\Data\SeriaExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \Kowal\WFirma\Api\Data\SeriaExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Kowal\WFirma\Api\Data\SeriaExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * Get order_attr_value
     * @return string|null
     */
    public function getOrderAttrValue()
    {
        return $this->_get(self::ORDER_ATTR_VALUE);
    }

    /**
     * Set order_attr_value
     * @param string $orderAttrValue
     * @return \Kowal\WFirma\Api\Data\SeriaInterface
     */
    public function setOrderAttrValue($orderAttrValue)
    {
        return $this->setData(self::ORDER_ATTR_VALUE, $orderAttrValue);
    }

    /**
     * Get wfirma_seria
     * @return string|null
     */
    public function getWfirmaSeria()
    {
        return $this->_get(self::WFIRMA_SERIA);
    }

    /**
     * Set wfirma_seria
     * @param string $wfirmaSeria
     * @return \Kowal\WFirma\Api\Data\SeriaInterface
     */
    public function setWfirmaSeria($wfirmaSeria)
    {
        return $this->setData(self::WFIRMA_SERIA, $wfirmaSeria);
    }

    /**
     * Get Opis
     * @return string|null
     */
    public function getOpis()
    {
        return $this->_get(self::OPIS);
    }

    /**
     * Set Opis
     * @param string $opis
     * @return \Kowal\WFirma\Api\Data\SeriaInterface
     */
    public function setOpis($opis)
    {
        return $this->setData(self::OPIS, $opis);
    }
}

