<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Model;

use Kowal\WFirma\Api\Data\FirmyInterface;
use Magento\Framework\Model\AbstractModel;

class Firmy extends AbstractModel implements FirmyInterface
{

    /**
     * @inheritDoc
     */
    public function _construct()
    {
        $this->_init(\Kowal\WFirma\Model\ResourceModel\Firmy::class);
    }

    /**
     * @inheritDoc
     */
    public function getFirmyId()
    {
        return $this->getData(self::FIRMY_ID);
    }

    /**
     * @inheritDoc
     */
    public function setFirmyId($firmyId)
    {
        return $this->setData(self::FIRMY_ID, $firmyId);
    }

    /**
     * @inheritDoc
     */
    public function getNazwaFirmy()
    {
        return $this->getData(self::NAZWA_FIRMY);
    }

    /**
     * @inheritDoc
     */
    public function setNazwaFirmy($nazwaFirmy)
    {
        return $this->setData(self::NAZWA_FIRMY, $nazwaFirmy);
    }

    /**
     * @inheritDoc
     */
    public function getCompanyIdWfirma()
    {
        return $this->getData(self::COMPANY_ID_WFIRMA);
    }

    /**
     * @inheritDoc
     */
    public function setCompanyIdWfirma($companyIdWfirma)
    {
        return $this->setData(self::COMPANY_ID_WFIRMA, $companyIdWfirma);
    }

    /**
     * @inheritDoc
     */
    public function getStoreIdMagento()
    {
        return $this->getData(self::STORE_ID_MAGENTO);
    }

    /**
     * @inheritDoc
     */
    public function setStoreIdMagento($storeIdMagento)
    {
        return $this->setData(self::STORE_ID_MAGENTO, $storeIdMagento);
    }

    /**
     * @inheritDoc
     */
    public function getPartner()
    {
        return $this->getData(self::PARTNER);
    }

    /**
     * @inheritDoc
     */
    public function setPartner($partner)
    {
        return $this->setData(self::PARTNER, $partner);
    }

    /**
     * @inheritDoc
     */
    public function getPartnerEmail()
    {
        return $this->getData(self::PARTNER_EMAIL);
    }

    /**
     * @inheritDoc
     */
    public function setPartnerEmail($partnerEmail)
    {
        return $this->setData(self::PARTNER_EMAIL, $partnerEmail);
    }
}

