<?php

namespace Kowal\WFirma\Model\Config;

/**
 * Class AttributeOptions
 * @package Kowal\WFirma\Model\Config
 */
class Attribute implements \Magento\Framework\Option\ArrayInterface
{
    /** @var \Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory */
    private $collectionFactory;

    /** @var \Kowal\WFirma\Model\Config\AttributeScope */
    private $scope;

    /** @var array */
    private $items;


    /**
     * @param \Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory $collectionFactory
     * @param \Kowal\WFirma\Model\Config\AttributeScope $scope
     */
    public function __construct(
        \Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory $collectionFactory,
        \Kowal\WFirma\Model\Config\AttributeScope $scope
    ) {
        $this->collectionFactory = $collectionFactory;
        $this->scope = $scope;
    }

    /**
     * @inheritdoc
     */
    public function toOptionArray()
    {
        if (is_null($this->items)) {
            $this->items = $this->getOptions();
        }

        return $this->items;
    }

    /**
     * @return \Magento\Catalog\Model\ResourceModel\Eav\Attribute[]|\Magento\Framework\DataObject[]
     */
    public function getAttributes()
    {

        $collection = $this->getCollection();
        return $collection->getItems();
    }


    /**
     * @return array
     */
    private function getOptions()
    {
        $items = ['label'=>' ','value' => ''];
        foreach ($this->getAttributes() as $attribute) {
//            if($attribute->getFrontendInput() == 'select' || $attribute->getFrontendInput() == 'multiselect') {
                $items[] = [
                    'label' => $attribute->getStoreLabel(), 'value' => $attribute->getName(),
                ];
//            }
        }
        return $items;
    }

    /**
     * @return \Magento\Catalog\Model\ResourceModel\Product\Attribute\Collection
     */
    private function getCollection()
    {
        return $this->collectionFactory->create();
    }
}
