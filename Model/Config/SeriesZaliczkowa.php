<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Model\Config;

class SeriesZaliczkowa implements \Magento\Framework\Option\ArrayInterface
{

    public function __construct(
        \Kowal\WFirma\lib\Series                         $seriesFactory,
        \Magento\Framework\Encryption\EncryptorInterface $encryptor,
        \Psr\Log\LoggerInterface                         $logger
    )
    {
        $this->seriesFactory = $seriesFactory;
        $this->logger = $logger;
        $this->_encryptor = $encryptor;
    }

    public function toOptionArray()
    {
        return $this->getSeries(); // [['value' => 'Bring', 'label' => __('Bring')], ['value' => 'DB Schenker', 'label' => __('DB Schenker')], ['value' => 'DHL Express', 'label' => __('DHL Express')]];
    }

    public function toArray()
    {
        return $this->getArray(); // ['Bring' => __('Bring'), 'DB Schenker' => __('DB Schenker'), 'DHL Express' => __('DHL Express')];
    }


    private function getSeries()
    {
        try {
            $limit = 20;
            $condition = ['parameters' => ['page' => 1, 'limit' => $limit]];
            $this->seriesFactory->setFindData($condition);
            $serries_ = [];


            if ($series = $this->seriesFactory->findAll()) {
                $pages = round((int)$series['parameters']['total'] / $limit, 0) + 1;

                foreach ($series as $keyContr => $seria) {
                    if (isset($seria['series']['type']) && (string)$seria['series']['type'] == "advance") {
                        $serries_[] = ["value" => (string)$seria['series']['id'], "label" => (string)$seria['series']['template']];
                    }
                }
                for ($i = 2; $i <= $pages; $i++) {
                    $condition = ['parameters' => ['page' => $i, 'limit' => $limit]];
                    $this->seriesFactory->setFindData($condition);
                    if ($series = $this->seriesFactory->findAll()) {
                        foreach ($series as $keyContr => $seria) {
                            if (isset($seria['series']['type']) && (string)$seria['series']['type'] == "advance") {
                                $serries_[] = ["value" => (string)$seria['series']['id'], "label" => (string)$seria['series']['template']];
                            }
                        }
                    }
                }
            }


            return $serries_;
        } catch (\Exception $e) {
            $this->logger->critical($e->getMessage());
            return [];
        }
    }

    private function getArray()
    {
        $seriers = $this->getSeries();
        $arrSer = [];
        foreach ($seriers as $seria) {
            $arrSer[$seria['value']] = $seria['label'];
        }
        return $arrSer;
    }
}

