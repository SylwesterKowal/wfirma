<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Model\Config;

class Companies implements \Magento\Framework\Option\ArrayInterface
{

    public function __construct(
        \Kowal\WFirma\lib\Companies                      $companiesFactory,
        \Magento\Framework\Encryption\EncryptorInterface $encryptor,
        \Psr\Log\LoggerInterface                         $logger
    )
    {
        $this->companiesFactory = $companiesFactory;
        $this->logger = $logger;
        $this->_encryptor = $encryptor;
    }

    public function toOptionArray()
    {
        return $this->getCompanies(); // [['value' => 'Bring', 'label' => __('Bring')], ['value' => 'DB Schenker', 'label' => __('DB Schenker')], ['value' => 'DHL Express', 'label' => __('DHL Express')]];
    }

    public function toArray()
    {
        return $this->getArray(); // ['Bring' => __('Bring'), 'DB Schenker' => __('DB Schenker'), 'DHL Express' => __('DHL Express')];
    }


    private function getCompanies()
    {
        try {
            $limit = 20;
            $condition = ['parameters' => ['page' => 1, 'limit' => $limit]];
            $this->companiesFactory->setFindData($condition);
            $companies_ = [];

            if ($companies = $this->companiesFactory->findAll()) {
                file_put_contents("_companies.txt", "---".print_r($companies, true));
                $pages = round((int)$companies['parameters']['total'] / $limit, 0) + 1;

                foreach ($companies as $keyContr => $seria) {
                    if (isset($seria['companies']['type']) && (string)$seria['companies']['type'] == "normal") {
                        $companies_[] = ["value" => (string)$seria['companies']['id'], "label" => (string)$seria['companies']['template']];
                    }
                }
                for ($i = 2; $i <= $pages; $i++) {
                    $condition = ['parameters' => ['page' => $i, 'limit' => $limit]];
                    $this->companiesFactory->setFindData($condition);
                    if ($companies = $this->companiesFactory->findAll()) {
                        foreach ($companies as $keyContr => $seria) {
                            if (isset($seria['companies']['type']) && (string)$seria['companies']['type'] == "normal") {
                                $companies_[] = ["value" => (string)$seria['companies']['id'], "label" => (string)$seria['companies']['template']];
                            }
                        }
                    }
                }
            }


            return $companies_;
        } catch (\Exception $e) {
            $this->logger->critical($e->getMessage());
            return [];
        }
    }

    private function getArray()
    {
        $companies = $this->getCompanies();
        $arrCom = [];
        foreach ($companies as $companie) {
            $arrCom[$companie['value']] = $companie['label'];
        }
        return $arrCom;
    }
}

