<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Model\Config\Source;

class Shipping implements \Magento\Framework\Option\ArrayInterface
{
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Shipping\Model\Config $shipconfig
    ) {

        $this->shipconfig = $shipconfig;
        $this->scopeConfig = $scopeConfig;
    }


    public function toOptionArray()
    {
        $activeCarriers = $this->shipconfig->getAllCarriers();
        $methods = [];
        foreach($activeCarriers as $carrierCode => $carrierModel) {
            $options = array();

            if ($carrierMethods = $carrierModel->getAllowedMethods()) {
                foreach ($carrierMethods as $methodCode => $method) {
                    $code = $carrierCode . '_' . $methodCode;
                    $options[] = array('value' => $code, 'label' => $method);
                }
                $carrierTitle = $this->scopeConfig
                    ->getValue('carriers/'.$carrierCode.'/title');
            }

            $methods[] = array('value' => $options, 'label' => $carrierTitle);
        }

        return $methods;
    }

    public function toArray()
    {
        $shippings = $this->toOptionArray();
        $shippArray = [];
        foreach ($shippings as $shipping){
            $shippArray[$shipping['value']] = $shipping['label'];
        }
        return $shippArray;
    }

}