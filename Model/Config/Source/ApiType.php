<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Model\Config\Source;

class ApiType implements \Magento\Framework\Option\ArrayInterface
{

    public function toOptionArray()
    {
        return [['value' => 'api_klucze', 'label' => __('Klucze API')], ['value' => 'api_oauth', 'label' => __('Aplikacja OAuth 2.0')]];
    }

    public function toArray()
    {
        return ['api_klucze' => __('Klucze API'),'api_oauth' => __('Aplikacja OAuth 2.0')];
    }
}