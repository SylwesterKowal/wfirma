<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Model\Config\Source;

class DisplayType implements \Magento\Framework\Option\ArrayInterface
{

    public function __construct(
        \Magento\InventoryApi\Api\SourceRepositoryInterface $sourceRepository,
        \Psr\Log\LoggerInterface                            $logger
    )
    {
        $this->logger = $logger;
        $this->sourceRepository = $sourceRepository;
    }

    public function toOptionArray()
    {
        return [['value' => 'pojedynczo', 'label' => __('Pojedynczo')], ['value' => 'wszystkie', 'label' => __('Wszystkie')]];
    }

    public function toArray()
    {

        return ['pojedynczo' => __('mPojedynczo1'), 'wszystkie' => __('Wszystkie')];
    }


}

