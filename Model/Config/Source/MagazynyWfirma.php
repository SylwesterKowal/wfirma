<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Model\Config\Source;

class MagazynyWfirma implements \Magento\Framework\Option\ArrayInterface
{

    public $request;

    public function __construct(
        \Kowal\WFirma\lib\Warehouses            $warehousesFactory,
        \Psr\Log\LoggerInterface                $logger,
        \Magento\Framework\App\RequestInterface $request
    )
    {
        $this->warehousesFactory = $warehousesFactory;
        $this->logger = $logger;
        $this->request = $request;
    }

    public function toOptionArray()
    {
        return $this->getWarehouses(); // [['value' => 'w1', 'label' => __('w1')], ['value' => 'w2', 'label' => __('w2')], ['value' => 'w3', 'label' => __('w3')]];
    }

    public function toArray()
    {
        $warehouses = $this->getWarehouses();
        $arrWare = [];
        foreach ($warehouses as $wer) {
            $arrWare[$wer['value']] = $wer['label'];
        }
        return $arrWare;
    }

    private function getWarehouses()
    {
        try {
            $limit = 20;
            $condition = ['parameters' => ['page' => 1, 'limit' => $limit]];
            $this->warehousesFactory->setFindData($condition);
            $warehouses_ = [];

             $storeIdRequest = (int)$this->request->getParam('store', 0);

            if ($warehouses = $this->warehousesFactory->findAll($storeIdRequest)) {
                $pages = round((int)$warehouses['parameters']['total'] / $limit, 0) + 1;

                foreach ($warehouses as $keyContr => $warehous) {
                    if (isset($warehous['warehouse']['id'])) {
                        $warehouses_[] = ["value" => (string)$warehous['warehouse']['id'], "label" => (string)$warehous['warehouse']['name']];
                    }
                }
                for ($i = 2; $i <= $pages; $i++) {
                    $condition = ['parameters' => ['page' => $i, 'limit' => $limit]];
                    $this->warehousesFactory->setFindData($condition);
                    if ($warehouses = $this->warehousesFactory->findAll($storeIdRequest)) {
                        foreach ($warehouses as $keyContr => $warehous) {
                            if (isset($warehous['warehouse']['id'])) {
                                $warehouses_[] = ["value" => (string)$warehous['warehouse']['id'], "label" => (string)$warehous['warehouse']['name']];
                            }
                        }
                    }
                }
            }

            return $warehouses_;
        } catch (\Exception $e) {
            $this->logger->critical($e->getMessage());
            return [];
        }
    }

}

