<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Model\Config\Source;

use Magento\Sales\Model\ResourceModel\Order\Status\Collection as OrderStatusCollection;

class Statusy implements \Magento\Framework\Option\ArrayInterface
{

    private $orderStatusCollection;

    public function __construct(OrderStatusCollection $orderStatusCollection)
    {
        $this->orderStatusCollection = $orderStatusCollection;
    }

    public function toOptionArray()
    {
        return $this->orderStatusCollection->toOptionArray();
    }

    public function toArray()
    {
        $statusy = $this->toOptionArray();
        $statusy_ = [];
        foreach ($statusy as $status){
            $statusy_[$status['value']] = $status['label'];
        }
        return $statusy_;
    }
}
