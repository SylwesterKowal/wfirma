<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Model\Config\Source;

use  Magento\User\Model\ResourceModel\User\CollectionFactory as UserCollectionFactory;

class AdminUsers implements \Magento\Framework\Option\ArrayInterface
{

    public function __construct(
        UserCollectionFactory $userFactory
    )
    {
        $this->_userFactory = $userFactory;
    }

    public function toOptionArray()
    {
        $users = [];
        $adminUsers = $this->_userFactory->create();
        $users[] = ['value' => '','label' => __(' -- Wybierz użytkownika -- ')];
        foreach($adminUsers as $adminUser) {
            $users[] = ['value' => $adminUser->getUsername(),'label' => $adminUser->getUsername()];
        }
        return $users; //[['value' => 'pojedynczo', 'label' => __('Pojedynczo')], ['value' => 'wszystkie', 'label' => __('Wszystkie')]];
    }

    public function toArray()
    {
        $aUsers = [];
        $users = $this->toOptionArray();
        foreach($users as $user) {
            $aUsers[$user['value']] = $user['label'];
        }
        return $aUsers;
    }



}

