<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Model\Config\Source;

class MagazynyMagento implements \Magento\Framework\Option\ArrayInterface
{

    public function __construct(
        \Kowal\WFirma\lib\Series                            $seriesFactory,
        \Magento\InventoryApi\Api\SourceRepositoryInterface $sourceRepository,
        \Psr\Log\LoggerInterface                            $logger
    )
    {
        $this->seriesFactory = $seriesFactory;
        $this->logger = $logger;
        $this->sourceRepository = $sourceRepository;
    }

    public function toOptionArray()
    {
        $magazyny = [];
        if($sources = $this->getSourceList()) {
            foreach ($sources as $source) {
                $magazyny[] = ['value' => $source->getData('source_code'), 'label' => $source->getData('name') . ' (' . $source->getData('source_code') . ')'];
            }
        }
        return $magazyny; //[['value' => 'm1', 'label' => __('m1')], ['value' => 'm2', 'label' => __('m2')], ['value' => 'm3', 'label' => __('m3')]];
    }

    public function toArray()
    {
        $magazyny = [];
        if ($mag = $this->toOptionArray()) {
            foreach ($mag as $mag_) {
                $magazyny[$mag_['value']] = $mag_['label'];
            }
        }
        return $magazyny; // ['m1' => __('m1'), 'm2' => __('m2'), 'm3' => __('m3')];
    }

    private function getSourceList()
    {
        $sourceData = $this->sourceRepository->getList();
        return $sourceData->getItems();
    }
}

