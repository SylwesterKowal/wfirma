<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Model;

use Kowal\WFirma\Api\Data\MagazynyInterfaceFactory;
use Kowal\WFirma\Api\Data\MagazynySearchResultsInterfaceFactory;
use Kowal\WFirma\Api\MagazynyRepositoryInterface;
use Kowal\WFirma\Model\ResourceModel\Magazyny as ResourceMagazyny;
use Kowal\WFirma\Model\ResourceModel\Magazyny\CollectionFactory as MagazynyCollectionFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;

class MagazynyRepository implements MagazynyRepositoryInterface
{

    protected $dataMagazynyFactory;

    protected $magazynyFactory;

    protected $dataObjectHelper;

    protected $extensibleDataObjectConverter;
    private $collectionProcessor;

    protected $magazynyCollectionFactory;

    private $storeManager;

    protected $searchResultsFactory;

    protected $resource;

    protected $extensionAttributesJoinProcessor;

    protected $dataObjectProcessor;


    /**
     * @param ResourceMagazyny $resource
     * @param MagazynyFactory $magazynyFactory
     * @param MagazynyInterfaceFactory $dataMagazynyFactory
     * @param MagazynyCollectionFactory $magazynyCollectionFactory
     * @param MagazynySearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourceMagazyny $resource,
        MagazynyFactory $magazynyFactory,
        MagazynyInterfaceFactory $dataMagazynyFactory,
        MagazynyCollectionFactory $magazynyCollectionFactory,
        MagazynySearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource = $resource;
        $this->magazynyFactory = $magazynyFactory;
        $this->magazynyCollectionFactory = $magazynyCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataMagazynyFactory = $dataMagazynyFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \Kowal\WFirma\Api\Data\MagazynyInterface $magazyny
    ) {
        /* if (empty($magazyny->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $magazyny->setStoreId($storeId);
        } */
        
        $magazynyData = $this->extensibleDataObjectConverter->toNestedArray(
            $magazyny,
            [],
            \Kowal\WFirma\Api\Data\MagazynyInterface::class
        );
        
        $magazynyModel = $this->magazynyFactory->create()->setData($magazynyData);
        
        try {
            $this->resource->save($magazynyModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the magazyny: %1',
                $exception->getMessage()
            ));
        }
        return $magazynyModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function get($magazynyId)
    {
        $magazyny = $this->magazynyFactory->create();
        $this->resource->load($magazyny, $magazynyId);
        if (!$magazyny->getId()) {
            throw new NoSuchEntityException(__('Magazyny with id "%1" does not exist.', $magazynyId));
        }
        return $magazyny->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->magazynyCollectionFactory->create();
        
        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \Kowal\WFirma\Api\Data\MagazynyInterface::class
        );
        
        $this->collectionProcessor->process($criteria, $collection);
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }
        
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \Kowal\WFirma\Api\Data\MagazynyInterface $magazyny
    ) {
        try {
            $magazynyModel = $this->magazynyFactory->create();
            $this->resource->load($magazynyModel, $magazyny->getMagazynyId());
            $this->resource->delete($magazynyModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the Magazyny: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($magazynyId)
    {
        return $this->delete($this->get($magazynyId));
    }
}

