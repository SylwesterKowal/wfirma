<?php

namespace Kowal\WFirma\Model;

class CreateShipment
{
    public $orderRepository;
    public $convertOrder;
    public $shipmentNotifier;

    public function __construct(
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Sales\Api\OrderRepositoryInterface     $orderRepository,
        \Magento\Sales\Model\Convert\Order              $convertOrder
    )
    {
        $this->productRepository = $productRepository;
        $this->orderRepository = $orderRepository;
        $this->convertOrder = $convertOrder;
    }

    public function createShipmentForOrder($orderId)
    {
        $order = $this->orderRepository->get($orderId);
        if (!$order->canShip()) {
//            throw new \Magento\Framework\Exception\LocalizedException(
//                __("You can't create the Shipment of this order.")
//            );
            return 1;
        }
        $orderShipment = $this->convertOrder->toShipment($order);
        foreach ($order->getAllItems() as $orderItem) {
            // Check virtual item and item Quantity
            if (!$orderItem->getQtyToShip() || $orderItem->getIsVirtual()) {
                continue;
            }
            $qty = $orderItem->getQtyToShip();
            $shipmentItem = $this->convertOrder->itemToShipmentItem($orderItem)->setQty($qty);
            $orderShipment->addItem($shipmentItem);
        }
        $orderShipment->register();
        $orderShipment->getOrder()->setIsInProcess(true);
        // Save created Order Shipment
        $orderShipment->save();
        $orderShipment->getOrder()->save();
//        $this->shipmentNotifier->notify($orderShipment);
        echo "WYSYŁKA: order_id " . $order->getEntityId() . ' ' . $order->getCreatedAt().', '.$order->getStatus().PHP_EOL;
    }
}