<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Model;

use Kowal\WFirma\Api\Data\KompletacjaItemsInterface;
use Magento\Framework\Model\AbstractModel;

class KompletacjaItems extends AbstractModel implements KompletacjaItemsInterface
{

    /**
     * @inheritDoc
     */
    public function _construct()
    {
        $this->_init(\Kowal\WFirma\Model\ResourceModel\KompletacjaItems::class);
    }

    /**
     * @inheritDoc
     */
    public function getKompletacjaitemsId()
    {
        return $this->getData(self::KOMPLETACJAITEMS_ID);
    }

    /**
     * @inheritDoc
     */
    public function setKompletacjaitemsId($kompletacjaitemsId)
    {
        return $this->setData(self::KOMPLETACJAITEMS_ID, $kompletacjaitemsId);
    }

    /**
     * @inheritDoc
     */
    public function getKompletacjaId()
    {
        return $this->getData(self::KOMPLETACJA_ID);
    }

    /**
     * @inheritDoc
     */
    public function setKompletacjaId($kompletacjaId)
    {
        return $this->setData(self::KOMPLETACJA_ID, $kompletacjaId);
    }

    /**
     * @inheritDoc
     */
    public function getSku()
    {
        return $this->getData(self::SKU);
    }

    /**
     * @inheritDoc
     */
    public function setSku($sku)
    {
        return $this->setData(self::SKU, $sku);
    }

    /**
     * @inheritDoc
     */
    public function getNazwa()
    {
        return $this->getData(self::NAZWA);
    }

    /**
     * @inheritDoc
     */
    public function setNazwa($nazwa)
    {
        return $this->setData(self::NAZWA, $nazwa);
    }

    /**
     * @inheritDoc
     */
    public function getIlosc()
    {
        return $this->getData(self::ILOSC);
    }

    /**
     * @inheritDoc
     */
    public function setIlosc($ilosc)
    {
        return $this->setData(self::ILOSC, $ilosc);
    }

    /**
     * @inheritDoc
     */
    public function getOrderId()
    {
        return $this->getData(self::ORDER_ID);
    }

    /**
     * @inheritDoc
     */
    public function setOrderId($orderId)
    {
        return $this->setData(self::ORDER_ID, $orderId);
    }

    /**
     * @inheritDoc
     */
    public function getZMagazynu()
    {
        return $this->getData(self::Z_MAGAZYNU);
    }

    /**
     * @inheritDoc
     */
    public function setZMagazynu($zMagazynu)
    {
        return $this->setData(self::Z_MAGAZYNU, $zMagazynu);
    }

    /**
     * @inheritDoc
     */
    public function getDoMagazynu()
    {
        return $this->getData(self::DO_MAGAZYNU);
    }

    /**
     * @inheritDoc
     */
    public function setDoMagazynu($doMagazynu)
    {
        return $this->setData(self::DO_MAGAZYNU, $doMagazynu);
    }
}

