<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Model;

use Kowal\WFirma\Api\Data\SeriaInterface;
use Kowal\WFirma\Api\Data\SeriaInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;

class Seria extends \Magento\Framework\Model\AbstractModel
{

    protected $_eventPrefix = 'kowal_wfirma_seria';
    protected $dataObjectHelper;

    protected $seriaDataFactory;


    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param SeriaInterfaceFactory $seriaDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \Kowal\WFirma\Model\ResourceModel\Seria $resource
     * @param \Kowal\WFirma\Model\ResourceModel\Seria\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        SeriaInterfaceFactory $seriaDataFactory,
        DataObjectHelper $dataObjectHelper,
        \Kowal\WFirma\Model\ResourceModel\Seria $resource,
        \Kowal\WFirma\Model\ResourceModel\Seria\Collection $resourceCollection,
        array $data = []
    ) {
        $this->seriaDataFactory = $seriaDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve seria model with seria data
     * @return SeriaInterface
     */
    public function getDataModel()
    {
        $seriaData = $this->getData();
        
        $seriaDataObject = $this->seriaDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $seriaDataObject,
            $seriaData,
            SeriaInterface::class
        );
        
        return $seriaDataObject;
    }
}

