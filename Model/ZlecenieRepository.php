<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Model;

use Kowal\WFirma\Api\Data\ZlecenieInterface;
use Kowal\WFirma\Api\Data\ZlecenieInterfaceFactory;
use Kowal\WFirma\Api\Data\ZlecenieSearchResultsInterfaceFactory;
use Kowal\WFirma\Api\ZlecenieRepositoryInterface;
use Kowal\WFirma\Model\ResourceModel\Zlecenie as ResourceZlecenie;
use Kowal\WFirma\Model\ResourceModel\Zlecenie\CollectionFactory as ZlecenieCollectionFactory;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;

class ZlecenieRepository implements ZlecenieRepositoryInterface
{

    /**
     * @var ResourceZlecenie
     */
    protected $resource;

    /**
     * @var CollectionProcessorInterface
     */
    protected $collectionProcessor;

    /**
     * @var ZlecenieCollectionFactory
     */
    protected $zlecenieCollectionFactory;

    /**
     * @var Zlecenie
     */
    protected $searchResultsFactory;

    /**
     * @var ZlecenieInterfaceFactory
     */
    protected $zlecenieFactory;


    /**
     * @param ResourceZlecenie $resource
     * @param ZlecenieInterfaceFactory $zlecenieFactory
     * @param ZlecenieCollectionFactory $zlecenieCollectionFactory
     * @param ZlecenieSearchResultsInterfaceFactory $searchResultsFactory
     * @param CollectionProcessorInterface $collectionProcessor
     */
    public function __construct(
        ResourceZlecenie $resource,
        ZlecenieInterfaceFactory $zlecenieFactory,
        ZlecenieCollectionFactory $zlecenieCollectionFactory,
        ZlecenieSearchResultsInterfaceFactory $searchResultsFactory,
        CollectionProcessorInterface $collectionProcessor
    ) {
        $this->resource = $resource;
        $this->zlecenieFactory = $zlecenieFactory;
        $this->zlecenieCollectionFactory = $zlecenieCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->collectionProcessor = $collectionProcessor;
    }

    /**
     * @inheritDoc
     */
    public function save(ZlecenieInterface $zlecenie)
    {
        try {
            $this->resource->save($zlecenie);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the zlecenie: %1',
                $exception->getMessage()
            ));
        }
        return $zlecenie;
    }

    /**
     * @inheritDoc
     */
    public function get($zlecenieId)
    {
        $zlecenie = $this->zlecenieFactory->create();
        $this->resource->load($zlecenie, $zlecenieId);
        if (!$zlecenie->getId()) {
            throw new NoSuchEntityException(__('Zlecenie with id "%1" does not exist.', $zlecenieId));
        }
        return $zlecenie;
    }

    /**
     * @inheritDoc
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->zlecenieCollectionFactory->create();
        
        $this->collectionProcessor->process($criteria, $collection);
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model;
        }
        
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * @inheritDoc
     */
    public function delete(ZlecenieInterface $zlecenie)
    {
        try {
            $zlecenieModel = $this->zlecenieFactory->create();
            $this->resource->load($zlecenieModel, $zlecenie->getZlecenieId());
            $this->resource->delete($zlecenieModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the Zlecenie: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * @inheritDoc
     */
    public function deleteById($zlecenieId)
    {
        return $this->delete($this->get($zlecenieId));
    }
}

