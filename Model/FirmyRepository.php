<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Model;

use Kowal\WFirma\Api\Data\FirmyInterface;
use Kowal\WFirma\Api\Data\FirmyInterfaceFactory;
use Kowal\WFirma\Api\Data\FirmySearchResultsInterfaceFactory;
use Kowal\WFirma\Api\FirmyRepositoryInterface;
use Kowal\WFirma\Model\ResourceModel\Firmy as ResourceFirmy;
use Kowal\WFirma\Model\ResourceModel\Firmy\CollectionFactory as FirmyCollectionFactory;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;

class FirmyRepository implements FirmyRepositoryInterface
{

    /**
     * @var ResourceFirmy
     */
    protected $resource;

    /**
     * @var Firmy
     */
    protected $searchResultsFactory;

    /**
     * @var CollectionProcessorInterface
     */
    protected $collectionProcessor;

    /**
     * @var FirmyInterfaceFactory
     */
    protected $firmyFactory;

    /**
     * @var FirmyCollectionFactory
     */
    protected $firmyCollectionFactory;


    /**
     * @param ResourceFirmy $resource
     * @param FirmyInterfaceFactory $firmyFactory
     * @param FirmyCollectionFactory $firmyCollectionFactory
     * @param FirmySearchResultsInterfaceFactory $searchResultsFactory
     * @param CollectionProcessorInterface $collectionProcessor
     */
    public function __construct(
        ResourceFirmy $resource,
        FirmyInterfaceFactory $firmyFactory,
        FirmyCollectionFactory $firmyCollectionFactory,
        FirmySearchResultsInterfaceFactory $searchResultsFactory,
        CollectionProcessorInterface $collectionProcessor
    ) {
        $this->resource = $resource;
        $this->firmyFactory = $firmyFactory;
        $this->firmyCollectionFactory = $firmyCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->collectionProcessor = $collectionProcessor;
    }

    /**
     * @inheritDoc
     */
    public function save(FirmyInterface $firmy)
    {
        try {
            $this->resource->save($firmy);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the firmy: %1',
                $exception->getMessage()
            ));
        }
        return $firmy;
    }

    /**
     * @inheritDoc
     */
    public function get($firmyId)
    {
        $firmy = $this->firmyFactory->create();
        $this->resource->load($firmy, $firmyId);
        if (!$firmy->getId()) {
            throw new NoSuchEntityException(__('Firmy with id "%1" does not exist.', $firmyId));
        }
        return $firmy;
    }

    /**
     * @inheritDoc
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->firmyCollectionFactory->create();
        
        $this->collectionProcessor->process($criteria, $collection);
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model;
        }
        
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * @inheritDoc
     */
    public function delete(FirmyInterface $firmy)
    {
        try {
            $firmyModel = $this->firmyFactory->create();
            $this->resource->load($firmyModel, $firmy->getFirmyId());
            $this->resource->delete($firmyModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the Firmy: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * @inheritDoc
     */
    public function deleteById($firmyId)
    {
        return $this->delete($this->get($firmyId));
    }
}

