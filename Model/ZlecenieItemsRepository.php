<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Model;

use Kowal\WFirma\Api\Data\ZlecenieItemsInterface;
use Kowal\WFirma\Api\Data\ZlecenieItemsInterfaceFactory;
use Kowal\WFirma\Api\Data\ZlecenieItemsSearchResultsInterfaceFactory;
use Kowal\WFirma\Api\ZlecenieItemsRepositoryInterface;
use Kowal\WFirma\Model\ResourceModel\ZlecenieItems as ResourceZlecenieItems;
use Kowal\WFirma\Model\ResourceModel\ZlecenieItems\CollectionFactory as ZlecenieItemsCollectionFactory;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;

class ZlecenieItemsRepository implements ZlecenieItemsRepositoryInterface
{

    /**
     * @var CollectionProcessorInterface
     */
    protected $collectionProcessor;

    /**
     * @var ZlecenieItemsInterfaceFactory
     */
    protected $zlecenieItemsFactory;

    /**
     * @var ResourceZlecenieItems
     */
    protected $resource;

    /**
     * @var ZlecenieItems
     */
    protected $searchResultsFactory;

    /**
     * @var ZlecenieItemsCollectionFactory
     */
    protected $zlecenieItemsCollectionFactory;


    /**
     * @param ResourceZlecenieItems $resource
     * @param ZlecenieItemsInterfaceFactory $zlecenieItemsFactory
     * @param ZlecenieItemsCollectionFactory $zlecenieItemsCollectionFactory
     * @param ZlecenieItemsSearchResultsInterfaceFactory $searchResultsFactory
     * @param CollectionProcessorInterface $collectionProcessor
     */
    public function __construct(
        ResourceZlecenieItems $resource,
        ZlecenieItemsInterfaceFactory $zlecenieItemsFactory,
        ZlecenieItemsCollectionFactory $zlecenieItemsCollectionFactory,
        ZlecenieItemsSearchResultsInterfaceFactory $searchResultsFactory,
        CollectionProcessorInterface $collectionProcessor
    ) {
        $this->resource = $resource;
        $this->zlecenieItemsFactory = $zlecenieItemsFactory;
        $this->zlecenieItemsCollectionFactory = $zlecenieItemsCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->collectionProcessor = $collectionProcessor;
    }

    /**
     * @inheritDoc
     */
    public function save(ZlecenieItemsInterface $zlecenieItems)
    {
        try {
            $this->resource->save($zlecenieItems);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the zlecenieItems: %1',
                $exception->getMessage()
            ));
        }
        return $zlecenieItems;
    }

    /**
     * @inheritDoc
     */
    public function get($zlecenieItemsId)
    {
        $zlecenieItems = $this->zlecenieItemsFactory->create();
        $this->resource->load($zlecenieItems, $zlecenieItemsId);
        if (!$zlecenieItems->getId()) {
            throw new NoSuchEntityException(__('ZlecenieItems with id "%1" does not exist.', $zlecenieItemsId));
        }
        return $zlecenieItems;
    }

    /**
     * @inheritDoc
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->zlecenieItemsCollectionFactory->create();
        
        $this->collectionProcessor->process($criteria, $collection);
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model;
        }
        
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * @inheritDoc
     */
    public function delete(ZlecenieItemsInterface $zlecenieItems)
    {
        try {
            $zlecenieItemsModel = $this->zlecenieItemsFactory->create();
            $this->resource->load($zlecenieItemsModel, $zlecenieItems->getZlecenieitemsId());
            $this->resource->delete($zlecenieItemsModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the ZlecenieItems: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * @inheritDoc
     */
    public function deleteById($zlecenieItemsId)
    {
        return $this->delete($this->get($zlecenieItemsId));
    }
}

