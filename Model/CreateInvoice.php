<?php

namespace Kowal\WFirma\Model;

use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Model\Service\InvoiceService;
use Magento\Framework\DB\Transaction;
use Magento\Sales\Model\Order\Email\Sender\InvoiceSender;

class CreateInvoice
{
    protected $orderRepository;
    protected $invoiceService;
    protected $transaction;
    protected $invoiceSender;

    public function __construct(
        OrderRepositoryInterface $orderRepository,
        InvoiceService $invoiceService,
        InvoiceSender $invoiceSender,
        Transaction $transaction
    ) {
        $this->orderRepository = $orderRepository;
        $this->invoiceService = $invoiceService;
        $this->transaction = $transaction;
        $this->invoiceSender = $invoiceSender;
    }

    public function execute($orderId, $state_ = null, $status_ = null)
    {
        $order = $this->orderRepository->get($orderId);
        $state = $order->getState();
        $status = $order->getStatus();

//        if ($order->canInvoice() && $order->getGrandTotal() != $order->getTotalPaid() ) {
        if ($order->canInvoice()  ) {
            $invoice = $this->invoiceService->prepareInvoice($order);
            $invoice->setCreatedAt($order->getCreatedAt());
            $invoice->register();
            $invoice->save();

            $transactionSave =
                $this->transaction
                    ->addObject($invoice)
                    ->addObject($invoice->getOrder());
            $transactionSave->save();
            $this->invoiceSender->send($invoice);



            $order->addCommentToStatusHistory(
                __('Generowanie FV #%1.', $invoice->getId())
            )->setIsCustomerNotified(false)->save();

            $_state = (is_null($state_)) ? $state : $state_;
            $_status = (is_null($status_)) ? $status : $status_;
            $order->setState($_state);
            $order->setStatus($_status);
            $order->save();

            echo "FAKTURA order_id, " . $order->getEntityId() . ', ' . $order->getCreatedAt().', '.$order->getStatus().PHP_EOL;
        }else if(!is_null($state_) && !is_null($status_)){
            $order->setState($state_);
            $order->setStatus($status_);
            $order->save();
            echo "FAKTURA tylko status order_id, " . $order->getEntityId() . ', ' . $order->getCreatedAt().', '.$order->getStatus().PHP_EOL;
        }
    }
}