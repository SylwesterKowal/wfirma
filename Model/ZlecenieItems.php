<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Model;

use Kowal\WFirma\Api\Data\ZlecenieItemsInterface;
use Magento\Framework\Model\AbstractModel;

class ZlecenieItems extends AbstractModel implements ZlecenieItemsInterface
{

    /**
     * @inheritDoc
     */
    public function _construct()
    {
        $this->_init(\Kowal\WFirma\Model\ResourceModel\ZlecenieItems::class);
    }

    /**
     * @inheritDoc
     */
    public function getZlecenieitemsId()
    {
        return $this->getData(self::ZLECENIEITEMS_ID);
    }

    /**
     * @inheritDoc
     */
    public function setZlecenieitemsId($zlecenieitemsId)
    {
        return $this->setData(self::ZLECENIEITEMS_ID, $zlecenieitemsId);
    }

    /**
     * @inheritDoc
     */
    public function getZlecenieId()
    {
        return $this->getData(self::ZLECENIE_ID);
    }

    /**
     * @inheritDoc
     */
    public function setZlecenieId($zlecenieId)
    {
        return $this->setData(self::ZLECENIE_ID, $zlecenieId);
    }

    /**
     * @inheritDoc
     */
    public function getOrderId()
    {
        return $this->getData(self::ORDER_ID);
    }

    /**
     * @inheritDoc
     */
    public function setOrderId($orderId)
    {
        return $this->setData(self::ORDER_ID, $orderId);
    }

    /**
     * @inheritDoc
     */
    public function getSku()
    {
        return $this->getData(self::SKU);
    }

    /**
     * @inheritDoc
     */
    public function setSku($sku)
    {
        return $this->setData(self::SKU, $sku);
    }

    /**
     * @inheritDoc
     */
    public function getNazwa()
    {
        return $this->getData(self::NAZWA);
    }

    /**
     * @inheritDoc
     */
    public function setNazwa($nazwa)
    {
        return $this->setData(self::NAZWA, $nazwa);
    }

    /**
     * @inheritDoc
     */
    public function getIlosc()
    {
        return $this->getData(self::ILOSC);
    }

    /**
     * @inheritDoc
     */
    public function setIlosc($ilosc)
    {
        return $this->setData(self::ILOSC, $ilosc);
    }

    /**
     * @inheritDoc
     */
    public function getParametry()
    {
        return $this->getData(self::PARAMETRY);
    }

    /**
     * @inheritDoc
     */
    public function setParametry($parametry)
    {
        return $this->setData(self::PARAMETRY, $parametry);
    }

    /**
     * @inheritDoc
     */
    public function getTypZlecenia()
    {
        return $this->getData(self::TYP_ZLECENIA);
    }

    /**
     * @inheritDoc
     */
    public function setTypZlecenia($typZlecenia)
    {
        return $this->setData(self::TYP_ZLECENIA, $typZlecenia);
    }

    /**
     * @inheritDoc
     */
    public function getDataZlecenia()
    {
        return $this->getData(self::DATA_ZLECENIA);
    }

    /**
     * @inheritDoc
     */
    public function setDataZlecenia($dataZlecenia)
    {
        return $this->setData(self::DATA_ZLECENIA, $dataZlecenia);
    }
}

