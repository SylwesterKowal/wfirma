<?php
/**
 * Copyright © kowal sp zoo All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\WFirma\Model;

use Kowal\WFirma\Api\Data\ZlecenieInterface;
use Magento\Framework\Model\AbstractModel;

class Zlecenie extends AbstractModel implements ZlecenieInterface
{

    /**
     * @inheritDoc
     */
    public function _construct()
    {
        $this->_init(\Kowal\WFirma\Model\ResourceModel\Zlecenie::class);
    }

    /**
     * @inheritDoc
     */
    public function getZlecenieId()
    {
        return $this->getData(self::ZLECENIE_ID);
    }

    /**
     * @inheritDoc
     */
    public function setZlecenieId($zlecenieId)
    {
        return $this->setData(self::ZLECENIE_ID, $zlecenieId);
    }

    /**
     * @inheritDoc
     */
    public function getStatus()
    {
        return $this->getData(self::STATUS);
    }

    /**
     * @inheritDoc
     */
    public function setStatus($status)
    {
        return $this->setData(self::STATUS, $status);
    }

    /**
     * @inheritDoc
     */
    public function getDataZlecenia()
    {
        return $this->getData(self::DATA_ZLECENIA);
    }

    /**
     * @inheritDoc
     */
    public function setDataZlecenia($dataZlecenia)
    {
        return $this->setData(self::DATA_ZLECENIA, $dataZlecenia);
    }

    /**
     * @inheritDoc
     */
    public function getOpis()
    {
        return $this->getData(self::OPIS);
    }

    /**
     * @inheritDoc
     */
    public function setOpis($opis)
    {
        return $this->setData(self::OPIS, $opis);
    }

    /**
     * @inheritDoc
     */
    public function getWfPwId()
    {
        return $this->getData(self::WF_PW_ID);
    }

    /**
     * @inheritDoc
     */
    public function setWfPwId($wfPwId)
    {
        return $this->setData(self::WF_PW_ID, $wfPwId);
    }

    /**
     * @inheritDoc
     */
    public function getWfRwId()
    {
        return $this->getData(self::WF_RW_ID);
    }

    /**
     * @inheritDoc
     */
    public function setWfRwId($wfRwId)
    {
        return $this->setData(self::WF_RW_ID, $wfRwId);
    }

    /**
     * @inheritDoc
     */
    public function getWfMmId()
    {
        return $this->getData(self::WF_MM_ID);
    }

    /**
     * @inheritDoc
     */
    public function setWfMmId($wfMmId)
    {
        return $this->setData(self::WF_MM_ID, $wfMmId);
    }
}

