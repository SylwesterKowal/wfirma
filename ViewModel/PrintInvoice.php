<?php
declare(strict_types=1);

namespace Kowal\WFirma\ViewModel;

use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\Block\ArgumentInterface;

class PrintInvoice implements ArgumentInterface
{
    /**
     * @var UrlInterface
     */
    private UrlInterface $urlInterface;

    /**
     * @param UrlInterface $urlInterface
     */
    public function __construct(
        UrlInterface                               $urlInterface,
        \Kowal\WFirma\lib\Encrypt                  $encrypt,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    )
    {
        $this->urlInterface = $urlInterface;
        $this->encrypt = $encrypt;
        $this->storeManager = $storeManager;
    }

    /**
     * @param $invoice
     * @return string
     */
    public function getPrintInvoiceUrl($invoice)
    {
        return $this->getBaseUrl()."invoice/pdf/download/id/".$this->getLink($invoice->getId());
    }

    public function getLink($invoice_id)
    {
        return $this->encrypt->encrypt($invoice_id);
    }

    public function getBaseUrl()
    {

        return $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_WEB);
    }
}
