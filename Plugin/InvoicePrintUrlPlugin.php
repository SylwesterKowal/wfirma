<?php
declare(strict_types=1);

namespace Kowal\WFirma\Plugin;

use Magento\Sales\Block\Adminhtml\Order\Invoice\View;

class InvoicePrintUrlPlugin
{

    public function __construct(
        \Kowal\WFirma\lib\Encrypt                        $encrypt,
        \Magento\Store\Model\StoreManagerInterface       $storeManager
    )
    {
        $this->encrypt = $encrypt;
        $this->storeManager = $storeManager;
    }

    /**
     * @param View $subject
     * @param $result
     * @return string
     */
    public function afterGetPrintInvoiceUrl(View $subject, $result)
    {
        $order = $subject->getOrder();
        if($order->getWfirmaFv()) {
            return $this->getBaseUrl() . "invoice/pdf/download/id/" . $this->getLink($subject->getId());
        }else{
            return $result;
        }
    }

    public function getLink($invoice_id)
    {
        return $this->encrypt->encrypt($invoice_id);
    }

    public function getBaseUrl()
    {

        return $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_WEB);
    }
}
